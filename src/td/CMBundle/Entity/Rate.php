<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rate
 *
 * @ORM\Table(name="cm_rate")
 * @ORM\Entity
 */
class Rate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="rateIdPortal", type="string", length=255)
     */
    private $rateIdPortal;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isChildRate", type="boolean")
     */
    private $isChildRate;

    /**
     * @var integer
     *
     * @ORM\Column(name="maxPersons", type="integer")
     */
    private $maxPersons;

    /**
     * @var string
     *
     * @ORM\Column(name="0policy", type="string", length=255)
     */
    private $policy;

    /**
     * @var integer
     *
     * @ORM\Column(name="policyId", type="integer")
     */
    private $policyId;

    /**
     * @var string
     *
     * @ORM\Column(name="rateName", type="string", length=255)
     */
    private $rateName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="readonly", type="boolean")
     */
    private $readonly;

    /**
     * @var integer
     *
     * @ORM\Column(name="fixedOccupancy", type="integer")
     */
    private $fixedOccupancy = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datumKreiranja", type="date")
     */
    private $datumKreiranja;

    /**
     * @ORM\OneToMany(targetEntity="UnitRateStavke", mappedBy="rate")
     */
    private $unitRates;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->unitRates = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rateIdPortal
     *
     * @param string $rateIdPortal
     *
     * @return Rate
     */
    public function setRateIdPortal($rateIdPortal)
    {
        $this->rateIdPortal = $rateIdPortal;

        return $this;
    }

    /**
     * Get rateIdPortal
     *
     * @return string
     */
    public function getRateIdPortal()
    {
        return $this->rateIdPortal;
    }

    /**
     * Set isChildRate
     *
     * @param boolean $isChildRate
     *
     * @return Rate
     */
    public function setIsChildRate($isChildRate)
    {
        $this->isChildRate = $isChildRate;

        return $this;
    }

    /**
     * Get isChildRate
     *
     * @return boolean
     */
    public function getIsChildRate()
    {
        return $this->isChildRate;
    }

    /**
     * Set maxPersons
     *
     * @param integer $maxPersons
     *
     * @return Rate
     */
    public function setMaxPersons($maxPersons)
    {
        $this->maxPersons = $maxPersons;

        return $this;
    }

    /**
     * Get maxPersons
     *
     * @return integer
     */
    public function getMaxPersons()
    {
        return $this->maxPersons;
    }

    /**
     * Set policy
     *
     * @param string $policy
     *
     * @return Rate
     */
    public function setPolicy($policy)
    {
        $this->policy = $policy;

        return $this;
    }

    /**
     * Get policy
     *
     * @return string
     */
    public function getPolicy()
    {
        return $this->policy;
    }

    /**
     * Set policyId
     *
     * @param integer $policyId
     *
     * @return Rate
     */
    public function setPolicyId($policyId)
    {
        $this->policyId = $policyId;

        return $this;
    }

    /**
     * Get policyId
     *
     * @return integer
     */
    public function getPolicyId()
    {
        return $this->policyId;
    }

    /**
     * Set rateName
     *
     * @param string $rateName
     *
     * @return Rate
     */
    public function setRateName($rateName)
    {
        $this->rateName = $rateName;

        return $this;
    }

    /**
     * Get rateName
     *
     * @return string
     */
    public function getRateName()
    {
        return $this->rateName;
    }

    /**
     * Set readonly
     *
     * @param boolean $readonly
     *
     * @return Rate
     */
    public function setReadonly($readonly)
    {
        $this->readonly = $readonly;

        return $this;
    }

    /**
     * Get readonly
     *
     * @return boolean
     */
    public function getReadonly()
    {
        return $this->readonly;
    }

    /**
     * Set fixedOccupancy
     *
     * @param integer $fixedOccupancy
     *
     * @return Rate
     */
    public function setFixedOccupancy($fixedOccupancy)
    {
        $this->fixedOccupancy = $fixedOccupancy;

        return $this;
    }

    /**
     * Get fixedOccupancy
     *
     * @return integer
     */
    public function getFixedOccupancy()
    {
        return $this->fixedOccupancy;
    }

    /**
     * Set datumKreiranja
     *
     * @param \DateTime $datumKreiranja
     *
     * @return Rate
     */
    public function setDatumKreiranja($datumKreiranja)
    {
        $this->datumKreiranja = $datumKreiranja;

        return $this;
    }

    /**
     * Get datumKreiranja
     *
     * @return \DateTime
     */
    public function getDatumKreiranja()
    {
        return $this->datumKreiranja;
    }

    /**
     * Add unitRate
     *
     * @param \td\CMBundle\Entity\UnitRate $unitRate
     *
     * @return Rate
     */
    public function addUnitRate(\td\CMBundle\Entity\UnitRate $unitRate)
    {
        $this->unitRates[] = $unitRate;

        return $this;
    }

    /**
     * Remove unitRate
     *
     * @param \td\CMBundle\Entity\UnitRate $unitRate
     */
    public function removeUnitRate(\td\CMBundle\Entity\UnitRate $unitRate)
    {
        $this->unitRates->removeElement($unitRate);
    }

    /**
     * Get unitRates
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUnitRates()
    {
        return $this->unitRates;
    }
}
