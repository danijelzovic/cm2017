<?php

namespace td\CMBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use td\CMBundle\Entity\Client;

/**
 * Client controller.
 *
 */
class ClientController extends Controller
{

    /**
     * Lists all Client entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('tdCMBundle:Client')->findAll();

        return $this->render('tdCMBundle:Client:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Client entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('tdCMBundle:Client')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Client entity.');
        }

        return $this->render('tdCMBundle:Client:show.html.twig', array(
            'entity'      => $entity,
        ));
    }
}
