<?php

namespace td\CMBundle\Controller;


use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Date;
use td\CMBundle\Entity\Promjena;
use td\CMBundle\Entity\SetupPropertyUnit;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JMS;

use td\CMBundle\Entity\Transfer;
use td\CMBundle\Entity\TransferLog;
use td\CMBundle\Obrada\TransferObrada;

/**
 * Transfer controller.
 *
 */
class TransferController extends Controller
{

    /**
     * Lists all Transfer entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

//        $entities = $em->getRepository('tdCMBundle:Transfer')->findAll();

        $entities = $em->getRepository('tdCMBundle:Transfer')->findBy(
            array(
                'setupPropertyUnit' => 2354,
                'ok' => false)
        );

        return $this->render('tdCMBundle:Transfer:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Transfer entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('tdCMBundle:Transfer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Transfer entity.');
        }

        return $this->render('tdCMBundle:Transfer:show.html.twig', array(
            'entity' => $entity,
        ));
    }

    /**
     * @param SetupPropertyUnit $setupPropertyUnit
     * @return Response
     */
    public function spuTransferiAction(SetupPropertyUnit $setupPropertyUnit)
    {
        $em = $this->getDoctrine()->getManager();
        $grupePromjena = $em->getRepository('tdCMBundle:PromjenaGrupa')->findAll();
        $grupePromjenaStr = ':[Sve]';
        foreach ($grupePromjena as $gp) {
            $grupePromjenaStr .= ';' . $gp->getNaziv() . ':' . $gp->getNaziv();
        }

        $tipoviPromjena = $em->getRepository('tdCMBundle:PromjenaTip')->findAll();
        $tipoviPromjenaStr = ':[Sve]';
        foreach ($tipoviPromjena as $tp) {
            $tipoviPromjenaStr .= ';' . $tp->getNaziv() . ':' . $tp->getNaziv();
        }

        $neuspjeliTransferi = $em->getRepository('tdCMBundle:Transfer')->findBy(
            array(
                'setupPropertyUnit' => $setupPropertyUnit,
                'ok' => false
            ));


        $query = $em->createQuery(
            'SELECT t.id AS id,
t.ok AS ok,
p.id AS idPromjene,
p.grupaPromjene AS grupaPromjene,
p.tipPromjene AS tipPromjene,
p.dateFrom AS dateFrom,
p.dateTo AS dateTo,
p.datumvrijeme AS datumVrijemePromjene,
(SELECT max(tl.datumVrijeme) FROM tdCMBundle:TransferLog tl WHERE tl.transfer = t AND tl.uspjesanPrijenos = true) AS datumVrijemeUspjesnogTrasnfera,
(SELECT count(tl2) FROM tdCMBundle:TransferLog tl2 WHERE tl2.transfer = t) AS brojPokusaja,
spu.idPropertyUnitFront AS idPropertyUnitFront
FROM tdCMBundle:Transfer t JOIN t.promjena p JOIN t.setupPropertyUnit spu
WHERE t.setupPropertyUnit = ?1 '
        );
        $transferi = $query->setParameters(array(1 => $setupPropertyUnit))->getResult();

        if (count($neuspjeliTransferi) > 0) {
            $this->addFlash('danger', 'Ova smještajna jedinica ima ' . count($neuspjeliTransferi) . ' neuspješnih transfera!');
        } elseif (count($transferi) == 0) {
            $this->addFlash('notice', 'Nema transfera za ovu smještajnu jedinicu.');
        } else {
            $this->addFlash('notice', 'Svi transferi su uspješno obrađeni.');
        }
        $polje = array('page' => 1, 'total' => 1, 'records' => count($transferi), 'rows' => $transferi);

//        $serializer = JMS\Serializer\SerializerBuilder::create()->build();
//        $jsonTransferi = $serializer->serialize($transferi, 'json');
//dump($transferi);
//       $tekst = json_encode($transferi);
//        $this->render('')
//        $response = new JsonResponse();
//        $response->setData($polje);
//        return new Response(json_encode($polje));
        return $this->render('tdCMBundle:Transfer/Snippets:spuTransferi.html.twig',
            array(
                'podaci' => $transferi,
                'setupPropertyUnit' => $setupPropertyUnit,
                'grupePromjenaStr' => $grupePromjenaStr,
                'tipoviPromjenaStr' => $tipoviPromjenaStr
            )
        );
//        return $response;
    }

    public function obradiTransferOldAction(Transfer $transfer)
    {
        $transferObrada = new TransferObrada();
        if (!$transfer->getOk()) {
            $poruka = $transferObrada->obradiTransfer($transfer);
            return $transfer->getOk();
        }
    }

    /**
     * @param Transfer $transfer
     * @return Response
     */
    public function obradiTransferAction(Transfer $transfer)
    {
        $poruka = '';
        if (!$transfer) {
            $poruka = 'Zadani transfer ne postoji!';
        } else {
            if (!$transfer->getOk()) {
                $this->obradiTransferBatch($transfer);
                /* @var $zadnjiTransferLog TransferLog */
                $zadnjiTransferLog = $transfer->getTransferLogovi()->last();
                if ($zadnjiTransferLog) {
                    $poruka = $zadnjiTransferLog->getResponseTransfera();
                } else {
                    $poruka = 'Ne postoji kontroler za portal';
                }
            } else {
                $poruka = 'Transfer je već obrađen!';
            }
        }
//        return $this->render('@tdCM/Transfer/Snippets/obradaTransfera.html.twig', array(
////            'poruka' => $poruka,
//            'poruka' => 'Uspješan transfer!',
//        ));
        return new Response($poruka);
    }

    /**
     * Obrada novih transfera koji nemaju zapisa u TransferLog-u
     *
     * @return Response
     */
    public function obradiNoveTransfereAction()
    {
        //Dohvat novih transfera
        $noviTransferi = $this->getDoctrine()->getManager()->getRepository('tdCMBundle:Transfer')->findAllNoviTtransferi();
        $messages = array();

        /* @var Transfer $transfer */
        foreach ($noviTransferi as $transfer) {
            $this->obradiTransferBatch($transfer);
            $messages[] = array(
                'transfer' => $transfer->getId(),
                'status' => $transfer->getOk(),
                'unit' => $transfer->getSetupPropertyUnit()->getPropertyUnit()->getPropertyUnitFront(),
                'portal' => $transfer->getSetupPropertyUnit()->getPortalKorisnik()->getPortal()->getNaziv(),
            );
        }

        return JsonResponse::create($messages);

        // ----------- DEPRECATED -----
//        // jSON String for request
//        $serializer = JMS\Serializer\SerializerBuilder::create()->build();
//        $json_string = $serializer->serialize($noviTransferi, 'json');
//
////        dump($noviTransferi);
////        dump($json_string);
//        return $this->render('tdCMBundle:Transfer:log.html.twig', array(
//            'noviTransferi' => $json_string,
//        ));
    }

    /**
     *  Manual sending LOS table to Booking.com from Backend
     *
     * @param SetupPropertyUnit $setupPropertyUnit
     * @return Response|static
     */
    public function manualSendLosToBookingComAction(SetupPropertyUnit $setupPropertyUnit)
    {
        //Sigurnost

        $request = $this->get('request');

        $message = array(
            'status' => 'Error!',
            'message' => 'Input data is not valid!',
            'message2' => 'dateFrom = ' . $request->request->get('dateFrom') . ' dateTo = ' . $request->request->get('dateTo'),
        );

//        return JsonResponse::create($message);


        if (!is_null($request->request->get('dateFrom'))) { //Create context for sending
            $em = $this->getDoctrine()->getManager();
            $promjena = new Promjena();
            $promjena->setDateFrom(new DateTime($request->request->get('dateFrom')));
            $promjena->setDateTo(new DateTime($request->request->get('dateTo')));
            $promjena->setPropertyUnit($setupPropertyUnit->getPropertyUnit());
            $em->persist($promjena);
            $transfer = new Transfer();
            $transfer->setSetupPropertyUnit($setupPropertyUnit);
            $transfer->setPromjena($promjena);
            $em->persist($transfer);
//            $em->flush();

            //Send LOS
            $losResponse = $this->pokreniLosTablicu($transfer); //DEPRECATED
//            $losResponse = $this->get('obr')
            if ($transfer->getOk()) {
                $message = array(
                    'status' => 'ok',
                    'message' => 'LOS slanje je uspješno!',
                    'losResponse' => $losResponse['response'],
                    'podaci' => $losResponse['json_string'],
                );
            } else {
                $message = array(
                    'status' => 'false',
                    'message' => 'LOS slanje je neuspješno',
                    'losResponse' => $losResponse['response'],
                    'podaci' => $losResponse['json_string'],
                );
            }
//            $em->remove($transfer);
//            $em->remove($promjena);
        }

        return JsonResponse::create($message);
    }


    /**
     * Batch obrada jednog transfera za sve prijavljene portale
     *
     * @param Transfer $transfer
     */
    private function obradiTransferBatch(Transfer &$transfer)
    {
        switch ($transfer->getSetupPropertyUnit()->getPortalKorisnik()->getPortal()->getCode()->getCode()) { // POSTAVITI NA RAZINI PORTALA
            case 'bookingcom':
                $this->obradiBookingCom($transfer);
                break;
            case 'atraveo':
                //$this->obradiAtraveo($transfer);
                break;
            case 'edomizil':
                //$this->obradiEdomizil($transfer);
                break;
        }
        return;
    }

    /**
     * Obrada transfera za Booking.com. Poziva web service od Tomislva koji komunicira s booking.com
     *
     * @param Transfer $transfer
     */
    private function obradiBookingCom(Transfer &$transfer)
    {
        //Obrada za Booking.Com
        $em = $this->getDoctrine()->getManager();
        if ('property_unit' != $transfer->getPromjena()->getGrupaPromjene()) { //Provjera da li je grupa promjene dozvoljena
            $povratnaPoruka = $this->pokreniLosTablicu($transfer);//Pozivanje WS od Tomislava
            $this->zapisiTransferLog($transfer, $povratnaPoruka); //Spremanje odgovora u bazu
//dump($transfer  );
            //Dohvati broj log promjena za transfer
            $brojPokusaja = $transfer->getSetupPropertyUnit()->getPortalKorisnik()->getRaspolozivostBrojPokusaja();
            $logovi = $em->getRepository('tdCMBundle:TransferLog')->findByTransfer($transfer);
            if (count($logovi) > $brojPokusaja) {
                //javi na email da je nešto krivo
            }

            if (0) { //Ovo za sad ne delan jer neću transferirat nove dok nisu stari preneseni
                if ($transfer->getOk()) { //Ako je uspješan onda ažuriraj sve stare neuspjele transfere.
                    //            $log->setUspjesanPrijenos(true);
                    //            $transfer->setOk(true);
                    //            $em->flush();

                    $stariNeuspjesniTransferi = $em->getRepository('tdCMBundle:Transfer')->findBy(
                        array(
                            'setupPropertyUnit' => $transfer->getSetupPropertyUnit(),
                            'ok' => false)
                    );

                    foreach ($stariNeuspjesniTransferi as $zadnjiTransfer) {
                        /* @var $zadnjiTransfer Transfer */
                        $zadnjiTransfer->setOk(true);

                        //Ažuriraj stari log
                        $log2 = new TransferLog();
                        $log2->setDatumVrijeme(new DateTime());
                        $log2->setTransfer($zadnjiTransfer);
                        $log2->setUspjesanPrijenos(true);
                        $log2->setPrijenosNaknadnomPromjenom(true);
                        $log2->setResponseTransfera('naknadna promjena');

                        $em->persist($log2);
                    }
                }
            }
            $em->flush();

        } else {
            $transfer->setOk(true);
            $this->zapisiTransferLog($transfer, 'property_unit se ne prenosi na booking.com');
        }
        return;
    }

    /**
     * Još nije dovršeno
     *
     * @param Transfer $transfer
     */
    private function obradiAtraveo(Transfer &$transfer)
    {
        //Obrada za Atraveo
        $transfer->setOk(true);
        $this->zapisiTransferLog($transfer, 'Ne postoji kontroler');
        return;
    }

    /**
     * Još nije dovršeno
     *
     * @param Transfer $transfer
     */
    private function obradiEdomizil(Transfer &$transfer)
    {
        //Obrada za E-domizil
        $transfer->setOk(true);
        $this->zapisiTransferLog($transfer, 'Ne postoji kontroler');
        return;
    }

    /**
     * @param $transfer
     * @param $poruka
     * @return TransferLog
     */
    private function zapisiTransferLog(&$transfer, $poruka)
    {
        /* @var $transfer Transfer */
        //Ažuriraj u log - bez obzira da li je transfer uspješan ili ne
        $em = $this->getDoctrine()->getManager();
        $log = new TransferLog();
        $log->setDatumVrijeme(new DateTime());
        $log->setTransfer($transfer);

        $log->setResponseTransfera($poruka);
        if ($transfer->getOk()) {
            $log->setUspjesanPrijenos(true);
        }
        $transfer->addTransferLogovi($log);

        $em->persist($log);
        $em->flush();

        return $log;
        //Ažuriram sve ostale logove koji su otvoreni za aktualni unit
    }

    /**
     * Ovo se koristi za Booking.com - OVO JE ZA SADA VAŽEĆE. TransferObrada SE NE KORISTI
     *
     * @param $transfer
     * @return string
     */
    private function pokreniLosTablicu($transfer)
    {
        /* @var $transfer Transfer */
        $polje = array();
        $danas = new DateTime('now');
        $polje['id_property'] = $transfer->getSetupPropertyUnit()->getPropertyUnit()->getProperty()->getId();
        $polje['id_property_bookingcom'] = (int)$transfer->getSetupPropertyUnit()->getSetupProperty()->getIdPropertyNaPortalu();
        if ($transfer->getPromjena()->getDateFrom() >= $danas) {
            $polje['date_start'] = $transfer->getPromjena()->getDateFrom()->format('Y-m-d');
        } else {
            $polje['date_start'] = $danas->format('Y-m-d');
        }
        $polje['date_end'] = $transfer->getPromjena()->getDateTo()->format('Y-m-d');

        // 2.9.2019 - Dodano je da se šalje podatak o transferu i promjeni
        $polje['id_transfer'] = $transfer->getId();
        $polje['id_promjena'] = $transfer->getPromjena()->getId();

        $polje['units'] = array();

//        return new Response('Broj unita je: ' . $transfer->getSetupPropertyUnit()->getSetupProperties()->getSetupPropertyUnits()->count());

        $brojač = 0;
        $brojač2 = 0;
        /* @var $setupPropertyUnit SetupPropertyUnit */
//        foreach ($transfer->getSetupPropertyUnit()->getSetupProperties()->getSetupPropertyUnits() as $setupPropertyUnit) {
//            if ($setupPropertyUnit->getAktivan() && $setupPropertyUnit->getIdPropertyUnitNaPortalu() != '') {
//                $polje['units'][$brojač] = array();
//                $polje['units'][$brojač]['id_unit'] = $setupPropertyUnit->getPropertyUnit()->getId();
//                $polje['units'][$brojač]['id_unit_bookingcom'] = (int)$setupPropertyUnit->getIdPropertyUnitNaPortalu();
//                $polje['units'][$brojač]['rates'] = array();
//
//                /* @var $unitRate UnitRateStavke */
//                foreach ($setupPropertyUnit->getUnitRates() as $unitRate) {
//                    if ($unitRate->getAktivan()) {
//                        $polje['units'][$brojač]['rates'][$brojač2] = array();
//                        $polje['units'][$brojač]['rates'][$brojač2]['id_cjenik'] = $unitRate->getIdCjenik();
//                        $polje['units'][$brojač]['rates'][$brojač2]['id_rate_bookingcom'] = $unitRate->getRate()->getRateIdPortal();
//                        $polje['units'][$brojač]['rates'][$brojač2]['samo_cjenik'] = $unitRate->getSamoCjenik();
//                        if (!$unitRate->getSamoCjenik()) {
//                            $polje['units'][$brojač]['rates'][$brojač2]['id_set'] = $unitRate->getIdSet();
//                        }
//                        $brojač2++;
//                    }
//                }
//                $brojač++;
//                $brojač2 = 0;
//            }
//        }

        $polje['units'] = array();
        $polje['units'][0] = array();
        $polje['units'][0]['id_unit'] = $transfer->getPromjena()->getPropertyUnit()->getId();
        $polje['units'][0]['id_unit_bookingcom'] = $transfer->getSetupPropertyUnit()->getIdPropertyUnitNaPortalu();
        $polje['units'][0]['rates'] = array();

        /* @var $unitRate UnitRate */
        foreach ($transfer->getSetupPropertyUnit()->getUnitRates() as $unitRate) {
//            if ($unitRate->getAktivan()) {
                $polje['units'][$brojač]['rates'][$brojač2] = array();
                $polje['units'][$brojač]['rates'][$brojač2]['id_cjenik'] = $unitRate->getIdCjenik();
                $polje['units'][$brojač]['rates'][$brojač2]['id_rate_bookingcom'] = $unitRate->getRate()->getRateIdPortal();
                $polje['units'][$brojač]['rates'][$brojač2]['samo_cjenik'] = $unitRate->getSamoCjenik();
                if (!$unitRate->getSamoCjenik()) {
                    $polje['units'][$brojač]['rates'][$brojač2]['id_set'] = $unitRate->getIdSet();
                }
                $brojač2++;
//            }
        }
        //dump($polje);

        $username = "fiskalizacija";
        $password = "21jmdsh45";

        // jSON String for request
        $json_string = json_encode($polje);

        // Configuring curl options
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://agencije.atrade.hr/booking.com/lossender");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Cache-Control: no-cache'));
        curl_setopt($ch, CURLOPT_HEADER, 0); // bilo 0
        curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        // Getting results
        $result = curl_exec($ch);
        curl_close($ch);

//        $result = '{"response":"method,status,ticket_id\nlos_pricing,ok,c58c8b4a8378011f\n<ok><\/ok>\n<!-- RUID: [UmFuZG9tSVYkc2RlIyh9YVmB9GBKngJ4XoCvKihHCCS29uw9YGT+KcPYvfuKMofRJfswqPWwxj20cTgi7iVnNjm\/Pw\/Ce0sv] -->"}';
//        $result = '{"response":"method,fault_code,fault_message\nlos_pricing,1003,Failed to interpret line #1 with error message: \"rate_id [1592191] not valid for hotel [1165100]\". Raw line data: [2015-10-15,2,116510001,1592191].;';

//        $result = '{"response":"method,status,ticket_id\nlos_pricing,ok,68e87ae58e780156\n<ok><\/ok>\n<!-- RUID: [UmFuZG9tSVYkc2RlIyh9YU4O5ioyu\/wS9rpyxEpjx2eLdPXSnz+iE5k\/Qds3b7NMf2tfUAkzT4A5LIwxUbnFTZK230PHMAv2] -->"}';
        if (substr($result, 50, 2) == 'ok') {
            $transfer->setOk(true);
            $transfer->getSetupPropertyUnit()->getPropertyUnit()->setAzurirano(new DateTime());

        }
//        return substr($result,50,2);
        return array(
            'response' => $result,
            'json_string' => $json_string,
        );

        //Slanje Responsa
//        $response = new JsonResponse();
//        $response->setData($polje);
//        $response = new Response((string)$result);
//        return $response;

//        {
//            "id_property":8859,
//   "id_property_bookingcom":1165100,
//   "date_start":"2015-11-11",
//   "date_end":"2015-12-11",
//   "units":[
//      {
//          "id_unit":22174,
//         "id_unit_bookingcom":116510001,
//         "rates":[
//            {
//                "id_cjenik":59411,
//               "id_rate_bookingcom":4045295,
//               "samo_cjenik":true
//            },
//            {
//                "id_cjenik":59411,
//               "id_rate_bookingcom":4045294,
//               "samo_cjenik":true
//            }
//         ]
//      },
//      {
//          "id_unit":22175,
//         "id_unit_bookingcom":116510003,
//         "rates":[
//            {
//                "id_cjenik":59408,
//               "id_rate_bookingcom":4045292,
//               "samo_cjenik":false,
//               "id_set":32344
//            }
//         ]
//      }
//   ]
//}
    }

}
