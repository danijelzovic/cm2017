<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SetupPropertyLog
 *
 * @ORM\Table(name="cm_setup_property_LOG")
 * @ORM\Entity
 */
class SetupPropertyLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="idSetupProperty", type="integer")
     */
    private $idSetupProperty;

    /**
     * @ORM\ManyToOne(targetEntity="SetupProperty")
     * @ORM\JoinColumn(name="idSetupProperty", referencedColumnName="id", onDelete="RESTRICT")
     */
    private $setupProperty;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="idWebshop", type="integer", nullable=true)
     */
    private $idWebshop;

	/**
     * @ORM\ManyToOne(targetEntity="Webshop")
     * @ORM\JoinColumn(name="idWebshop", referencedColumnName="id", onDelete="RESTRICT")
	 */
	private $webshop;
	
    /**
     * @var integer
     *
     * @ORM\Column(name="idProperty", type="integer")
     */
    private $idProperty;

    /**
     * @ORM\ManyToOne(targetEntity="Property")
     * @ORM\JoinColumn(name="idProperty", referencedColumnName="id")
     **/
    private $property;
	
	  /**
     * @var string
     *
     * @ORM\Column(name="idPropertyNaPortalu", type="string", length=255, nullable=true)
     */
    private $idPropertyNaPortalu;

	 /**
     * @var boolean
     *
     * @ORM\Column(name="aktivan", type="boolean")
     */
    private $aktivan = true;

	/**
     * @ORM\ManyToOne(targetEntity="PortalKorisnik", inversedBy="setup")
     * @ORM\JoinColumn(name="ID_portal_korisnik", referencedColumnName="id", onDelete="CASCADE")
	 *
     */
    private $portalKorisnik;

    /**
     * @var string
     *
     * @ORM\Column(name="idSkrbnik", type="string", length=255)
     */
    private $idSkrbnik;

    /**
     * @ORM\OneToMany(targetEntity="SetupProperty", mappedBy="setupProperties")
     */
    private $setupPropertyUnits;
    
     /**
     * @var \DateTime
     *
     * @ORM\Column(name="datumVrijemePromjene", type="datetime", options={"default":"0000-00-00 00:00:00"})
     */
    private $datumVrijemePromjene;
    
    /**
     * @var string
     *
     * @ORM\Column(name="akcija", type="string", length=255)
     */
    private $akcija;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idWebshop
     *
     * @param integer $idWebshop
     * @return SetupProperty
     */
    public function setIdWebshop($idWebshop)
    {
        $this->idWebshop = $idWebshop;

        return $this;
    }

    /**
     * Get idWebshop
     *
     * @return integer 
     */
    public function getIdWebshop()
    {
        return $this->idWebshop;
    }

    /**
     * Set idProperty
     *
     * @param integer $idProperty
     * @return SetupProperty
     */
    public function setIdProperty($idProperty)
    {
        $this->idProperty = $idProperty;

        return $this;
    }

    /**
     * Get idProperty
     *
     * @return integer 
     */
    public function getIdProperty()
    {
        return $this->idProperty;
    }

    /**
     * Set idPropertyNaPortalu
     *
     * @param string $idPropertyNaPortalu
     * @return SetupProperty
     */
    public function setIdPropertyNaPortalu($idPropertyNaPortalu)
    {
        $this->idPropertyNaPortalu = $idPropertyNaPortalu;

        return $this;
    }

    /**
     * Get idPropertyNaPortalu
     *
     * @return string 
     */
    public function getIdPropertyNaPortalu()
    {
        return $this->idPropertyNaPortalu;
    }

    /**
     * Set aktivan
     *
     * @param boolean $aktivan
     * @return SetupProperty
     */
    public function setAktivan($aktivan)
    {
        $this->aktivan = $aktivan;

        return $this;
    }

    /**
     * Get aktivan
     *
     * @return boolean 
     */
    public function getAktivan()
    {
        return $this->aktivan;
    }

    /**
     * Set idSkrbnik
     *
     * @param string $idSkrbnik
     * @return SetupProperty
     */
    public function setIdSkrbnik($idSkrbnik)
    {
        $this->idSkrbnik = $idSkrbnik;

        return $this;
    }

    /**
     * Get idSkrbnik
     *
     * @return string 
     */
    public function getIdSkrbnik()
    {
        return $this->idSkrbnik;
    }

    /**
     * Set webshop
     *
     * @param \td\CMBundle\Entity\Webshop $webshop
     * @return SetupProperty
     */
    public function setWebshop(\td\CMBundle\Entity\Webshop $webshop = null)
    {
        $this->webshop = $webshop;

        return $this;
    }

    /**
     * Get webshop
     *
     * @return \td\CMBundle\Entity\Webshop 
     */
    public function getWebshop()
    {
        return $this->webshop;
    }

    /**
     * Set property
     *
     * @param \td\CMBundle\Entity\Property $property
     * @return SetupProperty
     */
    public function setProperty(\td\CMBundle\Entity\Property $property = null)
    {
        $this->property = $property;

        return $this;
    }

    /**
     * Get property
     *
     * @return \td\CMBundle\Entity\Property 
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * Set portalKorisnik
     *
     * @param \td\CMBundle\Entity\PortalKorisnik $portalKorisnik
     * @return SetupProperty
     */
    public function setPortalKorisnik(\td\CMBundle\Entity\PortalKorisnik $portalKorisnik = null)
    {
        $this->portalKorisnik = $portalKorisnik;

        return $this;
    }

    /**
     * Get portalKorisnik
     *
     * @return \td\CMBundle\Entity\PortalKorisnik 
     */
    public function getPortalKorisnik()
    {
        return $this->portalKorisnik;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setupPropertyUnits = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add setupPropertyUnits
     *
     * @param \td\CMBundle\Entity\SetupProperty $setupPropertyUnits
     * @return SetupProperty
     */
    public function addSetupPropertyUnit(\td\CMBundle\Entity\SetupProperty $setupPropertyUnits)
    {
        $this->setupPropertyUnits[] = $setupPropertyUnits;

        return $this;
    }

    /**
     * Remove setupPropertyUnits
     *
     * @param \td\CMBundle\Entity\SetupProperty $setupPropertyUnits
     */
    public function removeSetupPropertyUnit(\td\CMBundle\Entity\SetupProperty $setupPropertyUnits)
    {
        $this->setupPropertyUnits->removeElement($setupPropertyUnits);
    }

    /**
     * Get setupPropertyUnits
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSetupPropertyUnits()
    {
        return $this->setupPropertyUnits;
    }

    /**
     * Set idSetupProperty
     *
     * @param integer $idSetupProperty
     * @return SetupPropertyLog
     */
    public function setIdSetupProperty($idSetupProperty)
    {
        $this->idSetupProperty = $idSetupProperty;

        return $this;
    }

    /**
     * Get idSetupProperty
     *
     * @return integer 
     */
    public function getIdSetupProperty()
    {
        return $this->idSetupProperty;
    }

    /**
     * Set datumVrijemePromjene
     *
     * @param \DateTime $datumVrijemePromjene
     * @return SetupPropertyLog
     */
    public function setDatumVrijemePromjene($datumVrijemePromjene)
    {
        $this->datumVrijemePromjene = $datumVrijemePromjene;

        return $this;
    }

    /**
     * Get datumVrijemePromjene
     *
     * @return \DateTime 
     */
    public function getDatumVrijemePromjene()
    {
        return $this->datumVrijemePromjene;
    }

    /**
     * Set akcija
     *
     * @param string $akcija
     * @return SetupPropertyLog
     */
    public function setAkcija($akcija)
    {
        $this->akcija = $akcija;

        return $this;
    }

    /**
     * Get akcija
     *
     * @return string 
     */
    public function getAkcija()
    {
        return $this->akcija;
    }

    /**
     * Set setupProperties
     *
     * @param \td\CMBundle\Entity\SetupProperty $setupProperty
     *
     * @return SetupPropertyLog
     */
    public function setSetupProperty(\td\CMBundle\Entity\SetupProperty $setupProperty = null)
    {
        $this->setupProperty = $setupProperty;

        return $this;
    }

    /**
     * Get setupProperties
     *
     * @return \td\CMBundle\Entity\SetupProperty
     */
    public function getSetupProperty()
    {
        return $this->setupProperty;
    }
}
