<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SetupGrid
 *
 * @ORM\Table(name="cm_setup_grid")
 * @ORM\Entity
 */
class SetupGrid
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="idPortal", type="string", length=255)
     */
    private $idPortal;
	
	/**
     * @var string
     *
     * @ORM\Column(name="idSetup", type="integer")
     */
    private $idSetup;
	
	/**
     * @var string
     *
     * @ORM\Column(name="idUnit", type="integer")
     */
    private $idUnit;

	 /**
     * @var string
     *
     * @ORM\Column(name="idUnitNaPortalu", type="string", length=255, nullable=true)
     */
    private $idUnitNaPortalu;
	
    /**
     * @var string
     *
     * @ORM\Column(name="nazivObjekta", type="string", length=255)
     */
    private $nazivObjekta;

    /**
     * @var string
     *
     * @ORM\Column(name="nazivUnita", type="string", length=255)
     */
    private $nazivUnita;

    /**
     * @var string
     *
     * @ORM\Column(name="mjesto", type="string", length=255)
     */
    private $mjesto;

    /**
     * @var integer
     *
     * @ORM\Column(name="skrbnikId", type="integer")
     */
    private $skrbnikId;

    /**
     * @var string
     *
     * @ORM\Column(name="skrbnikNaziv", type="string", length=255, nullable=true)
     */
    private $skrbnikNaziv;

    /**
     * @var integer
     *
     * @ORM\Column(name="vlasnikId", type="integer")
     */
    private $vlasnikId;

    /**
     * @var string
     *
     * @ORM\Column(name="vlasnikNaziv", type="string", length=255, nullable=true)
     */
    private $vlasnikNaziv;

    /**
     * @var string
     *
     * @ORM\Column(name="adresa", type="string", length=255, nullable=true)
     */
    private $adresa;

	 /**
     * @var boolean
     *
     * @ORM\Column(name="Aktivan", type="boolean")
     */
    private $aktivan;
    
    /**
     * @var string
     *
     * @ORM\Column(name="idPropertyUnitFront", type="string", length=255, nullable=true)
     */
    private $idPropertyUnitFront;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPortal
     *
     * @param string $idPortal
     * @return SetupGrid
     */
    public function setIdPortal($idPortal)
    {
        $this->idPortal = $idPortal;

        return $this;
    }

    /**
     * Get idPortal
     *
     * @return string 
     */
    public function getIdPortal()
    {
        return $this->idPortal;
    }

    /**
     * Set nazivObjekta
     *
     * @param string $nazivObjekta
     * @return SetupGrid
     */
    public function setNazivObjekta($nazivObjekta)
    {
        $this->nazivObjekta = $nazivObjekta;

        return $this;
    }

    /**
     * Get nazivObjekta
     *
     * @return string 
     */
    public function getNazivObjekta()
    {
        return $this->nazivObjekta;
    }

    /**
     * Set nazivUnita
     *
     * @param string $nazivUnita
     * @return SetupGrid
     */
    public function setNazivUnita($nazivUnita)
    {
        $this->nazivUnita = $nazivUnita;

        return $this;
    }

    /**
     * Get nazivUnita
     *
     * @return string 
     */
    public function getNazivUnita()
    {
        return $this->nazivUnita;
    }

    /**
     * Set mjesto
     *
     * @param string $mjesto
     * @return SetupGrid
     */
    public function setMjesto($mjesto)
    {
        $this->mjesto = $mjesto;

        return $this;
    }

    /**
     * Get mjesto
     *
     * @return string 
     */
    public function getMjesto()
    {
        return $this->mjesto;
    }

    /**
     * Set skrbnikId
     *
     * @param integer $skrbnikId
     * @return SetupGrid
     */
    public function setSkrbnikId($skrbnikId)
    {
        $this->skrbnikId = $skrbnikId;

        return $this;
    }

    /**
     * Get skrbnikId
     *
     * @return integer 
     */
    public function getSkrbnikId()
    {
        return $this->skrbnikId;
    }

    /**
     * Set skrbnikNaziv
     *
     * @param string $skrbnikNaziv
     * @return SetupGrid
     */
    public function setSkrbnikNaziv($skrbnikNaziv)
    {
        $this->skrbnikNaziv = $skrbnikNaziv;

        return $this;
    }

    /**
     * Get skrbnikNaziv
     *
     * @return string 
     */
    public function getSkrbnikNaziv()
    {
        return $this->skrbnikNaziv;
    }

    /**
     * Set vlasnikId
     *
     * @param integer $vlasnikId
     * @return SetupGrid
     */
    public function setVlasnikId($vlasnikId)
    {
        $this->vlasnikId = $vlasnikId;

        return $this;
    }

    /**
     * Get vlasnikId
     *
     * @return integer 
     */
    public function getVlasnikId()
    {
        return $this->vlasnikId;
    }

    /**
     * Set vlasnikNaziv
     *
     * @param string $vlasnikNaziv
     * @return SetupGrid
     */
    public function setVlasnikNaziv($vlasnikNaziv)
    {
        $this->vlasnikNaziv = $vlasnikNaziv;

        return $this;
    }

    /**
     * Get vlasnikNaziv
     *
     * @return string 
     */
    public function getVlasnikNaziv()
    {
        return $this->vlasnikNaziv;
    }

    /**
     * Set adresa
     *
     * @param string $adresa
     * @return SetupGrid
     */
    public function setAdresa($adresa)
    {
        $this->adresa = $adresa;

        return $this;
    }

    /**
     * Get adresa
     *
     * @return string 
     */
    public function getAdresa()
    {
        return $this->adresa;
    }

    /**
     * Set idUnit
     *
     * @param integer $idUnit
     * @return SetupGrid
     */
    public function setIdUnit($idUnit)
    {
        $this->idUnit = $idUnit;

        return $this;
    }

    /**
     * Get idUnit
     *
     * @return integer 
     */
    public function getIdUnit()
    {
        return $this->idUnit;
    }

    /**
     * Set idUnitNaPortalu
     *
     * @param string $idUnitNaPortalu
     * @return SetupGrid
     */
    public function setIdUnitNaPortalu($idUnitNaPortalu)
    {
        $this->idUnitNaPortalu = $idUnitNaPortalu;

        return $this;
    }

    /**
     * Get idUnitNaPortalu
     *
     * @return string 
     */
    public function getIdUnitNaPortalu()
    {
        return $this->idUnitNaPortalu;
    }

    /**
     * Set idSetup
     *
     * @param integer $idSetup
     * @return SetupGrid
     */
    public function setIdSetup($idSetup)
    {
        $this->idSetup = $idSetup;

        return $this;
    }

    /**
     * Get idSetup
     *
     * @return integer 
     */
    public function getIdSetup()
    {
        return $this->idSetup;
    }

    /**
     * Set aktivan
     *
     * @param boolean $aktivan
     * @return SetupGrid
     */
    public function setAktivan($aktivan)
    {
        $this->aktivan = $aktivan;

        return $this;
    }

    /**
     * Get aktivan
     *
     * @return boolean 
     */
    public function getAktivan()
    {
        return $this->aktivan;
    }

    /**
     * Set idPropertyUnitFront
     *
     * @param string $idPropertyUnitFront
     * @return SetupGrid
     */
    public function setIdPropertyUnitFront($idPropertyUnitFront)
    {
        $this->idPropertyUnitFront = $idPropertyUnitFront;

        return $this;
    }

    /**
     * Get idPropertyUnitFront
     *
     * @return string 
     */
    public function getIdPropertyUnitFront()
    {
        return $this->idPropertyUnitFront;
    }
}
