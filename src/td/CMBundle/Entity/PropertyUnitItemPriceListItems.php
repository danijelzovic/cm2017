<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PropertyUnitItemPriceListItem
 *
 * @ORM\Table(name="property_unit_itempricelist_items")
 * @ORM\Entity(repositoryClass="td\CMBundle\Entity\PropertyUnitItemPriceListItemRepository")
 */
class PropertyUnitItemPriceListItems
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_head", type="integer")
     */
    private $idHead;
	
    /**
     * @ORM\ManyToOne(targetEntity="ItemPriceListItem")
     * @ORM\JoinColumn(name="id_itempricelist_item", referencedColumnName="id", onDelete="CASCADE")
    */
    private $itemPriceListItem;

    /**
     * @var string
     *
     * @ORM\Column(name="service", type="string", length=2)
     */
    private $service;

    /**
     * @var string
     *
     * @ORM\Column(name="odnosna", type="string", length=255)
     */
    private $odnosna;

    /**
     * @var boolean
     *
     * @ORM\Column(name="frontprikaz", type="boolean")
     */
    private $frontprikaz;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordering", type="smallint")
     */
    private $ordering;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idHead
     *
     * @param integer $idHead
     * @return PropertyUnitItemPriceListItems
     */
    public function setIdHead($idHead)
    {
        $this->idHead = $idHead;

        return $this;
    }

    /**
     * Get idHead
     *
     * @return integer 
     */
    public function getIdHead()
    {
        return $this->idHead;
    }

    /**
     * Set service
     *
     * @param string $service
     * @return PropertyUnitItemPriceListItems
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return string 
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set odnosna
     *
     * @param string $odnosna
     * @return PropertyUnitItemPriceListItems
     */
    public function setOdnosna($odnosna)
    {
        $this->odnosna = $odnosna;

        return $this;
    }

    /**
     * Get odnosna
     *
     * @return string 
     */
    public function getOdnosna()
    {
        return $this->odnosna;
    }

    /**
     * Set frontprikaz
     *
     * @param boolean $frontprikaz
     * @return PropertyUnitItemPriceListItems
     */
    public function setFrontprikaz($frontprikaz)
    {
        $this->frontprikaz = $frontprikaz;

        return $this;
    }

    /**
     * Get frontprikaz
     *
     * @return boolean 
     */
    public function getFrontprikaz()
    {
        return $this->frontprikaz;
    }

    /**
     * Set ordering
     *
     * @param integer $ordering
     * @return PropertyUnitItemPriceListItems
     */
    public function setOrdering($ordering)
    {
        $this->ordering = $ordering;

        return $this;
    }

    /**
     * Get ordering
     *
     * @return integer 
     */
    public function getOrdering()
    {
        return $this->ordering;
    }

    /**
     * Set itemPriceListItem
     *
     * @param \td\CMBundle\Entity\ItemPriceListItem $itemPriceListItem
     * @return PropertyUnitItemPriceListItems
     */
    public function setItemPriceListItem(\td\CMBundle\Entity\ItemPriceListItem $itemPriceListItem = null)
    {
        $this->itemPriceListItem = $itemPriceListItem;

        return $this;
    }

    /**
     * Get itemPriceListItem
     *
     * @return \td\CMBundle\Entity\ItemPriceListItem 
     */
    public function getItemPriceListItem()
    {
        return $this->itemPriceListItem;
    }
}
