<?php

namespace td\CMBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\DriverManager;
use Doctrine\ORM\Query;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use td\CMBundle\Entity\PortalKorisnik;
use td\CMBundle\Entity\Portal;
use td\CMBundle\Entity\Property;
use td\CMBundle\Entity\PropertyUnit;
use td\CMBundle\Entity\SetupProperty;
use td\CMBundle\Entity\SetupPropertyUnit;

class SetupJsonController extends Controller
{
    /**
     * @param Portal $portal
     * @return JsonResponse
     */
    public function propertiesUnitsBookingComAction(Portal $portal)
    {
        $loginData = $this->get('td_cm.obrada.cookie_obrada');

        if ($loginData->obradaCookie()) {
//            $em = $this->getDoctrine()->getManager();
            $em = $this->getDoctrine()->getManager();
            $portalKorisnik = $em->getRepository('tdCMBundle:PortalKorisnik')->findOneBy(array(
                'portal' => $portal,
                'idCompanyKorisnik' => $loginData->getIdCompanyKorisnik(),
            ));

            //Dohvat podataka ovisno o svojstvu korisnika
            //Ovo ću nadomjestit s funkcijom koja se poziva ako nema spu-a u dohvaćenim podacima.
            $this->checkForNewPropertiesUnits($portalKorisnik);
            if ($loginData->getPravaOgraniceni()) {
//                $data = $em->getRepository('tdCMBundle:SetupProperty')->findSetupPropertiesUnitsByPortalKorisnikPravaOgraniceni($portalKorisnik, $loginData->getIdCurrentUserClient());
//                $data = $em->getRepository('tdCMBundle:SetupPropertyUnit')->findSetupPropertiesUnitsByPortalKorisnikPravaOgraniceni($portalKorisnik, $loginData->getIdCurrentUserClient());
                $data = $em->getRepository('tdCMBundle:PropertyUnit')->findPropertiesUnitsByPortalKorisnikPravaOgraniceni($portalKorisnik, $loginData->getIdCurrentUserClient());
            } else {
//                $data = $em->getRepository('tdCMBundle:SetupProperty')->findSetupPropertiesUnitsByPortalKorisnikNotPravaOgraniceni($portalKorisnik, $loginData->getIdCompanyKorisnik());
//                $data = $em->getRepository('tdCMBundle:SetupPropertyUnit')->findSetupPropertiesUnitsByPortalKorisnikNotPravaOgraniceni($portalKorisnik, $loginData->getIdCompanyKorisnik());
                $data = $em->getRepository('tdCMBundle:PropertyUnit')->findPropertiesUnitsByPortalKorisnikNotPravaOgraniceni($portalKorisnik, $loginData->getIdCompanyKorisnik());
            }

            $propertiesUnits = array();  //Polje koje se vraća na front s podacima

            $counter = 1;
            $idSp = 0;
            foreach ($data as $item) {
                if (0 == $idSp || $idSp != $item['idSp']) {
                    $polje = array(
                        'type' => 'object',
                        'order' => $counter++,
                        'id' => $item['idSp'],
                        'idFront' => $item['idPropertyFront'],
                        'name' => $item['nameP'],
                        'idPropertyOnPortal' => $item['idPropertyNaPortalu'],
                        'idPropertyUnitOnPortal' => 0,
                        'aktivan' => $item['aktivanSp'],
                        'povezan' => $item['povezanSp'],
                        'linkPropertyOnPortal' => '',
                    );
                    if ('iCal' == $portal->getViewTypeSetup()) {
                        $polje['iCal'] = '';
                    }
                    $propertiesUnits[] = $polje;
                    $idSp = $item['idSp'];
                }
                $polje = array(
                    'type' => 'unit',
                    'order' => $counter,
                    'id' => $item['idSpu'],
                    'idFront' => $item['idPropertyUnitFront'],
                    'name' => $item['namePu'],
                    'idPropertyOnPortal' => $item['idPropertyNaPortalu'],
                    'idPropertyUnitOnPortal' => $item['idPropertyUnitNaPortalu'],
                    'aktivan' => $item['aktivanSpu'],
                    'povezan' => $item['povezanSpu'],
                    'linkPropertyUnitOnPortal' => $portalKorisnik->getPortal()->getLinkPropertyUnitOnPortal(),
//                    'linkPropertyUnitOnPortal' => $item['linkPropertyUnitOnPortal'],
                    //'ratesSet' => $item['ratesSet'],        //Status da li ima povezanih rateova. Ako nema onda je botun crveni za postavljanje rateova
                    'ratesSet' => null,
                );
                if ('iCal' == $portal->getViewTypeSetup()) {
                    $polje['iCal'] = 'bookertech.atrade.hr/app/channel/ical.php?id_unit=' . ($item['idPropertyUnit'] * 24680);
                }
                $propertiesUnits[] = $polje;
            }

        } else {
            $propertiesUnits = array('Error!');
        }

        return new JsonResponse($propertiesUnits);
    }

    /**
     *  Get properties and corresponding units for given portal and logged user
     *
     * @param Portal $portal
     * @return JsonResponse
     */
    public function getPropertiesUnitsAction(Portal $portal)
    {
        $loginData = $this->get('td_cm.obrada.cookie_obrada');

        if ($loginData->obradaCookie()) { //Check if user is Authenticated
            //Initialization
            $em = $this->getDoctrine()->getManager();
            $portalKorisnik = $em->getRepository('tdCMBundle:PortalKorisnik')->findOneBy(array(
                'portal' => $portal,
                'idCompanyKorisnik' => $loginData->getIdCompanyKorisnik(),
            ));

            //Get all properties for auth user. Depends if user is restricted
            if ($loginData->getPravaOgraniceni()) {
                $properties = $em->getRepository('tdCMBundle:Property')
                    ->findPropertiesAndSetupPropertiesByPravaOgraniceni($portalKorisnik, $loginData->getIdCurrentUserClient());
            } else {
                $properties = $em->getRepository('tdCMBundle:Property')
                    ->findPropertiesAndSetupPropertiesByNotPravaOgraniceni($portalKorisnik, $loginData->getIdCompanyKorisnik());
            }

            //Create an array for sending to front
            $order = 1;
            foreach ($properties as $key => $property) {
                //Check if SetupProperty exists. If not then create
                $properties[$key]['order'] = $order++;
                if (!$property['idSp']) {
                    $propertyObject = $em->getRepository('tdCMBundle:Property')->find($property['idProperty']);
                    //Create SetupPropertyUnit
                    $setupProperty = new SetupProperty();
                    $setupProperty->setDatumKreiranja(new \DateTime('now'));
                    $setupProperty->setPortalKorisnik($portalKorisnik);
                    $setupProperty->setProperty($propertyObject);
                    $setupProperty->setIdFront($propertyObject->getPropertyFront());
                    $em->persist($setupProperty);
                    $em->flush();
                    $property['idSp'] = $setupProperty->getId();
                }
//                return new JsonResponse($properties);

                //Check PropertyUnits
                $propertyUnits = $em->getRepository('tdCMBundle:PropertyUnit')->createQueryBuilder('pu')
                    ->select('pu.id as idPropertyUnit')
                    ->addSelect('CONCAT(p.idCountry, \'-\', LPAD(p.id,5,\'0\'), \'-\', LPAD(pu.localOrder,2,\'0\')) as idPropertyUnitFront')
                    ->addSelect('pu.name as namePu')
                    ->addSelect('spu.id as idSpu')
                    ->addSelect('spu.idPropertyUnitNaPortalu')
                    ->addSelect('spu.aktivan as aktivanSpu')
                    ->addSelect('spu.povezan as povezanSpu')
                    ->innerJoin('pu.property', 'p')
                    ->leftJoin('pu.setupPropertyUnits', 'spu', 'WITH', 'spu.portalKorisnik = :portalKorisnik')
                    ->where('pu.deleted = :deleted')
                    ->andWhere('pu.idProperty = :property')
                    ->setParameter('deleted', 0)
                    ->setParameter('property', $property['idProperty'])
                    ->setParameter('portalKorisnik', $portalKorisnik)
                    ->getQuery()
                    ->getResult();

//                return new JsonResponse($propertyUnits);
                /* @var PropertyUnit $propertyUnit */
                foreach ($propertyUnits as $key2 => $propertyUnit) {
                    //Check if SetupPropertyUnit exists. If not create one.
                    if (!$propertyUnit['idSpu']) {
                        $propertyUnitObject = $em->getRepository('tdCMBundle:PropertyUnit')->find($propertyUnit['idPropertyUnit']);
                        $setupPropertyObject = $em->getRepository('tdCMBundle:SetupProperty')->find($property['idSp']);
                        $setupPropertyUnit = new SetupPropertyUnit();
                        $setupPropertyUnit->setDatumKreiranja(new \DateTime('now'));
                        $setupPropertyUnit->setPortalKorisnik($portalKorisnik);
                        $setupPropertyUnit->setSetupProperty($setupPropertyObject);
                        $setupPropertyUnit->setPropertyUnit($propertyUnitObject);
                        $setupPropertyUnit->setIdPropertyUnitFront($propertyUnitObject->getPropertyUnitFront());
                        $em->persist($setupPropertyUnit);
                        $em->flush();
                        $propertyUnits[$key2]['idSpu'] = $setupPropertyUnit->getId();
                        $propertyUnits[$key2]['aktivanSpu'] = false;
                        $propertyUnits[$key2]['povezanSpu'] = false;

                    }

                    //Check rates for Bookingcom
                    if ($portalKorisnik->getPortal()->isBookingCom()) {
                        $setupPropertyUnit = $em->getRepository('tdCMBundle:SetupPropertyUnit')->find($propertyUnits[$key2]['idSpu']);
                        $propertyUnits[$key2]['countSettedRates'] = $em->getRepository('tdCMBundle:UnitRate')->countSettedUnitRates($setupPropertyUnit);
                    }
                    //Calculate iCal URL
                    $propertyUnits[$key2]['iCal'] = 'bookertech.atrade.hr/app/channel/ical.php?id_unit=' . ($propertyUnit['idPropertyUnit'] * 24680);
                }
                $properties[$key]['children'] = $propertyUnits;
            }
        } else {
            $properties = array('Error');
        }

        return new JsonResponse($properties);
    }

    /**
     * Get all propertyUnits ids from Booking.com for assocciation - filtered
     *
     * @param SetupPropertyUnit $setupPropertyUnit
     * @return JsonResponse
     * @throws \Exception
     * @internal param $idPropertyOnPortal
     */
    public function unitsBookingComAction(SetupPropertyUnit $setupPropertyUnit)
    {
        $loginData = $this->get('td_cm.obrada.cookie_obrada');
        $message = array(
            'status' => 'Error!',
            'message' => 'Input data is not valid!',
        );
        if ($loginData->obradaCookie()) { //Test if user is auth.
            //Get data from Booking.com
//            $podaci = $this->get('td_cm.obrada.setup_obrada')->getBookingComRoomRates($setupPropertyUnit->getSetupProperty()->getIdPropertyNaPortalu());

            //Filtering those ids which are already assigned
//            $podaciObrada = $this->getAvailableItems($setupPropertyUnit, $podaci);
            $podaciObrada = $this->get('td_cm.obrada.setup_obrada')->getAvailableItemsBookingCom($setupPropertyUnit);
        } else {
            $podaciObrada = $message;
        }

        return new JsonResponse($podaciObrada);
    }

    /**
     * @return JsonResponse
     */
    public function propertiesUnitsICalAction()
    {
        $loginData = $this->get('td_cm.obrada.cookie_obrada');
        if ($loginData->obradaCookie()) {
            $em = $this->getDoctrine()->getManager();
            if ($loginData->getPravaOgraniceni()) {
                $properties = $em->getRepository('tdCMBundle:Property')->findByPravaOgraniceni($loginData->getIdCurrentUserClient());
            } else {
                $properties = $em->getRepository('tdCMBundle:Property')->findByNotPravaOgraniceni($loginData->getIdCompanyKorisnik());
            }

            //Priprema polja za izlaz
            $propertiesUnits = array();
            $counter = 1;
            /* @var Property $property */
            foreach ($properties as $property) {
                $propertiesUnits[] = array(
                    'type' => 'object',
                    'order' => $counter++,
                    'idFront' => $property->getPropertyFront(),
                    'name' => $property->getName(),
                    'iCal' => 0,
                );
                /* @var PropertyUnit $propertyUnit */
                foreach ($property->getPropertyUnits() as $propertyUnit) {
                    if (0 == $propertyUnit->getDeleted()) {
                        $propertiesUnits[] = array(
                            'type' => 'unit',
                            'order' => 0,
                            'id' => $propertyUnit->getId(),
                            'idFront' => $propertyUnit->getPropertyUnitFront(),
                            'name' => $propertyUnit->getName(),
                            'iCal' => 'bookertech.atrade.hr/app/channel/ical.php?id_unit=' . ($propertyUnit->getId() * 24680),
                        );
                    }
                }
            }
        } else {
            $propertiesUnits = array('Error!');
        }
//        $propertiesUnits = array('korisnik' => $this->korisnik);
        return new JsonResponse($propertiesUnits);
    }

    /**
     * Method for saving new idPropertyOnPortal.
     * Id for propertyUntis remains in a database
     *
     * @return JsonResponse
     */
    public function savePropertyOnPortalAction()
    {
        $data = $this->firstProcessOfRequestData();

        $message = array(
            'status' => 'Error!',
            'message' => 'Input data is not valid!',
        );
        $flag = true;
        if (count($data)) {
            if (array_key_exists('idSp', $data)) {
                $em = $this->getDoctrine()->getManager();
                $setupProperty = $em->getRepository('tdCMBundle:SetupProperty')->find($data['idSp']);
                if ($setupProperty) {

                    //Check if objectId exists on Booking.com
                    if ($setupProperty->getPortalKorisnik()->getPortal()->isBookingCom()) {
                        $podaci = $this->get('td_cm.obrada.setup_obrada')->getBookingComRoomRates($data['idPropertyNaPortalu']);
                        if (0 == count($podaci)) {
                            $flag = false;
                            $message = array(
                                'status' => 'Error!',
                                'message' => 'HotelId ' . $data['idPropertyNaPortalu'] . ' not exists on Booking.com!',
                            );
                        }
                    }

                    //Check if objectId already exsits for some other property in BookerTech for given Portal
                    $setupPropertyWithEnteredPortalObjectId = array();
                    if ("" != $data['idPropertyNaPortalu']) {
                        $setupPropertyWithEnteredPortalObjectId = $em->getRepository('tdCMBundle:SetupProperty')->createQueryBuilder('sp')
                            ->innerJoin('sp.portalKorisnik', 'pk')
                            ->innerJoin('pk.portal', 'portal')
                            ->where('sp.id <> :setupPropertyId')
                            ->andWhere('pk.portal = :portal')
                            ->andWhere('sp.idPropertyNaPortalu = :idPropertyNaPortalu')
                            ->setParameter('setupPropertyId', $setupProperty->getId())
                            ->setParameter('portal', $setupProperty->getPortalKorisnik()->getPortal())
                            ->setParameter('idPropertyNaPortalu', $data['idPropertyNaPortalu'])
                            ->getQuery()->getResult();
                    } else {
                        $flag = false;
                        $setupProperty->setIdPropertyNaPortalu('');
                        $em->persist($setupProperty);
                        $em->flush();
                        $message = array(
                            'status' => 'ok',
                            'message' => 'Objekt ' . $setupProperty->getIdFront() . ' više nije povezan na Booking.com sustav.',
                        );
                    }

//                    $setupPropertyWithEnteredPortalObjectId = $em->getRepository('tdCMBundle:SetupProperty')->findOneBy(array(
//                        'id' => 111,
//                        'portalKorisnik' => $setupProperty->getPortalKorisnik(),
//                    ));

                    if (count($setupPropertyWithEnteredPortalObjectId)) {
                        $flag = false;
                        $message = array(
                            'status' => 'Error!',
                            'message' => 'ID objekta je već pridružen objektu ' .
                                $setupPropertyWithEnteredPortalObjectId[0]->getProperty()->getPropertyFront(),
                        );
                    }

                    //Store objectId to database if flag is not raised
                    if ($flag) {
                        $setupProperty->setIdPropertyNaPortalu($data['idPropertyNaPortalu']);
                        $em->persist($setupProperty);
                        $em->flush();
                        $message = array(
                            'status' => 'ok',
                            'message' => 'Objekt ' . $setupProperty->getIdFront() . ' je uspješno povezan sa Booking.com sustavom. Nakon odobrenja veze započeti će razmjena podataka.',
                        );
                    }
                }
            }
        }
        return new JsonResponse($message);
    }

    /**
     * @return JsonResponse
     */
    public function savePropertyUnitOnPortalAction()
    {

        $data = $this->firstProcessOfRequestData();
        $message = array(
            'status' => 'Error!',
            'message' => 'Input data is not valid!',
        );
        if ($data) {
            if (array_key_exists('idSpu', $data)) {
                $em = $this->getDoctrine()->getManager();
                $setupPropertyUnit = $em->getRepository('tdCMBundle:SetupPropertyUnit')->find($data['idSpu']);
                if ($setupPropertyUnit) {
                    $setupPropertyUnit->setIdPropertyUnitNaPortalu($data['idPropertyUnitNaPortalu']);
                    $em->persist($setupPropertyUnit);
                    $em->flush();
                    $message = array(
                        'status' => 'ok',
                        'message' => 'ID: ' .
                            $data['idPropertyUnitNaPortalu'] .
                            ' na portalu ' .
                            $setupPropertyUnit->getPortalKorisnik()->getPortal()->getNaziv() .
                            ' za smještajnu jedinicu ' .
                            $setupPropertyUnit->getIdPropertyUnitFront() .
                            ' je uspješno pohranjen.'
                    );
                }
            }
        }
        return new JsonResponse($message);
    }

    /**
     * @return JsonResponse
     */
    public function objectActivationAction()
    {
        $data = $this->firstProcessOfRequestData();
        $message = array(
            'status' => 'Error!',
            'message' => 'Input data is not valid!',
        );
        if ($data) {
            if (array_key_exists('idSp', $data)) {
                $em = $this->getDoctrine()->getManager();
                $setupProperty = $em->getRepository('tdCMBundle:SetupProperty')->find($data['idSp']);
//                $message = array(
//                    'status' => 'ok',
//                    'message' => 'Ovo je test ' . $setupProperty->getIdFront(),
//                );
                if ($setupProperty) {

                    $setupProperty->setAktivan($data['aktivanSp']);
                    // Recalculate status Povezan for SetupProperty
                    $povezan = false;
                    // Recalculate status Povezan for every setupPropertyUnit
                    /* @var SetupPropertyUnit $setupPropertyUnit */
                    foreach ($setupProperty->getSetupPropertyUnits() as $setupPropertyUnit) {
//                        $em->persist($this->get('td_cm.obrada.setup_obrada')->calculatePovezanSetupPropertyUnit($setupPropertyUnit));
                        $this->get('td_cm.obrada.setup_obrada')->calculatePovezanSetupPropertyUnit($setupPropertyUnit);
                        if($setupPropertyUnit->getPovezan()){
                            $povezan = true;
                        }
                    }

                    if($data['aktivanSp'] && $this->get('td_cm.obrada.setup_obrada')->checkIdOnPortalValidtiy($setupProperty->getIdPropertyNaPortalu() and $povezan)){
                        $setupProperty->setPovezan(true);
                    }else{
                        $setupProperty->setPovezan(false);
                    }
                    $em->persist($setupProperty);
                    $em->flush();
                    $message = array(
                        'status' => 'ok',
                        'message' => 'Objekt ' . $setupProperty->getIdFront() . ' je ' . ($data['aktivanSp'] ? 'aktiviran' : 'deaktiviran'),
                        'povezan' => $setupProperty->getPovezan(),
                    );
                }
            }
        }
        return new JsonResponse($message);
    }

    /**
     *  Unit activation process depends on portal.mustHaveWebshop property
     *  If portal.mustHaveWebshop == true then create record in WebshopPropertyUnit
     *  Else delete record in WebshopPropertyUnit
     *    Also create WebshopProperty if it's necessary
     *
     * @return JsonResponse
     */
    public function unitActivationAction()
    {
        $data = $this->firstProcessOfRequestData();
        $message = array(
            'status' => 'Error!',
            'message' => 'Input data is not valid!',
        );
        $checkIdHead = true;

        if ($data) {
            if (array_key_exists('idSpu', $data) && array_key_exists('aktivanSpu', $data)) { //Verify parameters
                $em = $this->getDoctrine()->getManager();
                $setupPropertyUnit = $em->getRepository('tdCMBundle:SetupPropertyUnit')->find($data['idSpu']);
                if ($setupPropertyUnit) {
                    //Check if idHead must be determined
                    $portalKorisnik = $setupPropertyUnit->getPortalKorisnik();
                    $processActivation = true;

//                    if ($portalKorisnik->getPortal()->getMustHaveWebshop()) {
//
//                        // Turned OFF
//                        if (true == $data['aktivanSpu']) { // Get idHead and create WebshopPropertyUnit
//                            //Check if webshopPropertyUnit exist
//                            $webshopPropertyUnit = $em->getRepository('tdCMBundle:WebshopPropertyUnit')->findOneBy(array(
//                                'webshop' => $portalKorisnik->getWebshop(),
//                                'propertyUnit' => $setupPropertyUnit->getPropertyUnit(),
//                            ));
//
//                            if (is_null($webshopPropertyUnit)) { //Create new if not exists
//                                $idHead = $this->get('td_cm.obrada.setup_obrada')->getIdHeadBookerTech(
//                                    $setupPropertyUnit->getPropertyUnit(),
//                                    $portalKorisnik->getIdCompanyKorisnik()
//                                );
//                                if (-1 == $idHead) {
//                                    $processActivation = false;
//                                    $message['message'] = 'Aktivacija nije moguća jer cjenik nije postavljen za smještajnu jedinicu <br><b>' .
//                                        $setupPropertyUnit->getPropertyUnit()->getPropertyUnitFront() . '</b>!';
//                                } else {
//                                    $webshopPropertyUnit = $this->get('td_cm.obrada.webshop_obrada')->createWebshopPropertyUnit(
//                                        $portalKorisnik->getWebshop(),
//                                        $setupPropertyUnit->getPropertyUnit(),
//                                        $portalKorisnik->getIdCompanyKorisnik()
//                                    );
//                                    if ($portalKorisnik->getCjenikKupovni()) {
//                                        $webshopPropertyUnit->setCjenikKupovni($idHead);
//                                    } else {
//                                        $webshopPropertyUnit->setCjenikProdajni($idHead);
//                                    }
//
//                                }
//                            } else {
//                                $webshopPropertyUnit->setDisabled(false);
//                            }
//
//                        } else { // Disable WebshopPropertyUnit
//                            $webshopPropertyUnit = $em->getRepository('tdCMBundle:WebshopPropertyUnit')->findOneBy(array(
//                                'webshop' => $portalKorisnik->getWebshop(),
//                                'propertyUnit' => $setupPropertyUnit->getPropertyUnit(),
//                            ));
//                            if (!is_null($webshopPropertyUnit)) {
//                                $webshopPropertyUnit->setDisabled(true);
//                            }
//                        }
//                    }

                    //If everything is ok, then change activity status
                    if ($processActivation) {
                        $setupPropertyUnit->setAktivan($data['aktivanSpu']);
                        if($setupPropertyUnit->getAktivan() && $this->get('td_cm.obrada.setup_obrada')->checkIdOnPortalValidtiy($setupPropertyUnit->getIdPropertyUnitNaPortalu())){
                            $this->get('td_cm.obrada.setup_obrada')->calculatePovezanSetupPropertyUnit($setupPropertyUnit);
                        }else{
                            $setupPropertyUnit->setPovezan(false);
                        }
                        $em->persist($setupPropertyUnit);
                        $em->flush();
                        $message = array(
                            'status' => 'ok',
                            'message' => 'Smještajna jedinica ' . $setupPropertyUnit->getIdPropertyUnitFront() . ' je ' . ($data['aktivanSpu'] ? 'aktivirana.' : 'deaktivirana.'),
                            'povezan' => $setupPropertyUnit->getPovezan(),
                        );
                    }

                }
            } else {
                $message['message'] = 'Parameters are not valid!';
            }
        }
        return new JsonResponse($message);
    }

    public function testCountAction()
    {
        $em = $this->getDoctrine()->getManager();
        $webshop = $em->getRepository('tdCMBundle:Webshop')->find(118);
        $property = $em->getRepository('tdCMBundle:Property')->find(526);
        $countWebshopPropertyUnit = (int)$this->getDoctrine()->getManager()->getRepository('tdCMBundle:WebshopPropertyUnit')
            ->countWebshopPropertyUnits($webshop, $property);
        return JsonResponse::create(array('countWebshopPropertyUnits' => $countWebshopPropertyUnit));
    }

    private function savePropertyBookingcom(SetupProperty $setupProperty)
    {
        //Check on Booking.com that Id Portal is valid
        return;
    }

    /**
     * @return array|mixed
     */
    private function firstProcessOfRequestData()
    {
        $loginData = $this->get('td_cm.obrada.cookie_obrada');
        if ($loginData->obradaCookie()) {
            $request = $this->get('request');
            $dataJson = $request->getContent();
            $data = json_decode($dataJson, true);
        } else {
            $data = null;
        }
        return $data;
    }

    /**
     * @param PortalKorisnik $portalKorisnik
     */
    private function checkForNewPropertiesUnits(PortalKorisnik $portalKorisnik)
    {
        //Check for new properties that not exists in CM and add it to CM
        $loginData = $this->get('td_cm.obrada.cookie_obrada');
        $em = $this->getDoctrine()->getManager();
//        if ($loginData->getPravaOgraniceni()) {
//            $properties = $em->getRepository('tdCMBundle:Property')->findByPravaOgraniceni($loginData->getIdCurrentUserClient());
//        } else {
//            $properties = $em->getRepository('tdCMBundle:Property')->findByNotPravaOgraniceni($loginData->getIdCompanyKorisnik());
//        }

//        $spProperties = array();
//        /* @var SetupProperty $setupProperty */
//        foreach ($setupProperties as $setupProperty) {
//            $spProperties[] = $setupProperty->getProperty();
//        }

        $connection = $this->getDoctrine()->getConnection();
        $connection->beginTransaction();

        $query = "INSERT INTO cm_setup_property
                (idProperty, aktivan, ID_portal_korisnik, idFront, povezan, datumKreiranja)
                (SELECT p.id, 0, :portalKorisnik, concat(p.id_country, '-', lpad(p.id,5,'0')),0, current_timestamp()
                    FROM property AS p
                    WHERE p.deleted = 0 and (p.id_company = :idCompanyKorisnik OR p.id_agent = :idCompanyKorisnik) and p.id not in (select sp.idProperty FROM cm_setup_property as sp WHERE sp.ID_portal_korisnik = :portalKorisnik)
                    );";

        $statement = $connection->prepare($query);
        $statement->bindValue('portalKorisnik', $portalKorisnik->getId());
        $statement->bindValue('idCompanyKorisnik', $loginData->getIdCompanyKorisnik());
        // $params = array(1 => );
        if ($statement->execute()) {
            //If insert is successfull then insert spu
            $query = "INSERT INTO cm_setup_property_unit (idPropertyUnit, aktivan, idSkrbnik, ID_portal_korisnik, idSetupProperty, idPropertyUnitFront, datumKreiranja, povezan)
              (SELECT pu.id, 0,0,:portalKorisnik,(select sp2.id from cm_setup_property as sp2 where sp2.idProperty = p.id and sp2.ID_portal_korisnik = :portalKorisnik limit 1) as idSetupProperty, concat(p.id_country, '-', lpad(p.id,5,'0'),'-', lpad(pu.local_order,2,'0')), current_timestamp(),0
                  FROM property_unit as pu INNER JOIN property as p ON pu.id_property = p.id
                  WHERE pu.deleted = 0 AND p.deleted = 0 AND (p.id_company = :idCompanyKorisnik OR p.id_agent = :idCompanyKorisnik) AND
                    pu.id NOT IN (SELECT spu.idPropertyUnit FROM cm_setup_property_unit as spu WHERE spu.ID_portal_korisnik = :portalKorisnik) AND
                    p.id IN (SELECT sp.idProperty FROM cm_setup_property as sp WHERE sp.ID_portal_korisnik = :portalKorisnik));";
            $statement = $connection->prepare($query);
            $statement->bindValue('portalKorisnik', $portalKorisnik->getId());
            $statement->bindValue('idCompanyKorisnik', $loginData->getIdCompanyKorisnik());
            $statement->execute();

            $connection->commit();
        } else {
            //Javi grešku
            $connection->rollBack();
        }


//        $propertiesNotInCm = $em->getRepository('tdCMBundle:Property')->findAllNotInSetupProperties($portalKorisnik, $loginData->getIdCompanyKorisnik());
//
//        /* @var $property Property */
//        foreach ($propertiesNotInCm as $property) {
//            $setupProperty = new SetupProperty();
//            $setupProperty->setDatumKreiranja(new \DateTime());
//            $setupProperty->setIdFront($property->getPropertyFront());
//            $setupProperty->setPortalKorisnik($portalKorisnik);
//            $setupProperty->setProperty($property);
//            $em->persist($setupProperty);
//
//            //Check for PropertyUnits that not exist in CM and add it to to CM
//            /* @var PropertyUnit $propertyUnit */
//            foreach ($property->getPropertyUnits() as $propertyUnit) {
//                $setupPropertyUnit = new SetupPropertyUnit();
//                $setupPropertyUnit->setDatumKreiranja(new \DateTime());
//                $setupPropertyUnit->setIdPropertyUnitFront($propertyUnit->getPropertyUnitFront());
//                $setupPropertyUnit->setPortalKorisnik($portalKorisnik);
//                $setupPropertyUnit->setPropertyUnit($propertyUnit);
//                $setupPropertyUnit->setSetupProperty($setupProperty);
//                $em->persist($setupPropertyUnit);
//            }
//        }
//        $em->flush();
//
//        //Update SetupPropertiesUnints in CM that already has SetupPropertyUnit
//        $propertyUnitsNotInCm = $em->getRepository('tdCMBundle:PropertyUnit')->findAllNotInSetupProperyUnits($portalKorisnik, $loginData->getIdCompanyKorisnik());
//        /* @var PropertyUnit $propertyUnit */
//        foreach ($propertyUnitsNotInCm as $propertyUnit) {
//            $setupPropertyUnit = new SetupPropertyUnit();
//            $setupPropertyUnit->setDatumKreiranja(new \DateTime());
//            $setupPropertyUnit->setIdPropertyUnitFront($propertyUnit->getPropertyUnitFront());
//            $setupPropertyUnit->setPortalKorisnik($portalKorisnik);
//            $setupPropertyUnit->setPropertyUnit($propertyUnit);
//            $setupProperty = $em->getRepository('tdCMBundle:SetupProperty')->findOneBy(array(
//                'property' => $propertyUnit->getProperty(),
//                'portalKorisnik' => $portalKorisnik,
//            ));
//            $setupPropertyUnit->setSetupProperty($setupProperty);
//            $em->persist($setupPropertyUnit);
//        }
//        $em->flush();
        return;
    }

    /**
     * Get Items for select. Only those that are not yet used can be returned.
     *
     * @param SetupPropertyUnit $setupPropertyUnit
     * @param $podaci
     * @return array
     */
    private function getAvailableItems(SetupPropertyUnit $setupPropertyUnit, $podaci)
    {
        $setupPropertyUnitsBookingComAssigned = $this->getDoctrine()->getManager()->getRepository('tdCMBundle:SetupPropertyUnit')->createQueryBuilder('sp')
            ->select('sp.idPropertyUnitNaPortalu')
            ->where('sp.idPropertyUnitNaPortalu IS NOT NULL')
            ->andWhere('sp.setupProperty = :setupProperty')
            ->setParameter('setupProperty', $setupPropertyUnit->getSetupProperty())
            ->getQuery()
            ->getResult();

        $podaciObrada = array();

        foreach ($podaci as $item) {
            $flag = true;
            foreach ($setupPropertyUnitsBookingComAssigned as $idPropertyUnitOnPortal) {
//                return new JsonResponse(array('itemId' => $item['id'], 'idProprertyUnitOnPortal' => $idPropertyUnitOnPortal['idPropertyUnitNaPortalu']));
//                if ($idPropertyUnitOnPortal['idPropertyUnitNaPortalu'] == $item['id']) {
//                    return new JsonResponse($setupPropertyUnitsBookingComAssigned);
//                }
                if ($item['id'] == $idPropertyUnitOnPortal['idPropertyUnitNaPortalu'] &&
                    $setupPropertyUnit->getIdPropertyUnitNaPortalu() != $idPropertyUnitOnPortal['idPropertyUnitNaPortalu']
                ) {
                    $flag = false;
                }
            }
            if ($flag) $podaciObrada[] = $item;
        }
        return $podaciObrada;
    }

    private function createSetupPropertyUnitsByProperty(Property $property, PortalKorisnik $portalKorisnik)
    {
        $em = $this->getDoctrine()->getManager();

        $setupProperty = new SetupProperty();
        $setupProperty->setDatumKreiranja(new \DateTime('now'));
        $setupProperty->setPortalKorisnik($portalKorisnik);
        $setupProperty->setProperty($property);

        $propertyUnits = $em->getRepository('tdCMBundle:PropertyUnit')->findBy(array());
    }
}
