<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Exclude;

/**
 * SetupPropertyUnit
 *
 * @ORM\Table(name="cm_setup_property_unit")
 * @ORM\Entity(repositoryClass="td\CMBundle\Entity\Repository\SetupPropertyUnitRepository")
 * @ExclusionPolicy("none")
 */
class SetupPropertyUnit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="idWebshop", type="integer", nullable=true)
     */
    private $idWebshop;

    /**
     * @ORM\ManyToOne(targetEntity="Webshop")
     * @ORM\JoinColumn(name="idWebshop", referencedColumnName="id", onDelete="CASCADE")
     * @Exclude
     */
    private $webshop;

    /**
     * @var integer
     *
     * @ORM\Column(name="idPropertyUnit", type="integer")
     */
    private $idPropertyUnit;

    /**
     * @ORM\ManyToOne(targetEntity="PropertyUnit", inversedBy="setupPropertyUnits")
     * @ORM\JoinColumn(name="idPropertyUnit", referencedColumnName="id")
     * @Exclude
     **/
    private $propertyUnit;

    /**
     * @ORM\OneToMany(targetEntity="Transfer", mappedBy="setupPropertyUnit")
     * @Exclude
     */
    private $transferi;

    /**
     * @ORM\OneToMany(targetEntity="UnitRate", mappedBy="setupPropertyUnit")
     * @Exclude
     */
    private $unitRates;

    /**
     * @var string
     *
     * @ORM\Column(name="idPropertyUnitNaPortalu", type="string", length=255, nullable=true)
     */
    private $idPropertyUnitNaPortalu;

    /**
     * @var boolean
     *
     * @ORM\Column(name="aktivan", type="boolean")
     */
    private $aktivan = false;

    /**
     * @ORM\ManyToOne(targetEntity="PortalKorisnik")
     * @ORM\JoinColumn(name="ID_portal_korisnik", referencedColumnName="id", onDelete="CASCADE")
     *
     */
    private $portalKorisnik;

    /**
     * @var integer
     *
     * @ORM\Column(name="idSkrbnik", type="integer", nullable=true)
     */
    private $idSkrbnik;

    /**
     * @ORM\ManyToOne(targetEntity="SetupProperty", inversedBy="setupPropertyUnits")
     * @ORM\JoinColumn(name="idSetupProperty", referencedColumnName="id")
     * @Exclude
     **/
    private $setupProperty;

    /**
     * @var string
     *
     * @ORM\Column(name="idPropertyUnitFront", type="string", length=255, nullable=true)
     */
    private $idPropertyUnitFront;

    /**
     * @var boolean
     *
     * @ORM\Column(name="povezan", type="boolean")
     */
    private $povezan = false;

/**
     * @var boolean
     *
     * @ORM\Column(name="pending", type="boolean")
     */
    private $pending = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datumKreiranja", type="datetime", options={"default":"0000-00-00 00:00:00"})
     */
    private $datumKreiranja;

  
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->transferi = new \Doctrine\Common\Collections\ArrayCollection();
        $this->unitRates = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idWebshop
     *
     * @param integer $idWebshop
     *
     * @return SetupPropertyUnit
     */
    public function setIdWebshop($idWebshop)
    {
        $this->idWebshop = $idWebshop;

        return $this;
    }

    /**
     * Get idWebshop
     *
     * @return integer
     */
    public function getIdWebshop()
    {
        return $this->idWebshop;
    }

    /**
     * Set idPropertyUnit
     *
     * @param integer $idPropertyUnit
     *
     * @return SetupPropertyUnit
     */
    public function setIdPropertyUnit($idPropertyUnit)
    {
        $this->idPropertyUnit = $idPropertyUnit;

        return $this;
    }

    /**
     * Get idPropertyUnit
     *
     * @return integer
     */
    public function getIdPropertyUnit()
    {
        return $this->idPropertyUnit;
    }

    /**
     * Set idPropertyUnitNaPortalu
     *
     * @param string $idPropertyUnitNaPortalu
     *
     * @return SetupPropertyUnit
     */
    public function setIdPropertyUnitNaPortalu($idPropertyUnitNaPortalu)
    {
        $this->idPropertyUnitNaPortalu = $idPropertyUnitNaPortalu;

        return $this;
    }

    /**
     * Get idPropertyUnitNaPortalu
     *
     * @return string
     */
    public function getIdPropertyUnitNaPortalu()
    {
        return $this->idPropertyUnitNaPortalu;
    }

    /**
     * Set aktivan
     *
     * @param boolean $aktivan
     *
     * @return SetupPropertyUnit
     */
    public function setAktivan($aktivan)
    {
        $this->aktivan = $aktivan;

        return $this;
    }

    /**
     * Get aktivan
     *
     * @return boolean
     */
    public function getAktivan()
    {
        return $this->aktivan;
    }

    /**
     * Set idSkrbnik
     *
     * @param integer $idSkrbnik
     *
     * @return SetupPropertyUnit
     */
    public function setIdSkrbnik($idSkrbnik)
    {
        $this->idSkrbnik = $idSkrbnik;

        return $this;
    }

    /**
     * Get idSkrbnik
     *
     * @return integer
     */
    public function getIdSkrbnik()
    {
        return $this->idSkrbnik;
    }

    /**
     * Set idPropertyUnitFront
     *
     * @param string $idPropertyUnitFront
     *
     * @return SetupPropertyUnit
     */
    public function setIdPropertyUnitFront($idPropertyUnitFront)
    {
        $this->idPropertyUnitFront = $idPropertyUnitFront;

        return $this;
    }

    /**
     * Get idPropertyUnitFront
     *
     * @return string
     */
    public function getIdPropertyUnitFront()
    {
        return $this->idPropertyUnitFront;
    }

    /**
     * Set datumKreiranja
     *
     * @param \DateTime $datumKreiranja
     *
     * @return SetupPropertyUnit
     */
    public function setDatumKreiranja($datumKreiranja)
    {
        $this->datumKreiranja = $datumKreiranja;

        return $this;
    }

    /**
     * Get datumKreiranja
     *
     * @return \DateTime
     */
    public function getDatumKreiranja()
    {
        return $this->datumKreiranja;
    }

    /**
     * Set webshop
     *
     * @param \td\CMBundle\Entity\Webshop $webshop
     *
     * @return SetupPropertyUnit
     */
    public function setWebshop(\td\CMBundle\Entity\Webshop $webshop = null)
    {
        $this->webshop = $webshop;

        return $this;
    }

    /**
     * Get webshop
     *
     * @return \td\CMBundle\Entity\Webshop
     */
    public function getWebshop()
    {
        return $this->webshop;
    }

    /**
     * Set propertyUnit
     *
     * @param \td\CMBundle\Entity\PropertyUnit $propertyUnit
     *
     * @return SetupPropertyUnit
     */
    public function setPropertyUnit(\td\CMBundle\Entity\PropertyUnit $propertyUnit = null)
    {
        $this->propertyUnit = $propertyUnit;

        return $this;
    }

    /**
     * Get propertyUnit
     *
     * @return \td\CMBundle\Entity\PropertyUnit
     */
    public function getPropertyUnit()
    {
        return $this->propertyUnit;
    }

    /**
     * Add transferi
     *
     * @param \td\CMBundle\Entity\Transfer $transferi
     *
     * @return SetupPropertyUnit
     */
    public function addTransferi(\td\CMBundle\Entity\Transfer $transferi)
    {
        $this->transferi[] = $transferi;

        return $this;
    }

    /**
     * Remove transferi
     *
     * @param \td\CMBundle\Entity\Transfer $transferi
     */
    public function removeTransferi(\td\CMBundle\Entity\Transfer $transferi)
    {
        $this->transferi->removeElement($transferi);
    }

    /**
     * Get transferi
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransferi()
    {
        return $this->transferi;
    }

    /**
     * Add unitRate
     *
     * @param \td\CMBundle\Entity\UnitRate $unitRate
     *
     * @return SetupPropertyUnit
     */
    public function addUnitRate(\td\CMBundle\Entity\UnitRate $unitRate)
    {
        $this->unitRates[] = $unitRate;

        return $this;
    }

    /**
     * Remove unitRate
     *
     * @param \td\CMBundle\Entity\UnitRate $unitRate
     */
    public function removeUnitRate(\td\CMBundle\Entity\UnitRate $unitRate)
    {
        $this->unitRates->removeElement($unitRate);
    }

    /**
     * Get unitRates
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUnitRates()
    {
        return $this->unitRates;
    }

    /**
     * Set portalKorisnik
     *
     * @param \td\CMBundle\Entity\PortalKorisnik $portalKorisnik
     *
     * @return SetupPropertyUnit
     */
    public function setPortalKorisnik(\td\CMBundle\Entity\PortalKorisnik $portalKorisnik = null)
    {
        $this->portalKorisnik = $portalKorisnik;

        return $this;
    }

    /**
     * Get portalKorisnik
     *
     * @return \td\CMBundle\Entity\PortalKorisnik
     */
    public function getPortalKorisnik()
    {
        return $this->portalKorisnik;
    }

    /**
     * Set setupProperty
     *
     * @param \td\CMBundle\Entity\SetupProperty $setupProperty
     *
     * @return SetupPropertyUnit
     */
    public function setSetupProperty(\td\CMBundle\Entity\SetupProperty $setupProperty = null)
    {
        $this->setupProperty = $setupProperty;

        return $this;
    }

    /**
     * Get setupProperty
     *
     * @return \td\CMBundle\Entity\SetupProperty
     */
    public function getSetupProperty()
    {
        return $this->setupProperty;
    }

    /**
     * Set povezan
     *
     * @param boolean $povezan
     *
     * @return SetupPropertyUnit
     */
    public function setPovezan($povezan)
    {
        $this->povezan = $povezan;

        return $this;
    }

    /**
     * Get povezan
     *
     * @return boolean
     */
    public function getPovezan()
    {
        return $this->povezan;
    }

    /**
     * Set pending
     *
     * @param boolean $pending
     *
     * @return SetupPropertyUnit
     */
    public function setPending($pending)
    {
        $this->pending = $pending;

        return $this;
    }

    /**
     * Get pending
     *
     * @return boolean
     */
    public function getPending()
    {
        return $this->pending;
    }
}
