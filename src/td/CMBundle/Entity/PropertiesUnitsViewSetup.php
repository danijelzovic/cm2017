<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PropertiesUnitsViewSetup
 *
 * @ORM\Table(name="cm_properties_units_view_setup")
 * @ORM\Entity(repositoryClass="td\CMBundle\Entity\Repository\PropertiesUnitsViewSetupRepository")
 */
class PropertiesUnitsViewSetup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255)
     */
    private $username;

    /**
     * @var integer
     *
     * @ORM\Column(name="idP1", type="integer")
     */
    private $idP1;

    /**
     * @var integer
     *
     * @ORM\Column(name="idP2", type="integer")
     */
    private $idP2;

    /**
     * @var integer
     *
     * @ORM\Column(name="idP3", type="integer")
     */
    private $idP3;

    /**
     * @var integer
     *
     * @ORM\Column(name="idP4", type="integer")
     */
    private $idP4;

    /**
     * @var integer
     *
     * @ORM\Column(name="idP5", type="integer")
     */
    private $idP5;

    /**
     * @var integer
     *
     * @ORM\Column(name="idP6", type="integer")
     */
    private $idP6;

    /**
     * @var integer
     *
     * @ORM\Column(name="idP7", type="integer")
     */
    private $idP7;

    /**
     * @var integer
     *
     * @ORM\Column(name="idP8", type="integer")
     */
    private $idP8;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return PropertiesUnitsViewSetup
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set idP1
     *
     * @param integer $idP1
     * @return PropertiesUnitsViewSetup
     */
    public function setIdP1($idP1)
    {
        $this->idP1 = $idP1;

        return $this;
    }

    /**
     * Get idP1
     *
     * @return integer 
     */
    public function getIdP1()
    {
        return $this->idP1;
    }

    /**
     * Set idP2
     *
     * @param integer $idP2
     * @return PropertiesUnitsViewSetup
     */
    public function setIdP2($idP2)
    {
        $this->idP2 = $idP2;

        return $this;
    }

    /**
     * Get idP2
     *
     * @return integer 
     */
    public function getIdP2()
    {
        return $this->idP2;
    }

    /**
     * Set idP3
     *
     * @param integer $idP3
     * @return PropertiesUnitsViewSetup
     */
    public function setIdP3($idP3)
    {
        $this->idP3 = $idP3;

        return $this;
    }

    /**
     * Get idP3
     *
     * @return integer 
     */
    public function getIdP3()
    {
        return $this->idP3;
    }

    /**
     * Set idP4
     *
     * @param integer $idP4
     * @return PropertiesUnitsViewSetup
     */
    public function setIdP4($idP4)
    {
        $this->idP4 = $idP4;

        return $this;
    }

    /**
     * Get idP4
     *
     * @return integer 
     */
    public function getIdP4()
    {
        return $this->idP4;
    }

    /**
     * Set idP5
     *
     * @param integer $idP5
     * @return PropertiesUnitsViewSetup
     */
    public function setIdP5($idP5)
    {
        $this->idP5 = $idP5;

        return $this;
    }

    /**
     * Get idP5
     *
     * @return integer 
     */
    public function getIdP5()
    {
        return $this->idP5;
    }

    /**
     * Set idP6
     *
     * @param integer $idP6
     * @return PropertiesUnitsViewSetup
     */
    public function setIdP6($idP6)
    {
        $this->idP6 = $idP6;

        return $this;
    }

    /**
     * Get idP6
     *
     * @return integer 
     */
    public function getIdP6()
    {
        return $this->idP6;
    }

    /**
     * Set idP7
     *
     * @param integer $idP7
     * @return PropertiesUnitsViewSetup
     */
    public function setIdP7($idP7)
    {
        $this->idP7 = $idP7;

        return $this;
    }

    /**
     * Get idP7
     *
     * @return integer 
     */
    public function getIdP7()
    {
        return $this->idP7;
    }

    /**
     * Set idP8
     *
     * @param integer $idP8
     * @return PropertiesUnitsViewSetup
     */
    public function setIdP8($idP8)
    {
        $this->idP8 = $idP8;

        return $this;
    }

    /**
     * Get idP8
     *
     * @return integer 
     */
    public function getIdP8()
    {
        return $this->idP8;
    }
}
