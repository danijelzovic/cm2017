<?php

namespace td\CMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use td\CMBundle\Entity\SetupPropertyUnit;

class SetupPropertyUnitJsonController extends Controller
{
    /**
     *  Delete SetupPropertyUnit JSON mode
     *
     * @param SetupPropertyUnit $setupPropertyUnit
     * @return Response
     */
    public function deleteAction(SetupPropertyUnit $setupPropertyUnit)
    {
        if (!is_null($setupPropertyUnit)) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($setupPropertyUnit);
            $em->flush();
            return new JsonResponse(array('status' => 'ok', 'message'=>'SetupPropertyUnit je uspješno obrisan!'));
        } else {
            return new JsonResponse(array('status'=> 'Error!', 'message'=> 'SetupPropertyUnit ne postoji!'),404);
        }
    }
}
