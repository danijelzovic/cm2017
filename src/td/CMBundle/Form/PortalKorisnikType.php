<?php

namespace td\CMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PortalKorisnikType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('idCompanyKorisnik')
            ->add('urlPortala')
            ->add('urlController')
            ->add('login')
            ->add('password')
            ->add('raspolozivostBrojPokusaja')
            ->add('cjenikBrojPokusaja')
            ->add('podaciBrojPokusaja')
            ->add('opisBrojPokusaja')
            ->add('fotoBrojPokusaja')
            ->add('idKorisnikaNaPortalu')
            ->add('idWebshopRates')
            ->add('idWebshopFront')
            ->add('urlFront')
            ->add('xmlLocations')
            ->add('poredak')
            ->add('povezan')
            ->add('pending')
            ->add('cjenikKupovni')
            ->add('portal')
            ->add('webshop')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'td\CMBundle\Entity\PortalKorisnik'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'td_cmbundle_portalkorisnik';
    }
}
