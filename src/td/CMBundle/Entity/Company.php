<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Company
 *
 * @ORM\Table(name="company")
 * @ORM\Entity(repositoryClass="td\CMBundle\Entity\Repository\CompanyRepository")
 */
class Company
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_crm_klijent", type="integer", nullable=true)
     */
    private $idCrmKlijent;

    /**
     * @var int
     *
     * @ORM\Column(name="id_contact_owner", type="integer")
     */
    private $idContactOwner;

    /**
     * @var int
     *
     * @ORM\Column(name="id_was", type="integer")
     */
    private $idWas;

    /**
     * @var bool
     *
     * @ORM\Column(name="agent", type="boolean")
     */
    private $agent;

    /**
     * @var string
     *
     * @ORM\Column(name="company_type", type="string", length=255)
     */
    private $companyType;

    /**
     * @var string
     *
     * @ORM\Column(name="mb", type="string", length=32)
     */
    private $mb;

    /**
     * @var string
     *
     * @ORM\Column(name="oib", type="string", length=32)
     */
    private $oib;

    /**
     * @var string
     *
     * @ORM\Column(name="agency_code", type="string", length=255)
     */
    private $agencyCode;

    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=255)
     */
    private $company;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="id_country", type="string", length=2)
     */
    private $idCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="id_language", type="string", length=2)
     */
    private $idLanguage;

    /**
     * @var string
     *
     * @ORM\Column(name="id_status", type="string", length=10)
     */
    private $idStatus;

    /**
     * @var bool
     *
     * @ORM\Column(name="atd_sustav_partneri", type="boolean")
     */
    private $atdSustavPartneri;

    /**
     * @var bool
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted;

    /**
     * @var string
     *
     * @ORM\Column(name="company_short", type="string", length=255)
     */
    private $companyShort;

    /**
     * @var string
     *
     * @ORM\Column(name="broj_ugovora", type="string", length=32)
     */
    private $brojUgovora;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastchange", type="datetimetz")
     */
    private $lastchange;

    /**
     * @var int
     *
     * @ORM\Column(name="id_client", type="integer")
     */
    private $idClient;

    /**
     * @var bool
     *
     * @ORM\Column(name="korisniksustava", type="boolean")
     */
    private $korisniksustava;

    /**
     * @var bool
     *
     * @ORM\Column(name="import", type="boolean")
     */
    private $import;


    /**
     * @ORM\OneToMany(targetEntity="PortalKorisnik", mappedBy="company")
     */
    private $portalKorisnici;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCrmKlijent
     *
     * @param integer $idCrmKlijent
     *
     * @return Company
     */
    public function setIdCrmKlijent($idCrmKlijent)
    {
        $this->idCrmKlijent = $idCrmKlijent;

        return $this;
    }

    /**
     * Get idCrmKlijent
     *
     * @return int
     */
    public function getIdCrmKlijent()
    {
        return $this->idCrmKlijent;
    }

    /**
     * Set idContactOwner
     *
     * @param integer $idContactOwner
     *
     * @return Company
     */
    public function setIdContactOwner($idContactOwner)
    {
        $this->idContactOwner = $idContactOwner;

        return $this;
    }

    /**
     * Get idContactOwner
     *
     * @return int
     */
    public function getIdContactOwner()
    {
        return $this->idContactOwner;
    }

    /**
     * Set idWas
     *
     * @param integer $idWas
     *
     * @return Company
     */
    public function setIdWas($idWas)
    {
        $this->idWas = $idWas;

        return $this;
    }

    /**
     * Get idWas
     *
     * @return int
     */
    public function getIdWas()
    {
        return $this->idWas;
    }

    /**
     * Set agent
     *
     * @param boolean $agent
     *
     * @return Company
     */
    public function setAgent($agent)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return bool
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set companyType
     *
     * @param string $companyType
     *
     * @return Company
     */
    public function setCompanyType($companyType)
    {
        $this->companyType = $companyType;

        return $this;
    }

    /**
     * Get companyType
     *
     * @return string
     */
    public function getCompanyType()
    {
        return $this->companyType;
    }

    /**
     * Set mb
     *
     * @param string $mb
     *
     * @return Company
     */
    public function setMb($mb)
    {
        $this->mb = $mb;

        return $this;
    }

    /**
     * Get mb
     *
     * @return string
     */
    public function getMb()
    {
        return $this->mb;
    }

    /**
     * Set oib
     *
     * @param string $oib
     *
     * @return Company
     */
    public function setOib($oib)
    {
        $this->oib = $oib;

        return $this;
    }

    /**
     * Get oib
     *
     * @return string
     */
    public function getOib()
    {
        return $this->oib;
    }

    /**
     * Set agencyCode
     *
     * @param string $agencyCode
     *
     * @return Company
     */
    public function setAgencyCode($agencyCode)
    {
        $this->agencyCode = $agencyCode;

        return $this;
    }

    /**
     * Get agencyCode
     *
     * @return string
     */
    public function getAgencyCode()
    {
        return $this->agencyCode;
    }

    /**
     * Set company
     *
     * @param string $company
     *
     * @return Company
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Company
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set idCountry
     *
     * @param string $idCountry
     *
     * @return Company
     */
    public function setIdCountry($idCountry)
    {
        $this->idCountry = $idCountry;

        return $this;
    }

    /**
     * Get idCountry
     *
     * @return string
     */
    public function getIdCountry()
    {
        return $this->idCountry;
    }

    /**
     * Set idLanguage
     *
     * @param string $idLanguage
     *
     * @return Company
     */
    public function setIdLanguage($idLanguage)
    {
        $this->idLanguage = $idLanguage;

        return $this;
    }

    /**
     * Get idLanguage
     *
     * @return string
     */
    public function getIdLanguage()
    {
        return $this->idLanguage;
    }

    /**
     * Set idStatus
     *
     * @param string $idStatus
     *
     * @return Company
     */
    public function setIdStatus($idStatus)
    {
        $this->idStatus = $idStatus;

        return $this;
    }

    /**
     * Get idStatus
     *
     * @return string
     */
    public function getIdStatus()
    {
        return $this->idStatus;
    }

    /**
     * Set atdSustavPartneri
     *
     * @param boolean $atdSustavPartneri
     *
     * @return Company
     */
    public function setAtdSustavPartneri($atdSustavPartneri)
    {
        $this->atdSustavPartneri = $atdSustavPartneri;

        return $this;
    }

    /**
     * Get atdSustavPartneri
     *
     * @return bool
     */
    public function getAtdSustavPartneri()
    {
        return $this->atdSustavPartneri;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Company
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return bool
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set companyShort
     *
     * @param string $companyShort
     *
     * @return Company
     */
    public function setCompanyShort($companyShort)
    {
        $this->companyShort = $companyShort;

        return $this;
    }

    /**
     * Get companyShort
     *
     * @return string
     */
    public function getCompanyShort()
    {
        return $this->companyShort;
    }

    /**
     * Set brojUgovora
     *
     * @param string $brojUgovora
     *
     * @return Company
     */
    public function setBrojUgovora($brojUgovora)
    {
        $this->brojUgovora = $brojUgovora;

        return $this;
    }

    /**
     * Get brojUgovora
     *
     * @return string
     */
    public function getBrojUgovora()
    {
        return $this->brojUgovora;
    }

    /**
     * Set lastchange
     *
     * @param \DateTime $lastchange
     *
     * @return Company
     */
    public function setLastchange($lastchange)
    {
        $this->lastchange = $lastchange;

        return $this;
    }

    /**
     * Get lastchange
     *
     * @return \DateTime
     */
    public function getLastchange()
    {
        return $this->lastchange;
    }

    /**
     * Set idClient
     *
     * @param integer $idClient
     *
     * @return Company
     */
    public function setIdClient($idClient)
    {
        $this->idClient = $idClient;

        return $this;
    }

    /**
     * Get idClient
     *
     * @return int
     */
    public function getIdClient()
    {
        return $this->idClient;
    }

    /**
     * Set korisniksustava
     *
     * @param boolean $korisniksustava
     *
     * @return Company
     */
    public function setKorisniksustava($korisniksustava)
    {
        $this->korisniksustava = $korisniksustava;

        return $this;
    }

    /**
     * Get korisniksustava
     *
     * @return bool
     */
    public function getKorisniksustava()
    {
        return $this->korisniksustava;
    }

    /**
     * Set import
     *
     * @param boolean $import
     *
     * @return Company
     */
    public function setImport($import)
    {
        $this->import = $import;

        return $this;
    }

    /**
     * Get import
     *
     * @return bool
     */
    public function getImport()
    {
        return $this->import;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->portalKorisnici = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add portalKorisnici
     *
     * @param \td\CMBundle\Entity\PortalKorisnik $portalKorisnici
     * @return Company
     */
    public function addPortalKorisnici(\td\CMBundle\Entity\PortalKorisnik $portalKorisnici)
    {
        $this->portalKorisnici[] = $portalKorisnici;

        return $this;
    }

    /**
     * Remove portalKorisnici
     *
     * @param \td\CMBundle\Entity\PortalKorisnik $portalKorisnici
     */
    public function removePortalKorisnici(\td\CMBundle\Entity\PortalKorisnik $portalKorisnici)
    {
        $this->portalKorisnici->removeElement($portalKorisnici);
    }

    /**
     * Get portalKorisnici
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPortalKorisnici()
    {
        return $this->portalKorisnici;
    }
}
