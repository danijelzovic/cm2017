<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VSetupPropertyGrid
 *
 * @ORM\Table(name="v_cm_setup_property_grid")
 * @ORM\Entity
 */
class
VSetupPropertyGrid
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="SetupProperty")
     * @ORM\JoinColumn(name="id", referencedColumnName="id")
     **/
    private $setupProperty;

    /**
     * @var integer
     *
     * @ORM\Column(name="idPortalKorisnik", type="integer")
     */
    private $idPortalKorisnik;

    /**
     * @var string
     *
     * @ORM\Column(name="idPropertyFront", type="string", length=255)
     */
    private $idPropertyFront;

    /**
     * @var string
     *
     * @ORM\Column(name="idPropertyNaPortalu", type="string", length=255)
     */
    private $idPropertyNaPortalu;

    /**
     * @var string
     *
     * @ORM\Column(name="nazivProperty", type="string", length=255)
     */
    private $nazivProperty;

    /**
     * @var string
     *
     * @ORM\Column(name="mjesto", type="string", length=255)
     */
    private $mjesto;

    /**
     * @var string
     *
     * @ORM\Column(name="skrbnik", type="string", length=255)
     */
    private $skrbnik;

    /**
     * @var string
     *
     * @ORM\Column(name="vlasnik", type="string", length=255)
     */
    private $vlasnik;

    /**
     * @var boolean
     *
     * @ORM\Column(name="aktivan", type="boolean")
     */
    private $aktivan;

    /**
     * @var integer
     *
     * @ORM\Column(name="brojUnita", type="integer")
     */
    private $brojUnita;

/**
     * @var integer
     *
     * @ORM\Column(name="brojNeprijavljenih", type="integer")
     */
    private $brojNeprijavljenih;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datumKreiranja", type="datetime")
     */
    private $datumKreiranja;

    /**
     * @var integer
     *
     * @ORM\Column(name="brojNeuspjelihTransfera", type="integer")
     */
    private $brojNeuspjelihTransfera;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPortalKorisnik
     *
     * @param integer $idPortalKorisnik
     * @return VSetupPropertyGrid
     */
    public function setIdPortalKorisnik($idPortalKorisnik)
    {
        $this->idPortalKorisnik = $idPortalKorisnik;

        return $this;
    }

    /**
     * Get idPortalKorisnik
     *
     * @return integer 
     */
    public function getIdPortalKorisnik()
    {
        return $this->idPortalKorisnik;
    }

    /**
     * Set idPropertyFront
     *
     * @param string $idPropertyFront
     * @return VSetupPropertyGrid
     */
    public function setIdPropertyFront($idPropertyFront)
    {
        $this->idPropertyFront = $idPropertyFront;

        return $this;
    }

    /**
     * Get idPropertyFront
     *
     * @return string 
     */
    public function getIdPropertyFront()
    {
        return $this->idPropertyFront;
    }

    /**
     * Set idPropertyNaPortalu
     *
     * @param string $idPropertyNaPortalu
     * @return VSetupPropertyGrid
     */
    public function setIdPropertyNaPortalu($idPropertyNaPortalu)
    {
        $this->idPropertyNaPortalu = $idPropertyNaPortalu;

        return $this;
    }

    /**
     * Get idPropertyNaPortalu
     *
     * @return string 
     */
    public function getIdPropertyNaPortalu()
    {
        return $this->idPropertyNaPortalu;
    }

    /**
     * Set nazivProperty
     *
     * @param string $nazivProperty
     * @return VSetupPropertyGrid
     */
    public function setNazivProperty($nazivProperty)
    {
        $this->nazivProperty = $nazivProperty;

        return $this;
    }

    /**
     * Get nazivProperty
     *
     * @return string 
     */
    public function getNazivProperty()
    {
        return $this->nazivProperty;
    }

    /**
     * Set mjesto
     *
     * @param string $mjesto
     * @return VSetupPropertyGrid
     */
    public function setMjesto($mjesto)
    {
        $this->mjesto = $mjesto;

        return $this;
    }

    /**
     * Get mjesto
     *
     * @return string 
     */
    public function getMjesto()
    {
        return $this->mjesto;
    }

    /**
     * Set skrbnik
     *
     * @param string $skrbnik
     * @return VSetupPropertyGrid
     */
    public function setSkrbnik($skrbnik)
    {
        $this->skrbnik = $skrbnik;

        return $this;
    }

    /**
     * Get skrbnik
     *
     * @return string 
     */
    public function getSkrbnik()
    {
        return $this->skrbnik;
    }

    /**
     * Set vlasnik
     *
     * @param string $vlasnik
     * @return VSetupPropertyGrid
     */
    public function setVlasnik($vlasnik)
    {
        $this->vlasnik = $vlasnik;

        return $this;
    }

    /**
     * Get vlasnik
     *
     * @return string 
     */
    public function getVlasnik()
    {
        return $this->vlasnik;
    }

    /**
     * Set aktivan
     *
     * @param boolean $aktivan
     * @return VSetupPropertyGrid
     */
    public function setAktivan($aktivan)
    {
        $this->aktivan = $aktivan;

        return $this;
    }

    /**
     * Get aktivan
     *
     * @return boolean 
     */
    public function getAktivan()
    {
        return $this->aktivan;
    }

    /**
     * Set brojUnita
     *
     * @param integer $brojUnita
     * @return VSetupPropertyGrid
     */
    public function setBrojUnita($brojUnita)
    {
        $this->brojUnita = $brojUnita;

        return $this;
    }

    /**
     * Get brojUnita
     *
     * @return integer 
     */
    public function getBrojUnita()
    {
        return $this->brojUnita;
    }

    /**
     * Set datumKreiranja
     *
     * @param \DateTime $datumKreiranja
     * @return VSetupPropertyGrid
     */
    public function setDatumKreiranja($datumKreiranja)
    {
        $this->datumKreiranja = $datumKreiranja;

        return $this;
    }

    /**
     * Get datumKreiranja
     *
     * @return \DateTime 
     */
    public function getDatumKreiranja()
    {
        return $this->datumKreiranja;
    }

    /**
     * Set brojNeprijavljenih
     *
     * @param integer $brojNeprijavljenih
     * @return VSetupPropertyGrid
     */
    public function setBrojNeprijavljenih($brojNeprijavljenih)
    {
        $this->brojNeprijavljenih = $brojNeprijavljenih;

        return $this;
    }

    /**
     * Get brojNeprijavljenih
     *
     * @return integer 
     */
    public function getBrojNeprijavljenih()
    {
        return $this->brojNeprijavljenih;
    }

    /**
     * Set setupProperties
     *
     * @param \td\CMBundle\Entity\SetupProperty $setupProperty
     *
     * @return VSetupPropertyGrid
     */
    public function setSetupProperty(\td\CMBundle\Entity\SetupProperty $setupProperty = null)
    {
        $this->setupProperty = $setupProperty;

        return $this;
    }

    /**
     * Get setupProperties
     *
     * @return \td\CMBundle\Entity\SetupProperty
     */
    public function getSetupProperty()
    {
        return $this->setupProperty;
    }

    /**
     * Set brojNeuspjelihTransfera
     *
     * @param integer $brojNeuspjelihTransfera
     *
     * @return VSetupPropertyGrid
     */
    public function setBrojNeuspjelihTransfera($brojNeuspjelihTransfera)
    {
        $this->brojNeuspjelihTransfera = $brojNeuspjelihTransfera;

        return $this;
    }

    /**
     * Get brojNeuspjelihTransfera
     *
     * @return integer
     */
    public function getBrojNeuspjelihTransfera()
    {
        return $this->brojNeuspjelihTransfera;
    }
}
