<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HelpText
 *
 * @ORM\Table(name="help_text")
 * @ORM\Entity(repositoryClass="td\CMBundle\Repository\HelpTextRepository")
 */
class HelpText
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="skripta", type="string", length=255)
     */
    private $skripta;

    /**
     * @var string
     *
     * @ORM\Column(name="id_skripte", type="string", length=32)
     */
    private $idSkripte;

    /**
     * @var string
     *
     * @ORM\Column(name="platforma", type="string", length=32)
     */
    private $platforma;

    /**
     * @var string
     *
     * @ORM\Column(name="kod", type="string", length=255)
     */
    private $kod;

    /**
     * @var int
     *
     * @ORM\Column(name="sirina", type="integer")
     */
    private $sirina;

    /**
     * @var int
     *
     * @ORM\Column(name="visina", type="integer")
     */
    private $visina;

    /**
     * @var string
     *
     * @ORM\Column(name="data_placement", type="string", length=32)
     */
    private $dataPlacement;

    /**
     * @var string
     *
     * @ORM\Column(name="tip", type="string", length=32)
     */
    private $tip;

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=255)
     */
    private $icon;

    /**
     * @var string
     *
     * @ORM\Column(name="media", type="string", length=255)
     */
    private $media;

    /**
     * @var int
     *
     * @ORM\Column(name="redoslijed", type="integer")
     */
    private $redoslijed;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set skripta
     *
     * @param string $skripta
     *
     * @return HelpText
     */
    public function setSkripta($skripta)
    {
        $this->skripta = $skripta;

        return $this;
    }

    /**
     * Get skripta
     *
     * @return string
     */
    public function getSkripta()
    {
        return $this->skripta;
    }

    /**
     * Set idSkripte
     *
     * @param string $idSkripte
     *
     * @return HelpText
     */
    public function setIdSkripte($idSkripte)
    {
        $this->idSkripte = $idSkripte;

        return $this;
    }

    /**
     * Get idSkripte
     *
     * @return string
     */
    public function getIdSkripte()
    {
        return $this->idSkripte;
    }

    /**
     * Set platforma
     *
     * @param string $platforma
     *
     * @return HelpText
     */
    public function setPlatforma($platforma)
    {
        $this->platforma = $platforma;

        return $this;
    }

    /**
     * Get platforma
     *
     * @return string
     */
    public function getPlatforma()
    {
        return $this->platforma;
    }

    /**
     * Set kod
     *
     * @param string $kod
     *
     * @return HelpText
     */
    public function setKod($kod)
    {
        $this->kod = $kod;

        return $this;
    }

    /**
     * Get kod
     *
     * @return string
     */
    public function getKod()
    {
        return $this->kod;
    }

    /**
     * Set sirina
     *
     * @param integer $sirina
     *
     * @return HelpText
     */
    public function setSirina($sirina)
    {
        $this->sirina = $sirina;

        return $this;
    }

    /**
     * Get sirina
     *
     * @return int
     */
    public function getSirina()
    {
        return $this->sirina;
    }

    /**
     * Set visina
     *
     * @param integer $visina
     *
     * @return HelpText
     */
    public function setVisina($visina)
    {
        $this->visina = $visina;

        return $this;
    }

    /**
     * Get visina
     *
     * @return int
     */
    public function getVisina()
    {
        return $this->visina;
    }

    /**
     * Set dataPlacement
     *
     * @param string $dataPlacement
     *
     * @return HelpText
     */
    public function setDataPlacement($dataPlacement)
    {
        $this->dataPlacement = $dataPlacement;

        return $this;
    }

    /**
     * Get dataPlacement
     *
     * @return string
     */
    public function getDataPlacement()
    {
        return $this->dataPlacement;
    }

    /**
     * Set tip
     *
     * @param string $tip
     *
     * @return HelpText
     */
    public function setTip($tip)
    {
        $this->tip = $tip;

        return $this;
    }

    /**
     * Get tip
     *
     * @return string
     */
    public function getTip()
    {
        return $this->tip;
    }

    /**
     * Set icon
     *
     * @param string $icon
     *
     * @return HelpText
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set media
     *
     * @param string $media
     *
     * @return HelpText
     */
    public function setMedia($media)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return string
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set redoslijed
     *
     * @param integer $redoslijed
     *
     * @return HelpText
     */
    public function setRedoslijed($redoslijed)
    {
        $this->redoslijed = $redoslijed;

        return $this;
    }

    /**
     * Get redoslijed
     *
     * @return int
     */
    public function getRedoslijed()
    {
        return $this->redoslijed;
    }
}
