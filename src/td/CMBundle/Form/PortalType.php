<?php

namespace td\CMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PortalType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('naziv')
            ->add('link', 'url')
            ->add('code', 'entity', array(
                'class' => 'td\CMBundle\Entity\PortalCode',
                'property' => 'id',
            ))
            ->add('portalGroup', 'entity', array(
                'class' => 'td\CMBundle\Entity\PortalGroup',
                'property' => 'name',
            ))
            ->add('contactPerson', 'text', array('required' => false))
            ->add('contactEmail', 'email', array('required' => false))
            ->add('contactTel', 'text', array('required' => false))
            ->add('description', 'textarea')
            ->add('file', 'file', array('required' => false))
            ->add('viewTypeSetup', 'text', array('required' => false))
            ->add('viewTypeConnect', 'text', array('required' => false))
            ->add('linkPropertyOnPortal', 'url', array('required' => false))
            ->add('linkPropertyUnitOnPortal', 'url', array('required' => false))
            ->add('codeName', 'text', array('required' => false))
            ->add('connectProperty', 'checkbox', array('required' => false))
            ->add('connectPropertyUnit', 'checkbox', array('required' => false))
            ->add('manageActivityProperty', 'checkbox', array('required' => false))
            ->add('manageActivityPropertyUnit', 'checkbox', array('required' => false))
            ->add('mustHaveWebshop', 'checkbox', array('required' => false))
//            ->add('processChange')
//            ->add('settingsTemplate')
//            ->add('code')
//            ->add('portalGroup')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'td\CMBundle\Entity\Portal'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'td_cmbundle_portal';
    }
}
