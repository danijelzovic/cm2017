<?php

namespace td\CMBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Response;
use td\CMBundle\Entity\SetupPropertyUnit;
use td\CMBundle\Entity\UnitRate;

/**
 * UnitRate controller.
 *
 */
class UnitRateController extends Controller
{
    /**
     * Lists all UnitRate entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $unitRates = $em->getRepository('tdCMBundle:UnitRate')->findAll();

        return $this->render('unitrate/index.html.twig', array(
            'unitRates' => $unitRates,
        ));
    }

    /**
     * Finds and displays a UnitRate entity.
     *
     */
    public function showAction(UnitRate $unitRate)
    {

        return $this->render('unitrate/show.html.twig', array(
            'unitRate' => $unitRate,
        ));
    }

    public function editRatesSnippetAction(SetupPropertyUnit $setupPropertyUnit)
    {
        // Sigurnost

        // Initialization
        $em = $this->getDoctrine()->getManager();

        // Get all rates for given SPU
        $podaciBookingCom = $this->get('td_cm.obrada.setup_obrada')->getBookingComRoomRates($setupPropertyUnit->getSetupProperty()->getIdPropertyNaPortalu());
        $rates = $this->get('td_cm.obrada.setup_obrada')->updateLocalDataBookingCom($setupPropertyUnit, $podaciBookingCom);

//        $unitRates = $em->getRepository('tdCMBundle:UnitRate')->findBySetupPropertyUnit($setupPropertyUnit);
        if (count($rates)) {
            $idHead = $this->get('td_cm.obrada.setup_obrada')->odrediCjenik($setupPropertyUnit);
        } else {
            $idHead = -6;
        }

//        dump($rates);
//        return new Response($idHead);
        return $this->render('@tdCM/UnitRate/Snippets/editRates.html.twig',array(
            'rates' => $rates,
            'idHead' => $idHead,
            'setupPropertyUnit' => $setupPropertyUnit,
            'portalCode' => $setupPropertyUnit->getPortalKorisnik()->getPortal()->getCodeName(),
        ));
    }
}
