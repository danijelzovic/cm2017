<?php

namespace td\CMBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use td\CMBundle\Entity\UnitRateStavke;
use td\CMBundle\Form\UnitRateStavkeType;

/**
 * UnitRateStavke controller.
 *
 */
class UnitRateStavkeController extends Controller
{
    /**
     * Lists all UnitRateStavke entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $unitRateStavkes = $em->getRepository('tdCMBundle:UnitRateStavke')->findAll();

        return $this->render('UnitRateStavke/index.html.twig', array(
            'unitRateStavkes' => $unitRateStavkes,
        ));
    }

    /**
     * Creates a new UnitRateStavke entity.
     *
     */
    public function newAction(Request $request)
    {
        $unitRateStavke = new UnitRateStavke();
        $form = $this->createForm(new UnitRateStavkeType(), $unitRateStavke);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($unitRateStavke);
            $em->flush();

            return $this->redirectToRoute('unitrate_show', array('id' => $unitratestavke->getId()));
        }

        return $this->render('UnitRateStavke/new.html.twig', array(
            'unitRateStavke' => $unitRateStavke,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a UnitRateStavke entity.
     *
     */
    public function showAction(UnitRateStavke $unitRateStavke)
    {
        $deleteForm = $this->createDeleteForm($unitRateStavke);

        return $this->render('UnitRateStavke/show.html.twig', array(
            'unitRateStavke' => $unitRateStavke,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing UnitRateStavke entity.
     *
     */
    public function editAction(Request $request, UnitRateStavke $unitRateStavke)
    {
        $deleteForm = $this->createDeleteForm($unitRateStavke);
        $editForm = $this->createForm(new UnitRateStavkeType(), $unitRateStavke);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($unitRateStavke);
            $em->flush();

            return $this->redirectToRoute('unitrate_edit', array('id' => $unitRateStavke->getId()));
        }

        return $this->render('UnitRateStavke/edit.html.twig', array(
            'unitRateStavke' => $unitRateStavke,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a UnitRateStavke entity.
     *
     */
    public function deleteAction(Request $request, UnitRateStavke $unitRateStavke)
    {
        $form = $this->createDeleteForm($unitRateStavke);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($unitRateStavke);
            $em->flush();
        }

        return $this->redirectToRoute('unitrate_index');
    }

    /**
     * Creates a form to delete a UnitRateStavke entity.
     *
     * @param UnitRateStavke $unitRateStavke The UnitRateStavke entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(UnitRateStavke $unitRateStavke)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('unitrate_delete', array('id' => $unitRateStavke->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
