<?php

namespace td\CMBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use td\CMBundle\Entity\PortalKorisnik;

/**
 * PropertyRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PropertyRepository extends EntityRepository
{
    public function findPropertiesAndSetupPropertiesByPravaOgraniceni(PortalKorisnik $portalKorisnik, $idCurrentUserClient)
    {
        return $this->createQueryBuilder('p')
            //Property
            ->select('p.id as idProperty')
            ->addSelect('CONCAT(p.idCountry, \'-\', LPAD(p.id,5,\'0\')) as idPropertyFront')
            ->addSelect('p.name as nameP')

            //SetupProperty
            ->addSelect('sp.id as idSp')
            ->addSelect('sp.idPropertyNaPortalu')
            ->addSelect('sp.aktivan as aktivanSp')
            ->addSelect('sp.povezan as povezanSp')

            //Joins
            ->leftJoin('p.setupProperties', 'sp', 'WITH', 'sp.portalKorisnik = :portalKorisnik')

            //Where
            ->where('p.deleted = :deleted')
            ->andWhere('p.id IN (SELECT cpp.idObject FROM tdCMBundle:ClientPravoPristupa cpp WHERE cpp.tablica = :tablica AND cpp.idClient = :idClient)')

            //Parameters
            ->setParameter('deleted', 0)
            ->setParameter('tablica', 'property')
            ->setParameter('idClient', $idCurrentUserClient)
            ->setParameter('portalKorisnik', $portalKorisnik)
            ->setMaxResults(30)
            ->getQuery()
            ->getResult();
    }

    public function findPropertiesAndSetupPropertiesByNotPravaOgraniceni(PortalKorisnik $portalKorisnik, $idCompanyKorisnik)
    {
        return $this->createQueryBuilder('p')
            //Property
            ->select('p.id as idProperty')
            ->addSelect('CONCAT(p.idCountry, \'-\', LPAD(p.id,5,\'0\')) as idPropertyFront')
            ->addSelect('p.name as nameP')

            //SetupProperty
            ->addSelect('sp.id as idSp')
            ->addSelect('sp.idPropertyNaPortalu')
            ->addSelect('sp.aktivan as aktivanSp')
            ->addSelect('sp.povezan as povezanSp')

            //Joins
            ->leftJoin('p.setupProperties', 'sp', 'WITH', 'sp.portalKorisnik = :portalKorisnik')

            //Where
            ->where('p.deleted = :deleted')
            ->andWhere('(p.idCompany = :idCompany OR p.idAgent = :idCompany)')

            //Parameters
            ->setParameter('deleted', 0)
            ->setParameter('idCompany', $idCompanyKorisnik)
            ->setParameter('portalKorisnik', $portalKorisnik)
            ->setMaxResults(100)
            ->getQuery()
            ->getResult();
    }


    public function findPropertiesInCmByPortalKorisnik(PortalKorisnik $portalKorisnik)
    {
        return $this->createQueryBuilder('p')
            ->select('partial p.{id,idCountry,name}')
            ->innerJoin('p.setupProperties', 'sp')
            ->where('sp.portalKorisnik = :portalKorisnik')
            ->andWhere('p.deleted = :deleted')
            ->setParameter('portalKorisnik', $portalKorisnik)
            ->setParameter('deleted', 0)
            ->getQuery()
            ->getResult();
    }

    public function findAllNotInSetupProperties(PortalKorisnik $portalKorisnik, $idCompanyKorisnik)
    {
        return $this->createQueryBuilder('p')
            ->select('partial p.{id, idCountry, name}')
            ->where('p.deleted = :deleted')
            ->andWhere('(p.idCompany = :idCompanyKorisnik OR p.idAgent = :idCompanyKorisnik)')
            ->andWhere('p.id NOT IN (SELECT sp.idProperty FROM tdCMBundle:SetupProperty sp WHERE sp.portalKorisnik = :portalKorisnik)')
            ->setParameter('deleted', 0)
            ->setParameter('portalKorisnik', $portalKorisnik)
            ->setParameter('idCompanyKorisnik', $idCompanyKorisnik)
            ->setMaxResults(100)
            ->getQuery()
            ->getResult();
    }

    public function findAllNotDeletedForFront($limit, $offset, $frontFrom, $frontTo, $propertyName, $mjesto, $skrbnik, $vlasnik)
    {
        return $this->createQueryBuilder('p')
            ->select('p, skrbnik, vlasnik')
//            ->addSelect('CONCAT(p.idCountry, \'-\', LPAD(p.id,5,\'0\')) as idPropertyFront')
//            ->addSelect('p.name AS propertyName')
//            ->addSelect('p.city AS mjesto')
//            ->addSelect('p.idCountry AS idCountry')
//            ->addSelect('skrbnik.naziv AS srbnikNaziv')
//            ->addSelect('vlasnik.naziv AS vlasnikNaziv')
            ->leftJoin('p.agentC', 'skrbnik')
            ->leftJoin('p.ownerC', 'vlasnik')
            ->where('skrbnik.naziv LIKE :skrbnik')
            ->andWhere('p.id >= :frontFrom')
            ->andWhere('p.id <= :frontTo')
            ->andWhere('vlasnik.naziv LIKE :vlasnik')
            ->andWhere('p.city LIKE :mjesto')
            ->andWhere('p.name LIKE :propertyName')
//            ->andHaving('idPropertyFront LIKE :front')
            ->andWhere('p.deleted = :deleted')
            ->andWhere('p.active = :active')
            ->setParameter('propertyName', '%' . $propertyName . '%')
//            ->setParameter('front', '%' . $front . '%')
            ->setParameter('skrbnik', '%' . $skrbnik . '%')
            ->setParameter('frontFrom', $frontFrom)
            ->setParameter('frontTo', $frontTo)
            ->setParameter('vlasnik', '%' . $vlasnik . '%')
            ->setParameter('mjesto', '%'.$mjesto . '%')
            ->setParameter('deleted', 0)
            ->setParameter('active', 1)
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult(Query::HYDRATE_OBJECT);
    }

    public function countAllNotDeletedForFront($frontFrom, $frontTo, $propertyName, $mjesto, $skrbnik, $vlasnik)
    {
        return $this->createQueryBuilder('p')
            ->select('count(p)')
//            ->addSelect('CONCAT(p.idCountry, \'-\', LPAD(p.id,5,\'0\')) as idPropertyFront')
//            ->addSelect('p.name AS propertyName')
//            ->addSelect('p.city AS mjesto')
//            ->addSelect('p.idCountry AS idCountry')
//            ->addSelect('skrbnik.naziv AS srbnikNaziv')
//            ->addSelect('vlasnik.naziv AS vlasnikNaziv')
            ->leftJoin('p.agentC', 'skrbnik')
            ->leftJoin('p.ownerC', 'vlasnik')
            ->where('skrbnik.naziv LIKE :skrbnik')
            ->andWhere('p.id >= :frontFrom')
            ->andWhere('p.id <= :frontTo')
            ->andWhere('vlasnik.naziv LIKE :vlasnik')
            ->andWhere('p.city LIKE :mjesto')
            ->andWhere('p.name LIKE :propertyName')
//            ->andHaving('idPropertyFront LIKE :front')
            ->andWhere('p.deleted = :deleted')
            ->setParameter('propertyName', '%' . $propertyName . '%')
//            ->setParameter('front', '%' . $front . '%')
            ->setParameter('skrbnik', '%' . $skrbnik . '%')
            ->setParameter('frontFrom', $frontFrom)
            ->setParameter('frontTo', $frontTo)
            ->setParameter('vlasnik', '%' . $vlasnik . '%')
            ->setParameter('mjesto', '%'.$mjesto . '%')
            ->setParameter('deleted', 0)
//            ->setMaxResults($limit)
//            ->setFirstResult($offset)
            ->getQuery()
            ->getSingleScalarResult();
    }
}
