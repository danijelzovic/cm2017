<?php

namespace td\CMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use td\CMBundle\Entity\WebshopPropertyUnit;

class WebshopPropertyUnitJsonController extends Controller
{
    public function deleteAction(WebshopPropertyUnit $webshopPropertyUnit)
    {
        //
        return;
    }

    /**
     *  Activate WebshopPropertyUnit
     *
     * @param WebshopPropertyUnit $webshopPropertyUnit
     * @return JsonResponse
     */
    public function activateAction(WebshopPropertyUnit $webshopPropertyUnit)
    {
        if (!is_null($webshopPropertyUnit)) {
            $webshopPropertyUnit->setDisabled(0);
            $em = $this->getDoctrine()->getManager();
            $em->persist($webshopPropertyUnit);
            $em->flush();
            return new JsonResponse(array('status' => 'ok', 'message' => 'WebshopPropertyUnit aktiviran!'));
        } else {
            return new JsonResponse(array('status' => 'Error!', 'message' => 'WebshopPropertyUnit ne postoji!'));
        }
    }

    /**
     *  Deactivate WebshopPropertyUnit
     *
     * @param WebshopPropertyUnit $webshopPropertyUnit
     * @return JsonResponse
     */
    public function deactivateAction(WebshopPropertyUnit $webshopPropertyUnit)
    {
        if (!is_null($webshopPropertyUnit)) {
            $webshopPropertyUnit->setDisabled(1);
            $em = $this->getDoctrine()->getManager();
            $em->persist($webshopPropertyUnit);
            $em->flush();
            return new JsonResponse(array('status' => 'ok', 'message' => 'WebshopPropertyUnit deaktiviran!'));
        } else {
            return new JsonResponse(array('status' => 'Error!', 'message' => 'WebshopPropertyUnit ne postoji!'));
        }
    }
}
