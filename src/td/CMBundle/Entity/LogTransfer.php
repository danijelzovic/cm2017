<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LogTransfer
 *
 * @ORM\Table(name="cm_log_transfer")
 * @ORM\Entity
 */
class LogTransfer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_CM_promjena", type="integer")
     */
    private $iDCMPromjena;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date_time", type="datetime")
     */
    private $dateTime;

    /**
     * @var boolean
     *
     * @ORM\Column(name="OK", type="boolean")
     */
    private $oK;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set iDCMPromjena
     *
     * @param integer $iDCMPromjena
     * @return CM_log_transfer
     */
    public function setIDCMPromjena($iDCMPromjena)
    {
        $this->iDCMPromjena = $iDCMPromjena;

        return $this;
    }

    /**
     * Get iDCMPromjena
     *
     * @return integer 
     */
    public function getIDCMPromjena()
    {
        return $this->iDCMPromjena;
    }

    /**
     * Set dateTime
     *
     * @param \DateTime $dateTime
     * @return LogTransfer
     */
    public function setDateTime($dateTime)
    {
        $this->dateTime = $dateTime;

        return $this;
    }

    /**
     * Get dateTime
     *
     * @return \DateTime 
     */
    public function getDateTime()
    {
        return $this->dateTime;
    }

    /**
     * Set oK
     *
     * @param boolean $oK
     * @return LogTransfer
     */
    public function setOK($oK)
    {
        $this->oK = $oK;

        return $this;
    }

    /**
     * Get oK
     *
     * @return boolean 
     */
    public function getOK()
    {
        return $this->oK;
    }
}
