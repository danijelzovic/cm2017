<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Exclude;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * PortalCode
 *
 * @ORM\Table(name="cm_portal_code")
 * @ORM\Entity(repositoryClass="td\CMBundle\Repository\PortalCodeRepository")
 * @UniqueEntity("id", message="Ova vrijednost je već iskorištena!")
 */
class PortalCode
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     */
    private $code;

    /**
     * @var string $id
     *
     * @ORM\Id
     * @ORM\Column(name="code", type="string", length=32, unique=true)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="Portal", mappedBy="code")
     * @Exclude
     */
    private $portals;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return PortalCode
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return PortalCode
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->portals = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add portal
     *
     * @param \td\CMBundle\Entity\Portal $portal
     *
     * @return PortalCode
     */
    public function addPortal(\td\CMBundle\Entity\Portal $portal)
    {
        $this->portals[] = $portal;

        return $this;
    }

    /**
     * Remove portal
     *
     * @param \td\CMBundle\Entity\Portal $portal
     */
    public function removePortal(\td\CMBundle\Entity\Portal $portal)
    {
        $this->portals->removeElement($portal);
    }

    /**
     * Get portals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPortals()
    {
        return $this->portals;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return PortalCode
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}
