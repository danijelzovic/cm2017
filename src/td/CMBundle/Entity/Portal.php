<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Exclude;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Portal
 *
 * @ORM\Table(name="cm_all_portals")
 * @ORM\Entity(repositoryClass="td\CMBundle\Entity\Repository\PortalRepository")
 */
class Portal
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="naziv", type="string", length=255)
     */
    private $naziv;


    /**
     * @ORM\ManyToOne(targetEntity="PortalCode", inversedBy="portals")
     * @ORM\JoinColumn(name="code", referencedColumnName="code")
     */
    private $code;


//    private $portal;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255)
     */
    private $link;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_person", type="string", length=255)
     */
    private $contactPerson = '';

    /**
     * @var string
     *
     * @ORM\Column(name="contact_email", type="string", length=255)
     */
    private $contactEmail = '';

    /**
     * @var string
     *
     * @ORM\Column(name="contact_tel", type="string", length=255)
     */
    private $contactTel = '';

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="thumbnail", type="string", length=255)
     */
    private $thumbnail;


    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path
            ? null
            : $this->getUploadDir().'/'.$this->path;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'bundles/cm/images';
    }

    /**
     * @Assert\File(maxSize="6000000")
     */
    private $file;

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }

        // use the original file name here but you should
        // sanitize it at least to avoid any security issues

        // move takes the target directory and then the
        // target filename to move to
        $this->getFile()->move(
            $this->getUploadRootDir(),
            $this->getFile()->getClientOriginalName()
        );

        // set the path property to the filename where you've saved the file
        $this->thumbnail = $this->getFile()->getClientOriginalName();

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="view_type_setup", type="string", length=255)
     */
    private $viewTypeSetup;

    /**
     * @var string
     *
     * @ORM\Column(name="view_type_connect", type="string", length=255)
     */
    private $viewTypeConnect;

    /**
     * @ORM\OneToMany(targetEntity="PortalKorisnik", mappedBy="portal")
     * @Exclude
     */
    private $portaliKorisnici;

    /**
     * @var boolean
     *
     * @ORM\Column(name="connect_property", type="boolean")
     */
    private $connectProperty = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="connect_property_unit", type="boolean")
     */
    private $connectPropertyUnit = false;

    /**
     * @var string
     *
     * @ORM\Column(name="link_property_on_portal", type="string", length=255, nullable=true)
     */
    private $linkPropertyOnPortal;

    /**
     * @var string
     *
     * @ORM\Column(name="link_property_unit_on_portal", type="string", length=255, nullable=true)
     */
    private $linkPropertyUnitOnPortal;

    /**
     * @var boolean
     *
     * @ORM\Column(name="manage_activity_property", type="boolean")
     */
    private $manageActivityProperty = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="manage_activity_property_unit", type="boolean")
     */
    private $manageActivityPropertyUnit = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="must_have_webshop", type="boolean")
     */
    private $mustHaveWebshop = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="process_change", type="boolean")
     */
    private $processChange = false;

    /**
     * @var string
     *
     * @ORM\Column(name="settings_template", type="string", length=255, nullable=true)
     */
    private $settingsTemplate = '';

    /**
     * @ORM\ManyToOne(targetEntity="PortalGroup", inversedBy="portals")
     * @ORM\JoinColumn(name="portal_group_id", referencedColumnName="id")
     */
    private $portalGroup;

    /**
     * @var string
     *
     * @ORM\Column(name="code_name", type="string", length=255)
     */
    private $codeName;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set naziv
     *
     * @param string $naziv
     * @return Portal
     */
    public function setNaziv($naziv)
    {
        $this->naziv = $naziv;

        return $this;
    }

    /**
     * Get naziv
     *
     * @return string
     */
    public function getNaziv()
    {
        return $this->naziv;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return Portal
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set contactPerson
     *
     * @param string $contactPerson
     * @return Portal
     */
    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    /**
     * Get contactPerson
     *
     * @return string
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * Set contactEmail
     *
     * @param string $contactEmail
     * @return Portal
     */
    public function setContactEmail($contactEmail)
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    /**
     * Get contactEmail
     *
     * @return string
     */
    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    /**
     * Set contactTel
     *
     * @param string $contactTel
     * @return Portal
     */
    public function setContactTel($contactTel)
    {
        $this->contactTel = $contactTel;

        return $this;
    }

    /**
     * Get contactTel
     *
     * @return string
     */
    public function getContactTel()
    {
        return $this->contactTel;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Portal
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->portaliKorisnici = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add portaliKorisnici
     *
     * @param \td\CMBundle\Entity\PortalKorisnik $portaliKorisnici
     * @return Portal
     */
    public function addPortaliKorisnici(\td\CMBundle\Entity\PortalKorisnik $portaliKorisnici)
    {
        $this->portaliKorisnici[] = $portaliKorisnici;

        return $this;
    }

    /**
     * Remove portaliKorisnici
     *
     * @param \td\CMBundle\Entity\PortalKorisnik $portaliKorisnici
     */
    public function removePortaliKorisnici(\td\CMBundle\Entity\PortalKorisnik $portaliKorisnici)
    {
        $this->portaliKorisnici->removeElement($portaliKorisnici);
    }

    /**
     * Get portaliKorisnici
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPortaliKorisnici()
    {
        return $this->portaliKorisnici;
    }

    /**
     * Set viewTypeSetup
     *
     * @param string $viewTypeSetup
     *
     * @return Portal
     */
    public function setViewTypeSetup($viewTypeSetup)
    {
        $this->viewTypeSetup = $viewTypeSetup;

        return $this;
    }

    /**
     * Get viewTypeSetup
     *
     * @return string
     */
    public function getViewTypeSetup()
    {
        return $this->viewTypeSetup;
    }

    /**
     * Set viewTypeConnect
     *
     * @param string $viewTypeConnect
     *
     * @return Portal
     */
    public function setViewTypeConnect($viewTypeConnect)
    {
        $this->viewTypeConnect = $viewTypeConnect;

        return $this;
    }

    /**
     * Get viewTypeConnect
     *
     * @return string
     */
    public function getViewTypeConnect()
    {
        return $this->viewTypeConnect;
    }

    /**
     * Set code
     *
     * @param \td\CMBundle\Entity\PortalCode $code
     *
     * @return Portal
     */
    public function setCode(\td\CMBundle\Entity\PortalCode $code = null)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return \td\CMBundle\Entity\PortalCode
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set connectProperty
     *
     * @param boolean $connectProperty
     *
     * @return Portal
     */
    public function setConnectProperty($connectProperty)
    {
        $this->connectProperty = $connectProperty;

        return $this;
    }

    /**
     * Get connectProperty
     *
     * @return boolean
     */
    public function getConnectProperty()
    {
        return $this->connectProperty;
    }

    /**
     * Set connectPropertyUnit
     *
     * @param boolean $connectPropertyUnit
     *
     * @return Portal
     */
    public function setConnectPropertyUnit($connectPropertyUnit)
    {
        $this->connectPropertyUnit = $connectPropertyUnit;

        return $this;
    }

    /**
     * Get connectPropertyUnit
     *
     * @return boolean
     */
    public function getConnectPropertyUnit()
    {
        return $this->connectPropertyUnit;
    }

    /**
     * Set linkPropertyOnPortal
     *
     * @param string $linkPropertyOnPortal
     *
     * @return Portal
     */
    public function setLinkPropertyOnPortal($linkPropertyOnPortal)
    {
        $this->linkPropertyOnPortal = $linkPropertyOnPortal;

        return $this;
    }

    /**
     * Get linkPropertyOnPortal
     *
     * @return string
     */
    public function getLinkPropertyOnPortal()
    {
        return $this->linkPropertyOnPortal;
    }

    /**
     * Set linkPropertyUnitOnPortal
     *
     * @param string $linkPropertyUnitOnPortal
     *
     * @return Portal
     */
    public function setLinkPropertyUnitOnPortal($linkPropertyUnitOnPortal)
    {
        $this->linkPropertyUnitOnPortal = $linkPropertyUnitOnPortal;

        return $this;
    }

    /**
     * Get linkPropertyUnitOnPortal
     *
     * @return string
     */
    public function getLinkPropertyUnitOnPortal()
    {
        return $this->linkPropertyUnitOnPortal;
    }

    /**
     * Set manageActivityProperty
     *
     * @param boolean $manageActivityProperty
     *
     * @return Portal
     */
    public function setManageActivityProperty($manageActivityProperty)
    {
        $this->manageActivityProperty = $manageActivityProperty;

        return $this;
    }

    /**
     * Get manageActivityProperty
     *
     * @return boolean
     */
    public function getManageActivityProperty()
    {
        return $this->manageActivityProperty;
    }

    /**
     * Set manageActivityPropertyUnit
     *
     * @param boolean $manageActivityPropertyUnit
     *
     * @return Portal
     */
    public function setManageActivityPropertyUnit($manageActivityPropertyUnit)
    {
        $this->manageActivityPropertyUnit = $manageActivityPropertyUnit;

        return $this;
    }

    /**
     * Get manageActivityPropertyUnit
     *
     * @return boolean
     */
    public function getManageActivityPropertyUnit()
    {
        return $this->manageActivityPropertyUnit;
    }

    /**
     * Set mustHaveWebshop
     *
     * @param boolean $mustHaveWebshop
     *
     * @return Portal
     */
    public function setMustHaveWebshop($mustHaveWebshop)
    {
        $this->mustHaveWebshop = $mustHaveWebshop;

        return $this;
    }

    /**
     * Get mustHaveWebshop
     *
     * @return boolean
     */
    public function getMustHaveWebshop()
    {
        return $this->mustHaveWebshop;
    }

    /**
     * Set settingsTemplate
     *
     * @param string $settingsTemplate
     *
     * @return Portal
     */
    public function setSettingsTemplate($settingsTemplate)
    {
        $this->settingsTemplate = $settingsTemplate;

        return $this;
    }

    /**
     * Get settingsTemplate
     *
     * @return string
     */
    public function getSettingsTemplate()
    {
        return $this->settingsTemplate;
    }

    /**
     *  Check if portal is Booking.com
     *
     * @return bool
     */
    public function isBookingCom()
    {
        if((false !== strpos(strtolower($this->getNaziv()),'booking')) || false !== strpos(strtolower($this->getLink()), 'booking.com')){
            return true;
        }
    }

    /**
     * Set processChange
     *
     * @param boolean $processChange
     *
     * @return Portal
     */
    public function setProcessChange($processChange)
    {
        $this->processChange = $processChange;

        return $this;
    }

    /**
     * Get processChange
     *
     * @return boolean
     */
    public function getProcessChange()
    {
        return $this->processChange;
    }

    /**
     * Set portalGroup
     *
     * @param \td\CMBundle\Entity\PortalGroup $portalGroup
     *
     * @return Portal
     */
    public function setPortalGroup(\td\CMBundle\Entity\PortalGroup $portalGroup = null)
    {
        $this->portalGroup = $portalGroup;

        return $this;
    }

    /**
     * Get portalGroup
     *
     * @return \td\CMBundle\Entity\PortalGroup
     */
    public function getPortalGroup()
    {
        return $this->portalGroup;
    }

    /**
     * Set thumbnail
     *
     * @param string $thumbnail
     * @return Portal
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * Get thumbnail
     *
     * @return string 
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Set codeName
     *
     * @param string $codeName
     * @return Portal
     */
    public function setCodeName($codeName)
    {
        $this->codeName = $codeName;

        return $this;
    }

    /**
     * Get codeName
     *
     * @return string 
     */
    public function getCodeName()
    {
        return $this->codeName;
    }
}
