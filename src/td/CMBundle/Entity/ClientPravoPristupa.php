<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClientPravoPristupa
 *
 * @ORM\Table(name="client_pravapristupa")
 * @ORM\Entity(repositoryClass="td\CMBundle\Repository\ClientPravaPristupaRepository")
 */
class ClientPravoPristupa
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_client", type="integer")
     */
    private $idClient;

    /**
     * @var string
     *
     * @ORM\Column(name="tablica", type="string", length=32)
     */
    private $tablica;

    /**
     * @var int
     *
     * @ORM\Column(name="id_object", type="integer")
     */
    private $idObject;

    /**
     * @var bool
     *
     * @ORM\Column(name="view", type="boolean")
     */
    private $view;

    /**
     * @var bool
     *
     * @ORM\Column(name="edit", type="boolean")
     */
    private $edit;

    /**
     * @var bool
     *
     * @ORM\Column(name="del", type="boolean")
     */
    private $del;

    /**
     * @var int
     *
     * @ORM\Column(name="postavio", type="integer")
     */
    private $postavio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="vrijeme", type="datetime")
     */
    private $vrijeme;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idClient
     *
     * @param integer $idClient
     *
     * @return ClientPravoPristupa
     */
    public function setIdClient($idClient)
    {
        $this->idClient = $idClient;

        return $this;
    }

    /**
     * Get idClient
     *
     * @return int
     */
    public function getIdClient()
    {
        return $this->idClient;
    }

    /**
     * Set tablica
     *
     * @param string $tablica
     *
     * @return ClientPravoPristupa
     */
    public function setTablica($tablica)
    {
        $this->tablica = $tablica;

        return $this;
    }

    /**
     * Get tablica
     *
     * @return string
     */
    public function getTablica()
    {
        return $this->tablica;
    }

    /**
     * Set idObject
     *
     * @param integer $idObject
     *
     * @return ClientPravoPristupa
     */
    public function setIdObject($idObject)
    {
        $this->idObject = $idObject;

        return $this;
    }

    /**
     * Get idObject
     *
     * @return int
     */
    public function getIdObject()
    {
        return $this->idObject;
    }

    /**
     * Set view
     *
     * @param boolean $view
     *
     * @return ClientPravoPristupa
     */
    public function setView($view)
    {
        $this->view = $view;

        return $this;
    }

    /**
     * Get view
     *
     * @return bool
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * Set edit
     *
     * @param boolean $edit
     *
     * @return ClientPravoPristupa
     */
    public function setEdit($edit)
    {
        $this->edit = $edit;

        return $this;
    }

    /**
     * Get edit
     *
     * @return bool
     */
    public function getEdit()
    {
        return $this->edit;
    }

    /**
     * Set del
     *
     * @param boolean $del
     *
     * @return ClientPravoPristupa
     */
    public function setDel($del)
    {
        $this->del = $del;

        return $this;
    }

    /**
     * Get del
     *
     * @return bool
     */
    public function getDel()
    {
        return $this->del;
    }

    /**
     * Set postavio
     *
     * @param integer $postavio
     *
     * @return ClientPravoPristupa
     */
    public function setPostavio($postavio)
    {
        $this->postavio = $postavio;

        return $this;
    }

    /**
     * Get postavio
     *
     * @return int
     */
    public function getPostavio()
    {
        return $this->postavio;
    }

    /**
     * Set vrijeme
     *
     * @param \DateTime $vrijeme
     *
     * @return ClientPravoPristupa
     */
    public function setVrijeme($vrijeme)
    {
        $this->vrijeme = $vrijeme;

        return $this;
    }

    /**
     * Get vrijeme
     *
     * @return \DateTime
     */
    public function getVrijeme()
    {
        return $this->vrijeme;
    }
}
