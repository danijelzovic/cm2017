<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CompanyStatus
 *
 * @ORM\Table(name="company_status")
 * @ORM\Entity
 */
class CompanyStatus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_company", type="integer")
     */
    private $idCompany = 1;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="menu_description", type="text")
     */
    private $menuDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="menu_path", type="string", length=32)
     */
    private $menuPath = "menu_IZ";

    /**
     * @var string
     *
     * @ORM\Column(name="first_page", type="string", length=64)
     */
    private $firstPage;

    /**
     * @var string
     *
     * @ORM\Column(name="top_page", type="string", length=64)
     */
    private $topPage;

    /**
     * @var string
     *
     * @ORM\Column(name="bottom_page", type="string", length=64)
     */
    private $bottomPage;

    /**
     * @var boolean
     *
     * @ORM\Column(name="zadnji_projekt", type="boolean")
     */
    private $zadnjiProjekt = 1;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordering", type="smallint")
     */
    private $ordering = 0;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCompany
     *
     * @param integer $idCompany
     * @return CompanyStatus
     */
    public function setIdCompany($idCompany)
    {
        $this->idCompany = $idCompany;

        return $this;
    }

    /**
     * Get idCompany
     *
     * @return integer 
     */
    public function getIdCompany()
    {
        return $this->idCompany;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return CompanyStatus
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set menuDescription
     *
     * @param string $menuDescription
     * @return CompanyStatus
     */
    public function setMenuDescription($menuDescription)
    {
        $this->menuDescription = $menuDescription;

        return $this;
    }

    /**
     * Get menuDescription
     *
     * @return string 
     */
    public function getMenuDescription()
    {
        return $this->menuDescription;
    }

    /**
     * Set menuPath
     *
     * @param string $menuPath
     * @return CompanyStatus
     */
    public function setMenuPath($menuPath)
    {
        $this->menuPath = $menuPath;

        return $this;
    }

    /**
     * Get menuPath
     *
     * @return string 
     */
    public function getMenuPath()
    {
        return $this->menuPath;
    }

    /**
     * Set firstPage
     *
     * @param string $firstPage
     * @return CompanyStatus
     */
    public function setFirstPage($firstPage)
    {
        $this->firstPage = $firstPage;

        return $this;
    }

    /**
     * Get firstPage
     *
     * @return string 
     */
    public function getFirstPage()
    {
        return $this->firstPage;
    }

    /**
     * Set topPage
     *
     * @param string $topPage
     * @return CompanyStatus
     */
    public function setTopPage($topPage)
    {
        $this->topPage = $topPage;

        return $this;
    }

    /**
     * Get topPage
     *
     * @return string 
     */
    public function getTopPage()
    {
        return $this->topPage;
    }

    /**
     * Set bottomPage
     *
     * @param string $bottomPage
     * @return CompanyStatus
     */
    public function setBottomPage($bottomPage)
    {
        $this->bottomPage = $bottomPage;

        return $this;
    }

    /**
     * Get bottomPage
     *
     * @return string 
     */
    public function getBottomPage()
    {
        return $this->bottomPage;
    }

    /**
     * Set zadnjiProjekt
     *
     * @param boolean $zadnjiProjekt
     * @return CompanyStatus
     */
    public function setZadnjiProjekt($zadnjiProjekt)
    {
        $this->zadnjiProjekt = $zadnjiProjekt;

        return $this;
    }

    /**
     * Get zadnjiProjekt
     *
     * @return boolean 
     */
    public function getZadnjiProjekt()
    {
        return $this->zadnjiProjekt;
    }

    /**
     * Set ordering
     *
     * @param integer $ordering
     * @return CompanyStatus
     */
    public function setOrdering($ordering)
    {
        $this->ordering = $ordering;

        return $this;
    }

    /**
     * Get ordering
     *
     * @return integer 
     */
    public function getOrdering()
    {
        return $this->ordering;
    }
}
