<?php

namespace td\CMBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\Validator\Constraints\DateTime;
use td\CMBundle\Entity\ItempricelistSets;
use td\CMBundle\Entity\PropertyUnitItempricelistSetovi;
use td\CMBundle\Entity\Rate;
use td\CMBundle\Entity\SetUp;
use td\CMBundle\Entity\PortalKorisnik;
use td\CMBundle\Entity\PropertyUnit;
use td\CMBundle\Entity\Property;
use td\CMBundle\Entity\SetupProperty;
use td\CMBundle\Entity\SetupPropertyUnit;
use td\CMBundle\Entity\SetupPropertyUnitLog;
use td\CMBundle\Entity\WebshopPartner;
use td\CMBundle\Entity\WebshopProperty;
use td\CMBundle\Entity\WebshopPropertyUnit;
use td\CMBundle\Entity\UnitRate;
use td\CMBundle\Entity\UnitRateStavke;
use td\CMBundle\Entity\PropertyUnitItemPriceListItems;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use JMS;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DomCrawler\Crawler;


class RateController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function unitRatesAction(Request $request)
    {
        //Ova metoda vraća podatke za grid o Rateovima za jedan unit

        //Dohvati idSetupPropertyUnit
        $idSetupPropertyUnit = $request->query->get('idSetupPropertyUnit');
//        return new Response($idSetupPropertyUnit);
        //Dohvati idPropertyUnit
        /* @var $setupPropertyUnit SetupPropertyUnit */
        try {
            $setupPropertyUnit = $this->getDoctrine()->getManager()->getRepository('tdCMBundle:SetupPropertyUnit')->find($idSetupPropertyUnit);
        } catch (Exception $e) {
            return new Response('Smještajna jedinica nije pronađena');
        }

        $bookingComPodaci = $this->get('td_cm.obrada.setup_obrada')->getBookingComRoomRates($setupPropertyUnit->getSetupProperty()->getIdPropertyNaPortalu());
//        return JsonResponse::create($bookingComPodaci);
        $podaciXml = array();
        $podaciFront = array();// Upitno je dali mi to treba jer moren samo nadogradit podaciXml varijablu/polje
        $polje = array();
        $brojac = 0;

        foreach ($bookingComPodaci as $room) {
            if($room['id'] == $setupPropertyUnit->getIdPropertyUnitNaPortalu()){
                foreach ($room['rates'] as $rate) { //Punim polje $podaciXml s pročitanim podacima iz XML-a
                    $polje['rateIdPortal'] = (string)$rate['id'];
                    $polje['isChildRate'] = (string)$rate['is_child_rate'];
                    $polje['maxPersons'] = (string)$rate['max_persons'];
                    $polje['policy'] = (string)$rate['policy'];
                    $polje['policyId'] = (string)$rate['policy_id'];
                    $polje['rateName'] = (string)$rate['rate_name'];
                    $polje['readonly'] = (string)$rate['readonly'];
                    $polje['fixedOccupancy'] = (string)$rate['fixed_occupancy'];
                    $podaciXml[$brojac] = $polje;
                    $brojac++;
                }
            }
        }

        $em = $this->getDoctrine()->getManager();
        //Moran napravit tablicu od UnitRateStavke-a koju šaljen na front.
        //Za svaki rate kojega najden u XML-u napravin jedan zapis u UnitRateStavke-u ako već ne postoji.
        foreach ($podaciXml as $index => $rateXml) {
            //Čitaj podatke iz Rate-a i provjeri da li postoji rate
            //DOdatno bi trebalo testirati svaki polje i provjeriti da li se dogodila promjena, ažurirat polje i zapisat promjene u log. Ovo je za ksniju implementaciju.
            //Inicijalno postavljanje
            $podaciXml[$index]['set'] = '';
//            $podaciXml[$index]['nazivSet'] = '';
            $podaciXml[$index]['noviRate'] = false;
            $podaciXml[$index]['noviUnitRate'] = false;
            $podaciXml[$index]['samoCjenik'] = false;
            $podaciXml[$index]['postojeciSet'] = false;

            $rateDB = $em->getRepository('tdCMBundle:Rate')->findOneByRateIdPortal($rateXml['rateIdPortal']);
            if (!$rateDB) {
                //Kreiraj novi rate u tablici
                $noviRateDB = new Rate();
                $noviRateDB->setDatumKreiranja(new \DateTime('now'));
                $noviRateDB->setRateIdPortal($rateXml['rateIdPortal']);
                $noviRateDB->setIsChildRate($rateXml['isChildRate']);
                $noviRateDB->setMaxPersons($rateXml['maxPersons']);
                $noviRateDB->setPolicy($rateXml['policy']);
                $noviRateDB->setPolicyId($rateXml['policyId']);
                $noviRateDB->setRateName($rateXml['rateName']);
                $noviRateDB->setReadonly($rateXml['readonly']);
                $noviRateDB->setFixedOccupancy($rateXml['fixedOccupancy']);

                $em->persist($noviRateDB);
                $em->flush();
                //Kreiraj novi UnitRateStavke
                $unitRate = $this->kreirajUnitRate($noviRateDB, $setupPropertyUnit);

                //Dodan novo polje koje su slat na front da je ovaj rate novi tj da ga nema u bazi
                if ($unitRate) {
                    $podaciXml[$index]['noviRate'] = true;
                    $podaciXml[$index]['noviUnitRate'] = true;
                    $podaciXml[$index]['idCjenik'] = $unitRate->getIdCjenik();
                    $podaciXml[$index]['idUnitRate'] = $unitRate->getId();
                    $podaciXml[$index]['aktivan'] = false;
                }
            } else {
                //Provjeri dali mi je taj rate u UnitRateStavke-u
                /* @var $unitRate UnitRate */

                $unitRate = $em->getRepository('tdCMBundle:UnitRate')->findOneBy(array('idRate' => $rateDB->getId(), 'setupPropertyUnit' => $setupPropertyUnit));
                if (is_null($unitRate)) {
                    $podaciXml[$index]['noviUnitRate'] = true;
                    $unitRate = $this->kreirajUnitRate($rateDB, $setupPropertyUnit);
                    $podaciXml[$index]['idCjenik'] = $unitRate->getIdCjenik();
                    $podaciXml[$index]['idUnitRate'] = $unitRate->getId();
                    $podaciXml[$index]['aktivan'] = false;
                } else {
                    $podaciXml[$index]['idCjenik'] = $unitRate->getIdCjenik();
                    $podaciXml[$index]['samoCjenik'] = $unitRate->getSamoCjenik();
                    $podaciXml[$index]['idUnitRate'] = $unitRate->getId();
                    if ($unitRate->getIdSet() != 0) {

                        /* @var $set ItempricelistSets */
                        $set = $em->getRepository('tdCMBundle:ItempricelistSets')->find($unitRate->getIdSet());
                        $podaciXml[$index]['set'] = "[" . $unitRate->getIdSet() . "] " . $set->getName() . "]";
                        $podaciXml[$index]['postojeciSet'] = $unitRate->getPostojeciSet();
                    }
                    if($unitRate->getAktivan()) $podaciXml[$index]['aktivan'] = true;
                    else $podaciXml[$index]['aktivan'] = false;
                }
            }
        }
        //Provjeri da li iman kakov Rate u

        //Nakon tega šaljen UnitRAte na ekran s označenin promjenama ukoliko ih ima.
        //Na ekran šaljen - DODAT ZASTAVICU AKTIVAN. TAKO DA TA ZASTAAVICA U KONAČNO POVEZUJE NEKI UNIT
//            ID rate na portalu
//              NAziv rate
//              Readonly - s time da onemoguæin edititranje ako je 1; ili da ih ne prikažen (za početak).
//              Cjenik - to je idHead kojega je potrebno izračunat na temelju algoritma koji se nalazi u SetUpControlleru
//              SamoCjenik - zastavica koja onemogućuje unos, samo pregled stavaka koje su uključene
//              Dropdown mogućih setova - izvuć moguće setove iz propertyUnitItempricelistSetovi i nazive iz ItempricelistSets
//              Postojeći set - označeno po defaultu ako je odabran set. ako se iskljuèi onda se kopira u movi set. Vidit èa ako je veæ nešto zapisano u bazu
//              Botuni - pregled stavaka. Razmislit dali stavit 2 botuna, jedan za pregled drugi za manipulaciju sa stavkama pa da se prikaže jedan ili drugi.

//        foreach ($xml->children() as $room) {
//            $polje['id'] = (string)$room['id'];
//            $polje['room_name'] = (string)$room['room_name'] . ' [' . $room['id'] . ']';
//            $podaci[$brojac] = $polje;
//            $brojac++;
//        }

        //$podaci[0] = $polje;

        //Slanje Responsa
        $response = new JsonResponse();
        $response->setData($podaciXml);
        return $response;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function dohvatiMoguceSetoveAction(Request $request)
    {
        //dohvat setova za neki cjenik
        $em = $this->getDoctrine()->getManager();
        $idCjenik = $request->request->get('idCjenik');
        /* @var $set PropertyUnitItempricelistSetovi */
        $s = $em->getRepository('tdCMBundle:PropertyUnitItempricelistSetovi')->findBy(array('idHead' => $idCjenik));

        $setovi = array();
        $brojac = 0;
        foreach ($s as $index => $set) {
            $setovi[$brojac]['id'] = $set->getIdItempricelistSet();
            $setovi[$brojac]['naziv'] = "[" . $set->getIdItempricelistSet() . "] " . $set->getItempricelistSet()->getName();
            $brojac++;
        }

//        $setovi[0]['id'] = $idCjenik;
//        $setovi[0]['naziv']='Naziv1';
//        $setovi[1]['id']=2;
//        $setovi[1]['naziv']="Naziv2";

        $response = new JsonResponse();
        $response->setData($setovi);
        return $response;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function spremiUnitRateAction(Request $request)
    {
        //Ažuriran stavku unitrate
        //po potrebi kreiran novi set ukoliko je korisnik odabra neki set i ni reka da je postojeći.
        // Na način da kopiran odabrani u novi set i nakon potvrde refereshan grid da mi pobere novega.

        //Dohvat parametara iz requesta
        $rateIdPortal = $request->request->get('rateIdPortal');
        $samoCjenik = $request->request->get('samoCjenik');
        $idSet = $request->request->get('set');
        $postojeciSet = $request->request->get('postojeciSet');
        $aktivan = $request->request->get('aktivan');

        //Dohvat objekata iz baze podataka
        $em = $this->getDoctrine()->getManager();
        /* @var $rate Rate */
        $rate = $em->getRepository('tdCMBundle:Rate')->findOneBy(array('rateIdPortal' => $rateIdPortal));
        /* @var $unitRate UnitRate */
        $unitRate = $em->getRepository('tdCMBundle:UnitRate')->findOneBy(array('idRate' => $rate->getId()));
        $postojeciUnitRateSet = $unitRate->getItempricelistSet();

        //Spremi popratne parametre u $unitRate
        if ($postojeciSet == 'Yes') {
            $unitRate->setPostojeciSet(true);
        } else {
            $unitRate->setPostojeciSet(false);
        }
        if ($samoCjenik == 'Yes') {
            $unitRate->setSamoCjenik(true);
        } else {
            $unitRate->setSamoCjenik(false);
        }
        if ($aktivan == 'Yes') {
            $unitRate->setAktivan(true);
        } else {
            $unitRate->setAktivan(false);
        }
        $em->flush();

        //Provjeri dali treba kopirat set
        if ($idSet) {
            if ($postojeciSet == 'No') {
                //Kopiram set u novi ako već nije kopiran
                /* $itempricelistSet ItempricelistSets */
                $itempricelistSet = $em->getRepository('tdCMBundle:ItempricelistSets')->find($idSet);
                //Moran dodat provjeru da ne kopira vajk nego samo kada je set original.
                /* @var $propertyUnitItempricelistSetovi PropertyUnitItempricelistSetovi */
                $propertyUnitItempricelistSetovi = $em->getRepository('tdCMBundle:PropertyUnitItempricelistSetovi')->findBy(array('itempricelistSet' => $postojeciUnitRateSet));

                if (count($propertyUnitItempricelistSetovi) > 0) {
                    /* @var $noviItempricelistSet ItempricelistSets */
                    $noviItempricelistSet = clone $itempricelistSet;
                    //Moran ubacit kloniranje i stavaka koje stoje ispod seta.

                    $em->persist($noviItempricelistSet);
                    $unitRate->setItempricelistSet($noviItempricelistSet);
                    $em->flush();
                    return new Response('Snimljen klonirani set');
                } else {
                    return new Response('Set je već kloniran');
                }
            } else {
                $unitRate->setIdSet($idSet);
            }
        } else {
            return new Response('Set nije postavljen ili mijenjan!');
        }


        return new Response('OK');
    }

    /**
     * Ova metoda mora vratit moguće stavke i dodane stavke
     *
     * @param Request $request
     * @return Response
     */
    public function unitRateStavkeAction(Request $request)
    {
        //Opis procesa
        /*
            Za zadani setupPropertyUnit obavi sljedeće
            0. Dohvati webshop za taj setupPropertyUnit
            1. Čitaj webshopPropertyUnit->getCjenikProdajni() i ako je 0 onda gledaj Property.getIdAgent() tamo di je Property->getId == WebshopPropertyUnit->getIdProperty()
                                                                                                                        ili Property->getIdAgentC ovisno o postavkama
            2. Kad se pročita skrbnika iz Property-a onda čitaj WebshopPartner->getIdWebshopPartner() za webshop iz WebshopPropertyUnita i idCompanyPartner = skrbnik
                    *forši napravit neku rekurzivnu funkciju*  NE
            3. U UnitRateStavke spremi idCjenik na koji govori WebshopPartner->getPartnerJe() D ili P

            Dugi dio
            1. Dohvati sve stavke iz PropertyUnitItemPriceListItem za PuropertyUnit iz SetupPropertyUnit-a i potegni naziv stavaka iz ItemPricelistItem-a
                    samo za one stavke koje se ne nalaze u UnitRateStavke - ovo je kod kreranja sve
                     *          Napravi san mali modifikacijiu na način da koristin posebnu tablicu cm_unit_rate_stavke_grid
            2. Pretvorit to u polje i poslat na ekran
        */

        $idUnitRate = $request->query->get('idUnitRate');

        $em = $this->getDoctrine()->getManager();
        /* @var $unitRate UnitRate */
        $unitRate = $em->getRepository('tdCMBundle:UnitRate')->find($idUnitRate);
//

        //Za sada ne kreiran novi UnitRateStavke jer ga kreiran kod inicijalnog punjenja iz webshopova, sada kreiran novi za usporedbu
//        $unitRate = new UnitRateStavke(); //Tu punin nove podatke odnosno pregledavan da li su se dogodile promjene
//        $unitRate->setIdRate(0);
//        $unitRate->setSetupPropertyUnit($setupPropertyUnit);
//        $unitRate->setVrijediOd(new \DateTime("2015-02-19"));
//        $unitRate->setVrijediDo(new \DateTime("2015-12-31"));

        //Moguće stavke
        $query = $em->createQuery(
            'SELECT  pipli.idHead idCjenik, ipli.id idStavke, ipli.name, (SELECT u.id FROM tdCMBundle:UnitRateStavke u WHERE u.id = :idUnitRate) idUnitRate
                    FROM tdCMBundle:PropertyUnitItemPriceListItems pipli JOIN pipli.itemPriceListItem ipli
                    WHERE pipli.idHead = :idCjenik AND
                    ipli.id NOT IN (SELECT ipli2.id FROM tdCMBundle:UnitRateStavke urs JOIN urs.itemPriceListItem ipli2 WHERE urs.idUnitRate = :idUnitRate)'
        );
        $query->setParameters(array(
            'idCjenik' => $unitRate->getIdCjenik(),
            'idUnitRate' => $unitRate->getId()
        ));
        $moguceStavke = $query->getResult();

        //Dodane stavke
        $query2 = $em->createQuery(
            'SELECT urs.id, urs.idUnitRate, ur.idCjenik, ipli.id idStavke, ipli.name, ur.id idUnitRate
                    FROM tdCMBundle:UnitRateStavke urs JOIN urs.itemPriceListItem ipli JOIN urs.unitRate ur
                    WHERE ur = :unitRate'
        );
        $query2->setParameter('unitRate', $unitRate);
        $dodaneStavke = $query2->getResult();

        $serializer = JMS\Serializer\SerializerBuilder::create()->build();
        $moguceStavkes = $serializer->serialize($moguceStavke, 'json');
        $dodaneStavkes = $serializer->serialize($dodaneStavke, 'json');
        //2. slanje na ekran
        return $this->render('tdCMBundle:SetUp:unitRate.html.twig',
            array(
                'idPortalKorisnik' => $unitRate->getSetupPropertyUnit()->getPortalKorisnik()->getPortal()->getId(),
                'unitRate' => $unitRate,
                'moguceStavke' => $moguceStavkes,
                'dodaneStavke' => $dodaneStavkes
            ));
    }

    /**
     * Funkcija koja vraća cjenik za zadani setupPropertyUnit - PREBAČENO U SERVICE (samo prvio dio)
     *
     * @param $setupPropertyUnit
     * @return int
     */
    private function nadiCjenik($setupPropertyUnit)
    {
        $em = $this->getDoctrine()->getManager();
        //Razrada
        //Opis procesa
        /*
            Za zadani setupPropertyUnit obavi sljedeće
            0. Dohvati webshop za taj setupPropertyUnit
            1. Čitaj webshopPropertyUnit->getCjenikProdajni() i ako je 0 onda gledaj Property.getIdAgent() tamo di je Property->getId == WebshopPropertyUnit->getIdProperty()
                                                                                                                        ili Property->getIdAgentC ovisno o postavkama
            2. Kad se pročita skrbnika iz Property-a onda čitaj WebshopPartner->getIdWebshopPartner() za webshop iz WebshopPropertyUnita i idCompanyPartner = skrbnik
                    *forši napravit neku rekurzivnu funkciju*  NE
            3. U UnitRateStavke spremi idCjenik na koji govori WebshopPartner->getPartnerJe() D ili P

            Dugi dio
            1. Dohvati sve stavke iz PropertyUnitItemPriceListItem za PuropertyUnit iz SetupPropertyUnit-a i potegni naziv stavaka iz ItemPricelistItem-a
                    samo za one stavke koje se ne nalaze u UnitRateStavke - ovo je kod kreranja sve
                     *          Napravi san mali modifikacijiu na način da koristin posebnu tablicu cm_unit_rate_stavke_grid
            2. Pretvorit to u polje i poslat na ekran
        */

//      Već ga iman jer ga dobivan preko parametra
//        $em = $this->getDoctrine()->getManager();
        /* @var $setupPropertyUnit SetupPropertyUnit */
//        $setupPropertyUnit = $em->getRepository('tdCMBundle:SetupPropertyUnit')->find($idSetupPropertyUnit);

        /* @var $unitRatePostojeci UnitRate */
        $unitRatePostojeci = $em->getRepository('tdCMBundle:UnitRate')->findOneBySetupPropertyUnit($setupPropertyUnit); //Napravit da se išće zadnji
//
//        Ne kreiran novi unit rate nego samo išćen idHEad
        //Za sada ne kreiran novi UnitRateStavke jer ga kreiran kod inicijalnog punjenja iz webshopova, sada kreiran novi za usporedbu
//        $unitRate = new UnitRateStavke(); //Tu punin nove podatke odnosno pregledavan da li su se dogodile promjene
//        $unitRate->setIdRate(0);
//        $unitRate->setSetupPropertyUnit($setupPropertyUnit);
//        $unitRate->setVrijediOd(new \DateTime("2015-02-19"));
//        $unitRate->setVrijediDo(new \DateTime("2015-12-31"));

        //Krieraj novi zapis u UnitRateStavke ako se je dogodila promjena tj ako se je promijeni cjenik. Važeći cjenik odnosno zapis gleda se po zadnjem datumu.
        //Bilo bi potrebno napravit neku obavijest da se je dogodila promjena.
        /* @var $webshopPropertyUnit WebshopPropertyUnit */
        $webshopPropertyUnit = $em->getRepository('tdCMBundle:WebshopPropertyUnit')->findOneBy(
            array('webshop' => $setupPropertyUnit->getPortalKorisnik()->getWebshop(), 'propertyUnit' => $setupPropertyUnit->getPropertyUnit()));

        //1.
        if ($webshopPropertyUnit->getCjenikProdajni() != 0) {
            //Završi, imamo cjenik
//            $unitRate->setIdCjenik($webshopPropertyUnit->getCjenikProdajni());
            return $webshopPropertyUnit->getCjenikProdajni();
        } else {
            //Inače dohvati preko WebshopPartner-a
            //Provjera koji klijent se koristi (sljedeći if gleda dali će koristi novi ili stari tip klijenta/agenta)
            if (1) {
                /* @var $webshopPartner WebshopPartner */
                $webshopPartner = $em->getRepository('tdCMBundle:WebshopPartner')->findOneBy(
                    array('webshop' => $setupPropertyUnit->getWebshop(), 'idCompanyPartner' => $setupPropertyUnit->getPropertyUnit()->getProperty()->getIdAgent()));

            } else {
                $webshopPartner = $em->getRepository('tdCMBundle:WebshopPartner')->findOneBy(
                    array('webshop' => $setupPropertyUnit->getWebshop(), 'idCompanyPartner' => $setupPropertyUnit->getPropertyUnit()->getProperty()->getIdAgentC()));
            }

            //3.
            //Moran provjerit da mi webshopPartner ni nula. Jer mi javlja grešku da mi pozivan funkciju nad nepostojećim objektom.
            if ($webshopPartner) {
                //Ako je pronaša webshopPartner onda traži cjenik
                /* @var $webshopPropertyUnitPartner WebshopPropertyUnit */
                $webshopPropertyUnitPartner = $em->getRepository('tdCMBundle:WebshopPropertyUnit')->findOneByWebshop(//$setupPropertyUnit->getWebshop());
                    array('idShop' => $webshopPartner->getIdWebshopPartner(), 'idUnit' => $setupPropertyUnit->getIdPropertyUnit()));

                if ($webshopPartner->getPartnerJe() == 'D') {
                    return $webshopPropertyUnitPartner->getCjenikKupovni();
                } else {
                    return $webshopPartner->getWebshopPartner()->getCjenikProdajni();
                }
            } else {
                return -1;
            }
        }
    }

    /**
     * @return Response
     */
    private function dodijeliUnitRateStavke()
    {
        //Ako je novi unitRate različit od staroga onda ažuriraj tablicu i uzmi novi unitRate za referentni;
        $em = $this->getDoctrine()->getManager();
        $unitRatePostojeci = null;
        $unitRate = null;
        $setupPropertyUnit = null;
        if ($unitRatePostojeci) {
            if ($unitRate->getIdCjenik() != $unitRatePostojeci->getIdCjenik()) {
                $em->persist($unitRate);
            } else {
                $unitRate = $unitRatePostojeci;
            }
        } else {
            $em->persist($unitRate);
        }


        //Drugio dio - stavke
        //1.
        $stavke = $em->getRepository('tdCMBundle:PropertyUnitItemPriceListItems')->findByIdHead($unitRate->getIdCjenik());
//		$stavke = $em->getRepository('tdCMBundle:PropertyUnitItemPriceListItems')->findByIdHead(31400);
//		$stavke = null;
        $unitRateStavke = $em->getRepository('tdCMBundle:UnitRateStavke')->findBy(array('idUnitRate' => $unitRate->getId()));

        //Brisanje pomo�ne tablice
        $sql = "truncate table cm_unit_rate_stavke_grid";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        $gridPodaci = new \Doctrine\Common\Collections\ArrayCollection();
        /* @var $stavka PropertyUnitItemPriceListItems */
        foreach ($stavke as $stavka) {
            $zastavica = true;
            /* @var $uns UnitRateStavke */

            $unitRateStavkeGrid = new UnitRateStavkeGrid();
            $unitRateStavkeGrid->setIdCjenik($stavka->getIdHead());
            $unitRateStavkeGrid->setIdItempricelistitem($stavka->getItemPriceListItem()->getId());
//                $unitRateStavkeGrid->setIdItempricelistitem(999);
            $unitRateStavkeGrid->setIdUnitRate($unitRate->getId());
            //$unitRateStavkeGrid->setIdUnitRateStavke($idUnitRateStavke)
            $unitRateStavkeGrid->setNazivItempricelistitem($stavka->getItemPriceListItem()->getName());
//                $unitRateStavkeGrid->setNazivItempricelistitem("test");
            $unitRateStavkeGrid->setDodano(0);
            foreach ($unitRateStavke as $uns) {
                if ($stavka->getItemPriceListItem() == $uns->getItemPriceListItem()) {
                    $zastavica = false;
                    $unitRateStavkeGrid->setDodano(1);
//                        break;
                }
            }
            $em->persist($unitRateStavkeGrid);
            $gridPodaci->add($unitRateStavkeGrid);

        }
        $em->flush();

        //Moguće stavke
        $query = $em->createQuery(
            'SELECT  pipli.idHead idCjenik, ipli.id idStavke, ipli.name, (SELECT u.id FROM tdCMBundle:UnitRateStavke u WHERE u.id = :idUnitRate) idUnitRate
                    FROM tdCMBundle:PropertyUnitItemPriceListItems pipli JOIN pipli.itemPriceListItem ipli
                    WHERE pipli.idHead = :idCjenik AND
                    ipli.id NOT IN (SELECT ipli2.id FROM tdCMBundle:UnitRateStavke urs JOIN urs.itemPriceListItem ipli2 WHERE urs.idUnitRate = :idUnitRate)'
        );
        $query->setParameters(array(
            'idCjenik' => $unitRate->getIdCjenik(),
            'idUnitRate' => $unitRate->getId()
        ));
        $moguceStavke = $query->getResult();

        //Dodane stavke
        $query2 = $em->createQuery(
            'SELECT urs.id, urs.idUnitRate, ur.idCjenik, ipli.id idStavke, ipli.name, ur.id idUnitRate
                    FROM tdCMBundle:UnitRateStavke urs JOIN urs.itemPriceListItem ipli JOIN urs.unitRate ur
                    WHERE ur = :unitRate'
        );
        $query2->setParameter('unitRate', $unitRate);
        $dodaneStavke = $query2->getResult();

        $serializer = JMS\Serializer\SerializerBuilder::create()->build();
        $moguceStavkes = $serializer->serialize($moguceStavke, 'json');
        $dodaneStavkes = $serializer->serialize($dodaneStavke, 'json');
        //2. slanje na ekran
        return $this->render('tdCMBundle:SetUp:unitRate.html.twig',
            array(
                'idPortalKorisnik' => $setupPropertyUnit->getPortalKorisnik()->getPortal()->getId(),
                'unitRate' => $unitRate,
                'moguceStavke' => $moguceStavkes,
                'dodaneStavke' => $dodaneStavkes
            ));
    }

    private function kreirajUnitRate($rate, $setupPropertyUnit)
    {
        /* @var $rate Rate */
        $em = $this->getDoctrine()->getManager();
        $unitRate = new UnitRate();
        //$unitRate->setIdRate($rate->getId());
        $unitRate->setRate($rate);
        $unitRate->setSetupPropertyUnit($setupPropertyUnit);
        //Tu ću stavit logiku koja mi dohvaća idHead tj cjenik
        $idHead = $this->nadiCjenik($setupPropertyUnit);
        $unitRate->setIdCjenik($idHead); //-- Ovo treba izračunat
        $unitRate->setSamoCjenik(false);
        $unitRate->setPostojeciSet(false);
        $unitRate->setAktivan(false);
        $unitRate->setVrijediOd(new \DateTime("2015-01-01"));
        $unitRate->setVrijediDo(new \DateTime("2015-12-31"));
        $em->persist($unitRate);
        $em->flush();
        return $unitRate;
    }
}
