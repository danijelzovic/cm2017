<?php

namespace td\CMBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RateControllerTest extends WebTestCase
{
    public function testNadiCjenik()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'cm/rate/idheadforsetuppropertyunit/6069');
        $response = $client->getResponse();

//        $this->assertJson('[59411]', 'Nađen je točan cjenik');
//        $this->assertGreaterThan(
//            0,
//            $crawler->contains('59410')->count()
//        );

        $this->assertTrue(
            $client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            )
        );

        // Test if company was inserted
        $this->assertEquals('[59411]', $response->getContent());

    }
}
