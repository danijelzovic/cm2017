<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PropertyUnitPricesRange
 *
 * @ORM\Table(name="property_unit_prices_range")
 * @ORM\Entity(repositoryClass="td\CMBundle\Repository\PropertyUnitPricesRangeRepository")
 */
class PropertyUnitPricesRange
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="version", type="integer")
     */
    private $version;

    /**
     * @var int
     *
     * @ORM\Column(name="bookertech", type="integer")
     */
    private $bookertech;

    /**
     * @var int
     *
     * @ORM\Column(name="vlasnik", type="integer")
     */
    private $vlasnik;

    /**
     * @var int
     *
     * @ORM\Column(name="vlasnik_client", type="integer")
     */
    private $vlasnikClient;

    /**
     * @var int
     *
     * @ORM\Column(name="korisnik_b2b", type="integer")
     */
    private $korisnikB2b;

    /**
     * @var int
     *
     * @ORM\Column(name="korisnik_b2b_client", type="integer")
     */
    private $korisnikB2bClient;

    /**
     * @var int
     *
     * @ORM\Column(name="korisnik_b2b_grp", type="integer")
     */
    private $korisnikB2bGrp;

    /**
     * @var int
     *
     * @ORM\Column(name="korisnik_b2c", type="integer")
     */
    private $korisnikB2c;

    /**
     * @var string
     *
     * @ORM\Column(name="btip", type="string", length=3)
     */
    private $btip;

    /**
     * @var int
     *
     * @ORM\Column(name="parent", type="integer")
     */
    private $parent;

    /**
     * @var int
     *
     * @ORM\Column(name="id_unit", type="integer")
     */
    private $idUnit;

    /**
     * @var int
     *
     * @ORM\Column(name="id_property", type="integer")
     */
    private $idProperty;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="id_orig", type="string", length=255)
     */
    private $idOrig;

    /**
     * @var string
     *
     * @ORM\Column(name="id_ugovor", type="string", length=64)
     */
    private $idUgovor;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @var int
     *
     * @ORM\Column(name="userid", type="integer")
     */
    private $userid;

    /**
     * @var string
     *
     * @ORM\Column(name="priceactiv", type="string", length=255)
     */
    private $priceactiv;

    /**
     * @var string
     *
     * @ORM\Column(name="pricesedamjedan", type="string", length=255)
     */
    private $pricesedamjedan;

    /**
     * @var int
     *
     * @ORM\Column(name="kolikodana", type="integer")
     */
    private $kolikodana;

    /**
     * @var int
     *
     * @ORM\Column(name="price_nr_person_min", type="integer")
     */
    private $priceNrPersonMin;

    /**
     * @var int
     *
     * @ORM\Column(name="price_nr_person_2", type="integer")
     */
    private $priceNrPerson2;

    /**
     * @var int
     *
     * @ORM\Column(name="price_nr_person_3", type="integer")
     */
    private $priceNrPerson3;

    /**
     * @var int
     *
     * @ORM\Column(name="shortstay_nr_1", type="integer")
     */
    private $shortstayNr1;

    /**
     * @var int
     *
     * @ORM\Column(name="shortstay_nr_2", type="integer")
     */
    private $shortstayNr2;

    /**
     * @var int
     *
     * @ORM\Column(name="shortstay_nr_3", type="integer")
     */
    private $shortstayNr3;

    /**
     * @var int
     *
     * @ORM\Column(name="cj_godina", type="integer")
     */
    private $cjGodina;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="cj_start", type="datetime")
     */
    private $cjStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="cj_end", type="datetime")
     */
    private $cjEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="cj_valuta", type="string", length=255)
     */
    private $cjValuta;

    /**
     * @var int
     *
     * @ORM\Column(name="cj_tip", type="integer")
     */
    private $cjTip;

    /**
     * @var int
     *
     * @ORM\Column(name="cj_nom_osoba", type="integer")
     */
    private $cjNomOsoba;

    /**
     * @var int
     *
     * @ORM\Column(name="cj_nom_osoba_extrabed", type="integer")
     */
    private $cjNomOsobaExtrabed;

    /**
     * @var string
     *
     * @ORM\Column(name="shortstay_per", type="string", length=32)
     */
    private $shortstayPer;

    /**
     * @var string
     *
     * @ORM\Column(name="nrperson_per", type="string", length=3)
     */
    private $nrpersonPer;

    /**
     * @var string
     *
     * @ORM\Column(name="nrpersonunit_per", type="string", length=32)
     */
    private $nrpersonunitPer;

    /**
     * @var int
     *
     * @ORM\Column(name="restax", type="integer")
     */
    private $restax;

    /**
     * @var string
     *
     * @ORM\Column(name="restax_status", type="string", length=32)
     */
    private $restaxStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="salechannels", type="string", length=255)
     */
    private $salechannels;

    /**
     * @var string
     *
     * @ORM\Column(name="dob_a_start", type="decimal", precision=6, scale=2)
     */
    private $dobAStart;

    /**
     * @var string
     *
     * @ORM\Column(name="dob_a_end", type="decimal", precision=6, scale=2)
     */
    private $dobAEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="dob_b_start", type="decimal", precision=6, scale=2)
     */
    private $dobBStart;

    /**
     * @var string
     *
     * @ORM\Column(name="dob_b_end", type="decimal", precision=6, scale=2)
     */
    private $dobBEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="dob_c_start", type="decimal", precision=6, scale=2)
     */
    private $dobCStart;

    /**
     * @var string
     *
     * @ORM\Column(name="dob_c_end", type="decimal", precision=6, scale=2)
     */
    private $dobCEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="dob_d_start", type="decimal", precision=6, scale=2)
     */
    private $dobDStart;

    /**
     * @var string
     *
     * @ORM\Column(name="dob_d_end", type="decimal", precision=6, scale=2)
     */
    private $dobDEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="dob_per", type="string", length=32)
     */
    private $dobPer;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="text")
     */
    private $note;

    /**
     * @var string
     *
     * @ORM\Column(name="note_hr", type="text")
     */
    private $noteHr;

    /**
     * @var string
     *
     * @ORM\Column(name="note_en", type="text")
     */
    private $noteEn;

    /**
     * @var string
     *
     * @ORM\Column(name="note_de", type="text")
     */
    private $noteDe;

    /**
     * @var string
     *
     * @ORM\Column(name="note_it", type="text")
     */
    private $noteIt;

    /**
     * @var string
     *
     * @ORM\Column(name="note_si", type="text")
     */
    private $noteSi;

    /**
     * @var string
     *
     * @ORM\Column(name="rabat", type="decimal", precision=6, scale=2)
     */
    private $rabat;

    /**
     * @var string
     *
     * @ORM\Column(name="dobavna_provizija", type="decimal", precision=6, scale=2)
     */
    private $dobavnaProvizija;

    /**
     * @var string
     *
     * @ORM\Column(name="ll_nocenje", type="string", length=16)
     */
    private $llNocenje;

    /**
     * @var string
     *
     * @ORM\Column(name="ll_nocenjedorucak", type="string", length=16)
     */
    private $llNocenjedorucak;

    /**
     * @var string
     *
     * @ORM\Column(name="ll_polupansion", type="string", length=16)
     */
    private $llPolupansion;

    /**
     * @var string
     *
     * @ORM\Column(name="ll_pansion", type="string", length=16)
     */
    private $llPansion;

    /**
     * @var string
     *
     * @ORM\Column(name="ll_dnevni", type="string", length=16)
     */
    private $llDnevni;

    /**
     * @var string
     *
     * @ORM\Column(name="ll_allinc", type="string", length=16)
     */
    private $llAllinc;

    /**
     * @var string
     *
     * @ORM\Column(name="laser_ugovor", type="string", length=64)
     */
    private $laserUgovor;

    /**
     * @var string
     *
     * @ORM\Column(name="laser_cjenik", type="string", length=64)
     */
    private $laserCjenik;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastchange", type="datetimetz")
     */
    private $lastchange;

    /**
     * @var string
     *
     * @ORM\Column(name="atd_napomena", type="string", length=64)
     */
    private $atdNapomena;

    /**
     * @var int
     *
     * @ORM\Column(name="defaultni", type="integer")
     */
    private $defaultni;

    /**
     * @var int
     *
     * @ORM\Column(name="cjenicig", type="integer")
     */
    private $cjenicig;

    /**
     * @var int
     *
     * @ORM\Column(name="id_company", type="integer")
     */
    private $idCompany;

    /**
     * @var int
     *
     * @ORM\Column(name="id_partner", type="integer")
     */
    private $idPartner;

    /**
     * @var int
     *
     * @ORM\Column(name="id_grupa_partner", type="integer")
     */
    private $idGrupaPartner;

    /**
     * @var string
     *
     * @ORM\Column(name="frontprikaz", type="string", length=2)
     */
    private $frontprikaz;

    /**
     * @var int
     *
     * @ORM\Column(name="sezone", type="integer")
     */
    private $sezone;

    /**
     * @var int
     *
     * @ORM\Column(name="tipski_b2bd", type="integer")
     */
    private $tipskiB2bd;

    /**
     * @var string
     *
     * @ORM\Column(name="provider", type="string", length=32)
     */
    private $provider;

    /**
     * @var bool
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set version
     *
     * @param integer $version
     *
     * @return PropertyUnitPricesRange
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return int
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set bookertech
     *
     * @param integer $bookertech
     *
     * @return PropertyUnitPricesRange
     */
    public function setBookertech($bookertech)
    {
        $this->bookertech = $bookertech;

        return $this;
    }

    /**
     * Get bookertech
     *
     * @return int
     */
    public function getBookertech()
    {
        return $this->bookertech;
    }

    /**
     * Set vlasnik
     *
     * @param integer $vlasnik
     *
     * @return PropertyUnitPricesRange
     */
    public function setVlasnik($vlasnik)
    {
        $this->vlasnik = $vlasnik;

        return $this;
    }

    /**
     * Get vlasnik
     *
     * @return int
     */
    public function getVlasnik()
    {
        return $this->vlasnik;
    }

    /**
     * Set vlasnikClient
     *
     * @param integer $vlasnikClient
     *
     * @return PropertyUnitPricesRange
     */
    public function setVlasnikClient($vlasnikClient)
    {
        $this->vlasnikClient = $vlasnikClient;

        return $this;
    }

    /**
     * Get vlasnikClient
     *
     * @return int
     */
    public function getVlasnikClient()
    {
        return $this->vlasnikClient;
    }

    /**
     * Set korisnikB2b
     *
     * @param integer $korisnikB2b
     *
     * @return PropertyUnitPricesRange
     */
    public function setKorisnikB2b($korisnikB2b)
    {
        $this->korisnikB2b = $korisnikB2b;

        return $this;
    }

    /**
     * Get korisnikB2b
     *
     * @return int
     */
    public function getKorisnikB2b()
    {
        return $this->korisnikB2b;
    }

    /**
     * Set korisnikB2bClient
     *
     * @param integer $korisnikB2bClient
     *
     * @return PropertyUnitPricesRange
     */
    public function setKorisnikB2bClient($korisnikB2bClient)
    {
        $this->korisnikB2bClient = $korisnikB2bClient;

        return $this;
    }

    /**
     * Get korisnikB2bClient
     *
     * @return int
     */
    public function getKorisnikB2bClient()
    {
        return $this->korisnikB2bClient;
    }

    /**
     * Set korisnikB2bGrp
     *
     * @param integer $korisnikB2bGrp
     *
     * @return PropertyUnitPricesRange
     */
    public function setKorisnikB2bGrp($korisnikB2bGrp)
    {
        $this->korisnikB2bGrp = $korisnikB2bGrp;

        return $this;
    }

    /**
     * Get korisnikB2bGrp
     *
     * @return int
     */
    public function getKorisnikB2bGrp()
    {
        return $this->korisnikB2bGrp;
    }

    /**
     * Set korisnikB2c
     *
     * @param integer $korisnikB2c
     *
     * @return PropertyUnitPricesRange
     */
    public function setKorisnikB2c($korisnikB2c)
    {
        $this->korisnikB2c = $korisnikB2c;

        return $this;
    }

    /**
     * Get korisnikB2c
     *
     * @return int
     */
    public function getKorisnikB2c()
    {
        return $this->korisnikB2c;
    }

    /**
     * Set btip
     *
     * @param string $btip
     *
     * @return PropertyUnitPricesRange
     */
    public function setBtip($btip)
    {
        $this->btip = $btip;

        return $this;
    }

    /**
     * Get btip
     *
     * @return string
     */
    public function getBtip()
    {
        return $this->btip;
    }

    /**
     * Set parent
     *
     * @param integer $parent
     *
     * @return PropertyUnitPricesRange
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return int
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set idUnit
     *
     * @param integer $idUnit
     *
     * @return PropertyUnitPricesRange
     */
    public function setIdUnit($idUnit)
    {
        $this->idUnit = $idUnit;

        return $this;
    }

    /**
     * Get idUnit
     *
     * @return int
     */
    public function getIdUnit()
    {
        return $this->idUnit;
    }

    /**
     * Set idProperty
     *
     * @param integer $idProperty
     *
     * @return PropertyUnitPricesRange
     */
    public function setIdProperty($idProperty)
    {
        $this->idProperty = $idProperty;

        return $this;
    }

    /**
     * Get idProperty
     *
     * @return int
     */
    public function getIdProperty()
    {
        return $this->idProperty;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return PropertyUnitPricesRange
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set idOrig
     *
     * @param string $idOrig
     *
     * @return PropertyUnitPricesRange
     */
    public function setIdOrig($idOrig)
    {
        $this->idOrig = $idOrig;

        return $this;
    }

    /**
     * Get idOrig
     *
     * @return string
     */
    public function getIdOrig()
    {
        return $this->idOrig;
    }

    /**
     * Set idUgovor
     *
     * @param string $idUgovor
     *
     * @return PropertyUnitPricesRange
     */
    public function setIdUgovor($idUgovor)
    {
        $this->idUgovor = $idUgovor;

        return $this;
    }

    /**
     * Get idUgovor
     *
     * @return string
     */
    public function getIdUgovor()
    {
        return $this->idUgovor;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return PropertyUnitPricesRange
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set userid
     *
     * @param integer $userid
     *
     * @return PropertyUnitPricesRange
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return int
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set priceactiv
     *
     * @param string $priceactiv
     *
     * @return PropertyUnitPricesRange
     */
    public function setPriceactiv($priceactiv)
    {
        $this->priceactiv = $priceactiv;

        return $this;
    }

    /**
     * Get priceactiv
     *
     * @return string
     */
    public function getPriceactiv()
    {
        return $this->priceactiv;
    }

    /**
     * Set pricesedamjedan
     *
     * @param string $pricesedamjedan
     *
     * @return PropertyUnitPricesRange
     */
    public function setPricesedamjedan($pricesedamjedan)
    {
        $this->pricesedamjedan = $pricesedamjedan;

        return $this;
    }

    /**
     * Get pricesedamjedan
     *
     * @return string
     */
    public function getPricesedamjedan()
    {
        return $this->pricesedamjedan;
    }

    /**
     * Set kolikodana
     *
     * @param integer $kolikodana
     *
     * @return PropertyUnitPricesRange
     */
    public function setKolikodana($kolikodana)
    {
        $this->kolikodana = $kolikodana;

        return $this;
    }

    /**
     * Get kolikodana
     *
     * @return int
     */
    public function getKolikodana()
    {
        return $this->kolikodana;
    }

    /**
     * Set priceNrPersonMin
     *
     * @param integer $priceNrPersonMin
     *
     * @return PropertyUnitPricesRange
     */
    public function setPriceNrPersonMin($priceNrPersonMin)
    {
        $this->priceNrPersonMin = $priceNrPersonMin;

        return $this;
    }

    /**
     * Get priceNrPersonMin
     *
     * @return int
     */
    public function getPriceNrPersonMin()
    {
        return $this->priceNrPersonMin;
    }

    /**
     * Set priceNrPerson2
     *
     * @param integer $priceNrPerson2
     *
     * @return PropertyUnitPricesRange
     */
    public function setPriceNrPerson2($priceNrPerson2)
    {
        $this->priceNrPerson2 = $priceNrPerson2;

        return $this;
    }

    /**
     * Get priceNrPerson2
     *
     * @return int
     */
    public function getPriceNrPerson2()
    {
        return $this->priceNrPerson2;
    }

    /**
     * Set priceNrPerson3
     *
     * @param integer $priceNrPerson3
     *
     * @return PropertyUnitPricesRange
     */
    public function setPriceNrPerson3($priceNrPerson3)
    {
        $this->priceNrPerson3 = $priceNrPerson3;

        return $this;
    }

    /**
     * Get priceNrPerson3
     *
     * @return int
     */
    public function getPriceNrPerson3()
    {
        return $this->priceNrPerson3;
    }

    /**
     * Set shortstayNr1
     *
     * @param integer $shortstayNr1
     *
     * @return PropertyUnitPricesRange
     */
    public function setShortstayNr1($shortstayNr1)
    {
        $this->shortstayNr1 = $shortstayNr1;

        return $this;
    }

    /**
     * Get shortstayNr1
     *
     * @return int
     */
    public function getShortstayNr1()
    {
        return $this->shortstayNr1;
    }

    /**
     * Set shortstayNr2
     *
     * @param integer $shortstayNr2
     *
     * @return PropertyUnitPricesRange
     */
    public function setShortstayNr2($shortstayNr2)
    {
        $this->shortstayNr2 = $shortstayNr2;

        return $this;
    }

    /**
     * Get shortstayNr2
     *
     * @return int
     */
    public function getShortstayNr2()
    {
        return $this->shortstayNr2;
    }

    /**
     * Set shortstayNr3
     *
     * @param integer $shortstayNr3
     *
     * @return PropertyUnitPricesRange
     */
    public function setShortstayNr3($shortstayNr3)
    {
        $this->shortstayNr3 = $shortstayNr3;

        return $this;
    }

    /**
     * Get shortstayNr3
     *
     * @return int
     */
    public function getShortstayNr3()
    {
        return $this->shortstayNr3;
    }

    /**
     * Set cjGodina
     *
     * @param integer $cjGodina
     *
     * @return PropertyUnitPricesRange
     */
    public function setCjGodina($cjGodina)
    {
        $this->cjGodina = $cjGodina;

        return $this;
    }

    /**
     * Get cjGodina
     *
     * @return int
     */
    public function getCjGodina()
    {
        return $this->cjGodina;
    }

    /**
     * Set cjStart
     *
     * @param \DateTime $cjStart
     *
     * @return PropertyUnitPricesRange
     */
    public function setCjStart($cjStart)
    {
        $this->cjStart = $cjStart;

        return $this;
    }

    /**
     * Get cjStart
     *
     * @return \DateTime
     */
    public function getCjStart()
    {
        return $this->cjStart;
    }

    /**
     * Set cjEnd
     *
     * @param \DateTime $cjEnd
     *
     * @return PropertyUnitPricesRange
     */
    public function setCjEnd($cjEnd)
    {
        $this->cjEnd = $cjEnd;

        return $this;
    }

    /**
     * Get cjEnd
     *
     * @return \DateTime
     */
    public function getCjEnd()
    {
        return $this->cjEnd;
    }

    /**
     * Set cjValuta
     *
     * @param string $cjValuta
     *
     * @return PropertyUnitPricesRange
     */
    public function setCjValuta($cjValuta)
    {
        $this->cjValuta = $cjValuta;

        return $this;
    }

    /**
     * Get cjValuta
     *
     * @return string
     */
    public function getCjValuta()
    {
        return $this->cjValuta;
    }

    /**
     * Set cjTip
     *
     * @param integer $cjTip
     *
     * @return PropertyUnitPricesRange
     */
    public function setCjTip($cjTip)
    {
        $this->cjTip = $cjTip;

        return $this;
    }

    /**
     * Get cjTip
     *
     * @return int
     */
    public function getCjTip()
    {
        return $this->cjTip;
    }

    /**
     * Set cjNomOsoba
     *
     * @param integer $cjNomOsoba
     *
     * @return PropertyUnitPricesRange
     */
    public function setCjNomOsoba($cjNomOsoba)
    {
        $this->cjNomOsoba = $cjNomOsoba;

        return $this;
    }

    /**
     * Get cjNomOsoba
     *
     * @return int
     */
    public function getCjNomOsoba()
    {
        return $this->cjNomOsoba;
    }

    /**
     * Set cjNomOsobaExtrabed
     *
     * @param integer $cjNomOsobaExtrabed
     *
     * @return PropertyUnitPricesRange
     */
    public function setCjNomOsobaExtrabed($cjNomOsobaExtrabed)
    {
        $this->cjNomOsobaExtrabed = $cjNomOsobaExtrabed;

        return $this;
    }

    /**
     * Get cjNomOsobaExtrabed
     *
     * @return int
     */
    public function getCjNomOsobaExtrabed()
    {
        return $this->cjNomOsobaExtrabed;
    }

    /**
     * Set shortstayPer
     *
     * @param string $shortstayPer
     *
     * @return PropertyUnitPricesRange
     */
    public function setShortstayPer($shortstayPer)
    {
        $this->shortstayPer = $shortstayPer;

        return $this;
    }

    /**
     * Get shortstayPer
     *
     * @return string
     */
    public function getShortstayPer()
    {
        return $this->shortstayPer;
    }

    /**
     * Set nrpersonPer
     *
     * @param string $nrpersonPer
     *
     * @return PropertyUnitPricesRange
     */
    public function setNrpersonPer($nrpersonPer)
    {
        $this->nrpersonPer = $nrpersonPer;

        return $this;
    }

    /**
     * Get nrpersonPer
     *
     * @return string
     */
    public function getNrpersonPer()
    {
        return $this->nrpersonPer;
    }

    /**
     * Set nrpersonunitPer
     *
     * @param string $nrpersonunitPer
     *
     * @return PropertyUnitPricesRange
     */
    public function setNrpersonunitPer($nrpersonunitPer)
    {
        $this->nrpersonunitPer = $nrpersonunitPer;

        return $this;
    }

    /**
     * Get nrpersonunitPer
     *
     * @return string
     */
    public function getNrpersonunitPer()
    {
        return $this->nrpersonunitPer;
    }

    /**
     * Set restax
     *
     * @param integer $restax
     *
     * @return PropertyUnitPricesRange
     */
    public function setRestax($restax)
    {
        $this->restax = $restax;

        return $this;
    }

    /**
     * Get restax
     *
     * @return int
     */
    public function getRestax()
    {
        return $this->restax;
    }

    /**
     * Set restaxStatus
     *
     * @param string $restaxStatus
     *
     * @return PropertyUnitPricesRange
     */
    public function setRestaxStatus($restaxStatus)
    {
        $this->restaxStatus = $restaxStatus;

        return $this;
    }

    /**
     * Get restaxStatus
     *
     * @return string
     */
    public function getRestaxStatus()
    {
        return $this->restaxStatus;
    }

    /**
     * Set salechannels
     *
     * @param string $salechannels
     *
     * @return PropertyUnitPricesRange
     */
    public function setSalechannels($salechannels)
    {
        $this->salechannels = $salechannels;

        return $this;
    }

    /**
     * Get salechannels
     *
     * @return string
     */
    public function getSalechannels()
    {
        return $this->salechannels;
    }

    /**
     * Set dobAStart
     *
     * @param string $dobAStart
     *
     * @return PropertyUnitPricesRange
     */
    public function setDobAStart($dobAStart)
    {
        $this->dobAStart = $dobAStart;

        return $this;
    }

    /**
     * Get dobAStart
     *
     * @return string
     */
    public function getDobAStart()
    {
        return $this->dobAStart;
    }

    /**
     * Set dobAEnd
     *
     * @param string $dobAEnd
     *
     * @return PropertyUnitPricesRange
     */
    public function setDobAEnd($dobAEnd)
    {
        $this->dobAEnd = $dobAEnd;

        return $this;
    }

    /**
     * Get dobAEnd
     *
     * @return string
     */
    public function getDobAEnd()
    {
        return $this->dobAEnd;
    }

    /**
     * Set dobBStart
     *
     * @param string $dobBStart
     *
     * @return PropertyUnitPricesRange
     */
    public function setDobBStart($dobBStart)
    {
        $this->dobBStart = $dobBStart;

        return $this;
    }

    /**
     * Get dobBStart
     *
     * @return string
     */
    public function getDobBStart()
    {
        return $this->dobBStart;
    }

    /**
     * Set dobBEnd
     *
     * @param string $dobBEnd
     *
     * @return PropertyUnitPricesRange
     */
    public function setDobBEnd($dobBEnd)
    {
        $this->dobBEnd = $dobBEnd;

        return $this;
    }

    /**
     * Get dobBEnd
     *
     * @return string
     */
    public function getDobBEnd()
    {
        return $this->dobBEnd;
    }

    /**
     * Set dobCStart
     *
     * @param string $dobCStart
     *
     * @return PropertyUnitPricesRange
     */
    public function setDobCStart($dobCStart)
    {
        $this->dobCStart = $dobCStart;

        return $this;
    }

    /**
     * Get dobCStart
     *
     * @return string
     */
    public function getDobCStart()
    {
        return $this->dobCStart;
    }

    /**
     * Set dobCEnd
     *
     * @param string $dobCEnd
     *
     * @return PropertyUnitPricesRange
     */
    public function setDobCEnd($dobCEnd)
    {
        $this->dobCEnd = $dobCEnd;

        return $this;
    }

    /**
     * Get dobCEnd
     *
     * @return string
     */
    public function getDobCEnd()
    {
        return $this->dobCEnd;
    }

    /**
     * Set dobDStart
     *
     * @param string $dobDStart
     *
     * @return PropertyUnitPricesRange
     */
    public function setDobDStart($dobDStart)
    {
        $this->dobDStart = $dobDStart;

        return $this;
    }

    /**
     * Get dobDStart
     *
     * @return string
     */
    public function getDobDStart()
    {
        return $this->dobDStart;
    }

    /**
     * Set dobDEnd
     *
     * @param string $dobDEnd
     *
     * @return PropertyUnitPricesRange
     */
    public function setDobDEnd($dobDEnd)
    {
        $this->dobDEnd = $dobDEnd;

        return $this;
    }

    /**
     * Get dobDEnd
     *
     * @return string
     */
    public function getDobDEnd()
    {
        return $this->dobDEnd;
    }

    /**
     * Set dobPer
     *
     * @param string $dobPer
     *
     * @return PropertyUnitPricesRange
     */
    public function setDobPer($dobPer)
    {
        $this->dobPer = $dobPer;

        return $this;
    }

    /**
     * Get dobPer
     *
     * @return string
     */
    public function getDobPer()
    {
        return $this->dobPer;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return PropertyUnitPricesRange
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set noteHr
     *
     * @param string $noteHr
     *
     * @return PropertyUnitPricesRange
     */
    public function setNoteHr($noteHr)
    {
        $this->noteHr = $noteHr;

        return $this;
    }

    /**
     * Get noteHr
     *
     * @return string
     */
    public function getNoteHr()
    {
        return $this->noteHr;
    }

    /**
     * Set noteEn
     *
     * @param string $noteEn
     *
     * @return PropertyUnitPricesRange
     */
    public function setNoteEn($noteEn)
    {
        $this->noteEn = $noteEn;

        return $this;
    }

    /**
     * Get noteEn
     *
     * @return string
     */
    public function getNoteEn()
    {
        return $this->noteEn;
    }

    /**
     * Set noteDe
     *
     * @param string $noteDe
     *
     * @return PropertyUnitPricesRange
     */
    public function setNoteDe($noteDe)
    {
        $this->noteDe = $noteDe;

        return $this;
    }

    /**
     * Get noteDe
     *
     * @return string
     */
    public function getNoteDe()
    {
        return $this->noteDe;
    }

    /**
     * Set noteIt
     *
     * @param string $noteIt
     *
     * @return PropertyUnitPricesRange
     */
    public function setNoteIt($noteIt)
    {
        $this->noteIt = $noteIt;

        return $this;
    }

    /**
     * Get noteIt
     *
     * @return string
     */
    public function getNoteIt()
    {
        return $this->noteIt;
    }

    /**
     * Set noteSi
     *
     * @param string $noteSi
     *
     * @return PropertyUnitPricesRange
     */
    public function setNoteSi($noteSi)
    {
        $this->noteSi = $noteSi;

        return $this;
    }

    /**
     * Get noteSi
     *
     * @return string
     */
    public function getNoteSi()
    {
        return $this->noteSi;
    }

    /**
     * Set rabat
     *
     * @param string $rabat
     *
     * @return PropertyUnitPricesRange
     */
    public function setRabat($rabat)
    {
        $this->rabat = $rabat;

        return $this;
    }

    /**
     * Get rabat
     *
     * @return string
     */
    public function getRabat()
    {
        return $this->rabat;
    }

    /**
     * Set dobavnaProvizija
     *
     * @param string $dobavnaProvizija
     *
     * @return PropertyUnitPricesRange
     */
    public function setDobavnaProvizija($dobavnaProvizija)
    {
        $this->dobavnaProvizija = $dobavnaProvizija;

        return $this;
    }

    /**
     * Get dobavnaProvizija
     *
     * @return string
     */
    public function getDobavnaProvizija()
    {
        return $this->dobavnaProvizija;
    }

    /**
     * Set llNocenje
     *
     * @param string $llNocenje
     *
     * @return PropertyUnitPricesRange
     */
    public function setLlNocenje($llNocenje)
    {
        $this->llNocenje = $llNocenje;

        return $this;
    }

    /**
     * Get llNocenje
     *
     * @return string
     */
    public function getLlNocenje()
    {
        return $this->llNocenje;
    }

    /**
     * Set llNocenjedorucak
     *
     * @param string $llNocenjedorucak
     *
     * @return PropertyUnitPricesRange
     */
    public function setLlNocenjedorucak($llNocenjedorucak)
    {
        $this->llNocenjedorucak = $llNocenjedorucak;

        return $this;
    }

    /**
     * Get llNocenjedorucak
     *
     * @return string
     */
    public function getLlNocenjedorucak()
    {
        return $this->llNocenjedorucak;
    }

    /**
     * Set llPolupansion
     *
     * @param string $llPolupansion
     *
     * @return PropertyUnitPricesRange
     */
    public function setLlPolupansion($llPolupansion)
    {
        $this->llPolupansion = $llPolupansion;

        return $this;
    }

    /**
     * Get llPolupansion
     *
     * @return string
     */
    public function getLlPolupansion()
    {
        return $this->llPolupansion;
    }

    /**
     * Set llPansion
     *
     * @param string $llPansion
     *
     * @return PropertyUnitPricesRange
     */
    public function setLlPansion($llPansion)
    {
        $this->llPansion = $llPansion;

        return $this;
    }

    /**
     * Get llPansion
     *
     * @return string
     */
    public function getLlPansion()
    {
        return $this->llPansion;
    }

    /**
     * Set llDnevni
     *
     * @param string $llDnevni
     *
     * @return PropertyUnitPricesRange
     */
    public function setLlDnevni($llDnevni)
    {
        $this->llDnevni = $llDnevni;

        return $this;
    }

    /**
     * Get llDnevni
     *
     * @return string
     */
    public function getLlDnevni()
    {
        return $this->llDnevni;
    }

    /**
     * Set llAllinc
     *
     * @param string $llAllinc
     *
     * @return PropertyUnitPricesRange
     */
    public function setLlAllinc($llAllinc)
    {
        $this->llAllinc = $llAllinc;

        return $this;
    }

    /**
     * Get llAllinc
     *
     * @return string
     */
    public function getLlAllinc()
    {
        return $this->llAllinc;
    }

    /**
     * Set laserUgovor
     *
     * @param string $laserUgovor
     *
     * @return PropertyUnitPricesRange
     */
    public function setLaserUgovor($laserUgovor)
    {
        $this->laserUgovor = $laserUgovor;

        return $this;
    }

    /**
     * Get laserUgovor
     *
     * @return string
     */
    public function getLaserUgovor()
    {
        return $this->laserUgovor;
    }

    /**
     * Set laserCjenik
     *
     * @param string $laserCjenik
     *
     * @return PropertyUnitPricesRange
     */
    public function setLaserCjenik($laserCjenik)
    {
        $this->laserCjenik = $laserCjenik;

        return $this;
    }

    /**
     * Get laserCjenik
     *
     * @return string
     */
    public function getLaserCjenik()
    {
        return $this->laserCjenik;
    }

    /**
     * Set lastchange
     *
     * @param \DateTime $lastchange
     *
     * @return PropertyUnitPricesRange
     */
    public function setLastchange($lastchange)
    {
        $this->lastchange = $lastchange;

        return $this;
    }

    /**
     * Get lastchange
     *
     * @return \DateTime
     */
    public function getLastchange()
    {
        return $this->lastchange;
    }

    /**
     * Set atdNapomena
     *
     * @param string $atdNapomena
     *
     * @return PropertyUnitPricesRange
     */
    public function setAtdNapomena($atdNapomena)
    {
        $this->atdNapomena = $atdNapomena;

        return $this;
    }

    /**
     * Get atdNapomena
     *
     * @return string
     */
    public function getAtdNapomena()
    {
        return $this->atdNapomena;
    }

    /**
     * Set defaultni
     *
     * @param integer $defaultni
     *
     * @return PropertyUnitPricesRange
     */
    public function setDefaultni($defaultni)
    {
        $this->defaultni = $defaultni;

        return $this;
    }

    /**
     * Get defaultni
     *
     * @return int
     */
    public function getDefaultni()
    {
        return $this->defaultni;
    }

    /**
     * Set cjenicig
     *
     * @param integer $cjenicig
     *
     * @return PropertyUnitPricesRange
     */
    public function setCjenicig($cjenicig)
    {
        $this->cjenicig = $cjenicig;

        return $this;
    }

    /**
     * Get cjenicig
     *
     * @return int
     */
    public function getCjenicig()
    {
        return $this->cjenicig;
    }

    /**
     * Set idCompany
     *
     * @param integer $idCompany
     *
     * @return PropertyUnitPricesRange
     */
    public function setIdCompany($idCompany)
    {
        $this->idCompany = $idCompany;

        return $this;
    }

    /**
     * Get idCompany
     *
     * @return int
     */
    public function getIdCompany()
    {
        return $this->idCompany;
    }

    /**
     * Set idPartner
     *
     * @param integer $idPartner
     *
     * @return PropertyUnitPricesRange
     */
    public function setIdPartner($idPartner)
    {
        $this->idPartner = $idPartner;

        return $this;
    }

    /**
     * Get idPartner
     *
     * @return int
     */
    public function getIdPartner()
    {
        return $this->idPartner;
    }

    /**
     * Set idGrupaPartner
     *
     * @param integer $idGrupaPartner
     *
     * @return PropertyUnitPricesRange
     */
    public function setIdGrupaPartner($idGrupaPartner)
    {
        $this->idGrupaPartner = $idGrupaPartner;

        return $this;
    }

    /**
     * Get idGrupaPartner
     *
     * @return int
     */
    public function getIdGrupaPartner()
    {
        return $this->idGrupaPartner;
    }

    /**
     * Set frontprikaz
     *
     * @param string $frontprikaz
     *
     * @return PropertyUnitPricesRange
     */
    public function setFrontprikaz($frontprikaz)
    {
        $this->frontprikaz = $frontprikaz;

        return $this;
    }

    /**
     * Get frontprikaz
     *
     * @return string
     */
    public function getFrontprikaz()
    {
        return $this->frontprikaz;
    }

    /**
     * Set sezone
     *
     * @param integer $sezone
     *
     * @return PropertyUnitPricesRange
     */
    public function setSezone($sezone)
    {
        $this->sezone = $sezone;

        return $this;
    }

    /**
     * Get sezone
     *
     * @return int
     */
    public function getSezone()
    {
        return $this->sezone;
    }

    /**
     * Set tipskiB2bd
     *
     * @param integer $tipskiB2bd
     *
     * @return PropertyUnitPricesRange
     */
    public function setTipskiB2bd($tipskiB2bd)
    {
        $this->tipskiB2bd = $tipskiB2bd;

        return $this;
    }

    /**
     * Get tipskiB2bd
     *
     * @return int
     */
    public function getTipskiB2bd()
    {
        return $this->tipskiB2bd;
    }

    /**
     * Set provider
     *
     * @param string $provider
     *
     * @return PropertyUnitPricesRange
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return string
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return PropertyUnitPricesRange
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return bool
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
}
