<?php

namespace td\CMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use td\CMBundle\Entity\Portal;
use td\CMBundle\Entity\PortalCode;
use JMS;

/**
 * Class PortalJsonController
 * @package td\CMBundle\Controller
 */
class PortalJsonController extends Controller
{

    /**
     * BACKEND
     * Get all portals JSON
     *
     * @return Response
     */
    public function allPortalsJsonAction()
    {
//        $portals = $this->getDoctrine()->getManager()->getRepository('tdCMBundle:Portal')->findAll();
//        $serializer = JMS\Serializer\SerializerBuilder::create()->build();
//        $portalsJson = $serializer->serialize($portals, 'json');
//        $response = new Response($portalsJson, 200, array('content-type' => 'application/json'));
////        dump($portals);
////        $response = new Response($portalsJson,200);
//        return $response;
        $loginData = $this->get('td_cm.obrada.cookie_obrada');
        $data = array(
            'status' => 'Error!',
            'message' => 'You have no rights!',
        );

        if ($loginData->obradaCookie()) {
            $data = $this->getDoctrine()->getManager()->getRepository('tdCMBundle:Portal')->findAllBackendList();
        }
        return new JsonResponse($data);
    }

    /**
     * Save portal data for update action
     *
     * @param Portal $portal
     * @return JsonResponse
     */
    public function saveJsonAction(Portal $portal)
    {
        $request = $this->get('request');

        $dataJson = $request->getContent();
        $data = json_decode($dataJson, true);

        return new JsonResponse($this->savePortalParameters($portal, $data));
    }

    /**
     * Save portal data for create action
     *
     * @return JsonResponse
     */
    public function createJsonAction()
    {
        $request = $this->get('request');
        $dataJson = $request->getContent();
        $data = json_decode($dataJson, true);

        $portal = new Portal();

        return new JsonResponse($this->savePortalParameters($portal, $data));
    }

    /**
     * Get all portal codes. Mostly for select control
     *
     * @return JsonResponse
     */
    public function portalCodesAction()
    {
        $portalCodes = $this->getDoctrine()->getManager()->getRepository('tdCMBundle:PortalCode')->createQueryBuilder('pc')
            ->select('pc.id')
            ->addSelect('pc.description')
            ->orderBy('pc.id')
            ->getQuery()
            ->getResult();

        return new JsonResponse($portalCodes);
    }

    /**
     * Save portal data from update or create action
     *
     * @param Portal $portal
     * @param $data
     * @return array
     */
    private function savePortalParameters(Portal $portal, array $data)
    {
        $info = array();
        $enManager = $this->get('doctrine.orm.default_entity_manager');
        try {
            $portal->setNaziv($data['naziv']);
            $portal->setCode($enManager->getReference('tdCMBundle:PortalCode',$data['code']));
            $portal->setPortalGroup($enManager->getReference('tdCMBundle:PortalGroup',$data['group']));
            $portal->setLink($data['link']);
            $portal->setContactEmail($data['contactEmail']);
            $portal->setContactTel($data['contactTel']);
            $portal->setContactPerson($data['contactPerson']);
            $portal->setDescription($data['description']);
            $portal->setViewTypeConnect($data['viewTypeConnect']);
            $portal->setViewTypeSetup($data['viewTypeSetup']);
            $portal->setThumbnail($data['thumbnail']);
            $portal->setLinkPropertyOnPortal($data['linkPropertyOnPortal']);
            $portal->setLinkPropertyUnitOnPortal($data['linkPropertyUnitOnPortal']);
            $portal->setManageActivityProperty($data['manageActivityProperty']);
            $portal->setManageActivityPropertyUnit($data['manageActivityPropertyUnit']);
            $portal->setConnectProperty($data['connectProperty']);
            $portal->setConnectPropertyUnit($data['connectPropertyUnit']);
            $portal->setMustHaveWebshop($data['mustHaveWebshop']);
            $portal->setProcessChange($data['processChange']);
            $portal->setSettingsTemplate($data['settingsTemplate']);

            $this->getDoctrine()->getManager()->persist($portal);
            $this->getDoctrine()->getManager()->flush();
            $info['ok'] = 1;
        } catch (Exception $e) {
            $info['ok'] = 0;
        }

        return $info;
    }

}
