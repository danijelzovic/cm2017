<?php
namespace td\CMBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class User
{
    /**
     * @var integer 
     *
     * @ORM\Id @ORM\Column(name="id", type="integer")
     * 
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string 
     *
     * @ORM\Column(type="string", length=255, nullable=true, unique=false)
     */
    protected $firstName;

    /**
     * @var string 
     *
     * @ORM\Column(type="string", length=255, nullable=true, unique=false)
     */
    protected $lastName;

    /**
     * @var boolean 
     *
     * @ORM\Column(type="boolean")
     */
    protected $enabled = false;

    /**
     * @ORM\OneToMany(targetEntity="Order",  mappedBy="user", cascade={"all"}, orphanRemoval=true)
     */  
    protected $orders; 

    public function __construct()
    {
        $this->orders = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    public function getFistName()
    {
        return $this->firstName;
    }

    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    public function isEnabled()
    {
        return $this->enabled;
    }

    public function addOrder(Order $order)
    {
        if (!$this->orders->contains($order)){
            $this->orders[] = $order;
            $order->setUser($this);
        }

        return $this; 

    }

    public function getOrders()
    {
        return $this->orders;
    }

    public function removeOrder(Order $order)
    {
        if ($this->orders->contains($order)){
            $this->orders->remove($order);
            $order->setUser(null);
        }

        return $this;
    }


    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
}
