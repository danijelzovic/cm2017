<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Exclude;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * PortalGroup
 *
 * @ORM\Table(name="cm_portal_group")
 * @ORM\Entity(repositoryClass="td\CMBundle\Entity\Repository\PortalGroupRepository")
 * @UniqueEntity("name", message="Ovaj naziv grupe se već koristi!")
 */
class PortalGroup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255)
     */
    private $code;

    /**
     * @var int
     *
     * @ORM\Column(name="order_group", type="integer")
     */
    private $orderGroup;

    /**
     * @ORM\OneToMany(targetEntity="Portal", mappedBy="portalGroup")
     * @Exclude
     */
    private $portals;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PortalGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return PortalGroup
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->portals = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add portal
     *
     * @param \td\CMBundle\Entity\Portal $portal
     *
     * @return PortalGroup
     */
    public function addPortal(\td\CMBundle\Entity\Portal $portal)
    {
        $this->portals[] = $portal;

        return $this;
    }

    /**
     * Remove portal
     *
     * @param \td\CMBundle\Entity\Portal $portal
     */
    public function removePortal(\td\CMBundle\Entity\Portal $portal)
    {
        $this->portals->removeElement($portal);
    }

    /**
     * Get portals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPortals()
    {
        return $this->portals;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return PortalGroup
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set orderGroup
     *
     * @param integer $orderGroup
     * @return PortalGroup
     */
    public function setOrderGroup($orderGroup)
    {
        $this->orderGroup = $orderGroup;

        return $this;
    }

    /**
     * Get orderGroup
     *
     * @return integer 
     */
    public function getOrderGroup()
    {
        return $this->orderGroup;
    }
}
