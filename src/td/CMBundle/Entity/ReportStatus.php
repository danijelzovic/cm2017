<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ReportStatus
 *
 * @ORM\Table(name="cm_report_status")
 * @ORM\Entity
 */
class ReportStatus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_korisnik", type="integer")
     */
    private $idKorisnik;

    /**
     * @var string
     *
     * @ORM\Column(name="Email", type="string", length=255)
     */
    private $email;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set email
     *
     * @param string $email
     * @return ReportStatus
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set idKorisnik
     *
     * @param integer $idKorisnik
     * @return ReportStatus
     */
    public function setIdKorisnik($idKorisnik)
    {
        $this->idKorisnik = $idKorisnik;

        return $this;
    }

    /**
     * Get idKorisnik
     *
     * @return integer 
     */
    public function getIdKorisnik()
    {
        return $this->idKorisnik;
    }
}
