<?php

namespace td\CMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use td\CMBundle\Entity\SetUp;
use td\CMBundle\Entity\Portal;
use td\CMBundle\Entity\ClientTags;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\PhpBridgeSessionStorage;
use JMS;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;


class DefaultController extends Controller
{

    private $idCompanyKorisnik = 1;
    private $menu;

    //Upravljanje sesijom - za sada se koristit cookies
    private function sesija()
    {
        $array = explode(":", $_COOKIE['tdapplogin']);
        if (crypt($array[0], 'he6765zfh') == $array[1]) {
            $this->idCompanyKorisnik = $array[4];
            $companyStatus = $array[6];
            $em = $this->getDoctrine()->getManager();
            $connection = $em->getConnection();
            $statement = $connection->prepare("SELECT menu_path FROM company_status WHERE id= :id");
            $statement->bindValue('id', $companyStatus);
            $statement->execute();
            $results = $statement->fetchAll();
            $row = $results[0];
            $this->menu = $row['menu_path'];
        } else {
            header("location: https://agencije.atrade.hr");
            exit;
        }
    }


    /**
     * @return Response
     */
    public function index2Action()
    {
        $em = $this->getDoctrine()->getManager();
        $portalCount = $em->getRepository('tdCMBundle:Portal')->createQueryBuilder('p')
            ->select('count(p.id)')
            ->getQuery()
            ->getSingleScalarResult();
        $spCount = $em->getRepository('tdCMBundle:SetupProperty')->createQueryBuilder('sp')
            ->select('count(sp.id)')
            ->getQuery()
            ->getSingleScalarResult();
        $spuCount = $em->getRepository('tdCMBundle:SetupPropertyUnit')->createQueryBuilder('spu')
            ->select('count(spu.id)')
            ->getQuery()
            ->getSingleScalarResult();
        $pkCount = $em->getRepository('tdCMBundle:PortalKorisnik')->createQueryBuilder('pk')
            ->select('count(pk.id)')
            ->getQuery()
            ->getSingleScalarResult();
        $promjenaCount = $em->getRepository('tdCMBundle:Promjena')->createQueryBuilder('promjena')
            ->select('count(promjena.id)')
            ->getQuery()
            ->getSingleScalarResult();
        $transferCount = $em->getRepository('tdCMBundle:Transfer')->createQueryBuilder('transfer')
            ->select('count(transfer.id)')
            ->getQuery()
            ->getSingleScalarResult();
        $transferNotTransferedCount = $em->getRepository('tdCMBundle:Transfer')->createQueryBuilder('transferNotTransfered')
            ->select('count(transferNotTransfered.id)')
            ->where('transferNotTransfered.ok = :ok')
            ->setParameter('ok',false)
            ->getQuery()
            ->getSingleScalarResult();

        return $this->render('@tdCM/Default/index2.html.twig', array(
            'portalCount' => $portalCount,
            'spCount' => $spCount,
            'spuCount' => $spuCount,
            'pkCount' => $pkCount,
            'promjenaCount' => $promjenaCount,
            'transferCount' => $transferCount,
            'transferNotTransferedCount' => $transferNotTransferedCount,
        ));
    }

    /**
     * Show this page when login cookie session expire
     *
     * @return Response
     */
    public function loginAgainAction()
    {
        return $this->render('@tdCM/Default/loginAgain.html.twig');
    }

    //Funkcija za pripremu podataka za jqGridClient
    public function clientAction(Request $request)
    {
        //ako je postavljen parametar tagova u requestu onda moran napravit traženje po tagovima

        $pager = $this->get('datatheke.pager')->createPager('tdCMBundle:Client');

//            $filtersJson = $request->query->get('filters');
//            $filters = json_decode($filtersJson);
//            $filters = [1,2,3,4];
//            if($request->query->has('filters')) return new JsonResponse($filters);

        if ($filtersJson = $request->query->get('filters')) {
            //Ovo dela samo na PHP-u novijen od 5.2 i više. Ipak je na serveru 5.3.3.
            $filters = json_decode($filtersJson, TRUE);
//                $filters = moj_json_encode($filtersJson);

//                $serializer = JMS\Serializer\SerializerBuilder::create()->build();
//                $tagovis = $serializer->serialize($tagovi, 'json');

//                $encoders = array(new XmlEncoder(), new JsonEncoder());
//                $normalizers = array(new ObjectNormalizer());


//                $serializer = new Serializer(array(), $encoders);
//                
//                $filters = $serializer->decode($filtersJson, 'json');

            $rules = $filters['rules'];
            $noviFilters = array();
            $noviRules = array();
            $noviFilters['groupOp'] = $filters['groupOp'];

            foreach ($rules as $r) {
                if ($r['field'] != 'Tagovi') {
//                        $searchFields[] = $r['field'];
//                        $searchData[] = $r['data'];
//                        $searchOperators[] = $opMap[ $r['op'] ];
                    $noviRules[] = $r;
                } else {
                    $qb = $pager->getAdapter()->getQueryBuilder()
                        ->join('tdCMBundle:ClientTags', 'ct', 'WITH', 'e.idClient = ct.idClient')
                        ->where('ct.tag LIKE :tag')
//                            ->setParameter('tag','%' . $searchData[0] . '%');
                        ->setParameter('tag', '%' . $r['data'] . '%');

//                        $filtersJson = json_encode($qb)
                }
            }

            //Obnova filtersa
            $noviFilters['rules'] = $noviRules;
            $noviFiltersJson = json_encode($noviFilters);
//                $noviFiltersJson = $serializer->encode($noviFilters, 'json');
            $request->query->remove('filters');
            $request->query->set('filters', $noviFiltersJson);


//                if($searchFields[0] == 'Tagovi')
//                {
////                    $qbtag = $this->getDoctrine()->getManager()->createQueryBuilder();
//
//                    $qb = $pager->getAdapter()->getQueryBuilder()
//                            ->join('tdCMBundle:ClientTags','ct', 'WITH', 'e.idClient = ct.idClient')
//                            ->where('ct.tag LIKE :tag')
////                            ->setParameter('tag','%' . $searchData[0] . '%');
//                            ->setParameter('tag','%' . 'tag' . '%');
//                }
        }

        $datagrid = $this->get('datatheke.datagrid')->createHttpDataGrid($pager, array(), 'jqgrid');
        //            $datagrid = $this->get('datatheke.datagrid')->createHttpDataGrid('tdCMBundle:Client', array(), 'jqgrid');

        return $datagrid->handleRequest($request);


        //            $view = $pager->handleRequest($this->getRequest());

        //            return array('pager' => $view);
    }

    //kontroler za pripremu podataka za tagove
    //pogledat kako san napravi za propertyUnite za pregled povezanih
    public function tagDataAction(Request $request)
    {

        $idClient = $request->query->get('idClient');

        //S obzirom da koristin loadonce mora san stavit da mi inicijalno pošalje sve redove
//            $request->query->set('rows', '10000');
        //Staro	- DATATHEKE

        $pager = $this->get('datatheke.pager')->createPager('tdCMBundle:ClientTags');

        /**
         * Customize the QueryBuilder
         */

        if ($idClient != "") {
            $qb = $pager->getAdapter()->getQueryBuilder();
            //$qb->addSelect('pu')->innerJoin('e.propertyUnit', 'pu');

            $qb->select('e')
                ->where(
                //$qb->expr()->andX(
                    $qb->expr()->eq('e.idClient', '?1')
                //$qb->expr()->isNotNull('e.idPropertyUnitNaPortalu')
                //)
                );
            //            $qb->addSelect('a')->leftJoin('e.adress', 'a');
            $qb->setParameters(array(1 => $idClient));
        }

        // Add field 'city' to the pager
//            $pager->getAdapter()->addField(new Field('adress.city', Field::TYPE_STRING, 'a.city'), 'city');
        //$pager->getAdapter()->addField(new Field('propertyUnit.name', Field::TYPE_STRING, 'pu.name'), 'name');

        /**
         * Create the DataGrid
         */

        $datagrid = $this->get('datatheke.datagrid')->createHttpDataGrid($pager, array(), 'jqgrid');
//            $datagrid = $this->get('datatheke.datagrid')->createHttpDataGrid('tdCMBundle:SetupGrid', array(), 'jqgrid');

        return $datagrid->handleRequest($request);

    }

    //controller predložak
    public function predlozakAction()
    {
        return $this->render('tdCMBundle:Default:predlozak.html.twig');
//        return $this->render('tdCMBundle:Default:predlozak.html.twig', 
//            array(  'povezani' => $povezani, 
//                'nepovezani' => $nepovezani,
//                'portali' => $portali,
//                'idCompanyKorisnik' => $idCompanyKorisnik,
//                'debug' => $debug,
//                'menu' => $this->menu
//            ));
    }


    //controller testiranje filter na gridu
    public function filterAction()
    {
//        $this->sesija();
        $em = $this->getDoctrine()->getManager();

        //$tagovi = $em->getRepository("tdCMBundle:ClientTags")->findByIdCompany($this->idCompanyKorisnik);
        $query = $em->createQuery(
            "SELECT distinct(ct.tag) as tag FROM tdCMBundle:ClientTags ct WHERE ct.idCompany = :idCompany"
        );

        $query->setParameters(array(
            'idCompany' => $this->idCompanyKorisnik
        ));
        $tagovi = $query->getResult();

        $serializer = JMS\Serializer\SerializerBuilder::create()->build();
        $tagovis = $serializer->serialize($tagovi, 'json');

        return $this->render('tdCMBundle:Default:filter.html.twig',
//        return $this->render('tdCMBundle:Default:predlozak.html.twig', 
            array('tagovis' => $tagovis,
//                'nepovezani' => $nepovezani,
//                'portali' => $portali,
//                'idCompanyKorisnik' => $idCompanyKorisnik,
//                'debug' => $debug,
                'menu' => $this->menu
            ));
    }

    public function tagPersistRowAction(Request $request)
    {
        if (isset($request)) {
            $em = $this->getDoctrine()->getManager();

            $clientTags = new ClientTags();
            $clientTags->setIdClient($request->request->get('idClient'));
            $clientTags->setIdCompany($request->request->get('idCompany'));
            $clientTags->setTag($request->request->get('tag'));
            $clientTags->setVrijeme(new \DateTime('now'));
            $clientTags->setIdUserClient(0);
            $clientTags->setMostImport(0);

            $em->persist($clientTags);
            $em->flush();
        }

        //OVo san stori da rasteretin server. Bitno je dakle tornat neki HTTP response.
        $response = new Response(
            'Content',
            Response::HTTP_OK,
            array('content-type' => 'text/html')
        );
        return $response;
    }

    //AJAX brisanje tagova
    public function tagBrisiAction(Request $request)
    {
        if (isset($request)) {
            $em = $this->getDoctrine()->getManager();

            $clientTag = $em->getRepository('tdCMBundle:ClientTags')->findOneBy(
                array(
                    'idClient' => $request->request->get('idClient'),
                    'tag' => $request->request->get('tag')
                )
            );

            $em->remove($clientTag);
            $em->flush();
        }

        //OVo san stori da rasteretin server. Bitno je dakle tornat neki HTTP response.
        $response = new Response(
            'Content',
            Response::HTTP_OK,
            array('content-type' => 'text/html')
        );
        return $response;
    }

    /**
     * @param Request $request
     * @return JsonResponse|Response
     * @throws \Exception
     */
    public function autocompleteCLientAction(Request $request)
    {
        //$datagrid = $this->get('datatheke.datagrid')->createHttpDataGrid('tdCMBundle:Client', array(), 'autocomplete');

        //return $datagrid->handleRequest($request);

        //Ručno ću isprogramirat jer mi ne dela kako treba i upit vraća cijeli
        //Potrebno optimizirat
        if (isset($request)) {
            $em = $this->getDoctrine()->getManager();

            //Provjeri koje polje se traži
            if (!is_null($value = $request->query->get('naziv'))) {
                $var = 'naziv';
            } elseif (!is_null($value = $request->query->get('company'))) {
                $var = 'company';
            } elseif (!is_null($value = $request->query->get('address'))) {
                $var = 'address';
            } elseif (!is_null($value = $request->query->get('place'))) {
                $var = 'place';
            } elseif (!is_null($value = $request->query->get('country'))) {
                $var = 'country';
            } elseif (!is_null($value = $request->query->get('tel'))) {
                $var = 'tel';
            } elseif (!is_null($value = $request->query->get('email'))) {
                $var = 'email';
            } elseif (!is_null($value = $request->query->get('tag'))) {
                $var = 'tag';
            }

            if ($var != 'tag') {
                $query = $em->createQuery(
                    'SELECT DISTINCT c.' . $var . '
                                    FROM tdCMBundle:Client c
                                    WHERE c.' . $var . ' LIKE :value'
                )
                    ->setMaxResults(20);
                $query->setParameter('value', '%' . $value . '%');
                $data = $query->getResult();

            } else {
                $query = $em->createQuery(
                    'SELECT DISTINCT c.' . $var . '
                                    FROM tdCMBundle:ClientTags c
                                    WHERE c.' . $var . ' LIKE :value'
                )
                    ->setMaxResults(20);
                $query->setParameter('value', '%' . $value . '%');
                $data = $query->getResult();
            }

            $response = new JsonResponse();
//            $response->setData(array(array('naziv' => $klijenti[0]->getNaziv() )));
            $response->setData($data);

        } else {
            $response = new Response(
                'Content',
                Response::HTTP_OK,
                array('content-type' => 'text/html')
            );
        }

        return $response;
    }

    public function tagTraziAction(Request $request)
    {

    }

    public function filterdataAction(Request $request)
    {
        //S obzirom da koristin loadonce mora san stavit da mi inicijalno pošalje sve redove
        //$request->query->set('rows', '10000');
        //Staro	- DATATHEKE

        $pager = $this->get('datatheke.pager')->createPager('tdCMBundle:Rate');

        /**
         * Customize the QueryBuilder
         */

        // Retrieve QueryBuilder to join on the Adress Entity
//            $qb = $pager->getAdapter()->getQueryBuilder();
//            $qb->where(
//                    $qb->expr()->eq('e.idSetup', 1576)
//                    );
//            $qb->addSelect('a')->leftJoin('e.adress', 'a');

        // Add field 'city' to the pager
//            $pager->getAdapter()->addField(new Field('adress.city', Field::TYPE_STRING, 'a.city'), 'city');

        /**
         * Create the DataGrid
         */

        $datagrid = $this->get('datatheke.datagrid')->createHttpDataGrid($pager, array(), 'jqgrid');
//        $datagrid = $this->get('datatheke.datagrid')->createHttpDataGrid('tdCMBundle:Rate', array(), 'jqgrid');

        return $datagrid->handleRequest($request);
    }

    //Dohvat portala, povezani - nepovezani - svi
    public function indexAction()
    {
        //U ovom kontroleru samo na ovom mjestu se čita cookie
        //DOhvat korisnika iz COOKIE varijable kako bin iima ispravan set podataka
//                $this->sesija();
        $idCompanyKorisnik = $this->idCompanyKorisnik;

        //Ovaj kontroler priprema ulazni ekran koji prikazuje

        //Priprema podataka
        //Priprema osnovnih postavki
        //Najprije je potrebno dohvatiti sve portale za korisnika, a prije tega je potrbno dohvatiti PortalKorisnik

        //Povezani - Dohvat portala
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT p.id as id, p.naziv as naziv, p.link as link, p.thumbnail as thumbnail, p.contactPerson as contactPerson, p.contactEmail as contactEmail, p.contactTel as contactTel,
                        (SELECT COUNT(spu.id) FROM tdCMBundle:SetupPropertyUnit spu WHERE spu.idPropertyUnitNaPortalu IS NOT NULL AND spu.portalKorisnik = pk) as prijavljeni, 
                        (SELECT COUNT(spu2.id) FROM tdCMBundle:SetupPropertyUnit spu2 WHERE spu2.idPropertyUnitNaPortalu IS NULL AND spu2.portalKorisnik = pk) as neprijavljeni 
                                FROM tdCMBundle:Portal p INNER JOIN p.portaliKorisnici pk
                                WITH pk.idCompanyKorisnik = :idCompanyKorisnik'
        );
        $query->setParameter('idCompanyKorisnik', $idCompanyKorisnik);
        $povezani = $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);


        //Nepovezani
        $query = $em->createQuery(
            'SELECT p
                        FROM tdCMBundle:Portal p
                        WHERE p.id NOT IN (SELECT IDENTITY(pk.portal) FROM tdCMBundle:PortalKorisnik pk WHERE pk.idCompanyKorisnik = :idCompanyKorisnik)'
        );
        $query->setParameter('idCompanyKorisnik', $idCompanyKorisnik);
        $nepovezani = $query->getResult();

        //Svi - Dohvat portala
        $portalRepository = $this->getDoctrine()->getRepository('tdCMBundle:Portal');
        $portali = $portalRepository->findAll();
        //$nepovezani = $portali;

        //Dohvat popratnih podataka iz baze
        //Dohvat broja aktivnih
        //$brAktivnih = $portalRepository->count()
        //Dohvat broja neaktivnih
        $debug = "";

        //Dohvat naziva companyKorisnika
        // $companyKorisnik = $this->getDoctrine()->getManager()->getRepository('tdCMBundle:Client')->find($idCompanyKorisnik);

        return $this->render('tdCMBundle:Default:index.html.twig',
            array('povezani' => $povezani,
                'nepovezani' => $nepovezani,
                'portali' => $portali,
                'idCompanyKorisnik' => $idCompanyKorisnik,
                'debug' => $debug,
                'menu' => $this->menu
            ));
    }


}
