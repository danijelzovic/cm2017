<?php
/**
 * Created by PhpStorm.
 * User: danijel
 * Date: 15.10.2016.
 * Time: 12:32
 */

namespace td\CMBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use td\CMBundle\Entity\Property;
use td\CMBundle\Entity\Webshop;

/**
 * Class WebshopRepository
 * @package td\CMBundle\Entity\Repository
 */
class WebshopRepository extends EntityRepository
{

    /**
     * Get webshops in form ([ID] Title) for select element
     * @return array
     */
    public function getWebshopsForSelectElement()
    {
        return $this->createQueryBuilder('w')
            ->select('w.id')
            ->addSelect('w.idCompanyKorisnik')
            ->addSelect('(SELECT c.naziv FROM tdCMBundle:Client c WHERE c.idMojaTvrtka = w.idCompanyKorisnik) as nameCompany')
            ->addSelect('w.title')
            ->where('w.deleted = :deleted')
            ->orderBy('w.idCompanyKorisnik')
            ->setParameter('deleted', 0)
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * @return array
     */
    public function findActiveForSelectControl()
    {
        return $this->createQueryBuilder('w')
            ->select('w.id')
            ->addSelect('w.title')
            ->where('w.deleted = :deleted')
            ->setParameter('deleted', 0)
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
    }

}