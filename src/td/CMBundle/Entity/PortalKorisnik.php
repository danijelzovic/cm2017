<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Exclude;

/**
 * PortalKorisnik
 *
 * @ORM\Table(name="cm_portal_korisnik")
 * @ORM\Entity(repositoryClass="td\CMBundle\Entity\Repository\PortalKorisnikRepository")
 */
class PortalKorisnik
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
     * @var string
     *
     * @ORM\Column(name="id_company_korisnik", type="string", length=255, nullable = false)
     */
    private $idCompanyKorisnik;


    /**
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="portalKorisnici")
     * @ORM\JoinColumn(name="id_company_korisnik", referencedColumnName="id")
     */
    private $company;

    /**
     * @var string
     *
     * @ORM\Column(name="url_portala", type="string", length=255, nullable = false)
     */
    private $urlPortala;

    /**
     * @var string
     *
     * @ORM\Column(name="url_controller", type="string", length=255, nullable = true)
     * @Exclude
     */
    private $urlController;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=255, nullable = false)
     * @Exclude
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable = false)
     * @Exclude
     */
    private $password;

    /**
     * @var integer
     *
     * @ORM\Column(name="raspolozivost_broj_pokusaja", type="integer")
     */
    private $raspolozivostBrojPokusaja = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="cjenik_broj_pokusaja", type="integer")
     */
    private $cjenikBrojPokusaja = 0;

	/**
     * @var integer
     *
     * @ORM\Column(name="podaci_broj_pokusaja", type="integer")
     */
	private $podaciBrojPokusaja = 0;
    /**
     * @var integer
     *
     * @ORM\Column(name="opis_broj_pokusaja", type="integer")
     */
    private $opisBrojPokusaja = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="foto_broj_pokusaja", type="integer")
     */
    private $fotoBrojPokusaja = 0;

	/**
     * @var string
     *
     * @ORM\Column(name="id_korisnika_na_portalu", type="string", length=255, nullable = false)
     */
	private $idKorisnikaNaPortalu;
	
	 /**
     * @var integer
     *
     * @ORM\Column(name="id_webshop_rates", type="integer", nullable = true)
     */
	private $idWebshopRates;
	
	 /**
     * @var integer
     *
     * @ORM\Column(name="id_webshop_front", type="integer", nullable = true)
     */
	private $idWebshopFront;
	
	 /**
     * @var integer
     *
     * @ORM\Column(name="url_front", type="integer", nullable = true)
     */
	private $urlFront;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="xml_locations", type="string", length=255, nullable = true)
     */
	private $xmlLocations;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="poredak", type="integer")
     */
	private $poredak = 0;
	
    /**
     * @ORM\OneToMany(targetEntity="SetupProperty", mappedBy="portalKorisnik")
     * @Exclude
     */
	private $setupProperties;
	
     /**
     * @ORM\ManyToOne(targetEntity="Portal", inversedBy="portaliKorisnici")
     * @ORM\JoinColumn(name="id_portal", referencedColumnName="id")
     */
	private $portal;
	
      /**
     * @ORM\ManyToOne(targetEntity="Webshop", inversedBy="portaliKorisnici")
     * @ORM\JoinColumn(name="id_webshop_rates", referencedColumnName="id")
       * @Exclude
     */
	private $webshop;

    /**
     * @var boolean
     *
     * @ORM\Column(name="povezan", type="boolean", nullable=false)
     */
    private $povezan = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pending", type="boolean", nullable=false)
     */
    private $pending = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cjenik_kupovni", type="boolean", nullable=false)
     */
    private $cjenikKupovni = false;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setupProperties = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCompanyKorisnik
     *
     * @param string $idCompanyKorisnik
     *
     * @return PortalKorisnik
     */
    public function setIdCompanyKorisnik($idCompanyKorisnik)
    {
        $this->idCompanyKorisnik = $idCompanyKorisnik;

        return $this;
    }

    /**
     * Get idCompanyKorisnik
     *
     * @return string
     */
    public function getIdCompanyKorisnik()
    {
        return $this->idCompanyKorisnik;
    }

    /**
     * Set urlPortala
     *
     * @param string $urlPortala
     *
     * @return PortalKorisnik
     */
    public function setUrlPortala($urlPortala)
    {
        $this->urlPortala = $urlPortala;

        return $this;
    }

    /**
     * Get urlPortala
     *
     * @return string
     */
    public function getUrlPortala()
    {
        return $this->urlPortala;
    }

    /**
     * Set urlController
     *
     * @param string $urlController
     *
     * @return PortalKorisnik
     */
    public function setUrlController($urlController)
    {
        $this->urlController = $urlController;

        return $this;
    }

    /**
     * Get urlController
     *
     * @return string
     */
    public function getUrlController()
    {
        return $this->urlController;
    }

    /**
     * Set login
     *
     * @param string $login
     *
     * @return PortalKorisnik
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return PortalKorisnik
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set raspolozivostBrojPokusaja
     *
     * @param integer $raspolozivostBrojPokusaja
     *
     * @return PortalKorisnik
     */
    public function setRaspolozivostBrojPokusaja($raspolozivostBrojPokusaja)
    {
        $this->raspolozivostBrojPokusaja = $raspolozivostBrojPokusaja;

        return $this;
    }

    /**
     * Get raspolozivostBrojPokusaja
     *
     * @return integer
     */
    public function getRaspolozivostBrojPokusaja()
    {
        return $this->raspolozivostBrojPokusaja;
    }

    /**
     * Set cjenikBrojPokusaja
     *
     * @param integer $cjenikBrojPokusaja
     *
     * @return PortalKorisnik
     */
    public function setCjenikBrojPokusaja($cjenikBrojPokusaja)
    {
        $this->cjenikBrojPokusaja = $cjenikBrojPokusaja;

        return $this;
    }

    /**
     * Get cjenikBrojPokusaja
     *
     * @return integer
     */
    public function getCjenikBrojPokusaja()
    {
        return $this->cjenikBrojPokusaja;
    }

    /**
     * Set podaciBrojPokusaja
     *
     * @param integer $podaciBrojPokusaja
     *
     * @return PortalKorisnik
     */
    public function setPodaciBrojPokusaja($podaciBrojPokusaja)
    {
        $this->podaciBrojPokusaja = $podaciBrojPokusaja;

        return $this;
    }

    /**
     * Get podaciBrojPokusaja
     *
     * @return integer
     */
    public function getPodaciBrojPokusaja()
    {
        return $this->podaciBrojPokusaja;
    }

    /**
     * Set opisBrojPokusaja
     *
     * @param integer $opisBrojPokusaja
     *
     * @return PortalKorisnik
     */
    public function setOpisBrojPokusaja($opisBrojPokusaja)
    {
        $this->opisBrojPokusaja = $opisBrojPokusaja;

        return $this;
    }

    /**
     * Get opisBrojPokusaja
     *
     * @return integer
     */
    public function getOpisBrojPokusaja()
    {
        return $this->opisBrojPokusaja;
    }

    /**
     * Set fotoBrojPokusaja
     *
     * @param integer $fotoBrojPokusaja
     *
     * @return PortalKorisnik
     */
    public function setFotoBrojPokusaja($fotoBrojPokusaja)
    {
        $this->fotoBrojPokusaja = $fotoBrojPokusaja;

        return $this;
    }

    /**
     * Get fotoBrojPokusaja
     *
     * @return integer
     */
    public function getFotoBrojPokusaja()
    {
        return $this->fotoBrojPokusaja;
    }

    /**
     * Set idKorisnikaNaPortalu
     *
     * @param string $idKorisnikaNaPortalu
     *
     * @return PortalKorisnik
     */
    public function setIdKorisnikaNaPortalu($idKorisnikaNaPortalu)
    {
        $this->idKorisnikaNaPortalu = $idKorisnikaNaPortalu;

        return $this;
    }

    /**
     * Get idKorisnikaNaPortalu
     *
     * @return string
     */
    public function getIdKorisnikaNaPortalu()
    {
        return $this->idKorisnikaNaPortalu;
    }

    /**
     * Set idWebshopRates
     *
     * @param integer $idWebshopRates
     *
     * @return PortalKorisnik
     */
    public function setIdWebshopRates($idWebshopRates)
    {
        $this->idWebshopRates = $idWebshopRates;

        return $this;
    }

    /**
     * Get idWebshopRates
     *
     * @return integer
     */
    public function getIdWebshopRates()
    {
        return $this->idWebshopRates;
    }

    /**
     * Set idWebshopFront
     *
     * @param integer $idWebshopFront
     *
     * @return PortalKorisnik
     */
    public function setIdWebshopFront($idWebshopFront)
    {
        $this->idWebshopFront = $idWebshopFront;

        return $this;
    }

    /**
     * Get idWebshopFront
     *
     * @return integer
     */
    public function getIdWebshopFront()
    {
        return $this->idWebshopFront;
    }

    /**
     * Set urlFront
     *
     * @param integer $urlFront
     *
     * @return PortalKorisnik
     */
    public function setUrlFront($urlFront)
    {
        $this->urlFront = $urlFront;

        return $this;
    }

    /**
     * Get urlFront
     *
     * @return integer
     */
    public function getUrlFront()
    {
        return $this->urlFront;
    }

    /**
     * Set xmlLocations
     *
     * @param string $xmlLocations
     *
     * @return PortalKorisnik
     */
    public function setXmlLocations($xmlLocations)
    {
        $this->xmlLocations = $xmlLocations;

        return $this;
    }

    /**
     * Get xmlLocations
     *
     * @return string
     */
    public function getXmlLocations()
    {
        return $this->xmlLocations;
    }

    /**
     * Set poredak
     *
     * @param integer $poredak
     *
     * @return PortalKorisnik
     */
    public function setPoredak($poredak)
    {
        $this->poredak = $poredak;

        return $this;
    }

    /**
     * Get poredak
     *
     * @return integer
     */
    public function getPoredak()
    {
        return $this->poredak;
    }

    /**
     * Add setupProperty
     *
     * @param \td\CMBundle\Entity\SetupProperty $setupProperty
     *
     * @return PortalKorisnik
     */
    public function addSetupProperty(\td\CMBundle\Entity\SetupProperty $setupProperty)
    {
        $this->setupProperties[] = $setupProperty;

        return $this;
    }

    /**
     * Remove setupProperty
     *
     * @param \td\CMBundle\Entity\SetupProperty $setupProperty
     */
    public function removeSetupProperty(\td\CMBundle\Entity\SetupProperty $setupProperty)
    {
        $this->setupProperties->removeElement($setupProperty);
    }

    /**
     * Get setupProperties
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSetupProperties()
    {
        return $this->setupProperties;
    }

    /**
     * Set portal
     *
     * @param \td\CMBundle\Entity\Portal $portal
     *
     * @return PortalKorisnik
     */
    public function setPortal(\td\CMBundle\Entity\Portal $portal = null)
    {
        $this->portal = $portal;

        return $this;
    }

    /**
     * Get portal
     *
     * @return \td\CMBundle\Entity\Portal
     */
    public function getPortal()
    {
        return $this->portal;
    }

    /**
     * Set webshop
     *
     * @param \td\CMBundle\Entity\Webshop $webshop
     *
     * @return PortalKorisnik
     */
    public function setWebshop(\td\CMBundle\Entity\Webshop $webshop = null)
    {
        $this->webshop = $webshop;

        return $this;
    }

    /**
     * Get webshop
     *
     * @return \td\CMBundle\Entity\Webshop
     */
    public function getWebshop()
    {
        return $this->webshop;
    }

    /**
     * Set povezan
     *
     * @param boolean $povezan
     *
     * @return PortalKorisnik
     */
    public function setPovezan($povezan)
    {
        $this->povezan = $povezan;

        return $this;
    }

    /**
     * Get povezan
     *
     * @return boolean
     */
    public function getPovezan()
    {
        return $this->povezan;
    }

    /**
     * Set pending
     *
     * @param boolean $pending
     *
     * @return PortalKorisnik
     */
    public function setPending($pending)
    {
        $this->pending = $pending;

        return $this;
    }

    /**
     * Get pending
     *
     * @return boolean
     */
    public function getPending()
    {
        return $this->pending;
    }

    /**
     * Set cjenikKupovni
     *
     * @param boolean $cjenikKupovni
     *
     * @return PortalKorisnik
     */
    public function setCjenikKupovni($cjenikKupovni)
    {
        $this->cjenikKupovni = $cjenikKupovni;

        return $this;
    }

    /**
     * Get cjenikKupovni
     *
     * @return boolean
     */
    public function getCjenikKupovni()
    {
        return $this->cjenikKupovni;
    }

    /**
     * Set company
     *
     * @param \td\CMBundle\Entity\Company $company
     * @return PortalKorisnik
     */
    public function setCompany(\td\CMBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \td\CMBundle\Entity\Company 
     */
    public function getCompany()
    {
        return $this->company;
    }
}
