<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Date;
use DateTime;

/**
 * WebshopPropertyUnit
 *
 * @ORM\Table(name="webshop_property_unit", uniqueConstraints={@ORM\UniqueConstraint(name="id_shop", columns={"id_shop", "id_property", "id_unit"})}, indexes={@ORM\Index(name="inquiry_approved", columns={"inquiry_approved"}), @ORM\Index(name="online_approved", columns={"online_approved"}), @ORM\Index(name="disabled", columns={"disabled"}), @ORM\Index(name="approved", columns={"approved"}), @ORM\Index(name="online", columns={"online"}), @ORM\Index(name="inquiry", columns={"inquiry"}), @ORM\Index(name="id_shop_2", columns={"id_shop"}), @ORM\Index(name="id_unit", columns={"id_unit"}), @ORM\Index(name="cjenik_prodajni_2", columns={"cjenik_prodajni_2"}), @ORM\Index(name="cjenik_prodajni", columns={"cjenik_prodajni"}), @ORM\Index(name="id_property", columns={"id_property"}), @ORM\Index(name="booking_from", columns={"booking_from", "booking_until"}), @ORM\Index(name="period_from", columns={"period_from", "period_until"}), @ORM\Index(name="inquiry_approved_2", columns={"inquiry_approved", "online_approved"}), @ORM\Index(name="id_shop_3", columns={"id_shop", "id_unit"})})
 * @ORM\Entity(repositoryClass="td\CMBundle\Entity\Repository\WebshopPropertyUnitRepository")
 */
class WebshopPropertyUnit
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_shop", type="integer", nullable=false)
     */
    private $idShop = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webshop")
     * @ORM\JoinColumn(name="id_shop", referencedColumnName="id", onDelete="CASCADE")
     */
    private $webshop;
	
    /**
     * @var integer
     *
     * @ORM\Column(name="id_property", type="integer", nullable=false)
     */
    private $idProperty = null;

    /**
     * @ORM\ManyToOne(targetEntity="Property")
     * @ORM\JoinColumn(name="id_property", referencedColumnName="id")
     **/
    private $property;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id_unit", type="integer", nullable=false)
     */
    private $idUnit = null;

     /**
     * @ORM\ManyToOne(targetEntity="PropertyUnit")
     * @ORM\JoinColumn(name="id_unit", referencedColumnName="id")
     **/
    private $propertyUnit;
    
    /**
     * @var string
     *
     * @ORM\Column(name="comm_title_hr", type="string", length=64, nullable=false)
     */
    private $commTitleHr = '';

    /**
     * @var string
     *
     * @ORM\Column(name="comm_title_en", type="string", length=64, nullable=false)
     */
    private $commTitleEn = '';

    /**
     * @var string
     *
     * @ORM\Column(name="comm_title_de", type="string", length=64, nullable=false)
     */
    private $commTitleDe = '';

    /**
     * @var string
     *
     * @ORM\Column(name="comm_title_it", type="string", length=64, nullable=false)
     */
    private $commTitleIt = '';

    /**
     * @var string
     *
     * @ORM\Column(name="comm_title_si", type="string", length=64, nullable=false)
     */
    private $commTitleSi = '';

    /**
     * @var string
     *
     * @ORM\Column(name="comm_title_ru", type="string", length=64, nullable=false)
     */
    private $commTitleRu = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="cjenik_kupovni", type="integer", nullable=false)
     */
    private $cjenikKupovni = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="cjenik_prodajni", type="integer", nullable=false)
     */
    private $cjenikProdajni = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="id_his", type="string", length=15, nullable=false)
     */
    private $idHis = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="cjenik_kupovni_2", type="integer", nullable=false)
     */
    private $cjenikKupovni2 = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="cjenik_prodajni_2", type="integer", nullable=false)
     */
    private $cjenikProdajni2 = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="id_his_2", type="string", length=15, nullable=false)
     */
    private $idHis2 = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="online", type="boolean", nullable=false)
     */
    private $online = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="inquiry", type="boolean", nullable=false)
     */
    private $inquiry = true;

    /**
     * @var boolean
     *
     * @ORM\Column(name="calculation", type="boolean", nullable=false)
     */
    private $calculation = true;

    /**
     * @var boolean
     *
     * @ORM\Column(name="disabled", type="boolean", nullable=false)
     */
    private $disabled = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="inquiry_approved", type="boolean", nullable=true)
     */
    private $inquiryApproved = true;

    /**
     * @var integer
     *
     * @ORM\Column(name="inquiry_approved_by", type="integer", nullable=false)
     */
    private $inquiryApprovedBy = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="inquiry_approved_date", type="datetime", nullable=false)
     */
    private $inquiryApprovedDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="online_approved", type="boolean", nullable=true)
     */
    private $onlineApproved;

    /**
     * @var integer
     *
     * @ORM\Column(name="online_approved_by", type="integer", nullable=false)
     */
    private $onlineApprovedBy = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="online_approved_date", type="datetime", nullable=false)
     */
    private $onlineApprovedDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="approved", type="boolean", nullable=true)
     */
    private $approved = true;

    /**
     * @var integer
     *
     * @ORM\Column(name="approved_by", type="integer", nullable=false)
     */
    private $approvedBy = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="approved_date", type="datetime", nullable=false)
     */
    private $approvedDate;

    /**
     * @var string
     *
     * @ORM\Column(name="popust", type="decimal", precision=6, scale=2, nullable=false)
     */
    private $popust = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="booking_from", type="date", nullable=false)
     */
    private $bookingFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="booking_until", type="date", nullable=false)
     */
    private $bookingUntil;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="period_from", type="date", nullable=false)
     */
    private $periodFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="period_until", type="date", nullable=false)
     */
    private $periodUntil;

    /**
     * @var string
     *
     * @ORM\Column(name="napomena_operater", type="text", nullable=false)
     */
    private $napomenaOperater = '';

    /**
     * WebshopPropertyUnit constructor.
     */
    public function __construct()
    {
        $this->bookingFrom = new DateTime('0000-00-00');
        $this->bookingUntil = new DateTime('0000-00-00');
        $this->periodFrom = new DateTime('0000-00-00');
        $this->periodUntil= new DateTime('0000-00-00');
        $this->inquiryApprovedDate = new DateTime('0000-00-00');
        $this->approvedDate = new DateTime();
        $this->onlineApprovedDate = new DateTime('0000-00-00');
    }

    /**
     * Set idShop
     *
     * @param integer $idShop
     * @return WebshopPropertyUnit
     */
    public function setIdShop($idShop)
    {
        $this->idShop = $idShop;

        return $this;
    }

    /**
     * Get idShop
     *
     * @return integer 
     */
    public function getIdShop()
    {
        return $this->idShop;
    }

    /**
     * Set idProperty
     *
     * @param integer $idProperty
     * @return WebshopPropertyUnit
     */
    public function setIdProperty($idProperty)
    {
        $this->idProperty = $idProperty;

        return $this;
    }

    /**
     * Get idProperty
     *
     * @return integer 
     */
    public function getIdProperty()
    {
        return $this->idProperty;
    }

    /**
     * Set idUnit
     *
     * @param integer $idUnit
     * @return WebshopPropertyUnit
     */
    public function setIdUnit($idUnit)
    {
        $this->idUnit = $idUnit;

        return $this;
    }

    /**
     * Get idUnit
     *
     * @return integer 
     */
    public function getIdUnit()
    {
        return $this->idUnit;
    }

    /**
     * Set commTitleHr
     *
     * @param string $commTitleHr
     * @return WebshopPropertyUnit
     */
    public function setCommTitleHr($commTitleHr)
    {
        $this->commTitleHr = $commTitleHr;

        return $this;
    }

    /**
     * Get commTitleHr
     *
     * @return string 
     */
    public function getCommTitleHr()
    {
        return $this->commTitleHr;
    }

    /**
     * Set commTitleEn
     *
     * @param string $commTitleEn
     * @return WebshopPropertyUnit
     */
    public function setCommTitleEn($commTitleEn)
    {
        $this->commTitleEn = $commTitleEn;

        return $this;
    }

    /**
     * Get commTitleEn
     *
     * @return string 
     */
    public function getCommTitleEn()
    {
        return $this->commTitleEn;
    }

    /**
     * Set commTitleDe
     *
     * @param string $commTitleDe
     * @return WebshopPropertyUnit
     */
    public function setCommTitleDe($commTitleDe)
    {
        $this->commTitleDe = $commTitleDe;

        return $this;
    }

    /**
     * Get commTitleDe
     *
     * @return string 
     */
    public function getCommTitleDe()
    {
        return $this->commTitleDe;
    }

    /**
     * Set commTitleIt
     *
     * @param string $commTitleIt
     * @return WebshopPropertyUnit
     */
    public function setCommTitleIt($commTitleIt)
    {
        $this->commTitleIt = $commTitleIt;

        return $this;
    }

    /**
     * Get commTitleIt
     *
     * @return string 
     */
    public function getCommTitleIt()
    {
        return $this->commTitleIt;
    }

    /**
     * Set commTitleSi
     *
     * @param string $commTitleSi
     * @return WebshopPropertyUnit
     */
    public function setCommTitleSi($commTitleSi)
    {
        $this->commTitleSi = $commTitleSi;

        return $this;
    }

    /**
     * Get commTitleSi
     *
     * @return string 
     */
    public function getCommTitleSi()
    {
        return $this->commTitleSi;
    }

    /**
     * Set commTitleRu
     *
     * @param string $commTitleRu
     * @return WebshopPropertyUnit
     */
    public function setCommTitleRu($commTitleRu)
    {
        $this->commTitleRu = $commTitleRu;

        return $this;
    }

    /**
     * Get commTitleRu
     *
     * @return string 
     */
    public function getCommTitleRu()
    {
        return $this->commTitleRu;
    }

    /**
     * Set cjenikKupovni
     *
     * @param integer $cjenikKupovni
     * @return WebshopPropertyUnit
     */
    public function setCjenikKupovni($cjenikKupovni)
    {
        $this->cjenikKupovni = $cjenikKupovni;

        return $this;
    }

    /**
     * Get cjenikKupovni
     *
     * @return integer 
     */
    public function getCjenikKupovni()
    {
        return $this->cjenikKupovni;
    }

    /**
     * Set cjenikProdajni
     *
     * @param integer $cjenikProdajni
     * @return WebshopPropertyUnit
     */
    public function setCjenikProdajni($cjenikProdajni)
    {
        $this->cjenikProdajni = $cjenikProdajni;

        return $this;
    }

    /**
     * Get cjenikProdajni
     *
     * @return integer 
     */
    public function getCjenikProdajni()
    {
        return $this->cjenikProdajni;
    }

    /**
     * Set idHis
     *
     * @param string $idHis
     * @return WebshopPropertyUnit
     */
    public function setIdHis($idHis)
    {
        $this->idHis = $idHis;

        return $this;
    }

    /**
     * Get idHis
     *
     * @return string 
     */
    public function getIdHis()
    {
        return $this->idHis;
    }

    /**
     * Set cjenikKupovni2
     *
     * @param integer $cjenikKupovni2
     * @return WebshopPropertyUnit
     */
    public function setCjenikKupovni2($cjenikKupovni2)
    {
        $this->cjenikKupovni2 = $cjenikKupovni2;

        return $this;
    }

    /**
     * Get cjenikKupovni2
     *
     * @return integer 
     */
    public function getCjenikKupovni2()
    {
        return $this->cjenikKupovni2;
    }

    /**
     * Set cjenikProdajni2
     *
     * @param integer $cjenikProdajni2
     * @return WebshopPropertyUnit
     */
    public function setCjenikProdajni2($cjenikProdajni2)
    {
        $this->cjenikProdajni2 = $cjenikProdajni2;

        return $this;
    }

    /**
     * Get cjenikProdajni2
     *
     * @return integer 
     */
    public function getCjenikProdajni2()
    {
        return $this->cjenikProdajni2;
    }

    /**
     * Set idHis2
     *
     * @param string $idHis2
     * @return WebshopPropertyUnit
     */
    public function setIdHis2($idHis2)
    {
        $this->idHis2 = $idHis2;

        return $this;
    }

    /**
     * Get idHis2
     *
     * @return string 
     */
    public function getIdHis2()
    {
        return $this->idHis2;
    }

    /**
     * Set online
     *
     * @param boolean $online
     * @return WebshopPropertyUnit
     */
    public function setOnline($online)
    {
        $this->online = $online;

        return $this;
    }

    /**
     * Get online
     *
     * @return boolean 
     */
    public function getOnline()
    {
        return $this->online;
    }

    /**
     * Set inquiry
     *
     * @param boolean $inquiry
     * @return WebshopPropertyUnit
     */
    public function setInquiry($inquiry)
    {
        $this->inquiry = $inquiry;

        return $this;
    }

    /**
     * Get inquiry
     *
     * @return boolean 
     */
    public function getInquiry()
    {
        return $this->inquiry;
    }

    /**
     * Set calculation
     *
     * @param boolean $calculation
     * @return WebshopPropertyUnit
     */
    public function setCalculation($calculation)
    {
        $this->calculation = $calculation;

        return $this;
    }

    /**
     * Get calculation
     *
     * @return boolean 
     */
    public function getCalculation()
    {
        return $this->calculation;
    }

    /**
     * Set disabled
     *
     * @param boolean $disabled
     * @return WebshopPropertyUnit
     */
    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;

        return $this;
    }

    /**
     * Get disabled
     *
     * @return boolean 
     */
    public function getDisabled()
    {
        return $this->disabled;
    }

    /**
     * Set inquiryApproved
     *
     * @param boolean $inquiryApproved
     * @return WebshopPropertyUnit
     */
    public function setInquiryApproved($inquiryApproved)
    {
        $this->inquiryApproved = $inquiryApproved;

        return $this;
    }

    /**
     * Get inquiryApproved
     *
     * @return boolean 
     */
    public function getInquiryApproved()
    {
        return $this->inquiryApproved;
    }

    /**
     * Set inquiryApprovedBy
     *
     * @param integer $inquiryApprovedBy
     * @return WebshopPropertyUnit
     */
    public function setInquiryApprovedBy($inquiryApprovedBy)
    {
        $this->inquiryApprovedBy = $inquiryApprovedBy;

        return $this;
    }

    /**
     * Get inquiryApprovedBy
     *
     * @return integer 
     */
    public function getInquiryApprovedBy()
    {
        return $this->inquiryApprovedBy;
    }

    /**
     * Set inquiryApprovedDate
     *
     * @param \DateTime $inquiryApprovedDate
     * @return WebshopPropertyUnit
     */
    public function setInquiryApprovedDate($inquiryApprovedDate)
    {
        $this->inquiryApprovedDate = $inquiryApprovedDate;

        return $this;
    }

    /**
     * Get inquiryApprovedDate
     *
     * @return \DateTime
     */
    public function getInquiryApprovedDate()
    {
        return $this->inquiryApprovedDate;
    }

    /**
     * Set onlineApproved
     *
     * @param boolean $onlineApproved
     * @return WebshopPropertyUnit
     */
    public function setOnlineApproved($onlineApproved)
    {
        $this->onlineApproved = $onlineApproved;

        return $this;
    }

    /**
     * Get onlineApproved
     *
     * @return boolean 
     */
    public function getOnlineApproved()
    {
        return $this->onlineApproved;
    }

    /**
     * Set onlineApprovedBy
     *
     * @param integer $onlineApprovedBy
     * @return WebshopPropertyUnit
     */
    public function setOnlineApprovedBy($onlineApprovedBy)
    {
        $this->onlineApprovedBy = $onlineApprovedBy;

        return $this;
    }

    /**
     * Get onlineApprovedBy
     *
     * @return integer 
     */
    public function getOnlineApprovedBy()
    {
        return $this->onlineApprovedBy;
    }

    /**
     * Set onlineApprovedDate
     *
     * @param \DateTime $onlineApprovedDate
     * @return WebshopPropertyUnit
     */
    public function setOnlineApprovedDate($onlineApprovedDate)
    {
        $this->onlineApprovedDate = $onlineApprovedDate;

        return $this;
    }

    /**
     * Get onlineApprovedDate
     *
     * @return \DateTime
     */
    public function getOnlineApprovedDate()
    {
        return $this->onlineApprovedDate;
    }

    /**
     * Set approved
     *
     * @param boolean $approved
     * @return WebshopPropertyUnit
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;

        return $this;
    }

    /**
     * Get approved
     *
     * @return boolean 
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * Set approvedBy
     *
     * @param integer $approvedBy
     * @return WebshopPropertyUnit
     */
    public function setApprovedBy($approvedBy)
    {
        $this->approvedBy = $approvedBy;

        return $this;
    }

    /**
     * Get approvedBy
     *
     * @return integer 
     */
    public function getApprovedBy()
    {
        return $this->approvedBy;
    }

    /**
     * Set approvedDate
     *
     * @param \DateTime $approvedDate
     * @return WebshopPropertyUnit
     */
    public function setApprovedDate($approvedDate)
    {
        $this->approvedDate = $approvedDate;

        return $this;
    }

    /**
     * Get approvedDate
     *
     * @return \DateTime
     */
    public function getApprovedDate()
    {
        return $this->approvedDate;
    }

    /**
     * Set popust
     *
     * @param string $popust
     * @return WebshopPropertyUnit
     */
    public function setPopust($popust)
    {
        $this->popust = $popust;

        return $this;
    }

    /**
     * Get popust
     *
     * @return string 
     */
    public function getPopust()
    {
        return $this->popust;
    }

    /**
     * Set bookingFrom
     *
     * @param \DateTime $bookingFrom
     * @return WebshopPropertyUnit
     */
    public function setBookingFrom($bookingFrom)
    {
        $this->bookingFrom = $bookingFrom;

        return $this;
    }

    /**
     * Get bookingFrom
     *
     * @return \DateTime
     */
    public function getBookingFrom()
    {
        return $this->bookingFrom;
    }

    /**
     * Set bookingUntil
     *
     * @param \DateTime $bookingUntil
     * @return WebshopPropertyUnit
     */
    public function setBookingUntil($bookingUntil)
    {
        $this->bookingUntil = $bookingUntil;

        return $this;
    }

    /**
     * Get bookingUntil
     *
     * @return \DateTime
     */
    public function getBookingUntil()
    {
        return $this->bookingUntil;
    }

    /**
     * Set periodFrom
     *
     * @param \DateTime $periodFrom
     * @return WebshopPropertyUnit
     */
    public function setPeriodFrom($periodFrom)
    {
        $this->periodFrom = $periodFrom;

        return $this;
    }

    /**
     * Get periodFrom
     *
     * @return \DateTime
     */
    public function getPeriodFrom()
    {
        return $this->periodFrom;
    }

    /**
     * Set periodUntil
     *
     * @param \DateTime $periodUntil
     * @return WebshopPropertyUnit
     */
    public function setPeriodUntil($periodUntil)
    {
        $this->periodUntil = $periodUntil;

        return $this;
    }

    /**
     * Get periodUntil
     *
     * @return \DateTime
     */
    public function getPeriodUntil()
    {
        return $this->periodUntil;
    }

    /**
     * Set napomenaOperater
     *
     * @param string $napomenaOperater
     * @return WebshopPropertyUnit
     */
    public function setNapomenaOperater($napomenaOperater)
    {
        $this->napomenaOperater = $napomenaOperater;

        return $this;
    }

    /**
     * Get napomenaOperater
     *
     * @return string 
     */
    public function getNapomenaOperater()
    {
        return $this->napomenaOperater;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set webshop
     *
     * @param \td\CMBundle\Entity\Webshop $webshop
     * @return WebshopPropertyUnit
     */
    public function setWebshop(\td\CMBundle\Entity\Webshop $webshop = null)
    {
        $this->webshop = $webshop;

        return $this;
    }

    /**
     * Get webshop
     *
     * @return \td\CMBundle\Entity\Webshop 
     */
    public function getWebshop()
    {
        return $this->webshop;
    }

    /**
     * Set propertyUnit
     *
     * @param \td\CMBundle\Entity\PropertyUnit $propertyUnit
     * @return WebshopPropertyUnit
     */
    public function setPropertyUnit(\td\CMBundle\Entity\PropertyUnit $propertyUnit = null)
    {
        $this->propertyUnit = $propertyUnit;

        return $this;
    }

    /**
     * Get propertyUnit
     *
     * @return \td\CMBundle\Entity\PropertyUnit 
     */
    public function getPropertyUnit()
    {
        return $this->propertyUnit;
    }

    /**
     * Set property
     *
     * @param \td\CMBundle\Entity\Property $property
     * @return WebshopPropertyUnit
     */
    public function setProperty(\td\CMBundle\Entity\Property $property = null)
    {
        $this->property = $property;

        return $this;
    }

    /**
     * Get property
     *
     * @return \td\CMBundle\Entity\Property 
     */
    public function getProperty()
    {
        return $this->property;
    }
}
