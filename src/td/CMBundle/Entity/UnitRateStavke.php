<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UnitRateStavke
 *
 * @ORM\Table(name="cm_unit_rate_stavke")
 * @ORM\Entity
 */
class UnitRateStavke
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="idUnitRate", type="integer")
     */
    private $idUnitRate;
	
	 /**
     * @ORM\ManyToOne(targetEntity="ItemPriceListItem")
     * @ORM\JoinColumn(name="id_itempricelist_item", referencedColumnName="id", onDelete="CASCADE")
	 */
    private $itemPriceListItem;
    
    /**
     * @ORM\ManyToOne(targetEntity="UnitRateStavke")
     * @ORM\JoinColumn(name="idUnitRate", referencedColumnName="id", onDelete="CASCADE")
    */
    private $unitRate;

    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idUnitRate
     *
     * @param integer $idUnitRate
     * @return UnitRateStavke
     */
    public function setIdUnitRate($idUnitRate)
    {
        $this->idUnitRate = $idUnitRate;

        return $this;
    }

    /**
     * Get idUnitRate
     *
     * @return integer 
     */
    public function getIdUnitRate()
    {
        return $this->idUnitRate;
    }

    /**
     * Set itemPriceListItem
     *
     * @param \td\CMBundle\Entity\ItemPriceListItem $itemPriceListItem
     * @return UnitRateStavke
     */
    public function setItemPriceListItem(\td\CMBundle\Entity\ItemPriceListItem $itemPriceListItem = null)
    {
        $this->itemPriceListItem = $itemPriceListItem;

        return $this;
    }

    /**
     * Get itemPriceListItem
     *
     * @return \td\CMBundle\Entity\ItemPriceListItem 
     */
    public function getItemPriceListItem()
    {
        return $this->itemPriceListItem;
    }

    /**
     * Set unitRate
     *
     * @param \td\CMBundle\Entity\UnitRate $unitRate
     * @return UnitRateStavke
     */
    public function setUnitRate(\td\CMBundle\Entity\UnitRate $unitRate = null)
    {
        $this->unitRate = $unitRate;

        return $this;
    }

    /**
     * Get unitRate
     *
     * @return \td\CMBundle\Entity\UnitRate 
     */
    public function getUnitRate()
    {
        return $this->unitRate;
    }
}
