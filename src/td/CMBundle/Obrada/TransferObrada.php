<?php

/**
 * Created by PhpStorm.
 * User: Danijel
 * Date: 29.2.2016.
 * Time: 21:30
 */

namespace td\CMBundle\Obrada;


use Doctrine\Bundle\DoctrineBundle\Registry;
use td\CMBundle\Entity\Transfer;
use td\CMBundle\Entity\Promjena;
use td\CMBundle\Entity\SetupPropertyUnit;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use td\CMBundle\Entity\PortalKorisnik;
use td\CMBundle\Entity\SetupProperty;
use JMS;

/**
 * Class TransferObrada
 * @package td\CMBundle\Obrada
 */
class TransferObrada
{

//$url = 'https://agencije.atrade.hr/booking.com/lossender';
//$username = "fiskalizacija";
//$password = "21jmdsh45";

    const URL = 'https://agencije.atrade.hr/booking.com/lossender';
    const USERNAME = 'fiskalizacija';
    const PASSWORD = '21jmdsh45';

    /**
     * @param Transfer $transfer
     * @return Transfer
     */
    public function obradiTransfer(Transfer &$transfer)
    {
        $transferLog = null;
        if(!$transfer->getOk()){
            $transferLog = $this->transferiraj($transfer);
        }
        return $transferLog;
    }

    /**
     * @param Transfer $transfer
     * @return string
     */
    private function transferiraj(Transfer &$transfer)
    {
        //Ova fja komunicira trnsfer prema portalima
        //Ovisno o tome što vrati WS, radi se zapis u LOG tablicu
        /* @var $transfer Transfer */
        $doctrine = new Registry();
        $em = $doctrine->getManager();
        $povratnaPoruka = '';
        //Pozivanje booking.com
        if ($transfer->getSetupPropertyUnit()->getPortalKorisnik()->getPortal()->isBookingCom()) { //Provjera dali je portal Booking.com
            if ($transfer->getPromjena()->getGrupaPromjene() != "property_unit") { //Provjera da li je grupa promjene dozvoljena
                $povratnaPoruka = (string)$this->pokreniLosTablicu($transfer); //Pozivanje WS od Tomislava
                //Spremanje odgovora u bazu
                $log = $this->zapisiTransferLog($transfer, $povratnaPoruka);


                //Dohvati broj log promjena za transfer
                $brojPokusaja = $transfer->getSetupPropertyUnit()->getPortalKorisnik()->getRaspolozivostBrojPokusaja();
                $logovi = $em->getRepository('tdCMBundle:TransferLog')->findByTransfer($transfer);
                if (count($logovi) > $brojPokusaja) {
                    //javi na email da je nešto krivo
                }

                if (0) { //Ovo za sad ne delan jer neću transferirat nove dok nisu stari preneseni
                    if ($transfer->getOk()) { //Ako je uspješan onda ažuriraj sve stare neuspjele transfere.
                        //            $log->setUspjesanPrijenos(true);
                        //            $transfer->setOk(true);
                        //            $em->flush();

                        $stariNeuspjesniTransferi = $em->getRepository('tdCMBundle:Transfer')->findBy(
                            array(
                                'setupPropertyUnit' => $transfer->getSetupPropertyUnit(),
                                'ok' => false)
                        );

                        foreach ($stariNeuspjesniTransferi as $zadnjiTransfer) {
                            /* @var $zadnjiTransfer Transfer */
                            $zadnjiTransfer->setOk(true);

                            //Ažuriraj stari log
                            $log2 = new TransferLog();
                            $log2->setDatumVrijeme(new \DateTime());
                            $log2->setTransfer($zadnjiTransfer);
                            $log2->setUspjesanPrijenos(true);
                            $log2->setPrijenosNaknadnomPromjenom(true);
                            $log2->setResponseTransfera('naknadna promjena');

                            $em->persist($log2);
                        }
                    }
                }

                $em->flush();

                return $log;

            } else {
//                $povratnaPoruka = "Promjena je tipa property_unit i nije predviđena za Booking.com";
                return null;
            }
        }

        if($transfer->getSetupPropertyUnit()->getPortalKorisnik()->getPortal()->getNaziv() == 'E-domizil'){
            $povratnaPoruka = 'Obrada promjena za E-domizil još nisu postavljene!';
            return $povratnaPoruka;
            return null;
        }

        if($transfer->getSetupPropertyUnit()->getPortalKorisnik()->getPortal()->getNaziv() == 'Holiday Lettings'){
            $povratnaPoruka = 'Obrada promjena za Holiday Lettings još nisu postavljene!';
            return $povratnaPoruka;
            return null;
        }
    }

    /**
     * @param $transfer
     * @param $poruka
     * @return TransferLog
     */
    private function zapisiTransferLog($transfer, $poruka)
    {
        /* @var $transfer Transfer */
        //Ažuriraj u log - bez obzira da li je transfer uspješan ili ne
        $doctrine = new Registry();
        $doctrine = new Registry();
        $em = $doctrine->getManager();

        $log = new TransferLog();
        $log->setDatumVrijeme(new \DateTime());
        $log->setTransfer($transfer);

        $log->setResponseTransfera($poruka);
        if ($transfer->getOk()) {
            $log->setUspjesanPrijenos(true);
        }

        $em->persist($log);
        $em->flush();

        return $log;
        //Ažuriram sve ostale logove koji su otvoreni za aktualni unit
    }

    /**
     * Ovo se koristi za Booking.com
     *
     * @param $transfer
     * @return string
     */
    private function pokreniLosTablicu(Transfer &$transfer)
    {
        /* @var $transfer Transfer */
        $polje = array();
        $danas = new \DateTime('now');
        $polje['id_property'] = $transfer->getSetupPropertyUnit()->getPropertyUnit()->getProperty()->getId();
        $polje['id_property_bookingcom'] = $transfer->getSetupPropertyUnit()->getSetupProperty()->getIdPropertyNaPortalu();
        if ($transfer->getPromjena()->getDateFrom() >= $danas) {
            $polje['date_start'] = $transfer->getPromjena()->getDateFrom()->format('Y-m-d');
        } else {
            $polje['date_start'] = $danas->format('Y-m-d');
        }
        $polje['date_end'] = $transfer->getPromjena()->getDateTo()->format('Y-m-d');
        $polje['units'] = array();

//        return new Response('Broj unita je: ' . $transfer->getSetupPropertyUnit()->getSetupProperties()->getSetupPropertyUnits()->count());

        $brojač = 0;
        $brojač2 = 0;
        /* @var $setupPropertyUnit SetupPropertyUnit */
//        foreach ($transfer->getSetupPropertyUnit()->getSetupProperties()->getSetupPropertyUnits() as $setupPropertyUnit) {
//            if ($setupPropertyUnit->getAktivan() && $setupPropertyUnit->getIdPropertyUnitNaPortalu() != '') {
//                $polje['units'][$brojač] = array();
//                $polje['units'][$brojač]['id_unit'] = $setupPropertyUnit->getPropertyUnit()->getId();
//                $polje['units'][$brojač]['id_unit_bookingcom'] = (int)$setupPropertyUnit->getIdPropertyUnitNaPortalu();
//                $polje['units'][$brojač]['rates'] = array();
//
//                /* @var $unitRate UnitRateStavke */
//                foreach ($setupPropertyUnit->getUnitRates() as $unitRate) {
//                    if ($unitRate->getAktivan()) {
//                        $polje['units'][$brojač]['rates'][$brojač2] = array();
//                        $polje['units'][$brojač]['rates'][$brojač2]['id_cjenik'] = $unitRate->getIdCjenik();
//                        $polje['units'][$brojač]['rates'][$brojač2]['id_rate_bookingcom'] = $unitRate->getRate()->getRateIdPortal();
//                        $polje['units'][$brojač]['rates'][$brojač2]['samo_cjenik'] = $unitRate->getSamoCjenik();
//                        if (!$unitRate->getSamoCjenik()) {
//                            $polje['units'][$brojač]['rates'][$brojač2]['id_set'] = $unitRate->getIdSet();
//                        }
//                        $brojač2++;
//                    }
//                }
//                $brojač++;
//                $brojač2 = 0;
//            }
//        }

        $polje['units'] = array();
        $polje['units'][0]  = array();
        $polje['units'][0]['id_unit'] = $transfer->getPromjena()->getPropertyUnit()->getId();
        $polje['units'][0]['id_unit_bookingcom'] = $transfer->getSetupPropertyUnit()->getIdPropertyUnitNaPortalu();
        $polje['units'][0]['rates'] = array();

        /* @var $unitRate UnitRate */
        foreach ($transfer->getSetupPropertyUnit()->getUnitRates() as $unitRate) {
            if ($unitRate->getAktivan()) {
                $polje['units'][$brojač]['rates'][$brojač2] = array();
                $polje['units'][$brojač]['rates'][$brojač2]['id_cjenik'] = $unitRate->getIdCjenik();
                $polje['units'][$brojač]['rates'][$brojač2]['id_rate_bookingcom'] = $unitRate->getRate()->getRateIdPortal();
                $polje['units'][$brojač]['rates'][$brojač2]['samo_cjenik'] = $unitRate->getSamoCjenik();
                if (!$unitRate->getSamoCjenik()) {
                    $polje['units'][$brojač]['rates'][$brojač2]['id_set'] = $unitRate->getIdSet();
                }
                $brojač2++;
            }
        }

        $username = "fiskalizacija";
        $password = "21jmdsh45";

        // jSON String for request
        $json_string = json_encode($polje);

        // Configuring curl options
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://agencije.atrade.hr/booking.com/lossender");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Cache-Control: no-cache'));
        curl_setopt($ch, CURLOPT_HEADER, 0); // bilo 0
        curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        // Getting results
        $result = curl_exec($ch);
        curl_close($ch);
//        $result = '{"response":"method,status,ticket_id\nlos_pricing,ok,c58c8b4a8378011f\n<ok><\/ok>\n<!-- RUID: [UmFuZG9tSVYkc2RlIyh9YVmB9GBKngJ4XoCvKihHCCS29uw9YGT+KcPYvfuKMofRJfswqPWwxj20cTgi7iVnNjm\/Pw\/Ce0sv] -->"}';
//        $result = '{"response":"method,fault_code,fault_message\nlos_pricing,1003,Failed to interpret line #1 with error message: \"rate_id [1592191] not valid for hotel [1165100]\". Raw line data: [2015-10-15,2,116510001,1592191].;';

//        $result = '{"response":"method,status,ticket_id\nlos_pricing,ok,68e87ae58e780156\n<ok><\/ok>\n<!-- RUID: [UmFuZG9tSVYkc2RlIyh9YU4O5ioyu\/wS9rpyxEpjx2eLdPXSnz+iE5k\/Qds3b7NMf2tfUAkzT4A5LIwxUbnFTZK230PHMAv2] -->"}';
        if (substr($result, 50, 2) == 'ok') $transfer->setOk(true);
//        return substr($result,50,2);
        return $result;

        //Slanje Responsa
//        $response = new JsonResponse();
//        $response->setData($polje);
//        $response = new Response((string)$result);
//        return $response;

//        {
//            "id_property":8859,
//   "id_property_bookingcom":1165100,
//   "date_start":"2015-11-11",
//   "date_end":"2015-12-11",
//   "units":[
//      {
//          "id_unit":22174,
//         "id_unit_bookingcom":116510001,
//         "rates":[
//            {
//                "id_cjenik":59411,
//               "id_rate_bookingcom":4045295,
//               "samo_cjenik":true
//            },
//            {
//                "id_cjenik":59411,
//               "id_rate_bookingcom":4045294,
//               "samo_cjenik":true
//            }
//         ]
//      },
//      {
//          "id_unit":22175,
//         "id_unit_bookingcom":116510003,
//         "rates":[
//            {
//                "id_cjenik":59408,
//               "id_rate_bookingcom":4045292,
//               "samo_cjenik":false,
//               "id_set":32344
//            }
//         ]
//      }
//   ]
//}
    }
}