<?php

namespace td\CMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PropertiesUnitsViewSetupType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'text', array('attr' => array('readonly' => 'readonly')))
//            ->add('idP1')
//            ->add('idP2')
//            ->add('idP3')
//            ->add('idP4')
//            ->add('idP5')
//            ->add('idP6')
//            ->add('idP7')
//            ->add('idP8')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'td\CMBundle\Entity\PropertiesUnitsViewSetup'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'td_cmbundle_propertiesunitsviewsetup';
    }
}
