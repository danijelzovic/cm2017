<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\MaxDepth;

/**
 * Property
 *
 * @ORM\Table(name="property", indexes={@ORM\Index(name="id_property_type", columns={"id_property_type"}), @ORM\Index(name="order_country", columns={"order_country", "order_region", "order_destination"}), @ORM\Index(name="id_country", columns={"id_country"}), @ORM\Index(name="order_region", columns={"order_region"}), @ORM\Index(name="order_destination", columns={"order_destination"}), @ORM\Index(name="book", columns={"book"}), @ORM\Index(name="category", columns={"category"}), @ORM\Index(name="id_dest_td", columns={"id_dest_td"}), @ORM\Index(name="id_region", columns={"id_region"}), @ORM\Index(name="id_subregion", columns={"id_subregion"}), @ORM\Index(name="deleted", columns={"deleted"}), @ORM\Index(name="active", columns={"active"}), @ORM\Index(name="status", columns={"status"}), @ORM\Index(name="open_from", columns={"open_from"}), @ORM\Index(name="open_until", columns={"open_until"}), @ORM\Index(name="char_service", columns={"char_service"}), @ORM\Index(name="resort", columns={"resort"}), @ORM\Index(name="char_special", columns={"char_special"}), @ORM\Index(name="char_basic", columns={"char_basic"}), @ORM\Index(name="id_agent", columns={"id_agent"}), @ORM\Index(name="char_location", columns={"char_location"}), @ORM\Index(name="city", columns={"city"}), @ORM\Index(name="open_from_2", columns={"open_from", "open_until"}), @ORM\Index(name="id_owner", columns={"id_owner"}), @ORM\Index(name="id_agent_2", columns={"id_agent", "id_owner"}), @ORM\Index(name="fiktivni", columns={"fiktivni"}), @ORM\Index(name="id_bureau", columns={"id_bureau"}), @ORM\Index(name="owner_id", columns={"owner_id"}), @ORM\Index(name="local_order", columns={"local_order"}), @ORM\Index(name="island", columns={"island"}), @ORM\Index(name="complete", columns={"complete"}), @ORM\Index(name="id_agent_c", columns={"id_agent_c"}), @ORM\Index(name="id_contact", columns={"id_contact"})})
 * @ORM\Entity(repositoryClass="td\CMBundle\Entity\Repository\PropertyRepository")
 * @ExclusionPolicy("all")
 */
class Property
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Expose
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_company", type="integer", nullable=false)
     * @Expose
     */
    private $idCompany;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_entered_by", type="integer", nullable=false)
     * @Expose
     */
    private $idEnteredBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_agent", type="integer", nullable=false, options={"unsigned":true, "default":1})
     */
    private $idAgent = 1;

    /**
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumn(name="id_agent", referencedColumnName="id")
     *
     */
    private $company;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_owner", type="bigint", nullable=false, options={"unsigned":true, "default":0})
     */
    private $idOwner = 0;

    /**
     * @ORM\ManyToOne(targetEntity="Client")
     * @ORM\JoinColumn(name="id_owner", referencedColumnName="id_client")
     *
     */
    private $owner;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_agent_c", type="integer", nullable=false)
     */
    private $idAgentC;

    /**
     * @ORM\ManyToOne(targetEntity="Client")
     * @ORM\JoinColumn(name="id_agent_c", referencedColumnName="id_client")
     *
     */
    private $agentC;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_owner_c", type="integer", nullable=false)
     */
    private $idOwnerC;

    /**
     * @ORM\ManyToOne(targetEntity="Client")
     * @ORM\JoinColumn(name="id_owner_c", referencedColumnName="id_client")
     *
     */
    private $ownerC;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_was", type="integer", nullable=false)
     */
    private $idWas;

    /**
     * @var string
     *
     * @ORM\Column(name="id_contact", type="string", length=255, nullable=false)
     */
    private $idContact = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="id_bureau", type="integer", nullable=false, options={"unsigned":true, "default":0})
     */
    private $idBureau = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_country", type="smallint", nullable=false, options={"default":999})
     */
    private $orderCountry = 999;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_region", type="smallint", nullable=false, options={"default":999})
     */
    private $orderRegion = 999;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_destination", type="smallint", nullable=false, options={"default":999})
     */
    private $orderDestination = 999;

    /**
     * @var integer
     *
     * @ORM\Column(name="owner_id", type="integer", nullable=false, options={"default":0})
     */
    private $ownerId = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="tis", type="boolean", nullable=false, options={"default":0})
     */
    private $tis = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="id_atd", type="string", length=12, nullable=false)
     */
    private $idAtd = '';

    /**
     * @var string
     *
     * @ORM\Column(name="id_td", type="string", length=12, nullable=false)
     */
    private $idTd = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="default_inquiry", type="boolean", nullable=false, options={"unsigned":true, "default":0})
     */
    private $defaultInquiry = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="default_online", type="boolean", nullable=false, options={"unsigned":true, "default":0})
     */
    private $defaultOnline = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="book", type="boolean", nullable=false, options={"unsigned":true, "default":0})
     */
    private $book = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cardonline", type="boolean", nullable=false, options={"default":0})
     */
    private $cardonline = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="book_change_user", type="integer", nullable=false, options={"unsigned":true, "default":0})
     */
    private $bookChangeUser = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="book_change_time", type="datetime", nullable=false, options={"default":"0000-00-00 00:00:00"})
     */
    private $bookChangeTime = '0000-00-00 00:00:00';

    /**
     * @var integer
     *
     * @ORM\Column(name="local_order", type="integer", nullable=false, options={"default":0})
     * @Expose
     */
    private $localOrder = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="id_amadeus", type="string", length=16, nullable=false)
     */
    private $idAmadeus = '';

    /**
     * @var string
     *
     * @ORM\Column(name="id_giata", type="string", length=12, nullable=false)
     */
    private $idGiata = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="id_tz", type="integer", nullable=false, options={"unsigned":true, "default":0})
     */
    private $idTz = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="code_tz", type="string", length=5, nullable=false)
     */
    private $codeTz = '';

    /**
     * @var string
     *
     * @ORM\Column(name="code_tz_destination", type="string", length=3, nullable=false)
     */
    private $codeTzDestination = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="property_type_tz", type="integer", nullable=false, options={"unsigned":true, "default":0})
     */
    private $propertyTypeTz = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="owner_code_tz", type="string", length=13, nullable=false)
     */
    private $ownerCodeTz = '';

    /**
     * @var string
     *
     * @ORM\Column(name="code_mup", type="string", length=9, nullable=false)
     */
    private $codeMup = '';

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Expose
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="resort", type="string", length=64, nullable=false)
     */
    private $resort;

    /**
     * @var boolean
     *
     * @ORM\Column(name="category", type="boolean", nullable=false, options={"default":0})
     */
    private $category = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="id_property_type", type="integer", nullable=false, options={"unsigned":true, "default":0})
     */
    private $idPropertyType = 0;

    /**
     * @ORM\ManyToOne(targetEntity="PropertyType")
     * @ORM\JoinColumn(name="id_property_type", referencedColumnName="id")
     * @Expose
     */
    private $propertyType;

    /**
     * @var string
     *
     * @ORM\Column(name="type_other", type="string", length=64, nullable=false)
     */
    private $typeOther = '';

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="text", nullable=false)
     */
    private $note;

    /**
     * @var string
     *
     * @ORM\Column(name="lat", type="decimal", precision=16, scale=14, nullable=false)
     */
    private $lat;

    /**
     * @var string
     *
     * @ORM\Column(name="lon", type="decimal", precision=16, scale=14, nullable=false)
     */
    private $lon;

    /**
     * @var integer
     *
     * @ORM\Column(name="alt", type="integer", nullable=true)
     */
    private $alt;

    /**
     * @var string
     *
     * @ORM\Column(name="address1", type="string", length=255, nullable=false)
     */
    private $address1 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="address2", type="string", length=255, nullable=false)
     */
    private $address2 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="zip", type="string", length=16, nullable=false)
     */
    private $zip = '';

    /**
     * @var string
     *
     * @ORM\Column(name="id_dest_td", type="string", length=11, nullable=false)
     */
    private $idDestTd = '';

    /**
     * @var string
     *
     * @ORM\Column(name="id_region", type="string", length=32, nullable=false)
     */
    private $idRegion = '';

    /**
     * @var string
     *
     * @ORM\Column(name="id_subregion", type="string", length=32, nullable=false)
     */
    private $idSubregion = '';

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=false)
     * @Expose
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="island", type="string", length=255, nullable=false)
     */
    private $island;

    /**
     * @var string
     *
     * @ORM\Column(name="id_country", type="string", length=2, nullable=false)
     * @Expose
     */
    private $idCountry = '';

    /**
     * @var string
     *
     * @ORM\Column(name="tel1", type="string", length=32, nullable=false)
     */
    private $tel1 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="tel2", type="string", length=32, nullable=false)
     */
    private $tel2 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="fax1", type="string", length=32, nullable=false)
     */
    private $fax1 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="fax2", type="string", length=32, nullable=false)
     */
    private $fax2 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="email1", type="string", length=64, nullable=false)
     */
    private $email1 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="email2", type="string", length=64, nullable=false)
     */
    private $email2 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="web", type="string", length=255, nullable=false)
     */
    private $web = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="number_single", type="smallint", nullable=false, options={"unsigned":true, "default":0})
     */
    private $numberSingle = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="number_double", type="smallint", nullable=false, options={"unsigned":true, "default":0})
     */
    private $numberDouble = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="number_shared", type="smallint", nullable=false, options={"unsigned":true, "default":0})
     */
    private $numberShared = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="number_suite", type="smallint", nullable=false, options={"unsigned":true, "default":0})
     */
    private $numberSuite = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="number_apartment", type="smallint", nullable=false, options={"unsigned":true, "default":0})
     */
    private $numberApartment = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="number_house", type="smallint", nullable=false, options={"default":0})
     */
    private $numberHouse = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="number_units", type="smallint", nullable=false, options={"unsigned":true, "default":0})
     * @Expose
     */
    private $numberUnits = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="number_beds", type="smallint", nullable=false, options={"unsigned":true, "default":0})
     */
    private $numberBeds = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="complete", type="boolean", nullable=false, options={"default":0})
     */
    private $complete = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false, options={"default":0})
     * @Expose
     */
    private $active = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="fiktivni", type="boolean", nullable=false)
     */
    private $fiktivni;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=false)
     * @Expose
     */
    private $status = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false)
     * @Expose
     */
    private $deleted = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="char_electricity", type="string", nullable=false, options={"default":"electricity_network_220"})
     */
    private $charElectricity = 'electricity_network_220';

    /**
     * @var string
     *
     * @ORM\Column(name="char_water", type="string", nullable=false, options={"default":"water_supply"})
     */
    private $charWater = 'water_supply';

    /**
     * @var string
     *
     * @ORM\Column(name="char_hot_water", type="string", nullable=false, options={"default":"hot_water_supply"})
     */
    private $charHotWater = 'hot_water_supply';

    /**
     * @var string
     *
     * @ORM\Column(name="char_special", type="string", nullable=false, options={"default":""})
     */
    private $charSpecial = '';

    /**
     * @var string
     *
     * @ORM\Column(name="char_location", type="string", nullable=false, options={"default":""})
     */
    private $charLocation = '';

    /**
     * @var string
     *
     * @ORM\Column(name="char_view", type="string", nullable=false, options={"default":""})
     */
    private $charView = '';

    /**
     * @var string
     *
     * @ORM\Column(name="char_paying", type="string", nullable=false, options={"default":""})
     */
    private $charPaying = '';

    /**
     * @var string
     *
     * @ORM\Column(name="char_service", type="string", nullable=false, options={"default":""})
     */
    private $charService = '';

    /**
     * @var string
     *
     * @ORM\Column(name="char_language", type="string", nullable=false, options={"default":""})
     */
    private $charLanguage = '';

    /**
     * @var string
     *
     * @ORM\Column(name="char_basic", type="string", nullable=false, options={"default":""})
     */
    private $charBasic = '';

    /**
     * @var string
     *
     * @ORM\Column(name="char_food", type="string", nullable=false, options={"default":""})
     */
    private $charFood = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="open_from", type="date", nullable=false, options={"default":"0000-01-01"})
     */
    private $openFrom = '0000-01-01';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="open_until", type="date", nullable=false, options={"default":"0000-12-31"})
     */
    private $openUntil = '0000-12-31';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="checkin_after", type="time", nullable=false, options={"default":"00:00:00"})
     */
    private $checkinAfter = '00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="checkout_until", type="time", nullable=false, options={"default":"00:00:00"})
     */
    private $checkoutUntil = '00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="min_price", type="decimal", precision=7, scale=2, nullable=false, options={"default":"0.00"})
     */
    private $minPrice = '0.00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_entered", type="datetime", nullable=false, options={"default":"0000-00-00 00:00:00"})
     * @Expose
     */
    private $dateEntered = '0000-00-00 00:00:00';

    /**
     * @var integer
     *
     * @ORM\Column(name="useekoloska", type="integer", nullable=false)
     */
    private $useekoloska;

    /**
     * @var integer
     *
     * @ORM\Column(name="useosiguranje", type="integer", nullable=false)
     */
    private $useosiguranje;

    /**
     * @var integer
     *
     * @ORM\Column(name="useturisticka", type="integer", nullable=false)
     */
    private $useturisticka;

    /**
     * @ORM\OneToMany(targetEntity="PropertyUnit", mappedBy="property")
     */
    private $propertyUnits;

    /**
     * @ORM\OneToMany(targetEntity="SetupProperty", mappedBy="property", fetch="EXTRA_LAZY")
     */
    private $setupProperties;

    /**
     * Get propertyFront
     *
     * @return string
     */
    public function getPropertyFront()
    {
        return $this->idCountry . '-' . str_pad($this->id, 5, '0', STR_PAD_LEFT);
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->propertyUnits = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idEnteredBy
     *
     * @param integer $idEnteredBy
     *
     * @return Property
     */
    public function setIdEnteredBy($idEnteredBy)
    {
        $this->idEnteredBy = $idEnteredBy;

        return $this;
    }

    /**
     * Get idEnteredBy
     *
     * @return integer
     */
    public function getIdEnteredBy()
    {
        return $this->idEnteredBy;
    }

    /**
     * Set idAgent
     *
     * @param integer $idAgent
     *
     * @return Property
     */
    public function setIdAgent($idAgent)
    {
        $this->idAgent = $idAgent;

        return $this;
    }

    /**
     * Get idAgent
     *
     * @return integer
     */
    public function getIdAgent()
    {
        return $this->idAgent;
    }

    /**
     * Set idOwner
     *
     * @param integer $idOwner
     *
     * @return Property
     */
    public function setIdOwner($idOwner)
    {
        $this->idOwner = $idOwner;

        return $this;
    }

    /**
     * Get idOwner
     *
     * @return integer
     */
    public function getIdOwner()
    {
        return $this->idOwner;
    }

    /**
     * Set idAgentC
     *
     * @param integer $idAgentC
     *
     * @return Property
     */
    public function setIdAgentC($idAgentC)
    {
        $this->idAgentC = $idAgentC;

        return $this;
    }

    /**
     * Get idAgentC
     *
     * @return integer
     */
    public function getIdAgentC()
    {
        return $this->idAgentC;
    }

    /**
     * Set idOwnerC
     *
     * @param integer $idOwnerC
     *
     * @return Property
     */
    public function setIdOwnerC($idOwnerC)
    {
        $this->idOwnerC = $idOwnerC;

        return $this;
    }

    /**
     * Get idOwnerC
     *
     * @return integer
     */
    public function getIdOwnerC()
    {
        return $this->idOwnerC;
    }

    /**
     * Set idWas
     *
     * @param integer $idWas
     *
     * @return Property
     */
    public function setIdWas($idWas)
    {
        $this->idWas = $idWas;

        return $this;
    }

    /**
     * Get idWas
     *
     * @return integer
     */
    public function getIdWas()
    {
        return $this->idWas;
    }

    /**
     * Set idContact
     *
     * @param string $idContact
     *
     * @return Property
     */
    public function setIdContact($idContact)
    {
        $this->idContact = $idContact;

        return $this;
    }

    /**
     * Get idContact
     *
     * @return string
     */
    public function getIdContact()
    {
        return $this->idContact;
    }

    /**
     * Set idBureau
     *
     * @param integer $idBureau
     *
     * @return Property
     */
    public function setIdBureau($idBureau)
    {
        $this->idBureau = $idBureau;

        return $this;
    }

    /**
     * Get idBureau
     *
     * @return integer
     */
    public function getIdBureau()
    {
        return $this->idBureau;
    }

    /**
     * Set orderCountry
     *
     * @param integer $orderCountry
     *
     * @return Property
     */
    public function setOrderCountry($orderCountry)
    {
        $this->orderCountry = $orderCountry;

        return $this;
    }

    /**
     * Get orderCountry
     *
     * @return integer
     */
    public function getOrderCountry()
    {
        return $this->orderCountry;
    }

    /**
     * Set orderRegion
     *
     * @param integer $orderRegion
     *
     * @return Property
     */
    public function setOrderRegion($orderRegion)
    {
        $this->orderRegion = $orderRegion;

        return $this;
    }

    /**
     * Get orderRegion
     *
     * @return integer
     */
    public function getOrderRegion()
    {
        return $this->orderRegion;
    }

    /**
     * Set orderDestination
     *
     * @param integer $orderDestination
     *
     * @return Property
     */
    public function setOrderDestination($orderDestination)
    {
        $this->orderDestination = $orderDestination;

        return $this;
    }

    /**
     * Get orderDestination
     *
     * @return integer
     */
    public function getOrderDestination()
    {
        return $this->orderDestination;
    }

    /**
     * Set ownerId
     *
     * @param integer $ownerId
     *
     * @return Property
     */
    public function setOwnerId($ownerId)
    {
        $this->ownerId = $ownerId;

        return $this;
    }

    /**
     * Get ownerId
     *
     * @return integer
     */
    public function getOwnerId()
    {
        return $this->ownerId;
    }

    /**
     * Set tis
     *
     * @param boolean $tis
     *
     * @return Property
     */
    public function setTis($tis)
    {
        $this->tis = $tis;

        return $this;
    }

    /**
     * Get tis
     *
     * @return boolean
     */
    public function getTis()
    {
        return $this->tis;
    }

    /**
     * Set idAtd
     *
     * @param string $idAtd
     *
     * @return Property
     */
    public function setIdAtd($idAtd)
    {
        $this->idAtd = $idAtd;

        return $this;
    }

    /**
     * Get idAtd
     *
     * @return string
     */
    public function getIdAtd()
    {
        return $this->idAtd;
    }

    /**
     * Set idTd
     *
     * @param string $idTd
     *
     * @return Property
     */
    public function setIdTd($idTd)
    {
        $this->idTd = $idTd;

        return $this;
    }

    /**
     * Get idTd
     *
     * @return string
     */
    public function getIdTd()
    {
        return $this->idTd;
    }

    /**
     * Set defaultInquiry
     *
     * @param boolean $defaultInquiry
     *
     * @return Property
     */
    public function setDefaultInquiry($defaultInquiry)
    {
        $this->defaultInquiry = $defaultInquiry;

        return $this;
    }

    /**
     * Get defaultInquiry
     *
     * @return boolean
     */
    public function getDefaultInquiry()
    {
        return $this->defaultInquiry;
    }

    /**
     * Set defaultOnline
     *
     * @param boolean $defaultOnline
     *
     * @return Property
     */
    public function setDefaultOnline($defaultOnline)
    {
        $this->defaultOnline = $defaultOnline;

        return $this;
    }

    /**
     * Get defaultOnline
     *
     * @return boolean
     */
    public function getDefaultOnline()
    {
        return $this->defaultOnline;
    }

    /**
     * Set book
     *
     * @param boolean $book
     *
     * @return Property
     */
    public function setBook($book)
    {
        $this->book = $book;

        return $this;
    }

    /**
     * Get book
     *
     * @return boolean
     */
    public function getBook()
    {
        return $this->book;
    }

    /**
     * Set cardonline
     *
     * @param boolean $cardonline
     *
     * @return Property
     */
    public function setCardonline($cardonline)
    {
        $this->cardonline = $cardonline;

        return $this;
    }

    /**
     * Get cardonline
     *
     * @return boolean
     */
    public function getCardonline()
    {
        return $this->cardonline;
    }

    /**
     * Set bookChangeUser
     *
     * @param integer $bookChangeUser
     *
     * @return Property
     */
    public function setBookChangeUser($bookChangeUser)
    {
        $this->bookChangeUser = $bookChangeUser;

        return $this;
    }

    /**
     * Get bookChangeUser
     *
     * @return integer
     */
    public function getBookChangeUser()
    {
        return $this->bookChangeUser;
    }

    /**
     * Set bookChangeTime
     *
     * @param \DateTime $bookChangeTime
     *
     * @return Property
     */
    public function setBookChangeTime($bookChangeTime)
    {
        $this->bookChangeTime = $bookChangeTime;

        return $this;
    }

    /**
     * Get bookChangeTime
     *
     * @return \DateTime
     */
    public function getBookChangeTime()
    {
        return $this->bookChangeTime;
    }

    /**
     * Set localOrder
     *
     * @param integer $localOrder
     *
     * @return Property
     */
    public function setLocalOrder($localOrder)
    {
        $this->localOrder = $localOrder;

        return $this;
    }

    /**
     * Get localOrder
     *
     * @return integer
     */
    public function getLocalOrder()
    {
        return $this->localOrder;
    }

    /**
     * Set idAmadeus
     *
     * @param string $idAmadeus
     *
     * @return Property
     */
    public function setIdAmadeus($idAmadeus)
    {
        $this->idAmadeus = $idAmadeus;

        return $this;
    }

    /**
     * Get idAmadeus
     *
     * @return string
     */
    public function getIdAmadeus()
    {
        return $this->idAmadeus;
    }

    /**
     * Set idGiata
     *
     * @param string $idGiata
     *
     * @return Property
     */
    public function setIdGiata($idGiata)
    {
        $this->idGiata = $idGiata;

        return $this;
    }

    /**
     * Get idGiata
     *
     * @return string
     */
    public function getIdGiata()
    {
        return $this->idGiata;
    }

    /**
     * Set idTz
     *
     * @param integer $idTz
     *
     * @return Property
     */
    public function setIdTz($idTz)
    {
        $this->idTz = $idTz;

        return $this;
    }

    /**
     * Get idTz
     *
     * @return integer
     */
    public function getIdTz()
    {
        return $this->idTz;
    }

    /**
     * Set codeTz
     *
     * @param string $codeTz
     *
     * @return Property
     */
    public function setCodeTz($codeTz)
    {
        $this->codeTz = $codeTz;

        return $this;
    }

    /**
     * Get codeTz
     *
     * @return string
     */
    public function getCodeTz()
    {
        return $this->codeTz;
    }

    /**
     * Set codeTzDestination
     *
     * @param string $codeTzDestination
     *
     * @return Property
     */
    public function setCodeTzDestination($codeTzDestination)
    {
        $this->codeTzDestination = $codeTzDestination;

        return $this;
    }

    /**
     * Get codeTzDestination
     *
     * @return string
     */
    public function getCodeTzDestination()
    {
        return $this->codeTzDestination;
    }

    /**
     * Set propertyTypeTz
     *
     * @param integer $propertyTypeTz
     *
     * @return Property
     */
    public function setPropertyTypeTz($propertyTypeTz)
    {
        $this->propertyTypeTz = $propertyTypeTz;

        return $this;
    }

    /**
     * Get propertyTypeTz
     *
     * @return integer
     */
    public function getPropertyTypeTz()
    {
        return $this->propertyTypeTz;
    }

    /**
     * Set ownerCodeTz
     *
     * @param string $ownerCodeTz
     *
     * @return Property
     */
    public function setOwnerCodeTz($ownerCodeTz)
    {
        $this->ownerCodeTz = $ownerCodeTz;

        return $this;
    }

    /**
     * Get ownerCodeTz
     *
     * @return string
     */
    public function getOwnerCodeTz()
    {
        return $this->ownerCodeTz;
    }

    /**
     * Set codeMup
     *
     * @param string $codeMup
     *
     * @return Property
     */
    public function setCodeMup($codeMup)
    {
        $this->codeMup = $codeMup;

        return $this;
    }

    /**
     * Get codeMup
     *
     * @return string
     */
    public function getCodeMup()
    {
        return $this->codeMup;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Property
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set resort
     *
     * @param string $resort
     *
     * @return Property
     */
    public function setResort($resort)
    {
        $this->resort = $resort;

        return $this;
    }

    /**
     * Get resort
     *
     * @return string
     */
    public function getResort()
    {
        return $this->resort;
    }

    /**
     * Set category
     *
     * @param boolean $category
     *
     * @return Property
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return boolean
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set idPropertyType
     *
     * @param boolean $idPropertyType
     *
     * @return Property
     */
    public function setIdPropertyType($idPropertyType)
    {
        $this->idPropertyType = $idPropertyType;

        return $this;
    }

    /**
     * Get idPropertyType
     *
     * @return boolean
     */
    public function getIdPropertyType()
    {
        return $this->idPropertyType;
    }

    /**
     * Set typeOther
     *
     * @param string $typeOther
     *
     * @return Property
     */
    public function setTypeOther($typeOther)
    {
        $this->typeOther = $typeOther;

        return $this;
    }

    /**
     * Get typeOther
     *
     * @return string
     */
    public function getTypeOther()
    {
        return $this->typeOther;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return Property
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set lat
     *
     * @param string $lat
     *
     * @return Property
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return string
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lon
     *
     * @param string $lon
     *
     * @return Property
     */
    public function setLon($lon)
    {
        $this->lon = $lon;

        return $this;
    }

    /**
     * Get lon
     *
     * @return string
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * Set alt
     *
     * @param integer $alt
     *
     * @return Property
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;

        return $this;
    }

    /**
     * Get alt
     *
     * @return integer
     */
    public function getAlt()
    {
        return $this->alt;
    }

    /**
     * Set address1
     *
     * @param string $address1
     *
     * @return Property
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * Get address1
     *
     * @return string
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Set address2
     *
     * @param string $address2
     *
     * @return Property
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get address2
     *
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set zip
     *
     * @param string $zip
     *
     * @return Property
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set idDestTd
     *
     * @param string $idDestTd
     *
     * @return Property
     */
    public function setIdDestTd($idDestTd)
    {
        $this->idDestTd = $idDestTd;

        return $this;
    }

    /**
     * Get idDestTd
     *
     * @return string
     */
    public function getIdDestTd()
    {
        return $this->idDestTd;
    }

    /**
     * Set idRegion
     *
     * @param string $idRegion
     *
     * @return Property
     */
    public function setIdRegion($idRegion)
    {
        $this->idRegion = $idRegion;

        return $this;
    }

    /**
     * Get idRegion
     *
     * @return string
     */
    public function getIdRegion()
    {
        return $this->idRegion;
    }

    /**
     * Set idSubregion
     *
     * @param string $idSubregion
     *
     * @return Property
     */
    public function setIdSubregion($idSubregion)
    {
        $this->idSubregion = $idSubregion;

        return $this;
    }

    /**
     * Get idSubregion
     *
     * @return string
     */
    public function getIdSubregion()
    {
        return $this->idSubregion;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Property
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set island
     *
     * @param string $island
     *
     * @return Property
     */
    public function setIsland($island)
    {
        $this->island = $island;

        return $this;
    }

    /**
     * Get island
     *
     * @return string
     */
    public function getIsland()
    {
        return $this->island;
    }

    /**
     * Set idCountry
     *
     * @param string $idCountry
     *
     * @return Property
     */
    public function setIdCountry($idCountry)
    {
        $this->idCountry = $idCountry;

        return $this;
    }

    /**
     * Get idCountry
     *
     * @return string
     */
    public function getIdCountry()
    {
        return $this->idCountry;
    }

    /**
     * Set tel1
     *
     * @param string $tel1
     *
     * @return Property
     */
    public function setTel1($tel1)
    {
        $this->tel1 = $tel1;

        return $this;
    }

    /**
     * Get tel1
     *
     * @return string
     */
    public function getTel1()
    {
        return $this->tel1;
    }

    /**
     * Set tel2
     *
     * @param string $tel2
     *
     * @return Property
     */
    public function setTel2($tel2)
    {
        $this->tel2 = $tel2;

        return $this;
    }

    /**
     * Get tel2
     *
     * @return string
     */
    public function getTel2()
    {
        return $this->tel2;
    }

    /**
     * Set fax1
     *
     * @param string $fax1
     *
     * @return Property
     */
    public function setFax1($fax1)
    {
        $this->fax1 = $fax1;

        return $this;
    }

    /**
     * Get fax1
     *
     * @return string
     */
    public function getFax1()
    {
        return $this->fax1;
    }

    /**
     * Set fax2
     *
     * @param string $fax2
     *
     * @return Property
     */
    public function setFax2($fax2)
    {
        $this->fax2 = $fax2;

        return $this;
    }

    /**
     * Get fax2
     *
     * @return string
     */
    public function getFax2()
    {
        return $this->fax2;
    }

    /**
     * Set email1
     *
     * @param string $email1
     *
     * @return Property
     */
    public function setEmail1($email1)
    {
        $this->email1 = $email1;

        return $this;
    }

    /**
     * Get email1
     *
     * @return string
     */
    public function getEmail1()
    {
        return $this->email1;
    }

    /**
     * Set email2
     *
     * @param string $email2
     *
     * @return Property
     */
    public function setEmail2($email2)
    {
        $this->email2 = $email2;

        return $this;
    }

    /**
     * Get email2
     *
     * @return string
     */
    public function getEmail2()
    {
        return $this->email2;
    }

    /**
     * Set web
     *
     * @param string $web
     *
     * @return Property
     */
    public function setWeb($web)
    {
        $this->web = $web;

        return $this;
    }

    /**
     * Get web
     *
     * @return string
     */
    public function getWeb()
    {
        return $this->web;
    }

    /**
     * Set numberSingle
     *
     * @param integer $numberSingle
     *
     * @return Property
     */
    public function setNumberSingle($numberSingle)
    {
        $this->numberSingle = $numberSingle;

        return $this;
    }

    /**
     * Get numberSingle
     *
     * @return integer
     */
    public function getNumberSingle()
    {
        return $this->numberSingle;
    }

    /**
     * Set numberDouble
     *
     * @param integer $numberDouble
     *
     * @return Property
     */
    public function setNumberDouble($numberDouble)
    {
        $this->numberDouble = $numberDouble;

        return $this;
    }

    /**
     * Get numberDouble
     *
     * @return integer
     */
    public function getNumberDouble()
    {
        return $this->numberDouble;
    }

    /**
     * Set numberShared
     *
     * @param integer $numberShared
     *
     * @return Property
     */
    public function setNumberShared($numberShared)
    {
        $this->numberShared = $numberShared;

        return $this;
    }

    /**
     * Get numberShared
     *
     * @return integer
     */
    public function getNumberShared()
    {
        return $this->numberShared;
    }

    /**
     * Set numberSuite
     *
     * @param integer $numberSuite
     *
     * @return Property
     */
    public function setNumberSuite($numberSuite)
    {
        $this->numberSuite = $numberSuite;

        return $this;
    }

    /**
     * Get numberSuite
     *
     * @return integer
     */
    public function getNumberSuite()
    {
        return $this->numberSuite;
    }

    /**
     * Set numberApartment
     *
     * @param integer $numberApartment
     *
     * @return Property
     */
    public function setNumberApartment($numberApartment)
    {
        $this->numberApartment = $numberApartment;

        return $this;
    }

    /**
     * Get numberApartment
     *
     * @return integer
     */
    public function getNumberApartment()
    {
        return $this->numberApartment;
    }

    /**
     * Set numberHouse
     *
     * @param integer $numberHouse
     *
     * @return Property
     */
    public function setNumberHouse($numberHouse)
    {
        $this->numberHouse = $numberHouse;

        return $this;
    }

    /**
     * Get numberHouse
     *
     * @return integer
     */
    public function getNumberHouse()
    {
        return $this->numberHouse;
    }

    /**
     * Set numberUnits
     *
     * @param integer $numberUnits
     *
     * @return Property
     */
    public function setNumberUnits($numberUnits)
    {
        $this->numberUnits = $numberUnits;

        return $this;
    }

    /**
     * Get numberUnits
     *
     * @return integer
     */
    public function getNumberUnits()
    {
        return $this->numberUnits;
    }

    /**
     * Set numberBeds
     *
     * @param integer $numberBeds
     *
     * @return Property
     */
    public function setNumberBeds($numberBeds)
    {
        $this->numberBeds = $numberBeds;

        return $this;
    }

    /**
     * Get numberBeds
     *
     * @return integer
     */
    public function getNumberBeds()
    {
        return $this->numberBeds;
    }

    /**
     * Set complete
     *
     * @param boolean $complete
     *
     * @return Property
     */
    public function setComplete($complete)
    {
        $this->complete = $complete;

        return $this;
    }

    /**
     * Get complete
     *
     * @return boolean
     */
    public function getComplete()
    {
        return $this->complete;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Property
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set fiktivni
     *
     * @param boolean $fiktivni
     *
     * @return Property
     */
    public function setFiktivni($fiktivni)
    {
        $this->fiktivni = $fiktivni;

        return $this;
    }

    /**
     * Get fiktivni
     *
     * @return boolean
     */
    public function getFiktivni()
    {
        return $this->fiktivni;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Property
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Property
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set charElectricity
     *
     * @param string $charElectricity
     *
     * @return Property
     */
    public function setCharElectricity($charElectricity)
    {
        $this->charElectricity = $charElectricity;

        return $this;
    }

    /**
     * Get charElectricity
     *
     * @return string
     */
    public function getCharElectricity()
    {
        return $this->charElectricity;
    }

    /**
     * Set charWater
     *
     * @param string $charWater
     *
     * @return Property
     */
    public function setCharWater($charWater)
    {
        $this->charWater = $charWater;

        return $this;
    }

    /**
     * Get charWater
     *
     * @return string
     */
    public function getCharWater()
    {
        return $this->charWater;
    }

    /**
     * Set charHotWater
     *
     * @param string $charHotWater
     *
     * @return Property
     */
    public function setCharHotWater($charHotWater)
    {
        $this->charHotWater = $charHotWater;

        return $this;
    }

    /**
     * Get charHotWater
     *
     * @return string
     */
    public function getCharHotWater()
    {
        return $this->charHotWater;
    }

    /**
     * Set charSpecial
     *
     * @param string $charSpecial
     *
     * @return Property
     */
    public function setCharSpecial($charSpecial)
    {
        $this->charSpecial = $charSpecial;

        return $this;
    }

    /**
     * Get charSpecial
     *
     * @return string
     */
    public function getCharSpecial()
    {
        return $this->charSpecial;
    }

    /**
     * Set charLocation
     *
     * @param string $charLocation
     *
     * @return Property
     */
    public function setCharLocation($charLocation)
    {
        $this->charLocation = $charLocation;

        return $this;
    }

    /**
     * Get charLocation
     *
     * @return string
     */
    public function getCharLocation()
    {
        return $this->charLocation;
    }

    /**
     * Set charView
     *
     * @param string $charView
     *
     * @return Property
     */
    public function setCharView($charView)
    {
        $this->charView = $charView;

        return $this;
    }

    /**
     * Get charView
     *
     * @return string
     */
    public function getCharView()
    {
        return $this->charView;
    }

    /**
     * Set charPaying
     *
     * @param string $charPaying
     *
     * @return Property
     */
    public function setCharPaying($charPaying)
    {
        $this->charPaying = $charPaying;

        return $this;
    }

    /**
     * Get charPaying
     *
     * @return string
     */
    public function getCharPaying()
    {
        return $this->charPaying;
    }

    /**
     * Set charService
     *
     * @param string $charService
     *
     * @return Property
     */
    public function setCharService($charService)
    {
        $this->charService = $charService;

        return $this;
    }

    /**
     * Get charService
     *
     * @return string
     */
    public function getCharService()
    {
        return $this->charService;
    }

    /**
     * Set charLanguage
     *
     * @param string $charLanguage
     *
     * @return Property
     */
    public function setCharLanguage($charLanguage)
    {
        $this->charLanguage = $charLanguage;

        return $this;
    }

    /**
     * Get charLanguage
     *
     * @return string
     */
    public function getCharLanguage()
    {
        return $this->charLanguage;
    }

    /**
     * Set charBasic
     *
     * @param string $charBasic
     *
     * @return Property
     */
    public function setCharBasic($charBasic)
    {
        $this->charBasic = $charBasic;

        return $this;
    }

    /**
     * Get charBasic
     *
     * @return string
     */
    public function getCharBasic()
    {
        return $this->charBasic;
    }

    /**
     * Set charFood
     *
     * @param string $charFood
     *
     * @return Property
     */
    public function setCharFood($charFood)
    {
        $this->charFood = $charFood;

        return $this;
    }

    /**
     * Get charFood
     *
     * @return string
     */
    public function getCharFood()
    {
        return $this->charFood;
    }

    /**
     * Set openFrom
     *
     * @param \DateTime $openFrom
     *
     * @return Property
     */
    public function setOpenFrom($openFrom)
    {
        $this->openFrom = $openFrom;

        return $this;
    }

    /**
     * Get openFrom
     *
     * @return \DateTime
     */
    public function getOpenFrom()
    {
        return $this->openFrom;
    }

    /**
     * Set openUntil
     *
     * @param \DateTime $openUntil
     *
     * @return Property
     */
    public function setOpenUntil($openUntil)
    {
        $this->openUntil = $openUntil;

        return $this;
    }

    /**
     * Get openUntil
     *
     * @return \DateTime
     */
    public function getOpenUntil()
    {
        return $this->openUntil;
    }

    /**
     * Set checkinAfter
     *
     * @param \DateTime $checkinAfter
     *
     * @return Property
     */
    public function setCheckinAfter($checkinAfter)
    {
        $this->checkinAfter = $checkinAfter;

        return $this;
    }

    /**
     * Get checkinAfter
     *
     * @return \DateTime
     */
    public function getCheckinAfter()
    {
        return $this->checkinAfter;
    }

    /**
     * Set checkoutUntil
     *
     * @param \DateTime $checkoutUntil
     *
     * @return Property
     */
    public function setCheckoutUntil($checkoutUntil)
    {
        $this->checkoutUntil = $checkoutUntil;

        return $this;
    }

    /**
     * Get checkoutUntil
     *
     * @return \DateTime
     */
    public function getCheckoutUntil()
    {
        return $this->checkoutUntil;
    }

    /**
     * Set minPrice
     *
     * @param string $minPrice
     *
     * @return Property
     */
    public function setMinPrice($minPrice)
    {
        $this->minPrice = $minPrice;

        return $this;
    }

    /**
     * Get minPrice
     *
     * @return string
     */
    public function getMinPrice()
    {
        return $this->minPrice;
    }

    /**
     * Set dateEntered
     *
     * @param \DateTime $dateEntered
     *
     * @return Property
     */
    public function setDateEntered($dateEntered)
    {
        $this->dateEntered = $dateEntered;

        return $this;
    }

    /**
     * Get dateEntered
     *
     * @return \DateTime
     */
    public function getDateEntered()
    {
        return $this->dateEntered;
    }

    /**
     * Set useekoloska
     *
     * @param integer $useekoloska
     *
     * @return Property
     */
    public function setUseekoloska($useekoloska)
    {
        $this->useekoloska = $useekoloska;

        return $this;
    }

    /**
     * Get useekoloska
     *
     * @return integer
     */
    public function getUseekoloska()
    {
        return $this->useekoloska;
    }

    /**
     * Set useosiguranje
     *
     * @param integer $useosiguranje
     *
     * @return Property
     */
    public function setUseosiguranje($useosiguranje)
    {
        $this->useosiguranje = $useosiguranje;

        return $this;
    }

    /**
     * Get useosiguranje
     *
     * @return integer
     */
    public function getUseosiguranje()
    {
        return $this->useosiguranje;
    }

    /**
     * Set useturisticka
     *
     * @param integer $useturisticka
     *
     * @return Property
     */
    public function setUseturisticka($useturisticka)
    {
        $this->useturisticka = $useturisticka;

        return $this;
    }

    /**
     * Get useturisticka
     *
     * @return integer
     */
    public function getUseturisticka()
    {
        return $this->useturisticka;
    }

    /**
     * Set owner
     *
     * @param \td\CMBundle\Entity\Client $owner
     *
     * @return Property
     */
    public function setOwner(\td\CMBundle\Entity\Client $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \td\CMBundle\Entity\Client
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set agentC
     *
     * @param \td\CMBundle\Entity\Client $agentC
     *
     * @return Property
     */
    public function setAgentC(\td\CMBundle\Entity\Client $agentC = null)
    {
        $this->agentC = $agentC;

        return $this;
    }

    /**
     * Get agentC
     *
     * @return \td\CMBundle\Entity\Client
     */
    public function getAgentC()
    {
        return $this->agentC;
    }

    /**
     * Add propertyUnit
     *
     * @param \td\CMBundle\Entity\PropertyUnit $propertyUnit
     *
     * @return Property
     */
    public function addPropertyUnit(\td\CMBundle\Entity\PropertyUnit $propertyUnit)
    {
        $this->propertyUnits[] = $propertyUnit;

        return $this;
    }

    /**
     * Remove propertyUnit
     *
     * @param \td\CMBundle\Entity\PropertyUnit $propertyUnit
     */
    public function removePropertyUnit(\td\CMBundle\Entity\PropertyUnit $propertyUnit)
    {
        $this->propertyUnits->removeElement($propertyUnit);
    }

    /**
     * Get propertyUnits
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPropertyUnits()
    {
        return $this->propertyUnits;
    }

    /**
     * Set idCompany
     *
     * @param integer $idCompany
     *
     * @return Property
     */
    public function setIdCompany($idCompany)
    {
        $this->idCompany = $idCompany;

        return $this;
    }

    /**
     * Get idCompany
     *
     * @return integer
     */
    public function getIdCompany()
    {
        return $this->idCompany;
    }

    /**
     * Set propertyType
     *
     * @param \td\CMBundle\Entity\PropertyType $propertyType
     *
     * @return Property
     */
    public function setPropertyType(\td\CMBundle\Entity\PropertyType $propertyType = null)
    {
        $this->propertyType = $propertyType;

        return $this;
    }

    /**
     * Get propertyType
     *
     * @return \td\CMBundle\Entity\PropertyType
     */
    public function getPropertyType()
    {
        return $this->propertyType;
    }

    /**
     * Add setupProperty
     *
     * @param \td\CMBundle\Entity\SetupProperty $setupProperty
     *
     * @return Property
     */
    public function addSetupProperty(\td\CMBundle\Entity\SetupProperty $setupProperty)
    {
        $this->setupProperties[] = $setupProperty;

        return $this;
    }

    /**
     * Remove setupProperty
     *
     * @param \td\CMBundle\Entity\SetupProperty $setupProperty
     */
    public function removeSetupProperty(\td\CMBundle\Entity\SetupProperty $setupProperty)
    {
        $this->setupProperties->removeElement($setupProperty);
    }

    /**
     * Get setupProperties
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSetupProperties()
    {
        return $this->setupProperties;
    }

    /**
     * Set ownerC
     *
     * @param \td\CMBundle\Entity\Client $ownerC
     *
     * @return Property
     */
    public function setOwnerC(\td\CMBundle\Entity\Client $ownerC = null)
    {
        $this->ownerC = $ownerC;

        return $this;
    }

    /**
     * Get ownerC
     *
     * @return \td\CMBundle\Entity\Client
     */
    public function getOwnerC()
    {
        return $this->ownerC;
    }

    /**
     * Set company
     *
     * @param \td\CMBundle\Entity\Company $company
     *
     * @return Property
     */
    public function setCompany(\td\CMBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \td\CMBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }
}
