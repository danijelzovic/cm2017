<?php

namespace td\CMBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use td\CMBundle\Entity\PropertiesUnitsViewSetup;
use td\CMBundle\Form\PropertiesUnitsViewSetupType;

/**
 * PropertiesUnitsViewSetup controller.
 *
 */
class PropertiesUnitsViewSetupController extends Controller
{

    /**
     * Lists all PropertiesUnitsViewSetup entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('tdCMBundle:PropertiesUnitsViewSetup')->findAll();

        return $this->render('tdCMBundle:PropertiesUnitsViewSetup:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new PropertiesUnitsViewSetup entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new PropertiesUnitsViewSetup();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('propertiesunitsviewsetup_show', array('id' => $entity->getId())));
        }

        return $this->render('tdCMBundle:PropertiesUnitsViewSetup:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a PropertiesUnitsViewSetup entity.
     *
     * @param PropertiesUnitsViewSetup $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(PropertiesUnitsViewSetup $entity)
    {
        $form = $this->createForm(new PropertiesUnitsViewSetupType(), $entity, array(
            'action' => $this->generateUrl('propertiesunitsviewsetup_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Izradi',
            'attr' => array(
                'class' => 'btn-sm btn-success',
            )));

        return $form;
    }

    /**
     * Displays a form to create a new PropertiesUnitsViewSetup entity.
     *
     */
    public function newAction()
    {
        $entity = new PropertiesUnitsViewSetup();
        $form = $this->createCreateForm($entity);

        return $this->render('tdCMBundle:PropertiesUnitsViewSetup:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a PropertiesUnitsViewSetup entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('tdCMBundle:PropertiesUnitsViewSetup')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PropertiesUnitsViewSetup entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('tdCMBundle:PropertiesUnitsViewSetup:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing PropertiesUnitsViewSetup entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('tdCMBundle:PropertiesUnitsViewSetup')->find($id);
//        $choiceList = $this->preparePortalsChoiceList();
//        return new JsonResponse($choiceList);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PropertiesUnitsViewSetup entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('tdCMBundle:PropertiesUnitsViewSetup:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a PropertiesUnitsViewSetup entity.
     *
     * @param PropertiesUnitsViewSetup $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(PropertiesUnitsViewSetup $entity)
    {
        $form = $this->createForm(new PropertiesUnitsViewSetupType(), $entity, array(
            'action' => $this->generateUrl('propertiesunitsviewsetup_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $choiceList = $this->preparePortalsChoiceList();

        $form->add('idP1', 'choice', array('choices' => $choiceList));
        $form->add('idP2', 'choice', array('choices' => $choiceList));
        $form->add('idP3', 'choice', array('choices' => $choiceList));
        $form->add('idP4', 'choice', array('choices' => $choiceList));
        $form->add('idP5', 'choice', array('choices' => $choiceList));
        $form->add('idP6', 'choice', array('choices' => $choiceList));
//        $form->add('idP7', 'choice', array('choices' => $choiceList));
//        $form->add('idP8', 'choice', array('choices' => $choiceList));

        $form->add('submit', 'submit', array(
            'label' => 'Ažuriraj',
            'attr' => array(
                'class' => 'btn-primary btn-sm',
            )
        ));

        return $form;
    }

    /**
     * Edits an existing PropertiesUnitsViewSetup entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('tdCMBundle:PropertiesUnitsViewSetup')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PropertiesUnitsViewSetup entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('propertiesunitsviewsetup_edit', array('id' => $id)));
        }

        return $this->render('tdCMBundle:PropertiesUnitsViewSetup:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a PropertiesUnitsViewSetup entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('tdCMBundle:PropertiesUnitsViewSetup')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find PropertiesUnitsViewSetup entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('propertiesunitsviewsetup'));
    }

    /**
     * Creates a form to delete a PropertiesUnitsViewSetup entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('propertiesunitsviewsetup_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                'label' => 'Obriši',
                'attr' => array(
                    'class' => 'btn-danger btn-sm',
                )
            ))
            ->getForm();
    }

    /**
     * @return array
     */
    private function preparePortalsChoiceList()
    {
        $em = $this->getDoctrine()->getManager();

        $choiceList = array();
        $portalsIdAndName = $em->getRepository('tdCMBundle:Portal')->findAllIdAndName();
        foreach ($portalsIdAndName as $portalIdAndName) {
//            $choiceList['[' . $portalIdAndName['id'] . '] ' . $portalIdAndName['naziv']] = $portalIdAndName['id'];
            $choiceList[$portalIdAndName['id']] = '[' . $portalIdAndName['id'] . '] ' . $portalIdAndName['naziv'];
        }
        return $choiceList;
    }
}
