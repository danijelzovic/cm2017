<?php
/**
 * Created by PhpStorm.
 * User: danijel
 * Date: 12.4.2016.
 * Time: 4:43
 */

?>

<div class="row">
    <div class="col-md-3 pull-right">
        <div class="input-group">
            <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-search"
                                                                    aria-hidden="true"></span></span>
            <input type="text" class="form-control" placeholder="Tra�i..." aria-describedby="basic-addon1"
                   ng-model="query">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-hover table-bordered table-condensed table-custom-1">
            <thead>
            <tr>
                <th width="35">#</th>
                <th width="100">ID</th>
                <th>Objekt / Smje�tajna jedinica</th>
                <th>iCal poveznica</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>

            <tr ng-repeat="item in items | filter:query" class="object">
                <td class="text-right">1.</td>
                <td>HR-058978</td>
                <td class="object"><strong>Apartmani Polaris</strong></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="text-right">&nbsp;</td>
                <td>HR-058978-01</td>
                <td class="unit">Jednokrevetna soba [Single Room] [1+0]</td>
                <td>asdfaewfa486-sdf456asd4f-asd5fa6sf548-5641asdf1a5sdf-sadfas</td>
                <td class="text-right actions">
                    <a href="#" class="btn btn-small blue-booker"><span class="fa fa-check"></span> Kreiraj iCal poveznicu</a>
                </td>
            </tr>
            <tr>
                <td class="text-right">&nbsp;</td>
                <td>HR-058978-01</td>
                <td class="unit">Trokrevetna soba [Triple Room] [2+1]</td>
                <td>asdfaewfa486-sdf456asd4f-asd5fa6sf548-5641asdf1a5sdf-sadfas</td>
                <td class="text-right actions">
                    <a href="#" class="btn btn-small blue-booker"><span class="fa fa-check"></span> Kreiraj iCal poveznicu</a>
                </td>
            </tr>
            <tr>
                <td class="text-right">&nbsp;</td>
                <td>HR-058978-01</td>
                <td class="unit">Dvokrevetna soba [Double] [2+0]</td>
                <td>&nbsp;</td>
                <td class="text-right actions">
                    <a href="#" class="btn btn-small blue-booker"><span class="fa fa-check"></span> Kreiraj iCal poveznicu</a>
                </td>
            </tr>
            <tr class="object">
                <td class="text-right">2.</td>
                <td>HR-058978</td>
                <td class="object"><strong>Apartmani Dubrovnik</strong></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="text-right">&nbsp;</td>
                <td>HR-058978-01</td>
                <td class="unit">Jednokrevetna soba [Single Room] [1+0]</td>
                <td>asdfaewfa486-sdf456asd4f-asd5fa6sf548-5641asdf1a5sdf-sadfas</td>
                <td class="text-right actions">
                    <a href="#" class="btn btn-small blue-booker"><span class="fa fa-check"></span> Kreiraj iCal poveznicu</a>
                </td>
            </tr>
            <tr>
                <td class="text-right">&nbsp;</td>
                <td>HR-058978-01</td>
                <td class="unit">Jednokrevetna soba [Single Room] [1+0]</td>
                <td>asdfaewfa486-sdf456asd4f-asd5fa6sf548-5641asdf1a5sdf-sadfas</td>
                <td class="text-right actions">
                    <a href="#" class="btn btn-small blue-booker"><span class="fa fa-check"></span> Kreiraj iCal poveznicu</a>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>


