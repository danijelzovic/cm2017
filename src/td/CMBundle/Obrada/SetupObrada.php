<?php
/**
 * Created by PhpStorm.
 * User: danijel
 * Date: 4.5.2016.
 * Time: 20:19
 */

namespace td\CMBundle\Obrada;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\NoResultException;
use td\CMBundle\Entity\Portal;
use td\CMBundle\Entity\PortalKorisnik;
use td\CMBundle\Entity\Property;
use td\CMBundle\Entity\PropertyUnit;
use td\CMBundle\Entity\Rate;
use td\CMBundle\Entity\SetupProperty;
use td\CMBundle\Entity\SetupPropertyUnit;
use Doctrine\ORM\EntityManager;
use td\CMBundle\Entity\UnitRate;

class SetupObrada
{
    const XML_TEST = '<rooms>
<room id="46416701"
        hotel_id="464167"
        hotel_name="Steve\'s Sunny hotel"
        max_children="0"
        room_name="Standard Double Room with Sea View">
<rates>
<rate id="1590909"
            is_child_rate="1"
            max_persons="2"
            policy="General"
            policy_id="82024976"
            rate_name="DEAL89"
            readonly="1" />
<rate id="1592191"
            is_child_rate="1"
            max_persons="2"
            policy="Non Refundable"
            policy_id="82024984"
            rate_name="Non-refundable" />
<rate id="1578444"
            max_persons="2"
            policy="General"
            policy_id="82024976"
            rate_name="Standard Rate" />
<rate id="1589701"
            is_child_rate="1"
            max_persons="2"
            policy="General"
            policy_id="82024976"
            rate_name="DEAL71"
            readonly="1" />
</rates>
</room>
<room id="46416702"
        hotel_id="464167"
        hotel_name="Steve\'s Sunny hotel"
        max_children="0"
        room_name="Superior Studio">
<rates>
<rate id="1590909"
            is_child_rate="1"
            max_persons="4"
            policy="General"
            policy_id="82024976"
            rate_name="DEAL89"
            readonly="1" />
<rate id="1592191"
            is_child_rate="1"
            max_persons="4"
            policy="Non Refundable"
            policy_id="82024984"
            rate_name="Non-refundable" />
<rate id="1578444"
            max_persons="4"
            policy="General"
            policy_id="82024976"
            rate_name="Standard Rate" />
<rate id="1589701"
            is_child_rate="1"
            max_persons="4"
            policy="General"
            policy_id="82024976"
            rate_name="DEAL71"
            readonly="1" />
</rates>
</room>
<room id="46416703"
        hotel_id="464167"
        hotel_name="Steve\'s Sunny hotel"
        max_children="0"
        room_name="Standard Room">
<rates>
<rate id="1590909"
            is_child_rate="1"
            max_persons="2"
            policy="General"
            policy_id="82024976"
            rate_name="DEAL89"
            readonly="1" />
<rate id="1592191"
            is_child_rate="1"
            max_persons="2"
            policy="Non Refundable"
            policy_id="82024984"
            rate_name="Non-refundable" />
<rate id="1578444"
            max_persons="2"
            policy="General"
            policy_id="82024976"
            rate_name="Standard Rate" />
<rate id="1589701"
            is_child_rate="1"
            max_persons="2"
            policy="General"
            policy_id="82024976"
            rate_name="DEAL71"
            readonly="1" />
</rates>
</room>
</rooms>
<!-- RUID: [UmFuZG9tSVYkc2RlIyh9YXIOi0hY73GH9H+h2JUKAGDKUh+/68J9jOrgkmN45UXIaNe9s8vuwB6CnfOyv+mQ1A==] -->';

    protected $em;
    protected $cookieObrada;
    protected $bookingComXmlUrl;
    protected $bookingComXmlUsername;
    protected $bookingComXmlPassword;

    /**
     * @param EntityManager $em
     * @param cookieObrada $cookieObrada
     * @param $bookingComXmlUrl
     * @param $bookingComXmlUsername
     * @param $bookingComXmlPassword
     */
    public function __construct(EntityManager $em, cookieObrada $cookieObrada, $bookingComXmlUrl, $bookingComXmlUsername, $bookingComXmlPassword)
    {
        $this->em = $em;
        $this->cookieObrada = $cookieObrada;
        $this->bookingComXmlUrl = $bookingComXmlUrl;
        $this->bookingComXmlUsername = $bookingComXmlUsername;
        $this->bookingComXmlPassword = $bookingComXmlPassword;
    }

    /**
     * Nedovršena fja
     *
     * @param SetupProperty $setupProperty
     */
    public function azurirajStatus(SetupProperty $setupProperty)
    {
        if (0 != $setupProperty->getProperty()->getDeleted()) {
            $setupProperty->setAktivan(false);
        }
    }

    /**
     * Resovle idHead by given PropertyUnit - BookerTech method
     *
     * @param PropertyUnit $propertyUnit
     * @param $idCompanyKorisnik
     * @return int
     */
    public function getIdHeadBookerTech(PropertyUnit $propertyUnit, $idCompanyKorisnik)
    {
        $idHead = -1; //Inicijalizacija početne vrijednosti cjenika
        $em = $this->em;

        try {
            $idHead = (int)$em->createQueryBuilder()->select('pupr.id')
                ->from('tdCMBundle:PropertyUnitPricesRange', 'pupr')
                ->where('pupr.idProperty = :idProperty')
                ->andWhere('pupr.idUnit = :idUnit')
//                ->andWhere('pupr.vlasnik = :vlasnik')
                ->andWhere('pupr.deleted = :deleted')
                ->andWhere('pupr.bookertech = :bookertech')
                ->setParameters(array(
                        'idProperty' => $propertyUnit->getIdProperty(),
                        'idUnit' => $propertyUnit->getId(),
//                        'vlasnik' => $idCompanyKorisnik,
                        'deleted' => 0,
                        'bookertech' => 1,
                    )
                )
                ->setMaxResults(1)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException $e) {
            $idHead = -1;
        }
        return $idHead;
    }

    /**
     * Resolve idHead by given SetupPropertyUnit - OLD method
     *
     * @param SetupPropertyUnit $setupPropertyUnit
     * @return int
     */
    public function odrediCjenik(SetupPropertyUnit $setupPropertyUnit)
    {
        /*
         *  --- Opis procesa ---
         * 0. Odredi cjenik po prvoj metodi
         * 1. Dohvati webshop za taj setupPropertyUnit - dobijen ga iz $setupPropertyUnit
         * 2. Čitaj webshopPropertyUnit->getCjenikProdajni() i ako je 0 onda gledaj Property.getIdAgent() tamo di je Property->getId == WebshopPropertyUnit->getIdProperty()
         *      ili Property->getIdAgentC ovisno o postavkama
         * 3. Kad se pročita skrbnika iz Property-a onda čitaj WebshopPartner->getIdWebshopPartner() za webshop iz WebshopPropertyUnita i idCompanyPartner = skrbnik
         * 4. U UnitRateStavke spremi idCjenik na koji govori WebshopPartner->getPartnerJe() D ili P
         */

        //  ---  Inicijalizacija  ---
        $idHead = 0;
        $em = $this->em;

        // 0. Određivanje po prvoj metodi
        $idHead = $this->getIdHeadBookerTech($setupPropertyUnit->getPropertyUnit(), $setupPropertyUnit->getPortalKorisnik()->getIdCompanyKorisnik());

        // 1. Određivanje cjenika po drugoj metodi
        if (-1 == $idHead) {
            /* @var $unitRatePostojeci UnitRate */
//            $unitRatePostojeci = $em->getRepository('tdCMBundle:UnitRateStavke')->findOneBySetupPropertyUnit($setupPropertyUnit); //Napravit da se išće zadnji

            // Ako je ne nađe po metodi 2 onda neka vrati -2
            $idHead = -2;

            /* @var $webshopPropertyUnit WebshopPropertyUnit */
            $webshopPropertyUnit = $em->getRepository('tdCMBundle:WebshopPropertyUnit')->findOneBy(
                array('webshop' => $setupPropertyUnit->getPortalKorisnik()->getWebshop(), 'propertyUnit' => $setupPropertyUnit->getPropertyUnit()));

            //2. Čitaj Cjenik prodajni
            if ($webshopPropertyUnit) {
                if (0 != $webshopPropertyUnit->getCjenikProdajni()) {
                    //Završi, imamo cjenik
                    $idHead = $webshopPropertyUnit->getCjenikProdajni();
                } else {
                    //Inače dohvati preko WebshopPartner-a
                    //Provjera koji klijent se koristi (sljedeći if gleda dali će koristi novi ili stari tip klijenta/agenta)
                    if (1) {
                        /* @var $webshopPartner WebshopPartner */
                        $webshopPartner = $em->getRepository('tdCMBundle:WebshopPartner')->findOneBy(
                            array(
                                'webshop' => $setupPropertyUnit->getPortalKorisnik()->getWebshop(),
                                'idCompanyPartner' => $setupPropertyUnit->getPropertyUnit()->getProperty()->getIdAgent(),
                                'partnerJe' => 'D'
                            )
                        );

                    } else {
                        $webshopPartner = $em->getRepository('tdCMBundle:WebshopPartner')->findOneBy(
                            array('webshop' => $setupPropertyUnit->getWebshop(), 'idCompanyPartner' => $setupPropertyUnit->getPropertyUnit()->getProperty()->getIdAgentC()));
                    }

                    //3.
                    //Moran provjerit da mi webshopPartner ni nula. Jer mi javlja grešku da mi pozivan funkciju nad nepostojećim objektom.
                    if ($webshopPartner) {
                        //Ako je pronaša webshopPartner onda traži cjenik
                        /* @var $webshopPropertyUnitPartner WebshopPropertyUnit */
                        $webshopPropertyUnitPartner = $em->getRepository('tdCMBundle:WebshopPropertyUnit')->findOneByWebshop(//$setupPropertyUnit->getWebshop());
                            array(
                                'idShop' => $webshopPartner->getIdWebshopPartner(),
                                'idUnit' => $setupPropertyUnit->getIdPropertyUnit()
                            ));

                        if ($webshopPropertyUnitPartner) {
                            if ('B2B' == $webshopPartner->getReferentniCjenik() && $webshopPropertyUnitPartner) {
                                $idHead = $webshopPropertyUnitPartner->getCjenikKupovni();
                            } else {
                                $idHead = $webshopPropertyUnitPartner->getCjenikProdajni();
                            }
                        } else {
                            $idHead = -5;
                        }
                    } else {
                        $idHead = -4;
                    }
                }
            } else {
                $idHead = -3;
            }
        }

        return $idHead;
    }

    /**
     * Dohvat podataka za objekt s Booking.com-a
     *
     * @param $idPropertyOnPortal
     * @return array
     */
    public function getBookingComRoomRates($idPropertyOnPortal)
    {
        $hotelId = $idPropertyOnPortal;
        $podaci = array();
        $brojac = 0;

//        $url = 'https://supply-xml.booking.com/hotels/xml/roomrates';
//        $bookingcomXMLusername = "AdriaticTravel-atd";
//        $bookingcomXMLpassword = "*Odfc15gYwm_Ql?QCh(SCXMCqoQT!)SBMV-zHd4z";

        // Ovisno o postavkama korisite se lokalni testni podaci ili podaci sa Booking.com
        if ($this->cookieObrada->getCookieProcess()) {
            $xmlRequestPodaci = '<?xml version="1.0" encoding="utf-8"?><request><username>' . $this->bookingComXmlUsername . '</username><password>' . $this->bookingComXmlPassword . '</password><version>1.0</version><hotel_id>' . $hotelId . '</hotel_id></request>';
            $xmlTest = simplexml_load_string($xmlRequestPodaci);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->bookingComXmlUrl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlTest->asXML());
            $xml = simplexml_load_string(curl_exec($ch));
//            $greska = curl_error($ch);
            curl_close($ch);
        } else {
            $xml = simplexml_load_string(self::XML_TEST);
        }

        $polje = array();

        if (isset($xml->room)) {
            foreach ($xml->children() as $room) {
                $polje['id'] = (string)$room['id'];
                $polje['name'] = (string)$room['room_name'];

                //Punjenje podataka o rateovima
                $brojacRateova = 0;
                foreach ($room->children()->children() as $rate) {
                    //Dohvat cjenika za rate u tablici unitRate ako je posstavljen

                    $polje['rates'][$brojacRateova] = array(
                        'id' => (string)$rate['id'],
                        'is_child_rate' => (string)$rate['is_child_rate'],
                        'max_persons' => (string)$rate['max_persons'],
                        'policy' => (string)$rate['policy'],
                        'policy_id' => (string)$rate['policy_id'],
                        'rate_name' => (string)$rate['rate_name'],
                        'readonly' => (string)$rate['readonly'],
                        'fixed_occupancy' => (string)$rate['fixed_occupancy'],
                    );
                    $brojacRateova++;
                }

                $rates = array();

                if ($room['id']) {
                    $podaci[$brojac] = $polje;
                }
                $brojac++;
            }
        }

        return $podaci;
    }

    /**
     * @param Property $property
     * @param PortalKorisnik $portalKorisnik
     * @return SetupProperty|void
     */
    public function addPropertyToChannelManager(Property $property, PortalKorisnik $portalKorisnik)
    {
        // Chekck if spu exists
        $spu = $this->em->getRepository('tdCMBundle:SetupProperty')->findOneBy(array(
            'property' => $property,
            'portalKorisnik' => $portalKorisnik,
        ));

        if (is_null($spu)) {
            $spu = $this->createSetupProperty($property, $portalKorisnik);
        }

        return $spu;
    }

    /**
     *  Create SetupProperty object by given property and portalKorisnik
     *
     * @param Property $property
     * @param PortalKorisnik $portalKorisnik
     * @return SetupProperty
     */
    public function createSetupProperty(Property $property, PortalKorisnik $portalKorisnik)
    {
        // Check if already exist
        $setupProperty = $this->em->getRepository('tdCMBundle:SetupProperty')->findOneBy(array(
            'property' => $property,
            'portalKorisnik' => $portalKorisnik,
        ));
        if (is_null($setupProperty)) {
            $setupProperty = new SetupProperty();
            $setupProperty->setDatumKreiranja(new \DateTime());
            $setupProperty->setIdFront($property->getIdCountry() . '-' . str_pad($property->getId(), 5, "0", STR_PAD_LEFT));
            $setupProperty->setProperty($property);
            $setupProperty->setPortalKorisnik($portalKorisnik);
            $setupProperty->setAktivan(false);
            $setupProperty->setIdSkrbnik($property->getIdAgent());
            $setupProperty->setPovezan(false);
            $this->em->persist($setupProperty);
            $this->em->flush();
        }

        return $setupProperty;
    }

    /**
     *  Create SetupPropertyUnit object by given propertyUnit and portalKorisnik
     *
     * @param PropertyUnit $propertyUnit
     * @param PortalKorisnik $portalKorisnik
     * @return SetupPropertyUnit
     */
    public function createSetupPropertyUnit(PropertyUnit $propertyUnit, PortalKorisnik $portalKorisnik)
    {

        // Check if setupProperty exist
        $setupProperty = $this->em->getRepository('tdCMBundle:SetupProperty')->findOneBy(array(
            'property' => $propertyUnit->getProperty(),
            'portalKorisnik' => $portalKorisnik,
        ));

        if (is_null($setupProperty)) {
            $setupProperty = $this->createSetupProperty($propertyUnit->getProperty(), $portalKorisnik);
        }

        // Check if setupPropertyUnit already exist
        $setupPropertyUnit = $this->em->getRepository('tdCMBundle:SetupPropertyUnit')->findOneBy(array(
            'propertyUnit' => $propertyUnit,
            'portalKorisnik' => $portalKorisnik,
        ));

        if (is_null($setupPropertyUnit)) {
            $setupPropertyUnit = new SetupPropertyUnit();
            $setupPropertyUnit->setDatumKreiranja(new \DateTime());
            $setupPropertyUnit->setIdPropertyUnitFront($propertyUnit->getProperty()->getIdCountry() . '-' . str_pad($propertyUnit->getProperty()->getId(), 5, "0", STR_PAD_LEFT) . '-' . $propertyUnit->getLocalOrder());
            $setupPropertyUnit->setPropertyUnit($propertyUnit);
            $setupPropertyUnit->setSetupProperty($setupProperty);
            $setupPropertyUnit->setPortalKorisnik($portalKorisnik);
            $setupPropertyUnit->setAktivan(false);
            $setupPropertyUnit->setIdSkrbnik($propertyUnit->getProperty()->getIdAgent());
            $setupPropertyUnit->setPovezan(false);
            $this->em->persist($setupPropertyUnit);
            $this->em->flush();
        }

        return $setupPropertyUnit;
    }

    /**
     * @param SetupPropertyUnit $setupPropertyUnit
     * @param $podaciBookingCom
     * @return mixed
     */
    public function updateLocalDataBookingCom(SetupPropertyUnit $setupPropertyUnit, $podaciBookingCom)
    {
        $unitRates = $this->em->getRepository('tdCMBundle:UnitRate')->findBy(array(
            'setupPropertyUnit' => $setupPropertyUnit,
        ));

        //Ovdje pripremam id-ove koje imam u bazi
        $ratesIds = array();

        /* @var UnitRate $unitRate */
        foreach ($unitRates as $unitRate) {
            $ratesIds[] = $unitRate->getIdRateBookingCom();
        }

        // Data that should be returned
        $rates = array();

        //Prolaz po svim rateovima
        foreach ($podaciBookingCom as $polje) {
            if ($polje['id'] == $setupPropertyUnit->getIdPropertyUnitNaPortalu()) {
                //NASTAVITI
                // pogledati unitRatesAction iz RateControllera
                $rates = $polje['rates'];
                foreach ($rates as $key => $rate) {
                    //Inicijalizacija dodatnih polja
                    $rates[$key]['idHead'] = 0;
                    $rates[$key]['idUnitRate'] = 0;
                    if ('1' != $rate['readonly'] && '1' != $rate['is_child_rate']) {    //Gledaju se samo rateovi koji se mogu postaviti.
                        if (!in_array((int)$rate['id'], $ratesIds)) {
                            //Dohvati ili kreiraj novi zapis Rate
                            if (null == $rateDB = $this->em->getRepository('tdCMBundle:Rate')->findOneByRateIdPortal($rate['id'])) {
                                $rateDB = new Rate();
                                $rateDB->setDatumKreiranja(new \DateTime());
                                $rateDB->setMaxPersons($rate['max_persons']);
                                $rateDB->setPolicy($rate['policy']);
                                $rateDB->setPolicyId($rate['policy_id']);
                                $rateDB->setRateIdPortal($rate['id']);
                                $rateDB->setRateName($rate['rate_name']);
                                $rateDB->setReadonly(false);
                                $rateDB->setIsChildRate(false);
                                $rateDB->setFixedOccupancy(0);
                                $this->em->persist($rateDB);
                                $this->em->flush();
                            }

                            //Kreiraj UnitRateStavke
                            $unitRate = new UnitRate();
                            $unitRate->setIdRateBookingCom($rate['id']);
                            $unitRate->setSetupPropertyUnit($setupPropertyUnit);
                            $unitRate->setRate($rateDB);
                            $unitRate->setSamoCjenik(true);
                            $unitRate->setVrijediOd(new \DateTime("2015-01-01"));
                            $unitRate->setVrijediDo(new \DateTime("2015-12-31"));
                            $unitRate->setPostojeciSet(false);
                            $unitRate->setAktivan(false);
                            $this->em->persist($unitRate);
                            $this->em->flush();
                            $rates[$key]['idUnitRate'] = $unitRate->getId();
                        } else {
                            /* @var UnitRate $unitRateHead */
                            foreach ($unitRates as $unitRateHead) {
                                if ($unitRateHead->getIdRateBookingCom() == (int)$rate['id']) {
                                    $rates[$key]['idHead'] = $unitRateHead->getIdCjenik();
                                    $rates[$key]['idUnitRate'] = $unitRateHead->getId();
                                }
                            }
                        }
                    }
                }
            }
        }

        return $rates;
    }

    /**
     * Calculate status Povezan on SetupPropertyUnit for Booking.com
     *
     * @param SetupPropertyUnit $setupPropertyUnit
     * @return SetupPropertyUnit
     */
    public function calculatePovezanSetupPropertyUnit(SetupPropertyUnit $setupPropertyUnit)
    {
        // Calculate povezan on unit
        $povezan = false;

        $portal = $setupPropertyUnit->getPortalKorisnik()->getPortal();

        // Check connectivity object
        $connectivityObject = false;
        if ($portal->getConnectProperty()) {
            // Setup property must exist
            // Check if setup property is connected
            if ($this->checkIdOnPortalValidtiy($setupPropertyUnit->getSetupProperty()->getIdPropertyNaPortalu())) {// ID object on portal must be valid
                // Check if object must be active
                if ($portal->getManageActivityProperty()) {
                    if ($setupPropertyUnit->getSetupProperty()->getAktivan()) {
                        $connectivityObject = true;
                    }
                } else {
                    $connectivityObject = true;
                }
            }
        } else {
            $connectivityObject = true;
        }

        // Check connectivity unit
        $connectivityUnit = false;
        if ($portal->getConnectPropertyUnit()) {
            // Setup property unit must exist
            // Check if setup property unit is connected
            if ($this->checkIdOnPortalValidtiy($setupPropertyUnit->getIdPropertyUnitNaPortalu())) { // ID unit on portal must be valid
                if ($portal->getManageActivityPropertyUnit()) { // Check if unit must be active
                    if ($setupPropertyUnit->getAktivan()) {
                        $connectivityUnit = true;
                    }
                } else {
                    $connectivityUnit = true;
                }
            }
        } else {
            $connectivityUnit = true;
        }


//        if ($setupPropertyUnit->getSetupProperty()->getAktivan() and // Object must be active
//            $setupPropertyUnit->getAktivan() and // Unit must be active
//            $this->checkIdOnPortalValidtiy($setupPropertyUnit->getIdPropertyUnitNaPortalu()) and // ID unit on portal must be valid
//            $this->checkIdOnPortalValidtiy($setupPropertyUnit->getSetupProperty()->getIdPropertyNaPortalu())// ID object on portal must be valid
        if ($connectivityObject && $connectivityUnit) {

            if($portal->getCodeName() == 'BookingCom'){
                /* @var UnitRate $ur */
                foreach ($setupPropertyUnit->getUnitRates() as $ur) {
                    if ($ur->getIdCjenik() > 0) {
                        $povezan = true;
                    }
                }
            } else {
                $povezan = true;
            }

        }

        $setupPropertyUnit->setPovezan($povezan);
        return $setupPropertyUnit;
    }

    /**
     *  Check if ID of Object or Unit is valid
     *
     * @param $id
     * @return bool
     */
    public function checkIdOnPortalValidtiy($id)
    {
        $test = true;
        if ($id == 0 || $id == '' || is_null($id)) {
            $test = false;
        }
        return $test;
    }

    /**
     *  Check if setupProperty has webshopProperty
     *
     * @param SetupProperty $setupProperty
     * @return bool
     */
    public function checkIfSetupPropertyHasWebshopProperty(SetupProperty $setupProperty)
    {
        $webshopProperty = $this->em->getRepository('tdCMBundle:WebshopProperty')->findOneBy(array(
            'webshop' => $setupProperty->getPortalKorisnik()->getWebshop(),
            'property' => $setupProperty->getProperty(),
            'disabled' => 0,
        ));

        return !is_null($webshopProperty);
    }

    /**
     *  Check if setupPropertyUnit has webshopPropertyUnit
     *
     * @param SetupPropertyUnit $setupPropertyUnit
     * @return bool
     */
    public function checkIfSetupPropertyUnitHasWebshopPropertyUnit(SetupPropertyUnit $setupPropertyUnit)
    {
        $webshopPropertyUnit = $this->em->getRepository('tdCMBundle:WebshopPropertyUnit')->findOneBy(array(
            'webshop' => $setupPropertyUnit->getPortalKorisnik()->getWebshop(),
            'propertyUnit' => $setupPropertyUnit->getPropertyUnit(),
            'disabled' => 0,
        ));

        return !is_null($webshopPropertyUnit);
    }


    /**
     *  Get available booking.com id_units for html select.
     *   It has to be all of them that are not yet used.
     *
     * @param SetupPropertyUnit $setupPropertyUnit
     * @param $podaciBookingCom
     * @return array
     */
    public function getAvailableItemsBookingCom(SetupPropertyUnit $setupPropertyUnit)
    {
        $podaciBookingCom = $this->getBookingComRoomRates($setupPropertyUnit->getSetupProperty()->getIdPropertyNaPortalu());
        $setupPropertyUnitsBookingComAssigned = $this->em->getRepository('tdCMBundle:SetupPropertyUnit')->createQueryBuilder('sp')
            ->select('sp.idPropertyUnitNaPortalu')
            ->where('sp.idPropertyUnitNaPortalu IS NOT NULL')
            ->andWhere('sp.setupProperty = :setupProperty')
            ->setParameter('setupProperty', $setupPropertyUnit->getSetupProperty())
            ->getQuery()
            ->getResult();

        $podaciObrada = array();

        foreach ($podaciBookingCom as $item) {
            $flag = true;
            foreach ($setupPropertyUnitsBookingComAssigned as $idPropertyUnitOnPortal) {
//                return new JsonResponse(array('itemId' => $item['id'], 'idProprertyUnitOnPortal' => $idPropertyUnitOnPortal['idPropertyUnitNaPortalu']));
//                if ($idPropertyUnitOnPortal['idPropertyUnitNaPortalu'] == $item['id']) {
//                    return new JsonResponse($setupPropertyUnitsBookingComAssigned);
//                }
                if ($item['id'] == $idPropertyUnitOnPortal['idPropertyUnitNaPortalu'] &&
                    $setupPropertyUnit->getIdPropertyUnitNaPortalu() != $idPropertyUnitOnPortal['idPropertyUnitNaPortalu']
                ) {
                    $flag = false;
                }
            }
            if ($flag) $podaciObrada[] = $item;
        }
        return $podaciObrada;
    }

    /**
     *  Find SetupProperty for Booking.com
     *
     * @param Property $property
     * @param $code
     * @return null|SetupProperty
     */
    public function getSetupPropertyBookingCom(Property $property, $code)
    {
        // Find portal
        $portal = $this->em->getRepository('tdCMBundle:Portal')->findOneByCodeName($code);

        if (!is_null($portal)) {
            // Find portal korisnik
            $portalKorisnik = $this->em->getRepository('tdCMBundle:PortalKorisnik')->findOneBy(array(
                'idCompanyKorisnik' => $property->getIdAgent(),
                'portal' => $portal,
            ));

            if (!is_null($portalKorisnik)) {
                $setupProperty = $this->em->getRepository('tdCMBundle:SetupProperty')->findOneBy(array(
                    'portalKorisnik' => $portalKorisnik,
                    'property' => $property,
                ));
            } else {
                $setupProperty = null;
            }
        } else {
            $setupProperty = null;
        }
        return $setupProperty;
    }

    /**
     *  Find SetupPropertyUnit for Booking.com
     *
     * @param PropertyUnit $propertyUnit
     * @param $code
     * @param null $idCompanyKorisnik
     * @return null|SetupPropertyUnit
     */
    public function getSetupPropertyUnitBookingCom(PropertyUnit $propertyUnit, $code, $idCompanyKorisnik = null)
    {
        // Find portal
        $portal = $this->em->getRepository('tdCMBundle:Portal')->findOneByCodeName($code);

        if (!is_null($portal)) {
            // Find portal korisnik
            $portalKorisnik = $this->em->getRepository('tdCMBundle:PortalKorisnik')->findOneBy(array(
                'idCompanyKorisnik' => $idCompanyKorisnik ? $idCompanyKorisnik : $propertyUnit->getProperty()->getIdAgent(),
                'portal' => $portal,
            ));

            if (!is_null($portalKorisnik)) {
                $setupPropertyUnit = $this->em->getRepository('tdCMBundle:SetupPropertyUnit')->findOneBy(array(
                    'portalKorisnik' => $portalKorisnik,
                    'propertyUnit' => $propertyUnit,
                ));
            } else {
                $setupPropertyUnit = null;
            }
        } else {
            $setupPropertyUnit = null;
        }
        return $setupPropertyUnit;
    }

    /**
     *  Return WebshopPropertyUnit by given SetupPropertyUnit
     *
     * @param SetupPropertyUnit $setupPropertyUnit
     * @return null|\td\CMBundle\Entity\WebshopPropertyUnit
     */
    public function getWebshopPropertyUnitForGivenSetupPropertyUnit(SetupPropertyUnit $setupPropertyUnit)
    {
        if (!is_null($setupPropertyUnit)) {
            return $this->em->getRepository('tdCMBundle:WebshopPropertyUnit')->findOneBy(array(
                'propertyUnit' => $setupPropertyUnit->getPropertyUnit(),
                'webshop' => $setupPropertyUnit->getPortalKorisnik()->getWebshop(),
            ));
        } else {
            return null;
        }
    }

    /**
     * @param SetupPropertyUnit $setupPropertyUnit
     * @return int
     */
    public function findFirstIdHeadForSetupPropertyUnit(SetupPropertyUnit $setupPropertyUnit)
    {
        $unitRates = $setupPropertyUnit->getUnitRates();
        /* @var UnitRate $unitRate */
        foreach ($unitRates as $unitRate) {
            if($unitRate->getIdCjenik() > 0) return $unitRate->getIdCjenik();
        }

        return 0;

    }
}