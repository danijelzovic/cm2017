<?php
/**
 * Created by PhpStorm.
 * User: danijel
 * Date: 2.5.2016.
 * Time: 6:44
 */

namespace td\CMBundle\Obrada;
use Doctrine\ORM\EntityManager;


/**
 * Class cookieObrada
 * @package td\CMBundle\Obrada
 */
class cookieObrada
{
    private $idCompanyKorisnik = 177777;
    private $pravaOgraniceni = 0;
    private $idCurrentUserClient = 177777;
    private $korisnik;
    private $lanTranslate = 'hr';
    private $companyStatus;
    private $em;
    private $menu;
    private $cookieProcess;
    private $cookieSalt;
    private $cookieName;
    private $cookieApp;
    private $username = 'test';

    /**
     * @param $cookieProcess
     * @param $cookieSalt
     * @param $cookieName
     */
    public function __construct(EntityManager $em, $cookieProcess, $cookieSalt, $cookieName, $cookieApp)
    {
        $this->em = $em;
        $this->cookieProcess = $cookieProcess;
        $this->cookieSalt = $cookieSalt;
        $this->cookieName = $cookieName;
        $this->cookieApp = $cookieApp;
    }

    /**
     * @return mixed
     */
    public function getCookieProcess()
    {
        return $this->cookieProcess;
    }

    /**
     * @return string
     */
    public function getLanTranslate()
    {
        return $this->lanTranslate;
    }

    /**
     * @return mixed
     */
    public function getIdCompanyKorisnik()
    {
        return $this->idCompanyKorisnik;
    }

    /**
     * @return mixed
     */
    public function getPravaOgraniceni()
    {
        return $this->pravaOgraniceni;
    }

    /**
     * @return mixed
     */
    public function getIdCurrentUserClient()
    {
        return $this->idCurrentUserClient;
    }

    /**
     * @return mixed
     */
    public function getKorisnik()
    {
        return $this->korisnik;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return bool
     */
    public function obradaCookie()
    {

        $grant = false;

        // Ovo je postupak za Bookertech
        if ($this->cookieProcess) {
            if (array_key_exists($this->cookieName, $_COOKIE)) {
                $array = explode(":", $_COOKIE[$this->cookieName]);
                $salt = $this->cookieSalt;
                if (crypt($array[0], $salt) == $array[1]) //[0] - username; [1] - zakodirani username
                {
                    if ($this->cookieApp == 'backend') { // Obrada cookie ako je backend
                        $this->username = $array[0];
                        $this->idCompanyKorisnik = $array[4];
                        $this->companyStatus = $array[6];
                        $this->menu = $this->em->getRepository('tdCMBundle:CompanyStatus')->find($this->companyStatus)->getMenuPath();
                        $grant = true;
                    } elseif($this->cookieApp = 'eurobooker') { // Obrada cookie za eurobooker
                        $this->idCompanyKorisnik = $array[2];
                        $this->pravaOgraniceni = $array[3];
                        $this->idCurrentUserClient = $array[4];
                        $this->korisnik = $array[0];
                        $grant = true;
                    }
                }
            }
        } else {
            $grant = true;
        }

        return $grant;
    }
}