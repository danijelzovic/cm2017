<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TransferLog
 *
 * @ORM\Table(name="cm_transfer_log")
 * @ORM\Entity
 */
class TransferLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_transfer", type="integer")
     */
    private $idTransfer;

    /**
     * @ORM\ManyToOne(targetEntity="Transfer", inversedBy="transferLogovi")
     * @ORM\JoinColumn(name="id_transfer", referencedColumnName="id")
     */
    private $transfer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datum_vrijeme", type="datetime")
     */
    private $datumVrijeme;

    /**
     * @var boolean
     *
     * @ORM\Column(name="uspjesan_prijenos", type="boolean")
     */
    private $uspjesanPrijenos = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="prijenos_naknadnom_promjenom", type="boolean")
     */
    private $prijenosNaknadnomPromjenom = false;

    /**
     * @var text
     *
     * @ORM\Column(name="response_transfera", type="text")
     */
    private $responseTransfera;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idTransfer
     *
     * @param integer $idTransfer
     * @return TransferLog
     */
    public function setIdTransfer($idTransfer)
    {
        $this->idTransfer = $idTransfer;

        return $this;
    }

    /**
     * Get idTransfer
     *
     * @return integer 
     */
    public function getIdTransfer()
    {
        return $this->idTransfer;
    }

    /**
     * Set datumVrijeme
     *
     * @param \DateTime $datumVrijeme
     * @return TransferLog
     */
    public function setDatumVrijeme($datumVrijeme)
    {
        $this->datumVrijeme = $datumVrijeme;

        return $this;
    }

    /**
     * Get datumVrijeme
     *
     * @return \DateTime 
     */
    public function getDatumVrijeme()
    {
        return $this->datumVrijeme;
    }

    /**
     * Set uspjesanPrijenos
     *
     * @param boolean $uspjesanPrijenos
     * @return TransferLog
     */
    public function setUspjesanPrijenos($uspjesanPrijenos)
    {
        $this->uspjesanPrijenos = $uspjesanPrijenos;

        return $this;
    }

    /**
     * Get uspjesanPrijenos
     *
     * @return boolean 
     */
    public function getUspjesanPrijenos()
    {
        return $this->uspjesanPrijenos;
    }

    /**
     * Set prijenosNaknadnomPromjenom
     *
     * @param boolean $prijenosNaknadnomPromjenom
     * @return TransferLog
     */
    public function setPrijenosNaknadnomPromjenom($prijenosNaknadnomPromjenom)
    {
        $this->prijenosNaknadnomPromjenom = $prijenosNaknadnomPromjenom;

        return $this;
    }

    /**
     * Get prijenosNaknadnomPromjenom
     *
     * @return boolean 
     */
    public function getPrijenosNaknadnomPromjenom()
    {
        return $this->prijenosNaknadnomPromjenom;
    }

    /**
     * Set transfer
     *
     * @param \td\CMBundle\Entity\Transfer $transfer
     * @return TransferLog
     */
    public function setTransfer(\td\CMBundle\Entity\Transfer $transfer = null)
    {
        $this->transfer = $transfer;

        return $this;
    }

    /**
     * Get transfer
     *
     * @return \td\CMBundle\Entity\Transfer 
     */
    public function getTransfer()
    {
        return $this->transfer;
    }

    /**
     * Set responseTransfera
     *
     * @param string $responseTransfera
     * @return TransferLog
     */
    public function setResponseTransfera($responseTransfera)
    {
        $this->responseTransfera = $responseTransfera;

        return $this;
    }

    /**
     * Get responseTransfera
     *
     * @return string 
     */
    public function getResponseTransfera()
    {
        return $this->responseTransfera;
    }
}
