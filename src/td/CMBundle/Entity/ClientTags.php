<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClientTags
 *
 * @ORM\Table(name="client_tags")
 * @ORM\Entity
 */
class ClientTags
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_client", type="integer")
     */
    private $idClient;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_company", type="integer")
     */
    private $idCompany;

    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="string", length=255)
     */
    private $tag;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_user_client", type="integer")
     */
    private $idUserClient;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="vrijeme", type="datetime")
     */
    private $vrijeme;

    /**
     * @var integer
     *
     * @ORM\Column(name="most_import", type="integer")
     */
    private $mostImport;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idClient
     *
     * @param integer $idClient
     * @return ClientTags
     */
    public function setIdClient($idClient)
    {
        $this->idClient = $idClient;

        return $this;
    }

    /**
     * Get idClient
     *
     * @return integer 
     */
    public function getIdClient()
    {
        return $this->idClient;
    }

    /**
     * Set idCompany
     *
     * @param integer $idCompany
     * @return ClientTags
     */
    public function setIdCompany($idCompany)
    {
        $this->idCompany = $idCompany;

        return $this;
    }

    /**
     * Get idCompany
     *
     * @return integer 
     */
    public function getIdCompany()
    {
        return $this->idCompany;
    }

    /**
     * Set tag
     *
     * @param string $tag
     * @return ClientTags
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return string 
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set idUserClient
     *
     * @param integer $idUserClient
     * @return ClientTags
     */
    public function setIdUserClient($idUserClient)
    {
        $this->idUserClient = $idUserClient;

        return $this;
    }

    /**
     * Get idUserClient
     *
     * @return integer 
     */
    public function getIdUserClient()
    {
        return $this->idUserClient;
    }

    /**
     * Set vrijeme
     *
     * @param \DateTime $vrijeme
     * @return ClientTags
     */
    public function setVrijeme($vrijeme)
    {
        $this->vrijeme = $vrijeme;

        return $this;
    }

    /**
     * Get vrijeme
     *
     * @return \DateTime 
     */
    public function getVrijeme()
    {
        return $this->vrijeme;
    }

    /**
     * Set mostImport
     *
     * @param integer $mostImport
     * @return ClientTags
     */
    public function setMostImport($mostImport)
    {
        $this->mostImport = $mostImport;

        return $this;
    }

    /**
     * Get mostImport
     *
     * @return integer 
     */
    public function getMostImport()
    {
        return $this->mostImport;
    }
}
