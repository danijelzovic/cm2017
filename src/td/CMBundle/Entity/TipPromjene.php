<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipPromjene
 *
 * @ORM\Table(name="cm_tip_promjene")
 * @ORM\Entity(repositoryClass="td\CMBundle\Entity\TipPromjeneRepository")
 */
class TipPromjene
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tip_promjene", type="string", length=255, unique=true)
     */
    private $tipPromjene;

	/**
     * @var boolean
     *
     * @ORM\Column(name="od", type="boolean")
     */
    private $od;

	/**
     * @var boolean
     *
     * @ORM\Column(name="do", type="boolean")
     */
    private $do;

   /**
     * @var boolean
     *
     * @ORM\Column(name="kolicina", type="boolean")
     */
    private $kolicina;

    /**
     * @var boolean
     *
     * @ORM\Column(name="foto_tip", type="boolean")
     */
    private $fotoTip;

    /**
     * @var boolean
     *
     * @ORM\Column(name="foto_operation", type="boolean")
     */
    private $fotoOperation;

    /**
     * @var boolean
     *
     * @ORM\Column(name="foto_url", type="boolean")
     */
    private $fotoUrl;

	

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipPromjene
     *
     * @param string $tipPromjene
     * @return TipPromjene
     */
    public function setTipPromjene($tipPromjene)
    {
        $this->tipPromjene = $tipPromjene;

        return $this;
    }

    /**
     * Get tipPromjene
     *
     * @return string 
     */
    public function getTipPromjene()
    {
        return $this->tipPromjene;
    }

    /**
     * Set od
     *
     * @param boolean $od
     * @return TipPromjene
     */
    public function setOd($od)
    {
        $this->od = $od;

        return $this;
    }

    /**
     * Get od
     *
     * @return boolean 
     */
    public function getOd()
    {
        return $this->od;
    }

    /**
     * Set do
     *
     * @param boolean $do
     * @return TipPromjene
     */
    public function setDo($do)
    {
        $this->do = $do;

        return $this;
    }

    /**
     * Get do
     *
     * @return boolean 
     */
    public function getDo()
    {
        return $this->do;
    }

    /**
     * Set kolicina
     *
     * @param boolean $kolicina
     * @return TipPromjene
     */
    public function setKolicina($kolicina)
    {
        $this->kolicina = $kolicina;

        return $this;
    }

    /**
     * Get kolicina
     *
     * @return boolean 
     */
    public function getKolicina()
    {
        return $this->kolicina;
    }

    /**
     * Set fotoTip
     *
     * @param boolean $fotoTip
     * @return TipPromjene
     */
    public function setFotoTip($fotoTip)
    {
        $this->fotoTip = $fotoTip;

        return $this;
    }

    /**
     * Get fotoTip
     *
     * @return boolean 
     */
    public function getFotoTip()
    {
        return $this->fotoTip;
    }

    /**
     * Set fotoOperation
     *
     * @param boolean $fotoOperation
     * @return TipPromjene
     */
    public function setFotoOperation($fotoOperation)
    {
        $this->fotoOperation = $fotoOperation;

        return $this;
    }

    /**
     * Get fotoOperation
     *
     * @return boolean 
     */
    public function getFotoOperation()
    {
        return $this->fotoOperation;
    }

    /**
     * Set fotoUrl
     *
     * @param boolean $fotoUrl
     * @return TipPromjene
     */
    public function setFotoUrl($fotoUrl)
    {
        $this->fotoUrl = $fotoUrl;

        return $this;
    }

    /**
     * Get fotoUrl
     *
     * @return boolean 
     */
    public function getFotoUrl()
    {
        return $this->fotoUrl;
    }
}
