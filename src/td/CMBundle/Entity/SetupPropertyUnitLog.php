<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SetupPropertyUnitLog
 *
 * @ORM\Table(name="cm_setup_property_unit_LOG")
 * @ORM\Entity
 */
class SetupPropertyUnitLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="idSetupPropertyUnit", type="integer")
     */
    private $idSetupPropertyUnit;

    /**
     * @ORM\ManyToOne(targetEntity="SetupPropertyUnit")
     * @ORM\JoinColumn(name="idSetupPropertyUnit", referencedColumnName="id", onDelete="RESTRICT")
     */
    private $setupPropertyUnit;

    /**
     * @var integer
     *
     * @ORM\Column(name="idWebshop", type="integer", nullable=true)
     */
    private $idWebshop;

    /**
     * @ORM\ManyToOne(targetEntity="Webshop")
     * @ORM\JoinColumn(name="idWebshop", referencedColumnName="id", onDelete="RESTRICT")
    */
    private $webshop;
	
    /**
     * @var integer
     *
     * @ORM\Column(name="idPropertyUnit", type="integer")
     */
    private $idPropertyUnit;

    /**
     * @ORM\ManyToOne(targetEntity="PropertyUnit")
     * @ORM\JoinColumn(name="idPropertyUnit", referencedColumnName="id")
     **/
    private $propertyUnit;
	
    /**
     * @var string
     *
     * @ORM\Column(name="idPropertyUnitNaPortalu", type="string", length=255, nullable=true)
     */
    private $idPropertyUnitNaPortalu;

    /**
     * @var boolean
     *
     * @ORM\Column(name="aktivan", type="boolean")
     */
    private $aktivan =  true;

   /**
     * @ORM\ManyToOne(targetEntity="PortalKorisnik", inversedBy="setup")
     * @ORM\JoinColumn(name="ID_portal_korisnik", referencedColumnName="id", onDelete="CASCADE")
	 *
     */
    private $portalKorisnik;

    /**
     * @var integer
     *
     * @ORM\Column(name="idSkrbnik", type="integer")
     */
    private $idSkrbnik;

     /**
     * @ORM\ManyToOne(targetEntity="SetupProperty")
     * @ORM\JoinColumn(name="idSetupProperty", referencedColumnName="id")
     **/
    private $setupProperty;
    
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datumVrijemePromjene", type="datetime", options={"default":"0000-00-00 00:00:00"})
     */
    private $datumVrijemePromjene;
    
    /**
     * @var string
     *
     * @ORM\Column(name="akcija", type="string", length=255)
     */
    private $akcija;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idWebshop
     *
     * @param integer $idWebshop
     * @return SetupPropertyUnit
     */
    public function setIdWebshop($idWebshop)
    {
        $this->idWebshop = $idWebshop;

        return $this;
    }

    /**
     * Get idWebshop
     *
     * @return integer 
     */
    public function getIdWebshop()
    {
        return $this->idWebshop;
    }

    /**
     * Set idPropertyUnit
     *
     * @param integer $idPropertyUnit
     * @return SetupPropertyUnit
     */
    public function setIdPropertyUnit($idPropertyUnit)
    {
        $this->idPropertyUnit = $idPropertyUnit;

        return $this;
    }

    /**
     * Get idPropertyUnit
     *
     * @return integer 
     */
    public function getIdPropertyUnit()
    {
        return $this->idPropertyUnit;
    }

    /**
     * Set idPropertyUnitNaPortalu
     *
     * @param string $idPropertyUnitNaPortalu
     * @return SetupPropertyUnit
     */
    public function setIdPropertyUnitNaPortalu($idPropertyUnitNaPortalu)
    {
        $this->idPropertyUnitNaPortalu = $idPropertyUnitNaPortalu;

        return $this;
    }

    /**
     * Get idPropertyUnitNaPortalu
     *
     * @return string 
     */
    public function getIdPropertyUnitNaPortalu()
    {
        return $this->idPropertyUnitNaPortalu;
    }

    /**
     * Set aktivan
     *
     * @param boolean $aktivan
     * @return SetupPropertyUnit
     */
    public function setAktivan($aktivan)
    {
        $this->aktivan = $aktivan;

        return $this;
    }

    /**
     * Get aktivan
     *
     * @return boolean 
     */
    public function getAktivan()
    {
        return $this->aktivan;
    }

    /**
     * Set idSkrbnik
     *
     * @param integer $idSkrbnik
     * @return SetupPropertyUnit
     */
    public function setIdSkrbnik($idSkrbnik)
    {
        $this->idSkrbnik = $idSkrbnik;

        return $this;
    }

    /**
     * Get idSkrbnik
     *
     * @return integer 
     */
    public function getIdSkrbnik()
    {
        return $this->idSkrbnik;
    }

    /**
     * Set webshop
     *
     * @param \td\CMBundle\Entity\Webshop $webshop
     * @return SetupPropertyUnit
     */
    public function setWebshop(\td\CMBundle\Entity\Webshop $webshop = null)
    {
        $this->webshop = $webshop;

        return $this;
    }

    /**
     * Get webshop
     *
     * @return \td\CMBundle\Entity\Webshop 
     */
    public function getWebshop()
    {
        return $this->webshop;
    }

    /**
     * Set propertyUnit
     *
     * @param \td\CMBundle\Entity\PropertyUnit $propertyUnit
     * @return SetupPropertyUnit
     */
    public function setPropertyUnit(\td\CMBundle\Entity\PropertyUnit $propertyUnit = null)
    {
        $this->propertyUnit = $propertyUnit;

        return $this;
    }

    /**
     * Get propertyUnit
     *
     * @return \td\CMBundle\Entity\PropertyUnit 
     */
    public function getPropertyUnit()
    {
        return $this->propertyUnit;
    }

    /**
     * Set portalKorisnik
     *
     * @param \td\CMBundle\Entity\PortalKorisnik $portalKorisnik
     * @return SetupPropertyUnit
     */
    public function setPortalKorisnik(\td\CMBundle\Entity\PortalKorisnik $portalKorisnik = null)
    {
        $this->portalKorisnik = $portalKorisnik;

        return $this;
    }

    /**
     * Get portalKorisnik
     *
     * @return \td\CMBundle\Entity\PortalKorisnik 
     */
    public function getPortalKorisnik()
    {
        return $this->portalKorisnik;
    }

    /**
     * Set datumVrijemePromjene
     *
     * @param \DateTime $datumVrijemePromjene
     * @return SetupPropertyUnitLog
     */
    public function setDatumVrijemePromjene($datumVrijemePromjene)
    {
        $this->datumVrijemePromjene = $datumVrijemePromjene;

        return $this;
    }

    /**
     * Get datumVrijemePromjene
     *
     * @return \DateTime 
     */
    public function getDatumVrijemePromjene()
    {
        return $this->datumVrijemePromjene;
    }

    /**
     * Set idSetupPropertyUnit
     *
     * @param integer $idSetupPropertyUnit
     * @return SetupPropertyUnitLog
     */
    public function setIdSetupPropertyUnit($idSetupPropertyUnit)
    {
        $this->idSetupPropertyUnit = $idSetupPropertyUnit;

        return $this;
    }

    /**
     * Get idSetupPropertyUnit
     *
     * @return integer 
     */
    public function getIdSetupPropertyUnit()
    {
        return $this->idSetupPropertyUnit;
    }

    /**
     * Set setupProperties
     *
     * @param \td\CMBundle\Entity\SetupProperty $setupProperty
     * @return SetupPropertyUnitLog
     */
    public function setSetupProperty(\td\CMBundle\Entity\SetupProperty $setupProperty = null)
    {
        $this->setupProperty = $setupProperty;

        return $this;
    }

    /**
     * Get setupProperties
     *
     * @return \td\CMBundle\Entity\SetupProperty 
     */
    public function getSetupProperty()
    {
        return $this->setupProperty;
    }

    /**
     * Set akcija
     *
     * @param string $akcija
     * @return SetupPropertyUnitLog
     */
    public function setAkcija($akcija)
    {
        $this->akcija = $akcija;

        return $this;
    }

    /**
     * Get akcija
     *
     * @return string 
     */
    public function getAkcija()
    {
        return $this->akcija;
    }

    /**
     * Set setupPropertyUnit
     *
     * @param \td\CMBundle\Entity\SetupPropertyUnit $setupPropertyUnit
     *
     * @return SetupPropertyUnitLog
     */
    public function setSetupPropertyUnit(\td\CMBundle\Entity\SetupPropertyUnit $setupPropertyUnit = null)
    {
        $this->setupPropertyUnit = $setupPropertyUnit;

        return $this;
    }

    /**
     * Get setupPropertyUnit
     *
     * @return \td\CMBundle\Entity\SetupPropertyUnit
     */
    public function getSetupPropertyUnit()
    {
        return $this->setupPropertyUnit;
    }
}
