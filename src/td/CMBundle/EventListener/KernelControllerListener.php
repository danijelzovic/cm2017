<?php
/**
 * Created by PhpStorm.
 * User: danijel
 * Date: 22.7.2017.
 * Time: 16:08
 */

namespace td\CMBundle\EventListener;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use td\CMBundle\Obrada\cookieObrada;

class KernelControllerListener
{
    /**
     * @var cookieObrada
     */
    private $cookieObrada;

    private $router;

    /**
     * @param cookieObrada $cookieObrada
     */
    public function __construct(cookieObrada $cookieObrada, Router $router)
    {
        $this->cookieObrada = $cookieObrada;
        $this->router = $router;
    }

    /**
     * @param FilterControllerEvent $event
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        // Process cookie

        if (!$this->cookieObrada->obradaCookie()) {
            $route = 'default_login_again';
            $url = $this->router->generate($route);
            if ($event->getRequest()->get('_route') == $route) {
                return;
            }

            $event->setController(function () use ($url) {
                //            return new Response('test');

                return new RedirectResponse($url);
            });
        }else{
            return;
        }
    }

}