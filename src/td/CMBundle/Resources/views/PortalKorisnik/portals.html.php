<?php
/**
 * Created by PhpStorm.
 * User: danijel
 * Date: 11.4.2016.
 * Time: 5:29
 */
?>

<?php $view->extend('::base.html.php') ?>

<?php $view['slots']->set('title', 'CM') ?>



<?php $view['slots']->start('body') ?>


    <!-- BEGIN CONTENT HEADER -->
    <div class="content-header">

        <h3 class="page-title unit-title no-image has-icon">
            <span class="fa fa-icon fa-sitemap"></span>Channel Manager
        </h3>


        <!-- BEGIN CONTENT TABS -->
        <div class="tabbable tabbable-tabdrop">

            <div class="row">
                <div class="col-md-9">
                    <ul class="nav nav-tabs">
                        <li>
                            <a href="#" class="filter active" data-filter="all"><i class="fa fa-sitemap"></i> Svi
                                Channeli</a>
                        </li>
                        <li>
                            <a class="filter" data-filter=".connected" href="#"><i class="fa fa-chain"></i> Povezani</a>
                        </li>
                        <li>
                            <a class="filter" data-filter=".empty" href="#"><i class="fa fa-chain-broken"></i>
                                Nepovezani</a>
                        </li>

                    </ul>
                </div>

            </div>


        </div>
        <!-- END CONTENT TABS -->
    </div>
    <!-- END CONTENT HEADER -->

    <div id="channel-list" class="row channel-list">
        <?php foreach($allPortals as $portal) { ?>
        <div class="mix col-md-4 <?php if($portal['povezan']) { echo 'connected'; } else { echo 'empty'; } ?>">
            <div class="bt-box text-center">
                <img src="web/bundles/tdcm/images/<?php echo $portal['thumbnail'] ?>" alt=""/>

                <p class="url">URL: <a href="<?php echo $portal['link'] ?>" target="_blank"><?php echo substr($portal['link'],7); ?></a></p>

                <p><?php echo $portal['description'] ?></p>

                <p ng-if="portal.brojPovezanih > 0">Broj povezanih smještajnih jedinica: <?php echo $portal['brojPovezanih'] ?></p>

                <div class="text-center">
                    <a class="btn btn-medium blue-booker edit_help" href="channel_help.php?channel=<?=$portal['id']?>"><span
                            class="fa fa-info"></span>Info</a>

                    <?php if($portal['naziv'] != 'Airbnb.com') { ?>
                        <a class="btn btn-medium blue-booker edit_settings" href="channel_settings.php"><span
                            class="fa fa-cog"></span>Postavke</a>
                    <a class="btn btn-medium green-jungle edit_connection" href="channel_connection.php"
                       title="Povezivanje s <?php echo $portal['naziv'] ?> sustavom"><span
                            class="fa fa-link"></span>Poveži</a>
                    <?php } else { ?>
                        <a class="btn btn-medium green-jungle edit_connection" href="channel_connection_ical.php"><span
                                class="fa fa-link"></span>Poveži</a>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php } ?>


<!-- Modal -->
<div id="modaliCal" class="modal fade" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Povezivanje s iCal sustavom</h4>
            </div>
            <div class="modal-body">
                <div class="bt-box bt-box-unit property-item bg-white">
                    <header>
                        <a href="#"><img width="100" height="75"
                                         src="http://foto.atrade.hr/images/375-10912/Penguins.jpg" alt=""></a>
                        <h4>
                            <a href="#">De luxe(2+1)</a>
                        </h4>

                        <div class="info" style="margin:0;padding:0;"><span class="property-id">HR-00375-20</span> iCal
                            poveznica: <strong>NEMA</strong></div>
                        <div class="property-actions">
                            <a class="btn btn-medium blue-booker" href="#"><span class="fa fa-check"></span>Kreiraj iCal
                                Poveznicu</a>
                            <a class="btn btn-medium btn-icon blue-booker" href="#" data-toggle="dropdown"><span
                                    class="fa fa-chevron-down"></span></a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="#"><span class="fa fa-pencil"></span>Uredi jedinicu</a></li>
                                <li><a href="#"><span class="fa fa-photo"></span>Fotografije</a></li>
                                <li><a href="#"><span class="fa fa-file-text-o"></span>Cijene</a></li>
                                <li><a href="#"><span class="fa fa-calendar-check-o"></span>Raspoloživost</a></li>
                            </ul>
                        </div>
                    </header>
                </div>
                <div class="bt-box bt-box-unit property-item bg-white">
                    <header>
                        <a href="#"><img width="100" height="75"
                                         src="http://foto.atrade.hr/images/375-10912/Penguins.jpg" alt=""></a>
                        <h4>
                            <a href="#">De luxe(2+1)</a>
                        </h4>

                        <div class="info" style="margin:0;padding:0;"><span class="property-id">HR-00375-20</span> iCal
                            poveznica: <strong>K3UJ4POJ2-34L5234L5-23452345NH-23452345</strong></div>
                        <div class="property-actions">
                            <a class="btn btn-medium green-jungle" href="#"><span class="fa fa-check"></span>Kreiraj
                                novu iCal Poveznicu</a>
                            <a class="btn btn-medium btn-icon blue-booker" href="#" data-toggle="dropdown"><span
                                    class="fa fa-chevron-down"></span></a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="#"><span class="fa fa-pencil"></span>Uredi jedinicu</a></li>
                                <li><a href="#"><span class="fa fa-photo"></span>Fotografije</a></li>
                                <li><a href="#"><span class="fa fa-file-text-o"></span>Cijene</a></li>
                                <li><a href="#"><span class="fa fa-calendar-check-o"></span>Raspoloživost</a></li>
                            </ul>
                        </div>
                    </header>
                </div>
                <div class="bt-box bt-box-unit property-item bg-white">
                    <header>
                        <a href="#"><img width="100" height="75"
                                         src="http://foto.atrade.hr/images/375-10912/Penguins.jpg" alt=""></a>
                        <h4>
                            <a href="#">De luxe(2+1)</a>
                        </h4>

                        <div class="info" style="margin:0;padding:0;"><span class="property-id">HR-00375-20</span> iCal
                            poveznica: <strong>NEMA</strong></div>
                        <div class="property-actions">
                            <a class="btn btn-medium blue-booker" href="#"><span class="fa fa-check"></span>Kreiraj iCal
                                Poveznicu</a>
                            <a class="btn btn-medium btn-icon blue-booker" href="#" data-toggle="dropdown"><span
                                    class="fa fa-chevron-down"></span></a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="#"><span class="fa fa-pencil"></span>Uredi jedinicu</a></li>
                                <li><a href="#"><span class="fa fa-photo"></span>Fotografije</a></li>
                                <li><a href="#"><span class="fa fa-file-text-o"></span>Cijene</a></li>
                                <li><a href="#"><span class="fa fa-calendar-check-o"></span>Raspoloživost</a></li>
                            </ul>
                        </div>
                    </header>
                </div>
                <div class="bt-box bt-box-unit property-item bg-white">
                    <header>
                        <a href="#"><img width="100" height="75"
                                         src="http://foto.atrade.hr/images/375-10912/Penguins.jpg" alt=""></a>
                        <h4>
                            <a href="#">De luxe(2+1)</a>
                        </h4>

                        <div class="info" style="margin:0;padding:0;"><span class="property-id">HR-00375-20</span> iCal
                            poveznica: <strong>NEMA</strong></div>
                        <div class="property-actions">
                            <a class="btn btn-medium blue-booker" href="#"><span class="fa fa-check"></span>Kreiraj iCal
                                Poveznicu</a>
                            <a class="btn btn-medium btn-icon blue-booker" href="#" data-toggle="dropdown"><span
                                    class="fa fa-chevron-down"></span></a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="#"><span class="fa fa-pencil"></span>Uredi jedinicu</a></li>
                                <li><a href="#"><span class="fa fa-photo"></span>Fotografije</a></li>
                                <li><a href="#"><span class="fa fa-file-text-o"></span>Cijene</a></li>
                                <li><a href="#"><span class="fa fa-calendar-check-o"></span>Raspoloživost</a></li>
                            </ul>
                        </div>
                    </header>
                </div>
                <div class="bt-box bt-box-unit property-item bg-white">
                    <header>
                        <a href="#"><img width="100" height="75"
                                         src="http://foto.atrade.hr/images/375-10912/Penguins.jpg" alt=""></a>
                        <h4>
                            <a href="#">De luxe(2+1)</a>
                        </h4>

                        <div class="info" style="margin:0;padding:0;"><span class="property-id">HR-00375-20</span> iCal
                            poveznica: <strong>K3UJ4POJ2-34L5234L5-23452345NH-23452345</strong></div>
                        <div class="property-actions">
                            <a class="btn btn-medium green-jungle" href="#"><span class="fa fa-check"></span>Kreiraj
                                novu iCal Poveznicu</a>
                            <a class="btn btn-medium btn-icon blue-booker" href="#" data-toggle="dropdown"><span
                                    class="fa fa-chevron-down"></span></a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="#"><span class="fa fa-pencil"></span>Uredi jedinicu</a></li>
                                <li><a href="#"><span class="fa fa-photo"></span>Fotografije</a></li>
                                <li><a href="#"><span class="fa fa-file-text-o"></span>Cijene</a></li>
                                <li><a href="#"><span class="fa fa-calendar-check-o"></span>Raspoloživost</a></li>
                            </ul>
                        </div>
                    </header>
                </div>


            </div>
        </div>
    </div>
</div>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript">
    $(document).ready(function() {

        $('.edit_settings').on('click', function() {
            var el = $(this);
            var options = {
                size: 'lg', // sm, md, lg, xl
                url: el.attr('href'),
                title: 'Uređivanje postavki portala'
            };
            eModal.iframe(options);
            return false;
        });

        $('.edit_connection').on('click', function() {
            var el = $(this);
            var options = {
                size: 'xl', // sm, md, lg, xl
                url: el.attr('href'),
                title: el.attr('title')
            };
            eModal.iframe(options);
            return false;
        });

        $('.edit_help').on('click', function() {
            var el = $(this);
            var options = {
                size: 'md', // sm, md, lg, xl
                url: el.attr('href'),
                title: 'Informacije o spajanju'
            };
            eModal.iframe(options);
            return false;
        });

        $('.tabbable .nav a').on('click', function() {
            return false;
        });

        $('#channel-list').mixItUp();

    });

</script>
<!-- END PAGE LEVEL SCRIPTS -->

<?php $view['slots']->stop() ?>