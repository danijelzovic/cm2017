<?php

namespace td\CMBundle\Controller;

use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use td\CMBundle\Entity\PropertiesUnitsViewSetup;
use td\CMBundle\Entity\SetUp;
use td\CMBundle\Entity\Setupi;
use td\CMBundle\Entity\Portal;
use td\CMBundle\Entity\PortalKorisnik;
use td\CMBundle\Entity\PropertyUnit;
use td\CMBundle\Entity\Property;
use td\CMBundle\Entity\SetupProperty;
use td\CMBundle\Entity\SetupPropertyLog;
use td\CMBundle\Entity\SetupPropertyUnit;
use td\CMBundle\Entity\SetupPropertyUnitLog;
use td\CMBundle\Entity\WebshopProperty;
use td\CMBundle\Entity\WebshopPropertyUnit;
use td\CMBundle\Entity\UnitRate;
use td\CMBundle\Entity\UnitRateStavke;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use JMS;
use Datatheke\Bundle\PagerBundle\Pager\Field;
use Symfony\Component\HttpFoundation\JsonResponse;


class SetUpController extends Controller
{

    private $idCompanyKorisnik = 1;
    private $menu;
    private $portalKorisnik;

    /**
     * Upravljanje sesijom - za sada se koristi cookies
     */
    private function sesija()
    {
        $array = explode(":", $_COOKIE['tdapplogin']);
        if (crypt($array[0], 'he6765zfh') == $array[1]) {
            $this->idCompanyKorisnik = $array[4];
            $companyStatus = $array[6];
            $em = $this->getDoctrine()->getManager();
            $connection = $em->getConnection();
            $statement = $connection->prepare("SELECT menu_path FROM company_status WHERE id= :id");
            $statement->bindValue('id', $companyStatus);
            $statement->execute();
            $results = $statement->fetchAll();
            $row = $results[0];
            $this->menu = $row['menu_path'];
        } else {
            header("location: https://agencije.atrade.hr");
            exit;
        }
    }

    /**
     * Inicijalni prikaz objekata i unita za neki portal. Povezani, nepovezani, neprijavljeni
     *
     * @param $portal
     * @return Response
     */
    public function indexAction(Portal $portal)
    {
        //kontorler za prikaz setupa po portalu
        //DOhvat korisnika iz COOKIE varijable kako biH imaO ispravan set podataka
//		$this->sesija();
        $idCompanyKorisnik = $this->idCompanyKorisnik;
        $em = $this->getDoctrine()->getManager();
        $serializer = JMS\Serializer\SerializerBuilder::create()->build();

        //Pronađi portal korisnik za idCompanyKorisnik i portal
        /* @var $portalKorisnik PortalKorisnik */
        $portalKorisnik = $em->getRepository('tdCMBundle:PortalKorisnik')->findOneBy(
            array('portal' => $portal, 'idCompanyKorisnik' => $idCompanyKorisnik)
        );

        //Tab1 - CM WS Portal - BROJ POVEZANIH UNITA
        $query = $em->createQuery(
            "SELECT count(spu.id)
                        FROM tdCMBundle:SetupPropertyUnit spu JOIN spu.setupProperty sp
                        WHERE spu.portalKorisnik = :portalKorisnik AND
                              spu.idPropertyUnitNaPortalu IS NOT NULL AND
                              sp.idPropertyNaPortalu IS NOT NULL AND
                              sp.idPropertyNaPortalu <> '' AND
                              sp.aktivan = TRUE AND
                              spu.aktivan = TRUE"
        );
        $query->setParameters(array(
            'portalKorisnik' => $portalKorisnik
        ));
        $brojPovezanih = $query->getSingleScalarResult();

        //Tab2 - WS CM NOT Portal - BROJ NEPOVEZANIH UNITA
        $query = $em->createQuery(
            'SELECT count(spu.id)
                        FROM tdCMBundle:SetupPropertyUnit spu JOIN  spu.setupProperty sp
                        WHERE spu.portalKorisnik = :portalKorisnik AND
                              (spu.idPropertyUnitNaPortalu IS NULL OR spu.aktivan = FALSE OR sp.aktivan = FALSE )'
        );
        $query->setParameters(array(
            'portalKorisnik' => $portalKorisnik
        ));
        $brojNepovezanih = $query->getSingleScalarResult();

        //Tab3 - WS - NALAZE SE U WEBSHOPU, A NEMA IH U CHANNEL MANAGERU - BROJ
        //odaberi sve iz WebshopUnit za koje idWebshop = SetUp.idWebshop AND SetUp.portalKorisnik = odabrani
        //Polja: Property.name ID_Unit Unit.name2 Skrbnik Vlasnik

        $query = $em->createQuery(
            "SELECT count(wp.id)
              FROM tdCMBundle:WebshopProperty wp JOIN wp.property p JOIN p.agentC ac JOIN p.owner ow
              WHERE wp.idShop = :idWebshop AND wp.idProperty NOT IN (SELECT sp.idProperty FROM tdCMBundle:SetupProperty sp WHERE sp.portalKorisnik = :portalKorisnik)"
        );

        $query->setParameters(array(
            'portalKorisnik' => $portalKorisnik,
            'idWebshop' => $portalKorisnik->getWebshop()->getId()
        ));
        $brojWebshopUnits = $query->getSingleScalarResult();
//        $setUp3s = $serializer->serialize($webshopUnits, 'json');

        //Tab4 - NOT WS CM NOT Portal
        $qb2 = $em->getRepository('tdCMBundle:SetupPropertyUnit')->createQueryBuilder('s');
        $qb2->where(
            $qb2->expr()->andX(
                $qb2->expr()->eq('s.portalKorisnik', '?1'),
                $qb2->expr()->eq('s.idWebshop', '?2'), // WS = 0
                $qb2->expr()->neq('s.idPropertyUnit', '?2'), // ID Unit != 0
                $qb2->expr()->eq('s.aktivan', '?3'), // Aktivan DA
                $qb2->expr()->isNull('s.idPropertyUnitNaPortalu') // Unit na portalu NIJE definiran
            )
        );
        $qb2->setParameters(array(1 => $portalKorisnik, 2 => 0, 3 => true));
        $query = $qb2->getQuery();
        $setUp4 = $query->getResult();

        //Broj neuspjelih transfera
        $query = $em->createQuery(
            "SELECT count(t) FROM tdCMBundle:Transfer t JOIN t.setupPropertyUnit spu JOIN spu.portalKorisnik pk WHERE pk = :portalKorisnik AND t.ok = :false"
        );
        $query->setParameter('portalKorisnik', $portalKorisnik);
        $query->setParameter('false', false);

        $brojNeuspjelihTransfera = $query->getSingleScalarResult();
        if (0 < $brojNeuspjelihTransfera) {
            $this->addFlash('danger', 'Za ovaj portal postoje neuspjeli transferi (' . $brojNeuspjelihTransfera . ')');
        }

        //pozovi prikaz na ekran
        return $this->render('tdCMBundle:SetUp:index.html.twig',
            array('setup1' => $brojPovezanih,
                // 'collection' => $collection->createView(),
                'setup2' => $brojNepovezanih,
                'setup3' => $brojWebshopUnits,
                'setup4' => $setUp4,
                'portalKorisnik' => $portalKorisnik,
                'menu' => $this->menu,
            ));
    }

    //Slanje podataka na grid
    public function setupPropertyUnitJsonAction(Request $request)
    {

        $idSetupProperty = $request->query->get('idSetupProperty');

        //S obzirom da koristin loadonce mora san stavit da mi inicijalno pošalje sve redove
//            $request->query->set('rows', '10000');
        //Staro	- DATATHEKE

        $pager = $this->get('datatheke.pager')->createPager('tdCMBundle:SetupPropertyUnit');

        /**
         * Customize the QueryBuilder
         */

        // Retrieve QueryBuilder to join on the Adress Entity
        $qb = $pager->getAdapter()->getQueryBuilder();
        $qb->addSelect('pu')->innerJoin('e.propertyUnit', 'pu');

//            $qb->addSelect('p')->innerJoin('p.property', 'p');
//            $qb2->where(
//                    $qb2->expr()->andX(
//                        $qb2->expr()->eq('s.portalKorisnik', '?1'), 
//                        $qb2->expr()->neq('s.idWebshop', '?2'), // WS != 0
//                        $qb2->expr()->neq('s.idPropertyUnit', '?2'), // ID Unit != 0
//                        // $qb2->expr()->eq('s.aktivan', '?3'), // Aktivan DA
//                        $qb2->expr()->isNotNull('s.idPropertyUnitNaPortalu'), // Unit na portalu definiran
//                        $qb2->expr()->neq('s.idPropertyUnitNaPortalu', '?4')
//                    )
//                );
        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq('e.setupProperty', '?1'),
                $qb->expr()->isNotNull('e.idPropertyUnitNaPortalu')
            )
        );
//            $qb->addSelect('a')->leftJoin('e.adress', 'a');
        $qb->setParameters(array(1 => $idSetupProperty));
        // Add field 'city' to the pager
//            $pager->getAdapter()->addField(new Field('adress.city', Field::TYPE_STRING, 'a.city'), 'city');
        $pager->getAdapter()->addField(new Field('propertyUnit.name', Field::TYPE_STRING, 'pu.name'), 'name');

        /**
         * Create the DataGrid
         */

        $datagrid = $this->get('datatheke.datagrid')->createHttpDataGrid($pager, array(), 'jqgrid');
//            $datagrid = $this->get('datatheke.datagrid')->createHttpDataGrid('tdCMBundle:SetupGrid', array(), 'jqgrid');

        return $datagrid->handleRequest($request);
    }

    public function kreirajAction(Request $request)
    {
        // create a task and give it some dummy data for this example
        $setUp = new SetUp();
        $setUp->setAktivan(true);

        $form = $this->createForm(new SetUpType(), $setUp);

        $form->handleRequest($request);

        if ($form->isValid()) {
            //perform some action, such as saving the task to the database
            $em = $this->getDoctrine()->getManager();
            $em->persist($setUp);
            $em->flush();

            //Treba urednije riješit

            $nextAction = $form->get('saveAndAdd')->isClicked()
                ? 'td_cm_setUp_kreiraj'
                : 'td_cm_setUp_pregled';

            return $this->redirect($this->generateUrl($nextAction, array('idPortal' => $setUp->getPortalKorisnik()->getId(), 'idCompanyKorisnik' => 1)));
        }

        return $this->render('tdCMBundle:SetUp:kreiraj2.html.twig', array('form' => $form->createView()));

        //return $this->render('AcmeTaskBundle:Default:index.html.twig', array('name' => $name));
    }

    //Kreiranje za webshopUnits
    public function kreirajwsAction($idPortalKorisnik, $idWebshopUnit)
    {

        //Dohvati portalKorisnik
        $portalKorisnik = $this->getDoctrine()->getManager()->getRepository('tdCMBundle:PortalKorisnik')->find($idPortalKorisnik);

        $request = $this->get('request');
        // create a task and give it some dummy data for this example
        $setUp = new SetUp();
        $setUp->setAktivan(true);
        $setUp->setIdWebshop($portalKorisnik->getIdWebshopRates());
        $setUp->setPortalKorisnik($portalKorisnik);
        $setUp->setIdUnit($idWebshopUnit);

        $form = $this->createForm(new SetUp2Type(), $setUp);

        $form->handleRequest($request);

        if ($form->isValid()) {
            //perform some action, such as saving the task to the database
            $em = $this->getDoctrine()->getManager();
            $em->persist($setUp);
            $em->flush();

            $nextAction = 'td_cm_setUp_portal';
            return $this->redirect($this->generateUrl($nextAction, array('idPortal' => $portalKorisnik->getPortal()->getId(), 'idCompanyKorisnik' => 1)));
        }

        return $this->render('tdCMBundle:SetUp:kreirajws.html.twig', array('form' => $form->createView(), 'portalKorisnik' => $portalKorisnik));
    }

    //Ažuriraj setupPropertyUnit
    public function azurirajAction($idPortalKorisnik, $id)
    {
        //Dohvati portalKorisnik
        $portalKorisnik = $this->getDoctrine()->getManager()->getRepository('tdCMBundle:PortalKorisnik')->find($idPortalKorisnik);

        $request = $this->get('request');
        // create a task and give it some dummy data for this example
        $setupPropertyUnit = $this->getDoctrine()
            ->getRepository('tdCMBundle:SetupPropertyUnit')
            ->find($id);

        $form = $this->createForm(new SetupPropertyUnitType(), $setupPropertyUnit);

        $form->handleRequest($request);

        if ($form->isValid()) {
            //perform some action, such as saving the task to the database
            $em = $this->getDoctrine()->getManager();
            $em->persist($setupPropertyUnit);
            $em->flush();

            $nextAction = 'td_cm_setUp_portal';
            return $this->redirect($this->generateUrl($nextAction, array('idPortal' => $portalKorisnik->getPortal()->getId(), 'idCompanyKorisnik' => $portalKorisnik->getIdCompanyKorisnik())));
        }

        return $this->render('tdCMBundle:SetupPropertyUnit:azuriraj.html.twig',
            array('form' => $form->createView(),
                'portalKorisnik' => $portalKorisnik,
                'idPortal' => $portalKorisnik->getPortal()->getId(),
                'idCompanyKorisnik' => $portalKorisnik->getIdCompanyKorisnik()));
    }

    //U potpunosti obrišisetup iz CM
    public function obrisiAction($idPortalKorisnik, $id)
    {
        //Brisanje Setupa
        //Dohvati portalKorisnik
        $portalKorisnik = $this->getDoctrine()->getManager()->getRepository('tdCMBundle:PortalKorisnik')->find($idPortalKorisnik);

        // create a task and give it some dummy data for this example
        $setUp = $this->getDoctrine()
            ->getRepository('tdCMBundle:SetUp')
            ->find($id);

        if (!is_null($setUp)) {
            //perform some action, such as saving the task to the database
            $em = $this->getDoctrine()->getManager();
            $em->remove($setUp);
            $em->flush();


        }
        $nextAction = 'td_cm_setUp_portal';
        return $this->redirect($this->generateUrl($nextAction, array('idPortal' => $portalKorisnik->getPortal()->getId(), 'idCompanyKorisnik' => $portalKorisnik->getIdCompanyKorisnik())));
        // return $this->render('tdCMBundle:SetUp:kreirajws.html.twig', array('form' => $form->createView(), 'portalKorisnik' => $portalKorisnik));

    }

    //Brisanje samo ID unita na portalu
    public function obrisiVezuAction($idPortalKorisnik, $id)
    {
        //Brisanje Setupa
        //Dohvati portalKorisnik
        $portalKorisnik = $this->getDoctrine()->getManager()->getRepository('tdCMBundle:PortalKorisnik')->find($idPortalKorisnik);

        // create a task and give it some dummy data for this example
        $setUp = $this->getDoctrine()
            ->getRepository('tdCMBundle:SetUp')
            ->find($id);

        if (!is_null($setUp)) {
            //perform some action, such as saving the task to the database
            $setUp->setIdUnitNaPortalu(null);
            $em = $this->getDoctrine()->getManager();
            $em->persist($setUp);
            $em->flush();
        }
        $nextAction = 'td_cm_setUp_portal';
        return $this->redirect($this->generateUrl($nextAction, array('idPortal' => $portalKorisnik->getPortal()->getId(), 'idCompanyKorisnik' => $portalKorisnik->getIdCompanyKorisnik())));
        // return $this->render('tdCMBundle:SetUp:kreirajws.html.twig', array('form' => $form->createView(), 'portalKorisnik' => $portalKorisnik));
    }

    //Slanje SetupPropertyGrid podataka na grid
    public function vSetupPropertyGridJsonAction(Request $request)
    {

//        S obzirom da koristin loadonce mora san stavit da mi inicijalno pošalje sve redove
        $request->query->set('rows', '10000');

        $idPortalKorisnik = $request->query->get('idPortalKorisnik');
        $pager = $this->get('datatheke.pager')->createPager('tdCMBundle:VSetupPropertyGrid');

        //Provjera da li se traže povezani ili nepovezani
        $tip = $request->query->get('tip');
        if ($tip == "povezani") {
            $qb = $pager->getAdapter()->getQueryBuilder();
            $qb->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('e.idPortalKorisnik', $idPortalKorisnik),
                    $qb->expr()->isNotNull('e.idPropertyNaPortalu'),
                    $qb->expr()->eq('e.aktivan', true),
                    $qb->expr()->gt('e.brojUnita', 'e.brojNeprijavljenih')
                )
            );
        } else { //Ovo vadi nepovezane
            $qb = $pager->getAdapter()->getQueryBuilder();
            $qb->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('e.idPortalKorisnik', $idPortalKorisnik),
                    $qb->expr()->orX(
                        $qb->expr()->isNull('e.idPropertyNaPortalu'),
                        $qb->expr()->neq('e.brojNeprijavljenih', 0),
                        $qb->expr()->eq('e.aktivan', 0)
                    )
                )
            );
        }

        $datagrid = $this->get('datatheke.datagrid')->createHttpDataGrid($pager, array(), 'jqgrid');
        return $datagrid->handleRequest($request);
    }

    //Slanje SetupPropertyUnitGrid podataka na grid
    public function vSetupPropertyUnitGridJsonAction(Request $request)
    {
        $idSetupProperty = $request->query->get('idSetupProperty');
        $tip = $request->query->get('tip');

        $pager = $this->get('datatheke.pager')->createPager('tdCMBundle:VSetupPropertyUnitGrid');

        /* @var QueryBuilder $qb */
        if ($tip == "povezani") {
            $qb = $pager->getAdapter()->getQueryBuilder();
            $qb->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('e.idSetupProperty', $idSetupProperty),
                    $qb->expr()->isNotNull('e.idPropertyUnitNaPortalu'),
                    $qb->expr()->eq('e.aktivan', true)
                )
            );
        } elseif ('nepovezani' == $tip) {
            $qb = $pager->getAdapter()->getQueryBuilder();
            $qb->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('e.idSetupProperty', $idSetupProperty),
                    $qb->expr()->orX(
                        $qb->expr()->isNull('e.idPropertyUnitNaPortalu'),
                        $qb->expr()->eq('e.aktivan', 0)
                    )
                )
            );
        } else {
            $qb = $pager->getAdapter()->getQueryBuilder();
            $qb->where(
                $qb->expr()->eq('e.idSetupProperty', $idSetupProperty)
            );
        }

        $datagrid = $this->get('datatheke.datagrid')->createHttpDataGrid($pager, array(), 'jqgrid');
        return $datagrid->handleRequest($request);
    }


    //Spremanje u bazu jednog retka
    public function setupPropertyUnitPersistRowGridAction(Request $request)
    {
        if (isset($request)) {
            $idPropertyUnitNaPortalu = $request->request->get('idPropertyUnitNaPortalu');
            if ($idPropertyUnitNaPortalu == "") $idPropertyUnitNaPortalu = null;

            //ne moren ga iskat po idUnit jer u setupu more bit više istih unita, ali po različitim portalima. Moran dodat nova polja u SetupGrid
            $em = $this->getDoctrine()->getManager();

            /* @var $setupPropertyUnit SetupPropertyUnit */
            $setupPropertyUnit = $em->getRepository('tdCMBundle:SetupPropertyUnit')->find($request->request->get('id'));
            if ($setupPropertyUnit) {
                $setupPropertyUnit->setIdPropertyUnitNaPortalu($idPropertyUnitNaPortalu);
                if ($request->request->get('aktivan') == 'Yes') {
                    $setupPropertyUnit->setAktivan(true);
                } else {
                    $setupPropertyUnit->setAktivan(false);
                }
                if ($request->request->get('povezan') == 'Yes') {
                    $setupPropertyUnit->setPovezan(true);
                } else {
                    $setupPropertyUnit->setPovezan(false);
                }

                $em->persist($setupPropertyUnit);
                $em->flush();

                return new Response(
                    'Promjene na smještajnoj jedinici ' . $setupPropertyUnit->getIdPropertyUnitFront() . ' uspješno spremljene!',
                    Response::HTTP_OK,
                    array('content-type' => 'text/html')
                );
            }
        }
        return new Response(
            'Nije uspjelo spremanje!',
            Response::HTTP_OK,
            array('content-type' => 'text/html')
        );

    }

    public function setupPropertyPersistRowGridAction(Request $request)
    {
        if (isset($request)) {
            $idSetupProperty = $request->request->get('id');
            $idPropertyNaPortalu = $request->request->get('idPropertyNaPortalu');
            if ($idPropertyNaPortalu == "") $idPropertyNaPortalu = null;
            $aktivan = $request->request->get('aktivan');

            $em = $this->getDoctrine()->getManager();
            /* @var $setupProperty SetupProperty */
            $setupProperty = $em->getRepository('tdCMBundle:SetupProperty')->find($idSetupProperty);
            if ($setupProperty) {
//                $setup->setIdPropertyNaPortalu($idPropertyNaPortalu);
                $setupProperty->setIdPropertyNaPortalu($idPropertyNaPortalu);
                if ($request->request->get("aktivan") == "Yes") {
                    $setupProperty->setAktivan(true);
                } else {
                    $setupProperty->setAktivan(false);
                }
                if ('Yes' == $request->request->get('povezan')) {
                    $setupProperty->setPovezan(true);
                } else {
                    $setupProperty->setPovezan(false);
                }
                $em->persist($setupProperty);
                $em->flush();

                return new Response(
                    'Promjene na objektu ' . $setupProperty->getProperty()->getIdCountry() . '-' . str_pad($setupProperty->getProperty()->getId(), 5, '0', STR_PAD_LEFT) . ' uspješno spremljene!',
                    Response::HTTP_OK,
                    array('content-type' => 'text/html')
                );
            }
        }

        return new Response(
            'Nije uspjelo spremanje!',
            Response::HTTP_OK,
            array('content-type' => 'text/html')
        );
    }

    public function autocompleteSetupPropertyGridAction(Request $request)
    {
        if (isset($request)) {
            $em = $this->getDoctrine()->getManager();
            $idPortalKorisnik = $request->query->get('idPortalKorisnik');

            //Provjeri koje polje se traži
            if (!is_null($value = $request->query->get('idPropertyNaPortalu'))) {
                $var = 'idPropertyNaPortalu';
            } elseif (!is_null($value = $request->query->get('nazivProperty'))) {
                $var = 'nazivProperty';
            } elseif (!is_null($value = $request->query->get('mjesto'))) {
                $var = 'mjesto';
            } elseif (!is_null($value = $request->query->get('skrbnik'))) {
                $var = 'skrbnik';
            } elseif (!is_null($value = $request->query->get('vlasnik'))) {
                $var = 'vlasnik';
            } elseif (!is_null($value = $request->query->get('nameWebshop'))) {
                $var = 'nameWebshop';
            } elseif (!is_null($value = $request->query->get('nazivCompanyKorisnik'))) {
                $var = 'nazivCompanyKorisnik';
            } elseif (!is_null($value = $request->query->get('tag'))) {
                $var = 'tag';
            }

            if ($var != 'tag') {
                $sqlQuery = 'SELECT DISTINCT vsp.' . $var . '
                                    FROM tdCMBundle:VSetupPropertyAll vsp
                                    WHERE vsp.' . $var . ' LIKE :value';
                if (0 != $idPortalKorisnik) {
                    $sqlQuery .= ' AND vsp.idPortalKorisnik = :idPortalKorisnik';
                }
                $query = $em->createQuery($sqlQuery)
                    ->setMaxResults(20);
                $query->setParameter('value', '%' . $value . '%');

                if (0 != $idPortalKorisnik) {
                    $query->setParameter('idPortalKorisnik', $idPortalKorisnik);
                }
                $data = $query->getResult();

            } else {
                $query = $em->createQuery(
                    'SELECT DISTINCT vsp.' . $var . '
                                    FROM tdCMBundle:ClientTags vsp
                                    WHERE vsp.' . $var . ' LIKE :value'
                )
                    ->setMaxResults(20);
                $query->setParameter('value', '%' . $value . '%');
                $data = $query->getResult();
            }

            $response = new JsonResponse();
//            $response->setData(array(array('naziv' => $klijenti[0]->getNaziv() )));
            $response->setData($data);

        } else {
            $response = new Response(
                'Content',
                Response::HTTP_OK,
                array('content-type' => 'text/html')
            );
        }

        return $response;
    }

    public function indexBTAction(Portal $portal)
    {
        $em = $this->getDoctrine()->getManager();

        //Pregledaj dali postoje property koji trebaju biti ubačeni u CM.
        //Uzmi u obzir one koji imaju zastavicu deleted i javi u CM
        //Ubaci u CM i pripremi ih za postavljanje
        //Dohvati sve SetupProperty-e.
//        $idCurrentCompany = $_COOKIE[2];
        $idCurrentCompany = 340;
//        $pravaOgraniceni = $_COOKIE[3];
        $pravaOgraniceni = 1;
//        $idCurrentUserClient = $_COOKIE[4];
        $idCurrentUserClient = 135748;
        //Provjeri dali postoji PortalKorisnik za zadani Portal
        $portalKorisnik = $em->getRepository('tdCMBundle:PortalKorisnik')->findOneBy(array(
            'portal' => $portal,
            'idCompanyKorisnik' => $idCurrentCompany,
        ));

//        if(!$portalKorisnik){
//            $portalKorisnik = new PortalKorisnik();
//            $portalKorisnik->setIdCompanyKorisnik($idCurrentCompany);
//            $portalKorisnik->setPortal($portal);
//            $portalKorisnik->setUrlPortala($portal->getLink());
//            $portalKorisnik->setLogin('');
//            $portalKorisnik->setPassword('');
//            $portalKorisnik->setCjenikBrojPokusaja(5);
//            $portalKorisnik->setPodaciBrojPokusaja(5);
//            $portalKorisnik->setOpisBrojPokusaja(5);
//            $portalKorisnik->setFotoBrojPokusaja(5);
//            $portalKorisnik->setIdKorisnikaNaPortalu('');
//            $portalKorisnik->setPoredak(99);
//            $em->persist($portalKorisnik);
//            $em->flush();
//        }
        if (!$portalKorisnik) $this->redirect('tdCMBundle:Default:index');
        $this->portalKorisnik = $portalKorisnik;

        //Kreirati repository za dohvat svih objekata iz CM koji zadovoljavaju kriterije iz COOKIE-a

        $client = $em->getRepository('tdCMBundle:Client')->find($idCurrentUserClient);
        $setupProperties = $em->getRepository('tdCMBundle:SetupProperty')->findByPortalKorisnik($portalKorisnik);
        if ($pravaOgraniceni) {
            $properties = $em->getRepository('tdCMBundle:Property')->findByPravaOgraniceni($idCurrentUserClient);
        } else {
            $properties = $em->getRepository('tdCMBundle:Property')->findByNotPravaOgraniceni($idCurrentCompany);
        }
        $this->registerPropertiesInCM($setupProperties, $properties, $portalKorisnik);
//        $noviSetupProperties = $em->getRepository('tdCMBundle:SetupProperty')->findByPortalKorisnik($portalKorisnik);

        $noviSetupProperties = $em->getRepository('tdCMBundle:SetupProperty')->findByPortalKorisnikZaJson($portalKorisnik);

        $serializer = JMS\Serializer\SerializerBuilder::create()->build();

        $setupPropertiesJson = $serializer->serialize((array)$noviSetupProperties, 'json', JMS\Serializer\SerializationContext::create()->enableMaxDepthChecks());


        //Potrebno doraditi view tj kreirati novi
        return $this->render('tdCMBundle:SetUp:indexBT.html.php', array(
            'setupProperties' => $noviSetupProperties,
            'portalKorisnik' => $portalKorisnik,
            'setupPropertiesJson' => $setupPropertiesJson,
            'debug' => '',
        ));
    }


    //Spremanje u bazu jednog retka unitrate-a
    public function unitRateStavkePersistRowAction(Request $request)
    {
        if (isset($request)) {
            if ($request->request->get('operacija') == 'dodaj') {
                $em = $this->getDoctrine()->getManager();
                $unitRateStavka = new UnitRateStavke();

                //DOhvat i spremanje UnitRateStavke-a.
                $idUnitRate = $request->request->get('idUnitRate');
                $unitRate = $em->getRepository('tdCMBundle:UnitRate')->find($idUnitRate);
                $unitRateStavka->setUnitRate($unitRate);
//                    $unitRateStavka->setItemPriceListItem($request->request->get('idItempricelistitem'));

                $itempricelistitem = $em->getRepository('tdCMBundle:ItemPriceListItem')->find($request->request->get('idStavke'));
                $unitRateStavka->setItemPriceListItem($itempricelistitem);

                $em->persist($unitRateStavka);
                $em->flush();
            } elseif ($request->request->get('operacija') == 'brisi') {
                $em = $this->getDoctrine()->getManager();
                $idUnitRate = $request->request->get('idUnitRate');
                $unitRate = $em->getRepository('tdCMBundle:UnitRate')->find($idUnitRate);
                $itempricelistitem = $em->getRepository('tdCMBundle:ItemPriceListItem')->find($request->request->get('idStavke'));

                $unitRateStavka = $em->getRepository('tdCMBundle:UnitRateStavke')->findOneBy(array('unitRate' => $unitRate, 'itemPriceListItem' => $itempricelistitem));
                $em->remove($unitRateStavka);
                $em->flush();
            }
        }

        // $datagrid = $this->get('datatheke.datagrid')->createHttpDataGrid('tdCMBundle:SetUp', array(), 'jqgrid');
//             $datagrid = $this->get('datatheke.datagrid')->createHttpDataGrid('tdCMBundle:UnitRateStavkeGrid', array(), 'jqgrid');

        //OVo san stori da rasteretin server. Bitno je dakle tornat neki HTTP response.
        $response = new Response(
            'Content',
            Response::HTTP_OK,
            array('content-type' => 'text/html')
        );
        return $response;
//        return $datagrid->handleRequest($request);
    }


    /**
     * Dodavanje jednog objekta i njegovih unita u CM koji su izvan CM
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function t3Prijavi1Action(Request $request)
    {
        if (isset($request)) {
            //Dohvat GET parametara
            $idProperty = $request->query->get('idProperty');
            $idPortalKorisnik = $request->query->get('idPortalKorisnik');
            $em = $this->getDoctrine()->getManager();

            //Inicijalizacija potrebnih podataka
            /* @var $portalKorisnik PortalKorisnik */
            $portalKorisnik = $em->getRepository('tdCMBundle:PortalKorisnik')->find($idPortalKorisnik);
            /* @var $propertyUnit PropertyUnit */
            $property = $em->getRepository('tdCMBundle:Property')->find($idProperty);
            /* @var $webshopPropertyUnit WebshopPropertyUnit */

            if ($property) {
                $this->prijaviPropertyUChannelManager($portalKorisnik, $property);
                $this->addFlash('notice', 'Objekt ' . $property->getIdCountry() . '-' . str_pad($property->getId(), 5, '0', STR_PAD_LEFT) . ' je prijavljen u Channel Manager.');
            }


            //Na setupPortsl4 napraviti ažuriranje da se prebacuje stavka u neprijavljene (Tab2)

//                $response = $this->forward('tdCMBundle:SetUp:index2', array(
//                    'idPortal'  => $portalKorisnik->getPortal()->getId(),
//                ));
            $response = $this->redirectToRoute('td_cm_setUp_portal', array(
                'portal' => $portalKorisnik->getPortal()->getId()));

            return $response;
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse|Response
     * @throws \Exception
     */
    public function dohvatBookingComAction(Request $request)
    {
        //Potrebno je kreirati kolekciju, zatim pretvortii ju u json i poslati natrag na front
        $idSetupPropertyUnit = $request->request->get('idSetupPropertyUnit');

        $em = $this->getDoctrine()->getManager();
        /* @var $setupPropertyUnit SetupPropertyUnit */
        $setupPropertyUnit = $em->getRepository('tdCMBundle:SetupPropertyUnit')->find($idSetupPropertyUnit);

        if ($setupPropertyUnit) $hotelId = $setupPropertyUnit->getSetupProperty()->getIdPropertyNaPortalu();
        else return new Response('Nije nađen setup property unit!');

        // Call bookingComService
        $podaci = $this->get('td_cm.obrada.setup_obrada')->getAvailableItemsBookingCom($setupPropertyUnit);

        //Slanje Responsa
        $response = new JsonResponse();
        $response->setData($podaci);
        return $response;
    }

    /**
     * Priprema jqgrida za objekte koji su izvan CM
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function t3ObjektiIzvanCmAction(Request $request)
    {
//        Prikupljanje parametara koje šalje jqgrid
        $idPortalKorisnik = $request->query->get('idPortalKorisnik');
        $search = $request->query->get('_search');
        $rows = $request->query->get('rows');
        $page = $request->query->get('page');
        $sidx = $request->query->get('sidx');
        $sord = $request->query->get('sord');

        if ($search) {
            $filters = $request->query->get('filters');
            $filtersArray = json_decode($filters, true);
            $rules = $filtersArray['rules'];
        }

        $em = $this->getDoctrine()->getManager();
        $config = $em->getConfiguration();
        $config->addCustomStringFunction('LPAD', 'td\CMBundle\Query\MysqlLpad');

        /* @var $portalKorisnik PortalKorisnik */
        $portalKorisnik = $em->getRepository('tdCMBundle:PortalKorisnik')->find($idPortalKorisnik);

        $dql = "SELECT
                    wp.id idWebshopProperty,
                    wp.idProperty idProperty,
                    CONCAT(p.idCountry , '-', LPAD(p.id,5,'0')) idFrontProperty,
                    p.name nazivProperty,
                    p.city mjestoProperty,
                    ow.naziv nazivSkrbnik,
                    ac.naziv nazivVlasnik
                    FROM tdCMBundle:WebshopProperty wp JOIN wp.property p JOIN p.owner ow JOIN p.agentC ac
                  WHERE p.id NOT IN (SELECT sp.idProperty FROM tdCMBundle:SetupProperty sp WHERE sp.webshop = :webshop) AND
                  wp.webshop = :webshop";
        if ($sidx) {
            $dql = $dql . 'ORDER BY ' . $sidx . ' ' . $sord;
        }
        $query = $em->createQuery($dql)->setParameter('webshop', $portalKorisnik->getWebshop()->getId());
        $data = $query->getResult();

        $jqGridData = array();
        $jqGridData['page'] = 1;
        $jqGridData['total'] = 1;
        $jqGridData['records'] = count($data);
        $jqGridData['rows'] = $data;
//        $idPortalKorisnik = $request->query->get('idPortalKorisnik');

        return new JsonResponse($jqGridData);
    }

    /**
     * Prijavi samo označene sa popisa u Channel Manager - NIJE DOVRŠENO!!!
     *
     * @param PortalKorisnik $portalKorisnik
     * @return RedirectResponse
     * @internal param Request $request
     * @internal param PortalKorisnik $portalKorisnik
     */
    public function t3PrijaviOznaceneAction(PortalKorisnik $portalKorisnik)
    {
        $request = $this->get('request');
        if (isset($request)) {
            $idsJson = $request->request->get('ids');
            $ids = json_decode($idsJson, true);
            $em = $this->getDoctrine()->getManager();
            $countProperty = 0;
            $propertyRepository = $em->getRepository('tdCMBundle:Property');
            foreach ($ids as $idProperty) {
                $this->prijaviPropertyUChannelManager($portalKorisnik, $propertyRepository->find($idProperty));
                $countProperty++;
            }
            $this->addFlash('notice', 'Dodano je ' . $countProperty . ' objekata u Channel Manager');
        } else {
            $this->addFlash('notice', 'Greška! Nije bilo moguće prijaviti objekte u Channel Manager.');
        }

        return $this->redirectToRoute('td_cm_setUp_portal', array('portal' => $portalKorisnik->getPortal()->getId()));
    }

    /**
     * Dohvat svih objekata koji se nalaze u WebshopProperty, a da nisu
     * u CM-u i ubacivanje isitih u CM
     *
     * @param PortalKorisnik $portalKorisnik
     * @return RedirectResponse
     */
    public function t3PrijaviSveAction(PortalKorisnik $portalKorisnik)
    {
        //Prijavi objekte
        //Prolaz po svim objektima koji se nalaze u Webshopu od portala.
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            "SELECT wp
              FROM tdCMBundle:WebshopProperty wp
              WHERE wp.idShop = :idWebshop AND wp.idProperty NOT IN (SELECT sp.idProperty FROM tdCMBundle:SetupProperty sp WHERE sp.portalKorisnik = :portalKorisnik)"
        );

        $query->setParameters(array(
            'portalKorisnik' => $portalKorisnik,
            'idWebshop' => $portalKorisnik->getWebshop()->getId()
        ));
        $webshopProperties = $query->getResult();

        $countProperty = 0;
        /* @var $webshopProperty WebshopProperty */
        foreach ($webshopProperties as $webshopProperty) {
            $this->prijaviPropertyUChannelManager($portalKorisnik, $webshopProperty->getProperty());
            $countProperty++;
        }
        $this->addFlash('notice', 'Dodano je ' . $countProperty . ' objekata u Channel Manager');
        return $this->redirectToRoute('td_cm_setUp_portal', array('portal' => $portalKorisnik->getPortal()->getId()));
    }

    /**
     *  List all units for managing (jqGrid)
     *
     * @return Response
     */
    public function unitsAction()
    {
        //Sigurnost
        // Potrebno je implementirati

        // Dohvat portal korisnika
        // Dodavanje forme
        $em = $this->getDoctrine()->getManager();
        $portals = $em->getRepository('tdCMBundle:Portal')->findAll();

        return $this->render('tdCMBundle:SetUp:indexSp.html.twig', array(
            'portals' => $portals
        ));
    }

    /**
     * List all properties and units
     *
     * @return Response
     */
    public function propertiesUnitsAction(Request $request)
    {
        //Sigurnost
        //Procesing security

        //General settings
        $em = $this->getDoctrine()->getManager();

        //Processing parameters
        if ('GET' == $request->getMethod()) {
            $maxResult = 10;
            $firstResult = 0;
            $sFront = 0;
            $sPropertyName = '';
            $sMjesto = '';
            $sSkrbnik = '';
            $sVlasnik = '';
            $page = 1;
        } else {
            $maxResult = $request->request->get('maxResult');
            $page = $request->request->get('page');
            $firstResult = $maxResult * ($page - 1);
            $sFront = (int)$request->request->get('sFront');
            $sPropertyName = $request->request->get('sPropertyName');
            $sMjesto = $request->request->get('sMjesto');
            $sSkrbnik = $request->request->get('sSkrbnik');
            $sVlasnik = $request->request->get('sVlasnik');
        }

        if ($sFront == 0) {
            $sFrontFrom = 0;
            $sFrontTo = 99999;
        } else {
            $sFrontFrom = $sFront;
            $sFrontTo = $sFront;
        }

        //Get PropertiesUnitsViewSetup
        $propertiesUnitsViewSetup = $this->getPropertiesUnitsViewSetup();

        //Get all properties
        $properties = $em->getRepository('tdCMBundle:Property')->findAllNotDeletedForFront($maxResult, $firstResult, $sFrontFrom, $sFrontTo, $sPropertyName, $sMjesto, $sSkrbnik, $sVlasnik);

        $totalCount = $em->getRepository('tdCMBundle:Property')->countAllNotDeletedForFront($sFrontFrom, $sFrontTo, $sPropertyName, $sMjesto, $sSkrbnik, $sVlasnik);
        if ($totalCount > $maxResult) {
            $noPages = ceil($totalCount / $maxResult);
        } else {
            $noPages = 1;
        }

        return $this->render('tdCMBundle:SetUp:propertiesUnits.html.twig', array(
            'properties' => $properties,
            'maxResult' => $maxResult,
            'firstResult' => $firstResult,
//            'debug' => $request->getMethod(),
            'debug' => '',
            'sFront' => $sFront,
            'sPropertyName' => $sPropertyName,
            'sMjesto' => $sMjesto,
            'sSkrbnik' => $sSkrbnik,
            'sVlasnik' => $sVlasnik,
            'noPages' => $noPages,
            'totalCount' => $totalCount,
            'page' => $page,
            'objectExistBookingCom' => false,
            'unitExistBookingCom' => false,
            'propertiesUnitsViewSetup' => $propertiesUnitsViewSetup,
        ));

//        return JsonResponse::create($properties);

//        return $this->render('tdCMBundle:SetUp:indexSp.html.twig');
    }

    /**
     * Data for jqGrid - List all setupProperties for all Portals
     *
     * @param Request $request
     * @return mixed
     */
    public function setupPropertiesDataAction(Request $request)
    {
        $pager = $this->get('datatheke.pager')->createPager('tdCMBundle:VSetupPropertyAll');

        $datagrid = $this->get('datatheke.datagrid')->createHttpDataGrid($pager, array(), 'jqgrid');

        return $datagrid->handleRequest($request);
    }

    /*
     *  SNIPPETS *********************************
     * */

    /**
     * Modal for preparing the parameters for LOS sending
     *
     * @param SetupPropertyUnit $setupPropertyUnit
     * @return Response
     */
    public function losSendModalAction(SetupPropertyUnit $setupPropertyUnit)
    {
        $loginObrada = $this->get('td_cm.obrada.cookie_obrada');
        if ($loginObrada->obradaCookie()) {
        } else {
            return new Response('Nemate prava');
        }
        //Sigurnost
        return $this->render('tdCMBundle:SetUp/Snippets:losModal.html.twig', array('setupPropertyUnit' => $setupPropertyUnit));
    }

    public function objectConnectModalAction()
    {
        return $this->render('tdCMBundle:SetUp/Modals:objectConnect.html.twig');
    }

    public function addPropertyToChannelManagerAction(Request $request)
    {

        $idProperty = $request->request->get('idProperty');
        $idPortalKorisnik = $request->request->get('idPortalKorisnik');

        if ($idProperty && $idPortalKorisnik) {
            $em = $this->getDoctrine()->getManager();

            $portalKorisnik = $em->getRepository('tdCMBundle:PortalKorisnik')->find($idPortalKorisnik);
            $property = $em->getRepository('tdCMBundle:Property')->find($idProperty);

            $setupPropertyPostoji = $em->getRepository('tdCMBundle:SetupProperty')->findOneBy(array(
                'property' => $property,
                'portalKorisnik' => $portalKorisnik
            ));
            if (is_null($setupPropertyPostoji)) {
                $this->prijaviPropertyUChannelManager($portalKorisnik, $property);
                $this->addFlash('notice', 'U Channel Manager je dodan objekt [' . $property->getId() . '] ' . $property->getName());
            } else {
                $this->addFlash('danger', 'Objekt je vec prijavljen!');
            }

        } else {
            $this->addFlash('danger', 'Zadani parametri ne odgovaraju!');
        }

        return $this->redirectToRoute('spu_all');

    }

    /**
     * Snippet for modal
     *
     * @param Request $request
     * @return Response
     */
    public function objectConnectSnippetAction(Request $request)
    {
        // Sigurnost

        // Parameters
        $setupPropertyId = $request->request->get('setupPropertyId');
        if ($setupPropertyId) {

            $em = $this->getDoctrine()->getManager();

            $setupProperty = $em->getRepository('tdCMBundle:SetupProperty')->find($setupPropertyId);

            if ($setupProperty) {
                return $this->render('@tdCM/SetUp/Snippets/spObjectConnect.html.twig', array(
                    'setupProperty' => $setupProperty,
                ));
            } else {
                return new Response('Objekt ne postoji u Channel Manageru');
            }
        } else {
            return new Response('Id objekta nije točan!');
        }
    }

    /**
     * Snippet for modal
     *
     * @param Request $request
     * @return Response
     */
    public function unitConnectBookingComSnippetAction(Request $request)
    {
        // Sigurnost
        $cp = $this->get('td_cm.obrada.cookie_obrada')->getCookieProcess();
        // Parameters
        $setupPropertyUnitId = $request->request->get('setupPropertyUnitId');
        if ($setupPropertyUnitId) {

            $em = $this->getDoctrine()->getManager();

            $setupPropertyUnit = $em->getRepository('tdCMBundle:SetupPropertyUnit')->find($setupPropertyUnitId);

            if ($setupPropertyUnit) {
                $availableItems = $this->get('td_cm.obrada.setup_obrada')->getAvailableItemsBookingCom($setupPropertyUnit);
                return $this->render('@tdCM/SetupPropertyUnit/Snippets/spuConnectBookingCom.html.twig', array(
                    'setupPropertyUnit' => $setupPropertyUnit,
                    'availableItems' => $availableItems,
//                    'cp' => $this->get('td_cm.obrada.setup_obrada')->getTest(),
                    'cp' => $this->get('td_cm.obrada.cookie_obrada')->getCookieProcess(),
//                    'cp' => $this->container->getParameter('cookie_process'),
                    'portalCode' => $setupPropertyUnit->getPortalKorisnik()->getPortal()->getCodeName(),
                ));
            } else {
                return new Response('Objekt ne postoji u Channel Manageru');
            }
        } else {
            return new Response('Id objekta nije točan!');
        }
    }

    public function unitConnectGeneralSnippetAction(Request $request)
    {
        // Sigurnost
        $cp = $this->get('td_cm.obrada.cookie_obrada')->getCookieProcess();
        // Parameters
        $setupPropertyUnitId = $request->request->get('setupPropertyUnitId');
        if ($setupPropertyUnitId) {

            $em = $this->getDoctrine()->getManager();

            $setupPropertyUnit = $em->getRepository('tdCMBundle:SetupPropertyUnit')->find($setupPropertyUnitId);

            if ($setupPropertyUnit) {
//                $availableItems = $this->get('td_cm.obrada.setup_obrada')->getAvailableItemsBookingCom($setupPropertyUnit);
                return $this->render('@tdCM/SetupPropertyUnit/Snippets/spuConnectGeneral.html.twig', array(
                    'setupPropertyUnit' => $setupPropertyUnit,
//                    'availableItems' => $availableItems,
//                    'cp' => $this->get('td_cm.obrada.setup_obrada')->getTest(),
                    'cp' => $this->get('td_cm.obrada.cookie_obrada')->getCookieProcess(),
//                    'cp' => $this->container->getParameter('cookie_process'),
                    'portalCode' => $setupPropertyUnit->getPortalKorisnik()->getPortal()->getCodeName(),
                ));
            } else {
                return new Response('Objekt ne postoji u Channel Manageru');
            }
        } else {
            return new Response('Id objekta nije točan!');
        }
    }

    /**
     * Snippet for object for Booking.com
     *
     * @param Request $request
     * @return Response
     */
    public function spBookingComSnippetAction(Request $request)
    {
        // Sigurnost

        // provjera ispravnosti parametara
        if ($request->request->has('propertyId')) {
            $em = $this->getDoctrine()->getManager();
            $property = $em->getRepository('tdCMBundle:Property')->find($request->request->get('propertyId'));
            $portal = $em->getRepository('tdCMBundle:Portal')->findOneByCodeName($request->request->get('portalCode'));
            if (!is_null($property) and !is_null($portal)) {
                $portalKorisnik = $em->getRepository('tdCMBundle:PortalKorisnik')->findOneBy(array(
                    'portal' => $portal,
                    'idCompanyKorisnik' => $property->getIdAgent(),
                ));

                if (!is_null($portalKorisnik)) {
                    $setupProperty = $em->getRepository('tdCMBundle:SetupProperty')->findOneBy(array(
                        'property' => $property,
                        'portalKorisnik' => $portalKorisnik,
                    ));

                    // Check if setupPropertz exists, and try to
                    if(!is_null($setupProperty)) {
                        $povezan = false;
                        foreach ($setupProperty->getSetupPropertyUnits() as $setupPropertyUnit) {
                            if ($setupPropertyUnit->getPovezan()) $povezan = true;
                        }
                        if (true == $povezan && !$setupProperty->getPovezan()) {
                            $setupProperty->setPovezan(true);
                            $em->persist($setupProperty);
                            $em->flush();
                        } elseif (!$povezan && $setupProperty->getPovezan()) {
                            $setupProperty->setPovezan(true);
                            $em->persist($setupProperty);
                            $em->flush();
                        }
                    }

//                    return $this->render('@tdCM/SetUp/Snippets/spBookingCom.html.twig', array(
                    return $this->render('@tdCM/SetUp/Snippets/sp' . $portal->getCodeName() . '.html.twig', array(
                        'setupProperty' => $setupProperty,
                        'property' => $property,
                        'portal' => $portal,
                    ));
                } else {
                    return $this->render('@tdCM/SetUp/Snippets/connectPortal.html.twig', array(
                        'property' => $property,
                        'portal' => $portal->getId(),
                    ));
                }
            } else {
                return new Response('Objekti property ne postoji u bazi!');
            }
        } else {
            return new Response('Krivi parametar!');
        }
    }

    /**
     * Snippet for unit for Booking.com
     *
     *
     * @param Request $request
     * @return Response
     */
    public function spuBookingComSnippetAction(Request $request)
    {
        // Sigurnost

        // provjera ispravnosti parametara
        if ($request->request->has('propertyUnitId')) {
            $em = $this->getDoctrine()->getManager();
            $propertyUnit = $em->getRepository('tdCMBundle:PropertyUnit')->find($request->request->get('propertyUnitId'));
            $portal = $em->getRepository('tdCMBundle:Portal')->findOneByCodeName($request->request->get('portalCode'));

            if (!is_null($propertyUnit) and !is_null($portal)) {
                $portalKorisnik = $em->getRepository('tdCMBundle:PortalKorisnik')->findOneBy(array(
                    'portal' => $portal,
                    'idCompanyKorisnik' => $propertyUnit->getProperty()->getIdAgent(),
                ));

                if (!is_null($portalKorisnik)) {
                    $setupPropertyUnit = $em->getRepository('tdCMBundle:SetupPropertyUnit')->findOneBy(array(
                        'propertyUnit' => $propertyUnit,
                        'portalKorisnik' => $portalKorisnik,
                    ));
                    if (!is_null($setupPropertyUnit)) {
                        $this->get('td_cm.obrada.setup_obrada')->calculatePovezanSetupPropertyUnit($setupPropertyUnit);
                    }
                    return $this->render('@tdCM/SetupPropertyUnit/Snippets/spu' . $portal->getCodeName() . '.html.twig', array(
//                    return $this->render('@tdCM/SetupPropertyUnit/Snippets/spuBookingCom.html.twig', array(
                        'setupPropertyUnit' => $setupPropertyUnit,
                        'propertyUnit' => $propertyUnit,
                        'portal' => $portal,
                    ));
                } else {
                    return $this->render('@tdCM/SetupPropertyUnit/Snippets/connectUnit.html.twig', array(
                        'propertyUnit' => $propertyUnit,
                        'portalId' => $portal->getId(),
                        'portalCode' => $portal->getCodeName(),
                        'test' => 'test',
                    ));
                }
            } else {
                return new Response('Objekti propertyUnit ne postoji u bazi!');
            }
        } else {
            return new Response('Krivi parametar!');
        }
    }

    /**
     * Create setupProperty and return snippet (forward)
     *
     * @param Request $request
     * @return Response
     */
    public function createSetupPropertySnippetAction(Request $request)
    {
        // Sigurnost

        // provjera ispravnosti parametara
        $propertyId = $request->request->get('propertyId');
        $portalId = $request->request->get('portalId');
        $em = $this->getDoctrine()->getManager();

        $property = $em->getRepository('tdCMBundle:Property')->find($propertyId);
        $portal = $em->getRepository('tdCMBundle:Portal')->find($portalId);

        $portalKorisnik = $em->getRepository('tdCMBundle:PortalKorisnik')->findOneBy(array(
            'portal' => $portal,
            'idCompanyKorisnik' => $property->getIdAgent(),
        ));

        $setupProperty = $this->get('td_cm.obrada.setup_obrada')->createSetupProperty($property, $portalKorisnik);
        $request->request->set('setupPropertyId', $setupProperty->getId());
        return $this->forward('tdCMBundle:SetUp:spBookingComSnippet');
    }

    public function createSetupPropertyUnitSnippetAction(Request $request)
    {
        // Sigurnost
        if (!$this->get('td_cm.obrada.cookie_obrada')->obradaCookie()) {
            return new Response('Prijavite se ponovo!');
        }

        // provjera ispravnosti parametara
        if ($request->request->has('propertyUnitId')) {
            $propertyUnitId = $request->request->get('propertyUnitId');
            $portalId = $request->request->get('portalId');
            $em = $this->getDoctrine()->getManager();

            $propertyUnit = $em->getRepository('tdCMBundle:PropertyUnit')->find($propertyUnitId);
            $portal = $em->getRepository('tdCMBundle:Portal')->find($portalId);

            $portalKorisnik = $em->getRepository('tdCMBundle:PortalKorisnik')->findOneBy(array(
                'portal' => $portal,
                'idCompanyKorisnik' => $propertyUnit->getProperty()->getIdAgent(),
            ));

            $setupPropertyUnit = $this->get('td_cm.obrada.setup_obrada')->createSetupPropertyUnit($propertyUnit, $portalKorisnik);
            $request->request->set('setupPropertyUnitId', $setupPropertyUnit->getId());
            $request->request->set('portalCode', $portal->getCodeName());
            return $this->forward('tdCMBundle:SetUp:spuBookingComSnippet');
        } else {
            return new Response('Krivi parametar!');
        }
    }

    /**
     * Prijava jednog objekta i njegovih unita u Channel Manager
     *
     * @param PortalKorisnik $portalKorisnik
     * @param $property
     */
    private function prijaviPropertyUChannelManager(PortalKorisnik $portalKorisnik, Property $property)
    {
        $em = $this->getDoctrine()->getManager();
        $setupProperty = new SetupProperty();
        $setupProperty->setAktivan(false);
        $setupProperty->setDatumKreiranja(new \DateTime());
        $setupProperty->setIdFront($property->getIdCountry() . '-' . str_pad($property->getId(), 5, '0', STR_PAD_LEFT));
        $setupProperty->setProperty($property);
        $setupProperty->setIdSkrbnik($property->getAgentC()->getId());
        $setupProperty->setWebshop($portalKorisnik->getWebshop());
        $setupProperty->setPortalKorisnik($portalKorisnik);

        $em->persist($setupProperty);
        $this->logSetupProperty($portalKorisnik, $setupProperty, 'Kreirano');

        $webshopPropertyUnits = $em->getRepository('tdCMBundle:WebshopPropertyUnit')->findBy(
            array(
                'webshop' => $portalKorisnik->getWebshop(),
                'property' => $property
            )
        );

        /* @var $webshopPropertyUnit WebshopPropertyUnit */
        foreach ($webshopPropertyUnits as $webshopPropertyUnit) {
            $setupPropertyUnit = new SetupPropertyUnit();
            $setupPropertyUnit->setDatumKreiranja(new \DateTime());
            $setupPropertyUnit->setAktivan(false);
            $setupPropertyUnit->setPropertyUnit($webshopPropertyUnit->getPropertyUnit());
            $setupPropertyUnit->setIdPropertyUnitFront($setupProperty->getIdFront() . '-' . str_pad($webshopPropertyUnit->getPropertyUnit()->getLocalOrder(), 2, '0', STR_PAD_LEFT));
            $setupPropertyUnit->setPortalKorisnik($portalKorisnik);
            $setupPropertyUnit->setWebshop($portalKorisnik->getWebshop());
            $setupPropertyUnit->setIdSkrbnik(0);
            $setupPropertyUnit->setSetupProperty($setupProperty);
            $em->persist($setupPropertyUnit);
            $setupProperty->addSetupPropertyUnit($setupPropertyUnit);
            $this->logSetupPropertyUnit($portalKorisnik, $setupPropertyUnit, $setupProperty, 'Kreirano');
        }
        $em->flush();
    }

    /**
     * @param PortalKorisnik $portalKorisnik
     * @param $setupProperty
     * @param $akcija
     */
    private function logSetupProperty(PortalKorisnik $portalKorisnik, SetupProperty $setupProperty, $akcija)
    {
        $em = $this->getDoctrine()->getManager();
        $setupPropertyLog = new SetupPropertyLog();
        $setupPropertyLog->setAktivan($setupProperty->getAktivan());
        $setupPropertyLog->setDatumVrijemePromjene(new \DateTime());
        $setupPropertyLog->setPortalKorisnik($portalKorisnik);
        $setupPropertyLog->setProperty($setupProperty->getProperty());
        $setupPropertyLog->setWebshop($setupProperty->getWebshop());
        $setupPropertyLog->setIdSkrbnik($setupProperty->getIdSkrbnik());
        $setupPropertyLog->setAkcija($akcija);
        $setupPropertyLog->setSetupProperty($setupProperty);
        $em->persist($setupPropertyLog);
        $em->flush();
    }

    /**
     * @param PortalKorisnik $portalKorisnik
     * @param $setupPropertyUnit
     * @param $setupProperty
     * @param $akcija
     */
    private function logSetupPropertyUnit(PortalKorisnik $portalKorisnik, SetupPropertyUnit $setupPropertyUnit, SetupProperty $setupProperty, $akcija)
    {
        $em = $this->getDoctrine()->getManager();
        $setupPropertyUnitLog = new SetupPropertyUnitLog();
        $setupPropertyUnitLog->setAktivan($setupPropertyUnit->getAktivan());
        $setupPropertyUnitLog->setDatumVrijemePromjene(new \DateTime());
        $setupPropertyUnitLog->setPortalKorisnik($portalKorisnik);
        $setupPropertyUnitLog->setPropertyUnit($setupPropertyUnit->getPropertyUnit());
        $setupPropertyUnitLog->setSetupProperty($setupProperty);
        $setupPropertyUnitLog->setWebshop($portalKorisnik->getWebshop());
        $setupPropertyUnitLog->setIdSkrbnik($setupProperty->getIdSkrbnik());
        $setupPropertyUnitLog->setAkcija($akcija);
        $setupPropertyUnitLog->setSetupPropertyUnit($setupPropertyUnit);
        $em->persist($setupPropertyUnitLog);
        $em->flush();
    }


    /**
     * @param $setupProperties
     * @param $properties
     * @param PortalKorisnik $portalKorisnik
     */
    private function registerPropertiesInCM($setupProperties, $properties, PortalKorisnik $portalKorisnik)
    {

        $postoji = false;
        /* Property $property */
        foreach ($properties as $property) {
            /* SetupProperty $setupProperty */
            foreach ($setupProperties as $setupProperty) {
                if ($property == $setupProperty->getProperty()) {
                    $postoji = true;
                    break;
                }
            }
            if (!$postoji) {
                $this->prijaviPropertyUChannelManager($portalKorisnik, $property);
            } else {
                $postoji = false;
            }
        }
        return;
    }


    /**
     * @return array
     * @internal param $em
     */
    private function getPropertiesUnitsViewSetup()
    {
        $em = $this->getDoctrine()->getManager();
        $propertiesUnitViewSetup = $em->getRepository('tdCMBundle:PropertiesUnitsViewSetup')->findOneBy(array('username' => $this->get('td_cm.obrada.cookie_obrada')->getUsername()));
        //var_dump($propertiesUnitViewSetup);
        if (is_null($propertiesUnitViewSetup)) { // If username doesn't have setup already, then create new
            $propertiesUnitViewSetup = $this->initializePropertiesUnitViewSetup();
        }

        $polje = array(
            'p1' => $em->find('tdCMBundle:Portal', $propertiesUnitViewSetup->getIdP1()),
            'p2' => $em->find('tdCMBundle:Portal', $propertiesUnitViewSetup->getIdP2()),
            'p3' => $em->find('tdCMBundle:Portal', $propertiesUnitViewSetup->getIdP3()),
            'p4' => $em->find('tdCMBundle:Portal', $propertiesUnitViewSetup->getIdP4()),
            'p5' => $em->find('tdCMBundle:Portal', $propertiesUnitViewSetup->getIdP5()),
            'p6' => $em->find('tdCMBundle:Portal', $propertiesUnitViewSetup->getIdP6()),
            'id' => $propertiesUnitViewSetup->getId(),
        );

        return $polje;
    }


    /**
     * @return PropertiesUnitsViewSetup
     */
    private function initializePropertiesUnitViewSetup()
    {
        $em = $this->getDoctrine()->getManager();

        //try to copy one existing
        $propertiesUnitViewSetupExisting = $em->getRepository('tdCMBundle:PropertiesUnitsViewSetup')->findOneBy(array());
        if (!is_null($propertiesUnitViewSetupExisting)) {
            $propertiesUnitViewSetup = clone $propertiesUnitViewSetupExisting;
            $propertiesUnitViewSetup->setUsername($this->get('td_cm.obrada.cookie_obrada')->getUsername());
        } else {
            $portalsWithCode = $em->getRepository('tdCMBundle:Portal')->findBy(
                array(),
                array('id' => 'ASC'),
                6,
                0
            );

            //dump($portalsWithCode);
            $propertiesUnitViewSetup = new PropertiesUnitsViewSetup();
            $propertiesUnitViewSetup->setUsername($this->get('td_cm.obrada.cookie_obrada')->getUsername());
            $propertiesUnitViewSetup->setIdP1($portalsWithCode[0]->getId());
            $propertiesUnitViewSetup->setIdP2($portalsWithCode[1]->getId());
            $propertiesUnitViewSetup->setIdP3($portalsWithCode[2]->getId());
            $propertiesUnitViewSetup->setIdP4($portalsWithCode[3]->getId());
            $propertiesUnitViewSetup->setIdP5($portalsWithCode[4]->getId());
            $propertiesUnitViewSetup->setIdP6($portalsWithCode[5]->getId());
        }

        // Save to the database
        $em->persist($propertiesUnitViewSetup);
        $em->flush();

        return $propertiesUnitViewSetup;
    }

}
