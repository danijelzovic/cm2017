<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\MaxDepth;


/**
 * PropertyUnit
 *
 * @ORM\Table(name="property_unit", indexes={@ORM\Index(name="id_property", columns={"id_property"}), @ORM\Index(name="active", columns={"active"}), @ORM\Index(name="id_unit_type", columns={"id_unit_type"}), @ORM\Index(name="deleted", columns={"deleted"}), @ORM\Index(name="id_company", columns={"id_company"}), @ORM\Index(name="char_view", columns={"char_view"}), @ORM\Index(name="beds_basic", columns={"beds_basic"}), @ORM\Index(name="beds_additional", columns={"beds_additional"}), @ORM\Index(name="beds_basic_2", columns={"beds_basic", "beds_additional"}), @ORM\Index(name="char_general", columns={"char_general"}), @ORM\Index(name="id_HL", columns={"id_HL"})})
 * @ORM\Entity(repositoryClass="td\CMBundle\Entity\Repository\PropertyUnitRepository")
 */
class PropertyUnit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="id_amadeus", type="string", length=16, nullable=false)
     */
    private $idAmadeus = '';

    /**
     * @var string
     *
     * @ORM\Column(name="id_atd", type="string", length=12, nullable=false)
     */
    private $idAtd = '';

    /**
     * @var string
     *
     * @ORM\Column(name="tiplok1", type="string", length=32, nullable=false)
     */
    private $tiplok1;

    /**
     * @var string
     *
     * @ORM\Column(name="tiplok2", type="string", length=32, nullable=false)
     */
    private $tiplok2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_atd_unit", type="integer", nullable=false)
     */
    private $idAtdUnit = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="id_property", type="integer", nullable=false)
     */
    private $idProperty = '0';

    /**
     * @ORM\ManyToOne(targetEntity="Property", inversedBy="propertyUnits")
     * @ORM\JoinColumn(name="id_property", referencedColumnName="id")
     */
    private $property;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_unit_type", type="integer", nullable=false)
     */
    private $idUnitType = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="local_order", type="integer", nullable=false)
     */
    private $localOrder = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="name2", type="string", length=255, nullable=false)
     */
    private $name2;

    /**
     * @var boolean
     *
     * @ORM\Column(name="category", type="boolean", nullable=false)
     */
    private $category = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="text", nullable=false)
     */
    private $note;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="smallint", nullable=false)
     */
    private $quantity = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="area", type="decimal", precision=6, scale=1, nullable=true)
     */
    private $area;

    /**
     * @var boolean
     *
     * @ORM\Column(name="floor", type="boolean", nullable=false)
     */
    private $floor;

    /**
     * @var string
     *
     * @ORM\Column(name="char_view", type="string", nullable=false)
     */
    private $charView = '';

    /**
     * @var string
     *
     * @ORM\Column(name="char_general", type="string", nullable=false)
     */
    private $charGeneral;

    /**
     * @var string
     *
     * @ORM\Column(name="bed_linen", type="string", nullable=false)
     */
    private $bedLinen = 'included';

    /**
     * @var string
     *
     * @ORM\Column(name="towels", type="string", nullable=false)
     */
    private $towels = 'included';

    /**
     * @var string
     *
     * @ORM\Column(name="dish_towels", type="string", nullable=false)
     */
    private $dishTowels = 'included';

    /**
     * @var boolean
     *
     * @ORM\Column(name="beds_basic", type="integer", nullable=false)
     */
    private $bedsBasic = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="beds_additional", type="integer", nullable=false)
     */
    private $bedsAdditional = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="broj_kreveta", type="integer", nullable=false)
     */
    private $brojKreveta = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="broj_pom_kreveta", type="integer", nullable=false)
     */
    private $brojPomKreveta = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="max_odraslih", type="integer", nullable=false)
     */
    private $maxOdraslih = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="min_ljudi", type="integer", nullable=false)
     */
    private $minLjudi = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="min_boravak", type="integer", nullable=false)
     */
    private $minBoravak = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="online", type="boolean", nullable=false)
     */
    private $online;

    /**
     * @var boolean
     *
     * @ORM\Column(name="inquiry", type="boolean", nullable=false)
     */
    private $inquiry;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false)
     */
    private $deleted = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="kreveti_osnovni", type="boolean", nullable=false)
     */
    private $krevetiOsnovni;

    /**
     * @var boolean
     *
     * @ORM\Column(name="kreveti_pomocni", type="boolean", nullable=false)
     */
    private $krevetiPomocni;

    /**
     * @var boolean
     *
     * @ORM\Column(name="osnovni_lazi_inspekciji", type="boolean", nullable=true)
     */
    private $osnovniLaziInspekciji;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pomocni_lazi_inspekciji", type="boolean", nullable=true)
     */
    private $pomocniLaziInspekciji;

    /**
     * @var string
     *
     * @ORM\Column(name="destinacija", type="string", length=255, nullable=false)
     */
    private $destinacija;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_company", type="integer", nullable=false)
     */
    private $idCompany;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_user", type="integer", nullable=false)
     */
    private $idUser;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="azurirano", type="datetime", nullable=false)
     */
    private $azurirano;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_HL", type="integer", nullable=false)
     */
    private $idHl;

    /**
     * @var string
     *
     * @ORM\Column(name="HL_napomena", type="string", length=32, nullable=false)
     */
    private $hlNapomena;


    /**
     * @ORM\OneToMany(targetEntity="SetupPropertyUnit", mappedBy="propertyUnit")
     * @MaxDepth(1)
     */
    private $setupPropertyUnits;

    /**
     * @return string
     */
    public function getPropertyUnitFront()
    {
        return $this->property->getPropertyFront() . '-' . str_pad($this->localOrder, 2, '0', STR_PAD_LEFT);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idAmadeus
     *
     * @param string $idAmadeus
     *
     * @return PropertyUnit
     */
    public function setIdAmadeus($idAmadeus)
    {
        $this->idAmadeus = $idAmadeus;

        return $this;
    }

    /**
     * Get idAmadeus
     *
     * @return string
     */
    public function getIdAmadeus()
    {
        return $this->idAmadeus;
    }

    /**
     * Set idAtd
     *
     * @param string $idAtd
     *
     * @return PropertyUnit
     */
    public function setIdAtd($idAtd)
    {
        $this->idAtd = $idAtd;

        return $this;
    }

    /**
     * Get idAtd
     *
     * @return string
     */
    public function getIdAtd()
    {
        return $this->idAtd;
    }

    /**
     * Set tiplok1
     *
     * @param string $tiplok1
     *
     * @return PropertyUnit
     */
    public function setTiplok1($tiplok1)
    {
        $this->tiplok1 = $tiplok1;

        return $this;
    }

    /**
     * Get tiplok1
     *
     * @return string
     */
    public function getTiplok1()
    {
        return $this->tiplok1;
    }

    /**
     * Set tiplok2
     *
     * @param string $tiplok2
     *
     * @return PropertyUnit
     */
    public function setTiplok2($tiplok2)
    {
        $this->tiplok2 = $tiplok2;

        return $this;
    }

    /**
     * Get tiplok2
     *
     * @return string
     */
    public function getTiplok2()
    {
        return $this->tiplok2;
    }

    /**
     * Set idAtdUnit
     *
     * @param integer $idAtdUnit
     *
     * @return PropertyUnit
     */
    public function setIdAtdUnit($idAtdUnit)
    {
        $this->idAtdUnit = $idAtdUnit;

        return $this;
    }

    /**
     * Get idAtdUnit
     *
     * @return integer
     */
    public function getIdAtdUnit()
    {
        return $this->idAtdUnit;
    }

    /**
     * Set idProperty
     *
     * @param integer $idProperty
     *
     * @return PropertyUnit
     */
    public function setIdProperty($idProperty)
    {
        $this->idProperty = $idProperty;

        return $this;
    }

    /**
     * Get idProperty
     *
     * @return integer
     */
    public function getIdProperty()
    {
        return $this->idProperty;
    }

    /**
     * Set idUnitType
     *
     * @param integer $idUnitType
     *
     * @return PropertyUnit
     */
    public function setIdUnitType($idUnitType)
    {
        $this->idUnitType = $idUnitType;

        return $this;
    }

    /**
     * Get idUnitType
     *
     * @return integer
     */
    public function getIdUnitType()
    {
        return $this->idUnitType;
    }

    /**
     * Set localOrder
     *
     * @param integer $localOrder
     *
     * @return PropertyUnit
     */
    public function setLocalOrder($localOrder)
    {
        $this->localOrder = $localOrder;

        return $this;
    }

    /**
     * Get localOrder
     *
     * @return integer
     */
    public function getLocalOrder()
    {
        return $this->localOrder;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PropertyUnit
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name2
     *
     * @param string $name2
     *
     * @return PropertyUnit
     */
    public function setName2($name2)
    {
        $this->name2 = $name2;

        return $this;
    }

    /**
     * Get name2
     *
     * @return string
     */
    public function getName2()
    {
        return $this->name2;
    }

    /**
     * Set category
     *
     * @param boolean $category
     *
     * @return PropertyUnit
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return boolean
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return PropertyUnit
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return PropertyUnit
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set area
     *
     * @param string $area
     *
     * @return PropertyUnit
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return string
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set floor
     *
     * @param boolean $floor
     *
     * @return PropertyUnit
     */
    public function setFloor($floor)
    {
        $this->floor = $floor;

        return $this;
    }

    /**
     * Get floor
     *
     * @return boolean
     */
    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * Set charView
     *
     * @param string $charView
     *
     * @return PropertyUnit
     */
    public function setCharView($charView)
    {
        $this->charView = $charView;

        return $this;
    }

    /**
     * Get charView
     *
     * @return string
     */
    public function getCharView()
    {
        return $this->charView;
    }

    /**
     * Set charGeneral
     *
     * @param string $charGeneral
     *
     * @return PropertyUnit
     */
    public function setCharGeneral($charGeneral)
    {
        $this->charGeneral = $charGeneral;

        return $this;
    }

    /**
     * Get charGeneral
     *
     * @return string
     */
    public function getCharGeneral()
    {
        return $this->charGeneral;
    }

    /**
     * Set bedLinen
     *
     * @param string $bedLinen
     *
     * @return PropertyUnit
     */
    public function setBedLinen($bedLinen)
    {
        $this->bedLinen = $bedLinen;

        return $this;
    }

    /**
     * Get bedLinen
     *
     * @return string
     */
    public function getBedLinen()
    {
        return $this->bedLinen;
    }

    /**
     * Set towels
     *
     * @param string $towels
     *
     * @return PropertyUnit
     */
    public function setTowels($towels)
    {
        $this->towels = $towels;

        return $this;
    }

    /**
     * Get towels
     *
     * @return string
     */
    public function getTowels()
    {
        return $this->towels;
    }

    /**
     * Set dishTowels
     *
     * @param string $dishTowels
     *
     * @return PropertyUnit
     */
    public function setDishTowels($dishTowels)
    {
        $this->dishTowels = $dishTowels;

        return $this;
    }

    /**
     * Get dishTowels
     *
     * @return string
     */
    public function getDishTowels()
    {
        return $this->dishTowels;
    }

    /**
     * Set bedsBasic
     *
     * @param boolean $bedsBasic
     *
     * @return PropertyUnit
     */
    public function setBedsBasic($bedsBasic)
    {
        $this->bedsBasic = $bedsBasic;

        return $this;
    }

    /**
     * Get bedsBasic
     *
     * @return boolean
     */
    public function getBedsBasic()
    {
        return $this->bedsBasic;
    }

    /**
     * Set bedsAdditional
     *
     * @param boolean $bedsAdditional
     *
     * @return PropertyUnit
     */
    public function setBedsAdditional($bedsAdditional)
    {
        $this->bedsAdditional = $bedsAdditional;

        return $this;
    }

    /**
     * Get bedsAdditional
     *
     * @return boolean
     */
    public function getBedsAdditional()
    {
        return $this->bedsAdditional;
    }

    /**
     * Set brojKreveta
     *
     * @param boolean $brojKreveta
     *
     * @return PropertyUnit
     */
    public function setBrojKreveta($brojKreveta)
    {
        $this->brojKreveta = $brojKreveta;

        return $this;
    }

    /**
     * Get brojKreveta
     *
     * @return boolean
     */
    public function getBrojKreveta()
    {
        return $this->brojKreveta;
    }

    /**
     * Set brojPomKreveta
     *
     * @param boolean $brojPomKreveta
     *
     * @return PropertyUnit
     */
    public function setBrojPomKreveta($brojPomKreveta)
    {
        $this->brojPomKreveta = $brojPomKreveta;

        return $this;
    }

    /**
     * Get brojPomKreveta
     *
     * @return boolean
     */
    public function getBrojPomKreveta()
    {
        return $this->brojPomKreveta;
    }

    /**
     * Set maxOdraslih
     *
     * @param boolean $maxOdraslih
     *
     * @return PropertyUnit
     */
    public function setMaxOdraslih($maxOdraslih)
    {
        $this->maxOdraslih = $maxOdraslih;

        return $this;
    }

    /**
     * Get maxOdraslih
     *
     * @return boolean
     */
    public function getMaxOdraslih()
    {
        return $this->maxOdraslih;
    }

    /**
     * Set minLjudi
     *
     * @param boolean $minLjudi
     *
     * @return PropertyUnit
     */
    public function setMinLjudi($minLjudi)
    {
        $this->minLjudi = $minLjudi;

        return $this;
    }

    /**
     * Get minLjudi
     *
     * @return boolean
     */
    public function getMinLjudi()
    {
        return $this->minLjudi;
    }

    /**
     * Set minBoravak
     *
     * @param boolean $minBoravak
     *
     * @return PropertyUnit
     */
    public function setMinBoravak($minBoravak)
    {
        $this->minBoravak = $minBoravak;

        return $this;
    }

    /**
     * Get minBoravak
     *
     * @return boolean
     */
    public function getMinBoravak()
    {
        return $this->minBoravak;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return PropertyUnit
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set online
     *
     * @param boolean $online
     *
     * @return PropertyUnit
     */
    public function setOnline($online)
    {
        $this->online = $online;

        return $this;
    }

    /**
     * Get online
     *
     * @return boolean
     */
    public function getOnline()
    {
        return $this->online;
    }

    /**
     * Set inquiry
     *
     * @param boolean $inquiry
     *
     * @return PropertyUnit
     */
    public function setInquiry($inquiry)
    {
        $this->inquiry = $inquiry;

        return $this;
    }

    /**
     * Get inquiry
     *
     * @return boolean
     */
    public function getInquiry()
    {
        return $this->inquiry;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return PropertyUnit
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set krevetiOsnovni
     *
     * @param boolean $krevetiOsnovni
     *
     * @return PropertyUnit
     */
    public function setKrevetiOsnovni($krevetiOsnovni)
    {
        $this->krevetiOsnovni = $krevetiOsnovni;

        return $this;
    }

    /**
     * Get krevetiOsnovni
     *
     * @return boolean
     */
    public function getKrevetiOsnovni()
    {
        return $this->krevetiOsnovni;
    }

    /**
     * Set krevetiPomocni
     *
     * @param boolean $krevetiPomocni
     *
     * @return PropertyUnit
     */
    public function setKrevetiPomocni($krevetiPomocni)
    {
        $this->krevetiPomocni = $krevetiPomocni;

        return $this;
    }

    /**
     * Get krevetiPomocni
     *
     * @return boolean
     */
    public function getKrevetiPomocni()
    {
        return $this->krevetiPomocni;
    }

    /**
     * Set osnovniLaziInspekciji
     *
     * @param boolean $osnovniLaziInspekciji
     *
     * @return PropertyUnit
     */
    public function setOsnovniLaziInspekciji($osnovniLaziInspekciji)
    {
        $this->osnovniLaziInspekciji = $osnovniLaziInspekciji;

        return $this;
    }

    /**
     * Get osnovniLaziInspekciji
     *
     * @return boolean
     */
    public function getOsnovniLaziInspekciji()
    {
        return $this->osnovniLaziInspekciji;
    }

    /**
     * Set pomocniLaziInspekciji
     *
     * @param boolean $pomocniLaziInspekciji
     *
     * @return PropertyUnit
     */
    public function setPomocniLaziInspekciji($pomocniLaziInspekciji)
    {
        $this->pomocniLaziInspekciji = $pomocniLaziInspekciji;

        return $this;
    }

    /**
     * Get pomocniLaziInspekciji
     *
     * @return boolean
     */
    public function getPomocniLaziInspekciji()
    {
        return $this->pomocniLaziInspekciji;
    }

    /**
     * Set destinacija
     *
     * @param string $destinacija
     *
     * @return PropertyUnit
     */
    public function setDestinacija($destinacija)
    {
        $this->destinacija = $destinacija;

        return $this;
    }

    /**
     * Get destinacija
     *
     * @return string
     */
    public function getDestinacija()
    {
        return $this->destinacija;
    }

    /**
     * Set idCompany
     *
     * @param integer $idCompany
     *
     * @return PropertyUnit
     */
    public function setIdCompany($idCompany)
    {
        $this->idCompany = $idCompany;

        return $this;
    }

    /**
     * Get idCompany
     *
     * @return integer
     */
    public function getIdCompany()
    {
        return $this->idCompany;
    }

    /**
     * Set idUser
     *
     * @param integer $idUser
     *
     * @return PropertyUnit
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return integer
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set azurirano
     *
     * @param \DateTime $azurirano
     *
     * @return PropertyUnit
     */
    public function setAzurirano($azurirano)
    {
        $this->azurirano = $azurirano;

        return $this;
    }

    /**
     * Get azurirano
     *
     * @return \DateTime
     */
    public function getAzurirano()
    {
        return $this->azurirano;
    }

    /**
     * Set idHl
     *
     * @param integer $idHl
     *
     * @return PropertyUnit
     */
    public function setIdHl($idHl)
    {
        $this->idHl = $idHl;

        return $this;
    }

    /**
     * Get idHl
     *
     * @return integer
     */
    public function getIdHl()
    {
        return $this->idHl;
    }

    /**
     * Set hlNapomena
     *
     * @param string $hlNapomena
     *
     * @return PropertyUnit
     */
    public function setHlNapomena($hlNapomena)
    {
        $this->hlNapomena = $hlNapomena;

        return $this;
    }

    /**
     * Get hlNapomena
     *
     * @return string
     */
    public function getHlNapomena()
    {
        return $this->hlNapomena;
    }

    /**
     * Set property
     *
     * @param \td\CMBundle\Entity\Property $property
     *
     * @return PropertyUnit
     */
    public function setProperty(\td\CMBundle\Entity\Property $property = null)
    {
        $this->property = $property;

        return $this;
    }

    /**
     * Get property
     *
     * @return \td\CMBundle\Entity\Property
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * @param mixed $propertyUnitFront
     */
    public function setPropertyUnitFront($propertyUnitFront)
    {
        $this->propertyUnitFront = $propertyUnitFront;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setupPropertyUnits = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add setupPropertyUnit
     *
     * @param \td\CMBundle\Entity\SetupPropertyUnit $setupPropertyUnit
     *
     * @return PropertyUnit
     */
    public function addSetupPropertyUnit(\td\CMBundle\Entity\SetupPropertyUnit $setupPropertyUnit)
    {
        $this->setupPropertyUnits[] = $setupPropertyUnit;

        return $this;
    }

    /**
     * Remove setupPropertyUnit
     *
     * @param \td\CMBundle\Entity\SetupPropertyUnit $setupPropertyUnit
     */
    public function removeSetupPropertyUnit(\td\CMBundle\Entity\SetupPropertyUnit $setupPropertyUnit)
    {
        $this->setupPropertyUnits->removeElement($setupPropertyUnit);
    }

    /**
     * Get setupPropertyUnits
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSetupPropertyUnits()
    {
        return $this->setupPropertyUnits;
    }
}
