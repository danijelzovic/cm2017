<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PropertyUnitItempricelistSetovi
 *
 * @ORM\Table(name="property_unit_itempricelist_setovi")
 * @ORM\Entity
 */
class PropertyUnitItempricelistSetovi
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_company", type="integer")
     */
    private $idCompany;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_head", type="integer")
     */
    private $idHead;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_itempricelist_set", type="integer")
     */
    private $idItempricelistSet;

    /**
     * @ORM\ManyToOne(targetEntity="ItempricelistSets")
     * @ORM\JoinColumn(name="id_itempricelist_set", referencedColumnName="id")
     **/
    private $itempricelistSet;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordering", type="integer")
     */
    private $ordering;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCompany
     *
     * @param integer $idCompany
     * @return PropertyUnitItempricelistSetovi
     */
    public function setIdCompany($idCompany)
    {
        $this->idCompany = $idCompany;

        return $this;
    }

    /**
     * Get idCompany
     *
     * @return integer 
     */
    public function getIdCompany()
    {
        return $this->idCompany;
    }

    /**
     * Set idHead
     *
     * @param integer $idHead
     * @return PropertyUnitItempricelistSetovi
     */
    public function setIdHead($idHead)
    {
        $this->idHead = $idHead;

        return $this;
    }

    /**
     * Get idHead
     *
     * @return integer 
     */
    public function getIdHead()
    {
        return $this->idHead;
    }

    /**
     * Set idItempricelistItem
     *
     * @param integer $idItempricelistItem
     * @return PropertyUnitItempricelistSetovi
     */
    public function setIdItempricelistItem($idItempricelistItem)
    {
        $this->idItempricelistItem = $idItempricelistItem;

        return $this;
    }

    /**
     * Get idItempricelistItem
     *
     * @return integer 
     */
    public function getIdItempricelistItem()
    {
        return $this->idItempricelistItem;
    }

    /**
     * Set ordering
     *
     * @param integer $ordering
     * @return PropertyUnitItempricelistSetovi
     */
    public function setOrdering($ordering)
    {
        $this->ordering = $ordering;

        return $this;
    }

    /**
     * Get ordering
     *
     * @return integer 
     */
    public function getOrdering()
    {
        return $this->ordering;
    }

    /**
     * Set idItempricelistSet
     *
     * @param integer $idItempricelistSet
     * @return PropertyUnitItempricelistSetovi
     */
    public function setIdItempricelistSet($idItempricelistSet)
    {
        $this->idItempricelistSet = $idItempricelistSet;

        return $this;
    }

    /**
     * Get idItempricelistSet
     *
     * @return integer 
     */
    public function getIdItempricelistSet()
    {
        return $this->idItempricelistSet;
    }

    /**
     * Set itempricelistSet
     *
     * @param \td\CMBundle\Entity\ItempricelistSets $itempricelistSet
     * @return PropertyUnitItempricelistSetovi
     */
    public function setItempricelistSet(\td\CMBundle\Entity\ItempricelistSets $itempricelistSet = null)
    {
        $this->itempricelistSet = $itempricelistSet;

        return $this;
    }

    /**
     * Get itempricelistSet
     *
     * @return \td\CMBundle\Entity\ItempricelistSets 
     */
    public function getItempricelistSet()
    {
        return $this->itempricelistSet;
    }
}
