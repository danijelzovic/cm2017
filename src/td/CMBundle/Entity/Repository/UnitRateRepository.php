<?php
/**
 * Created by PhpStorm.
 * User: danijel
 * Date: 13.10.2016.
 * Time: 13:49
 */

namespace td\CMBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;
use td\CMBundle\Entity\SetupPropertyUnit;

class UnitRateRepository extends EntityRepository
{
    /**
     *  Count setted unitRate records for given setupPropertyUnit
     *
     * @param SetupPropertyUnit $setupPropertyUnit
     * @return int
     */
    public function countSettedUnitRates(SetupPropertyUnit $setupPropertyUnit)
    {
        return (int)$this->createQueryBuilder('ur')
            ->select('COUNT(ur.id)')
            ->where('ur.setupPropertyUnit = :setupPropertyUnit')
            ->andWhere('ur.idCjenik > :nula')
            ->setParameter('setupPropertyUnit', $setupPropertyUnit)
            ->setParameter('nula', 0)
            ->getQuery()
            ->getSingleScalarResult();
    }

}