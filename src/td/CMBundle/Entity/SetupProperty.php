<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\MaxDepth;

/**
 * SetupProperty
 *
 * @ORM\Table(name="cm_setup_property")
 * @ORM\Entity(repositoryClass="td\CMBundle\Entity\Repository\SetupPropertyRepository")
 * @ExclusionPolicy("none")
 */
class SetupProperty
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

	 /**
     * @var integer
     *
     * @ORM\Column(name="idWebshop", type="integer", nullable=true)
     * @Exclude
     */
    private $idWebshop;

	/**
     * @ORM\ManyToOne(targetEntity="Webshop")
     * @ORM\JoinColumn(name="idWebshop", referencedColumnName="id", onDelete="CASCADE")
     * @Exclude
	 */
	private $webshop;
	
    /**
     * @var integer
     *
     * @ORM\Column(name="idProperty", type="integer")
     */
    private $idProperty;

    /**
     * @ORM\ManyToOne(targetEntity="Property", inversedBy="setupProperties")
     * @ORM\JoinColumn(name="idProperty", referencedColumnName="id")
     * @MaxDepth(2)
     **/
    private $property;
	
	  /**
     * @var string
     *
     * @ORM\Column(name="idPropertyNaPortalu", type="string", length=255, nullable=true)
     */
    private $idPropertyNaPortalu;

	 /**
     * @var boolean
     *
     * @ORM\Column(name="aktivan", type="boolean")
     */
    private $aktivan = false;

	/**
     * @ORM\ManyToOne(targetEntity="PortalKorisnik", inversedBy="setupProperties")
     * @ORM\JoinColumn(name="ID_portal_korisnik", referencedColumnName="id", onDelete="CASCADE")
	 * @Exclude
     */
    private $portalKorisnik;

    /**
     * @var string
     *
     * @ORM\Column(name="idSkrbnik", type="string", length=255, nullable=true)
     */
    private $idSkrbnik;

    /**
     * @ORM\OneToMany(targetEntity="SetupPropertyUnit", mappedBy="setupProperty")
     * @MaxDepth(1)
     */
    private $setupPropertyUnits;

    /**
     * @var string
     *
     * @ORM\Column(name="idFront", type="string", length=255)
     */
    private $idFront;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datumKreiranja", type="datetime", options={"default":"0000-00-00 00:00:00"})
     */
    private $datumKreiranja;

    /**
     * @var boolean
     *
     * @ORM\Column(name="povezan", type="boolean")
     */
    private $povezan = false;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setupPropertyUnits = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idWebshop
     *
     * @param integer $idWebshop
     *
     * @return SetupProperty
     */
    public function setIdWebshop($idWebshop)
    {
        $this->idWebshop = $idWebshop;

        return $this;
    }

    /**
     * Get idWebshop
     *
     * @return integer
     */
    public function getIdWebshop()
    {
        return $this->idWebshop;
    }

    /**
     * Set idProperty
     *
     * @param integer $idProperty
     *
     * @return SetupProperty
     */
    public function setIdProperty($idProperty)
    {
        $this->idProperty = $idProperty;

        return $this;
    }

    /**
     * Get idProperty
     *
     * @return integer
     */
    public function getIdProperty()
    {
        return $this->idProperty;
    }

    /**
     * Set idPropertyNaPortalu
     *
     * @param string $idPropertyNaPortalu
     *
     * @return SetupProperty
     */
    public function setIdPropertyNaPortalu($idPropertyNaPortalu)
    {
        $this->idPropertyNaPortalu = $idPropertyNaPortalu;

        return $this;
    }

    /**
     * Get idPropertyNaPortalu
     *
     * @return string
     */
    public function getIdPropertyNaPortalu()
    {
        return $this->idPropertyNaPortalu;
    }

    /**
     * Set aktivan
     *
     * @param boolean $aktivan
     *
     * @return SetupProperty
     */
    public function setAktivan($aktivan)
    {
        $this->aktivan = $aktivan;

        return $this;
    }

    /**
     * Get aktivan
     *
     * @return boolean
     */
    public function getAktivan()
    {
        return $this->aktivan;
    }

    /**
     * Set idSkrbnik
     *
     * @param string $idSkrbnik
     *
     * @return SetupProperty
     */
    public function setIdSkrbnik($idSkrbnik)
    {
        $this->idSkrbnik = $idSkrbnik;

        return $this;
    }

    /**
     * Get idSkrbnik
     *
     * @return string
     */
    public function getIdSkrbnik()
    {
        return $this->idSkrbnik;
    }

    /**
     * Set idFront
     *
     * @param string $idFront
     *
     * @return SetupProperty
     */
    public function setIdFront($idFront)
    {
        $this->idFront = $idFront;

        return $this;
    }

    /**
     * Get idFront
     *
     * @return string
     */
    public function getIdFront()
    {
        return $this->idFront;
    }

    /**
     * Set datumKreiranja
     *
     * @param \DateTime $datumKreiranja
     *
     * @return SetupProperty
     */
    public function setDatumKreiranja($datumKreiranja)
    {
        $this->datumKreiranja = $datumKreiranja;

        return $this;
    }

    /**
     * Get datumKreiranja
     *
     * @return \DateTime
     */
    public function getDatumKreiranja()
    {
        return $this->datumKreiranja;
    }

    /**
     * Set webshop
     *
     * @param \td\CMBundle\Entity\Webshop $webshop
     *
     * @return SetupProperty
     */
    public function setWebshop(\td\CMBundle\Entity\Webshop $webshop = null)
    {
        $this->webshop = $webshop;

        return $this;
    }

    /**
     * Get webshop
     *
     * @return \td\CMBundle\Entity\Webshop
     */
    public function getWebshop()
    {
        return $this->webshop;
    }

    /**
     * Set property
     *
     * @param \td\CMBundle\Entity\Property $property
     *
     * @return SetupProperty
     */
    public function setProperty(\td\CMBundle\Entity\Property $property = null)
    {
        $this->property = $property;

        return $this;
    }

    /**
     * Get property
     *
     * @return \td\CMBundle\Entity\Property
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * Set portalKorisnik
     *
     * @param \td\CMBundle\Entity\PortalKorisnik $portalKorisnik
     *
     * @return SetupProperty
     */
    public function setPortalKorisnik(\td\CMBundle\Entity\PortalKorisnik $portalKorisnik = null)
    {
        $this->portalKorisnik = $portalKorisnik;

        return $this;
    }

    /**
     * Get portalKorisnik
     *
     * @return \td\CMBundle\Entity\PortalKorisnik
     */
    public function getPortalKorisnik()
    {
        return $this->portalKorisnik;
    }

    /**
     * Add setupPropertyUnit
     *
     * @param \td\CMBundle\Entity\SetupPropertyUnit $setupPropertyUnit
     *
     * @return SetupProperty
     */
    public function addSetupPropertyUnit(\td\CMBundle\Entity\SetupPropertyUnit $setupPropertyUnit)
    {
        $this->setupPropertyUnits[] = $setupPropertyUnit;

        return $this;
    }

    /**
     * Remove setupPropertyUnit
     *
     * @param \td\CMBundle\Entity\SetupPropertyUnit $setupPropertyUnit
     */
    public function removeSetupPropertyUnit(\td\CMBundle\Entity\SetupPropertyUnit $setupPropertyUnit)
    {
        $this->setupPropertyUnits->removeElement($setupPropertyUnit);
    }

    /**
     * Get setupPropertyUnits
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSetupPropertyUnits()
    {
        return $this->setupPropertyUnits;
    }

    /**
     * Set povezan
     *
     * @param boolean $povezan
     *
     * @return SetupProperty
     */
    public function setPovezan($povezan)
    {
        $this->povezan = $povezan;

        return $this;
    }

    /**
     * Get povezan
     *
     * @return boolean
     */
    public function getPovezan()
    {
        return $this->povezan;
    }
}
