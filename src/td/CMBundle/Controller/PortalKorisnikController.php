<?php

namespace td\CMBundle\Controller;

use Datatheke\Bundle\PagerBundle\Pager\Field;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use td\CMBundle\Entity\Portal;
use td\CMBundle\Entity\PortalKorisnik;
use td\CMBundle\Entity\UnitRate;
use td\CMBundle\Entity\SetupProperty;
use td\CMBundle\Entity\SetupPropertyUnit;
use td\CMBundle\Entity\WebshopProperty;
use td\CMBundle\Entity\WebshopPropertyUnit;
use Symfony\Component\HttpFoundation\Request;
use td\CMBundle\Form\PortalKorisnikType;
use Doctrine\ORM\EntityNotFoundException;

class PortalKorisnikController extends Controller
{

    private $idCompanyKorisnik = 1;
    private $menu;

    //Upravljanje sesijom - za sada se koristit cookies
    private function sesija()
    {
        $array = explode(":", $_COOKIE['tdapplogin']);
        if (crypt($array[0], 'he6765zfh') == $array[1]) {
            $this->idCompanyKorisnik = $array[4];
            $companyStatus = $array[6];
            $em = $this->getDoctrine()->getManager();
            $connection = $em->getConnection();
            $statement = $connection->prepare("SELECT menu_path FROM company_status WHERE id= :id");
            $statement->bindValue('id', $companyStatus);
            $statement->execute();
            $results = $statement->fetchAll();
            $row = $results[0];
            $this->menu = $row['menu_path'];
        } else {
            header("location: https://agencije.atrade.hr");
            exit;
        }
    }


    /**
     * @return Response
     */
    public function indexAction()
    {
        //inicijalizacija objekta
        //dohvat podataka iz baze
        $em = $this->getDoctrine()->getManager();
        $portalKorisnici = $em
            ->getRepository('tdCMBundle:PortalKorisnik')
            ->findAll();

        $availableWebshops = $em->getRepository('tdCMBundle:Webshop')->findAll();

        if (!$portalKorisnici) {
            throw $this->createNotFoundException(
                'Nema zapisa u tablici portalkorisnik'
            );
        }

        //prikaz podataka u tablici
        return $this->render('tdCMBundle:PortalKorisnik:index.html.twig', array('webshops' => $availableWebshops));
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function listByKorisnikAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
//        $portalKorisnici = $em->getRepository('tdCMBundle:PortalKorisnik')->findAll();
        $companyId = $request->query->get('company');

        // Get portal data if GET parameter is given
        if (!is_null($companyId)) {
            $portals = $em->getRepository('tdCMBundle:Portal')->findAllPortalsNotActiveForCompany($companyId);
            $portalKorisnici = $em->getRepository('tdCMBundle:PortalKorisnik')->findByIdCompanyKorisnik($companyId);
            $company = $em->getRepository('tdCMBundle:Company')->find($companyId);
        } else {
            $portals = $portalKorisnici = $company = null;
        }
//        $companies = $em->getRepository('tdCMBundle:Company')->findBy(
//            array(
//                'deleted' => 0,
//            ),
//            array(
//                'id' => 'ASC',
//            ), 100, 0);

        $pager = $this->get('datatheke.pager')->createHttpPager('tdCMBundle:Company');
        $view = $pager->handleRequest($request);

        $webshops = $em->getRepository('tdCMBundle:Webshop')->findActiveForSelectControl();

//        return $this->render('@tdCM/Promjena/test.html.twig', array('datagrid' => $view));

        return $this->render('@tdCM/PortalKorisnik/listByKorisnik.html.twig', array(
            'pager' => $view,
            'portals' => $portals,
            'portalKorisnici' => $portalKorisnici,
            'companyId' => $companyId,
            'company' => $company,
            'webshops' => $webshops,
        ));
    }

    /**
     * DEPRECATED
     * Kreiraj vezu portal korisnik - Ovo je za pojedinačno kreiranje zapisa u portalkorisnik bez punjenja Setup-a
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function kreirajAction(Request $request)
    {

        // create a task and give it some dummy data for this example
        $portalKorisnik = new PortalKorisnik();
        $portalKorisnik->setAktivan(true);

        $form = $this->createForm(new PortalKorisnikType(), $portalKorisnik);

        $form->handleRequest($request);

        if ($form->isValid()) {
            //perform some action, such as saving the task to the database
            $em = $this->getDoctrine()->getManager();
            $em->persist($portalKorisnik);
            $em->flush();

            $nextAction = $form->get('saveAndAdd')->isClicked()
                ? 'td_cm_portalKorisnik_kreiraj'
                : 'td_cm_portalKorisnik_pregled';

            return $this->redirect($this->generateUrl($nextAction));
        }

        return $this->render('tdCMBundle:PortalKorisnik:kreiraj2.html.twig', array('form' => $form->createView()));

        //return $this->render('AcmeTaskBundle:Default:index.html.twig', array('name' => $name));
    }

    /**
     * DEPRECATED
     * Povezivanje portala - punjenje CM_setup
     *
     * @param $idPortal
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws Exception
     * @throws \Exception
     */
    public function poveziAction($idPortal)
    {
        //DOhvat korisnika iz COOKIE varijable kako bin iima ispravan set podataka
//            $this->sesija();
        $idCompanyKorisnik = $this->idCompanyKorisnik;

        //Dohvat portala
//            $em = $this->getDoctrine()->getEntityManager();
        $em = $this->getDoctrine()->getManager();


        $portal = $em->getRepository('tdCMBundle:Portal')->find($idPortal);
        $portalKorisnik = new PortalKorisnik();
        $portalKorisnik->setIdCompanyKorisnik($idCompanyKorisnik);
        $portalKorisnik->setPortal($portal);
        $portalKorisnik->setUrlPortala($portal->getLink());

        $form = $this->createForm(new PortalKorisnikType($idCompanyKorisnik), $portalKorisnik);

        $request = $this->get('request');
        $form->handleRequest($request);

        if ($form->isValid()) {
            //perform some action, such as saving the task to the database
            //$em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();

            try {
                $em->persist($portalKorisnik);

                $em->flush();
                /* //More se obrisat - pušti san kao način odluke koja stranica će se pozvat
                $nextAction = $form->get('saveAndAdd')->isClicked()
                        ? 'td_cm_portalKorisnik_kreiraj'
                        : 'td_cm_portalKorisnik_pregled';
                */

                //Ovo je stari način ažuriranja baze. Novi način je putem PHP-a, ali novi način za sada ne koristin jer je sporiji. Napravi san upravljanje transakcijama pa je u redu.
                $db = $em->getConnection();
                //Punjenje korištenjem izravnim pristupom - ovo je staro
                // $query = "INSERT INTO cm_set_up (id_webshop, id_unit, id_portal_korisnik) 
                // SELECT id_shop, id_unit, ? FROM webshop_property_unit WHERE id_shop = ?";
                $query = "INSERT INTO cm_setup_property (idWebshop, idProperty, ID_portal_korisnik, aktivan) 
                                                SELECT wp.id_shop, wp.id_property, ?, 0
                                                FROM webshop_property as wp inner join webshop as w on wp.id_shop = w.id 
                                                WHERE wp.id_shop = ? and w.id_company = ?";
                $stmt = $db->prepare($query);
                $stmt->bindValue(1, $portalKorisnik->getId());
                $stmt->bindValue(2, $portalKorisnik->getWebshop()->getId());
                $stmt->bindValue(3, $idCompanyKorisnik);
                // $params = array(1 => );
                if ($stmt->execute()) {
                    //Unesi portalKorisnik u bazu
                    //Ovo mi ni uspjelo! Mora proć naredba flush() da bih moga dohvatit $portalKorisnik, zato san vrati gore upis zaglavlja u bazu
                } else {
                    //Javi grešku
                }

                $query = "UPDATE cm_setup_property SET datumKreiranja = now() WHERE idWebshop = ?";
                $stmt = $db->prepare($query);
                $stmt->bindValue(1, $portalKorisnik->getWebshop()->getId());
                $stmt->execute();
                //concat(p.id_country , "-", lpad(p.id,5,'0'),"-",lpad(pu.local_order,2,'0')) 
                $query = "INSERT INTO cm_setup_property_unit (idWebshop, idPropertyUnit, ID_portal_korisnik, aktivan, idSetupProperty, idPropertyUnitFront) 
                                SELECT wpu.id_shop, wpu.id_unit, ?, 0, sp.id,
                                concat(p.id_country , '-', lpad(p.id,5,'0'),'-',lpad(pu.local_order,2,'0'))  
                                FROM webshop_property_unit as wpu 
                                INNER JOIN webshop as w on wpu.id_shop = w.id 
                                INNER JOIN cm_setup_property as sp ON sp.idWebshop = wpu.id_shop
                                INNER JOIN property as p ON sp.idProperty = p.id
                                INNER JOIN property_unit as pu ON wpu.id_unit = pu.id
                                WHERE wpu.id_shop = ? and w.id_company = ?
                                AND wpu.id_property = sp.idProperty";
                $stmt = $db->prepare($query);
                $stmt->bindValue(1, $portalKorisnik->getId());
                $stmt->bindValue(2, $portalKorisnik->getWebshop()->getId());
                $stmt->bindValue(3, $idCompanyKorisnik);
                $stmt->execute();

                //Punjenje SetupProperty-a - PHP način stavka po stavka
//                    $webshopPropertyi = $em->getRepository("tdCMBundle:WebshopProperty")->findByIdShop($portalKorisnik->getWebshop()->getId());
//
//                    if($webshopPropertyi)
//                    {
//                        $izlaz = "";
//                        $i = 1;
//                        /* @var $webshopProperty WebshopProperty */
//                        foreach ($webshopPropertyi as $webshopProperty)
//                        {
//                            $setupProperties = new SetupProperty();
//                            $setupProperties->setAktivan(true);
////                            $setupProperties->setIdProperty($webshopProperty->getIdProperty());
//                            $setupProperties->setProperty($webshopProperty->getProperty());
////                            $setupProperties->setIdWebshop($webshopProperty->getIdShop());
//                            $setupProperties->setWebshop($webshopProperty->getWebshop());
//                            $setupProperties->setPortalKorisnik($portalKorisnik);
//                            if(1)
//                            {
//                                $setupProperties->setIdSkrbnik($webshopProperty->getProperty()->getIdAgent());
//                            }
//                            else
//                            {
//                                $setupProperties->setIdSkrbnik($webshopProperty->getProperty()->getIdAgentC());
//                            }
//                            
//                            $em->persist($setupProperties);
////                            $izlaz = $izlaz . " " . $i . "##" . $setupProperties->getIdProperty() . " " . $setupProperties->getIdWebshop() . " ";
////                            $i++;
//                        }
//                        
////                        return $izlaz;
//                        $webshopPropertyUniti = $em->getRepository("tdCMBundle:WebshopPropertyUnit")->findByWebshop($portalKorisnik->getWebshop());
//
//                        $setupPropertyUniti = array();
//                        /* @var $webshopPropertyUnit WebshopPropertyUnit */
//                        foreach ($webshopPropertyUniti as $webshopPropertyUnit) 
//                        {
//                            $setupPropertyUnit = new SetupPropertyUnit();
//                            $setupPropertyUnit->setAktivan(true);
//                            $setupPropertyUnit->setPropertyUnit($webshopPropertyUnit->getPropertyUnit());
//                            //$setupPropertyUnit->setIdPropertyUnit($webshopPropertyUnit->getIdUnit());
//                            //$setupPropertyUnit->setIdWebshop($webshopPropertyUnit->getIdShop());
//                            $setupPropertyUnit->setWebshop($webshopPropertyUnit->getWebshop());
//                            $setupPropertyUnit->setPortalKorisnik($portalKorisnik);
//                            if(1)
//                            {
//                                $setupPropertyUnit->setIdSkrbnik($webshopPropertyUnit->getProperty()->getIdAgent());
//                            }
//                            else
//                            {
//                                $setupPropertyUnit->setIdSkrbnik($webshopPropertyUnit->getProperty()->getIdAgentC());
//                            }
//                            $setupPropertyUniti[] = $setupPropertyUnit;
//                            $em->persist($setupPropertyUnit);
//                        }

                //Ažuriranje cjenika ako je portal booking com
                if ($portalKorisnik->getPortal()->isBookingCom()) {
                    //Dohvat svih unita iz cm-ab
                    $setupPropertyUniti = $em->getRepository('tdCMBundle:SetupPropertyUnit')->findByPortalKorisnik($portalKorisnik);

                    //Kreiranje unitRate-ova
                    /* @var $setupPropertyUnit SetupPropertyUnit */
                    foreach ($setupPropertyUniti as $setupPropertyUnit) {
                        $unitRate = new UnitRate();
                        $unitRate->setIdRate(0);
                        //$unitRate->setIdPropertyUnit($setupPropertyUnit->getIdPropertyUnit());
                        $unitRate->setSetupPropertyUnit($setupPropertyUnit);
                        $unitRate->setVrijediOd(new \DateTime("2015-02-19"));
                        $unitRate->setVrijediDo(new \DateTime("2015-12-31"));

                        //Opis procesa
                        /*
                            Za zadani setupPropertyUnit obavi sljedeće
                            0. Dohvati webshop za taj setupPropertyUnit
                            1. Čitaj webshopPropertyUnit->getCjenikProdajni() i ako je 0 onda gledaj Property.getIdAgent() tamo di je Property->getId == WebshopPropertyUnit->getIdProperty()
                                                                                                                                                                                                                                                    ili Property->getIdAgentC ovisno o postavkama
                            2. Kad se pročita skrbnika iz Property-a onda čitaj WebshopPartner->getIdWebshopPartner() za webshop iz WebshopPropertyUnita i idCompanyPartner = skrbnik
                                            *forši napravit neku rekurzivnu funkciju*  NE
                            3. U UnitRateStavke spremi idCjenik na koji govori WebshopPartner->getPartnerJe() D ili P

                            Dugi dio
                            1. Dohvati sve stavke iz PropertyUnitItemPriceListItem za PropertyUnit iz SetupPropertyUnit-a i potegni naziv stavaka iz ItemPricelistItem-a
                                            samo za one stavke koje se ne nalaze u UnitRateStavke - ovo je kod kreranja sve
                            2. Pretvorit to u polje i poslat na ekran
                        */

                        $webshopPropertyUnit = $em->getRepository('tdCMBundle:WebshopPropertyUnit')->findOneBy(
                            array('webshop' => $setupPropertyUnit->getWebshop(), 'propertyUnit' => $setupPropertyUnit->getPropertyUnit()));
//                                if($webshopProperty)
//                                {
//                                    $test = "nađen";
//                                }
//                                $izlaz = $izalz . $test . "<br />" . $setupPropertyUnit->getPropertyUnit()               
                        //1.
                        if ($webshopPropertyUnit->getCjenikProdajni() != 0) {
                            //Završi, imamo cjenik
                            $unitRate->setIdCjenik($webshopPropertyUnit->getCjenikProdajni());
                        } else {
                            //Inače dohvati preko WebshopPartner-a
                            //Provjera koji klijent se koristi
                            if (1) {
                                $webshopPartner = $em->getRepository('tdCMBundle:WebshopPartner')->findOneBy(
                                    array('webshop' => $setupPropertyUnit->getWebshop(), 'idCompanyPartner' => $setupPropertyUnit->getPropertyUnit()->getProperty()->getIdAgent()));

                            } else {
                                $webshopPartner = $em->getRepository('tdCMBundle:WebshopPartner')->findOneBy(
                                    array('webshop' => $setupPropertyUnit->getWebshop(), 'idCompanyPartner' => $setupPropertyUnit->getPropertyUnit()->getProperty()->getIdAgentC()));
                            }

                            //3.
                            //Moran provjerit da mi webshopPartner ni nula. Jer mi javlja grešku da mi pozivan funkciju nad nepostojećim objektom.
                            if ($webshopPartner) {
                                //Ako je pronaša webshopPartner onda traži cjenik
                                $webshopPropertyUnitPartner = $em->getRepository('tdCMBundle:WebshopPropertyUnit')->findOneByWebshop($setupPropertyUnit->getWebshop());
//                                                array('idShop' => $webshopPartner->getIdWebshopPartner(), 'idUnit' => $setupPropertyUnit->getIdPropertyUnit()));

                                if ($webshopPartner->getPartnerJe() == 'D') {
                                    $unitRate->setIdCjenik($webshopPropertyUnitPartner->getCjenikKupovni());
                                    $unitRate->setPovecanje($webshopPartner->getB2bToB2c());
                                    $unitRate->setZaokruzivanje($webshopPartner->getB2cZaokruzivanje());
                                } else {
                                    $unitRate->setIdCjenik($webshopPartner->getWebshopPartner()->getCjenikProdajni());
                                }
                            }
                        }

                        //Drugio dio - stavke
                        //1.
                        // $stavke = $em->getRepository('tdCMBundle:PropertyUnitItemPriceListItem')->findByIdHead($unitRate->getIdCjenik());
                        $em->persist($unitRate);
                    }
                }
                $em->flush();
                $em->getConnection()->commit();
                return $this->redirect($this->generateUrl('td_cm_homepage'));
//                    }

            } catch (Exception $ex) {
                $em->getConnection()->rollBack();
                $em->close();
                throw $ex;
            }

        }

        return $this->render('tdCMBundle:PortalKorisnik:povezi.html.twig',
            array(
                'form' => $form->createView(),
                'portal' => $portal,
                'menu' => $this->menu
            ));

        //return $this->render('AcmeTaskBundle:Default:index.html.twig', array('name' => $name));
    }


    /**
     * DEPRECATED
     *
     * @param $idPortal
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function azurirajAction($idPortal)
    {
        //DOhvat korisnika iz COOKIE varijable kako bin iima ispravan set podataka
//		$this->sesija();
        $idCompanyKorisnik = $this->idCompanyKorisnik;

        //Dohvat portala
        $em = $this->getDoctrine()->getManager();
        $portal = $em->getRepository('tdCMBundle:Portal')->find($idPortal);

        //Pronađi portal korisnik za idCompanyKorisnik i idPortal
        $repository = $this->getDoctrine()->getManager()->getRepository('tdCMBundle:PortalKorisnik');

        //Dohvat veze portalKorisnik
        $portalKorisnik = $repository->findOneBy(
            array('portal' => $portal, 'idCompanyKorisnik' => $idCompanyKorisnik)
        );

        //Obradi potvrđenu formu
        $form = $this->createForm(new PortalKorisnikType($idCompanyKorisnik), $portalKorisnik);

        $request = $this->get('request');
        $form->handleRequest($request);

        if ($form->isValid()) {
            //perform some action, such as saving the task to the database
            $em->persist($portalKorisnik);
            $em->flush();

            //Pošalji van na početni ekran
            return $this->redirect($this->generateUrl('td_cm_homepage'));
        }

        //Pošalji van na front
        return $this->render('tdCMBundle:PortalKorisnik:azuriraj.html.twig',
            array('form' => $form->createView(),
                'portal' => $portal,
                'portalKorisnik' => $portalKorisnik,
                'menu' => $this->menu
            ));
    }

    /**
     * Trenutno se ne koristi nigdje
     *
     * @param PortalKorisnik $portalKorisnik
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function obrisiAction(PortalKorisnik $portalKorisnik)
    {
        //Brisanje veze portala

        $em = $this->getDoctrine()->getManager();
        $em->remove($portalKorisnik);
        $em->flush();
        $this->addFlash('notice', 'Portalkorisnik ' . $portalKorisnik->getId() . ' je obrisan!');
        return $this->redirect($this->generateUrl('td_cm_homepage'));
    }

    /**
     * DEPRECATED!
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function portalsAction()
    {
        $loginData = $this->get('td_cm.obrada.cookie_obrada');
//        $loginData->obradaCookie();
        $em = $this->getDoctrine()->getManager();
        $allPortals = $em->getRepository('tdCMBundle:Portal')->findSviByIdCompanyKorisnik($this->idCompanyKorisnik, $loginData->getLanTranslate());
        return $this->render('tdCMBundle:PortalKorisnik:portals.html.php',
            array(
                'allPortals' => $allPortals,
            ));
    }


    /**
     * Get all portalKorisnik items for given portal.
     *
     * @param Portal $portal
     * @return mixed
     */
    public function portalKorisnikListByPortalAction(Portal $portal)
    {
        $em = $this->getDoctrine()->getManager();
        $availableWebshops = $em->getRepository('tdCMBundle:Webshop')->findAll();

        //Check if pending portalKorisnik exists
        $pendingPortalKorisniks = $em->getRepository('tdCMBundle:PortalKorisnik')->findBy(
            array(
                'pending' => true,
                'portal' => $portal,
            )
        );
        if (count($pendingPortalKorisniks)) {
            $message = 'Korisnici ';
            /* @var PortalKorisnik $pendingPortalKorisnik */
            foreach ($pendingPortalKorisniks as $pendingPortalKorisnik) {
                /* @var Client $clientCompanyKorisnik */
                $clientCompanyKorisnik = $em->getRepository('tdCMBundle:Client')->findOneByIdMojaTvrtka($pendingPortalKorisnik->getIdCompanyKorisnik());
                if (is_object($clientCompanyKorisnik)) {
                    $companyKorisnikNaziv = $clientCompanyKorisnik->getNaziv();
                } else {
                    $companyKorisnikNaziv = $pendingPortalKorisnik->getIdCompanyKorisnik();
                }

                $message .= ', <br><b>[' . $pendingPortalKorisnik->getId() . '] ' . $companyKorisnikNaziv . '</b>';
            }
            $message .= '<br>čekaju na odobrenje (određivanje webshopa)';

            $this->addFlash('notice', $message);
        }

        return $this->render('tdCMBundle:PortalKorisnik:portalKorisnikByPortal.html.twig', array(
            'portal' => $portal,
            'webshops' => $availableWebshops,
        ));

    }

    // Snippets

    /**
     * Snippet for part of object adding form
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function formPortalKorisnikAction(Request $request)
    {
        // Sigurnost
        //$this->sesija();
        // Dohvati potrebne podatke
        if ($idProperty = $request->request->get('idProperty')) {
            $em = $this->getDoctrine()->getManager();
            $property = $em->getRepository('tdCMBundle:Property')->find($idProperty);

            // Check if property exists
            if ($property) {
                try {
                    $portalKorisnici = $em->getRepository('tdCMBundle:PortalKorisnik')->findBy(array(
                        'povezan' => true,
                        'idCompanyKorisnik' => $property->getIdAgent()
                    ));
                    foreach ($portalKorisnici as $key => $portalKorisnik) {
                        if ($portalKorisnik->getSetupProperties()->exists(function ($key, $element) use ($property) {
                            return $element->getProperty()->getId() == $property->getId();
                        })
                        ) {
                            unset($portalKorisnici[$key]);
                        }
                    }

                } catch (EntityNotFoundException $ex) {
                    // Return a message if AGENT not defined on property
                    return new Response("Nije moguće odrediti portale jer ne postoji skrbnik za objekt <br /> <b>[" . $property->getId() . "] " . $property->getName() . "</b>");
                }
            } else {
                // Return a message if property not exists
                return new Response('Objekt sa šifrom <b>' . $idProperty . '</b> ne postoji');
            }
        } else {
            // Return a message if parametar is incorrect
            return new Response('Nije zadana šifra objekta!');
        }

        return $this->render('tdCMBundle:PortalKorisnik/Snippets:formPortalKkorisnik.html.twig', array(
            'portalKorisnici' => $portalKorisnici,
            'property' => $property
        ));
    }

    /**
     *  Partial - Create PortalKorisnik if not exists and create SetupProperty if not exist
     *            Return partial HTML
     *
     * @param Request $request
     * @return Response
     */
    public function createPortalKorisnikSnippetAction(Request $request)
    {
        // Sigurnost

        // Parameters
        $em = $this->getDoctrine()->getManager();

        if ($request->request->has('portalId')) {
            $portal = $em->getRepository('tdCMBundle:Portal')->find($request->request->has('portalId'));
            $property = null;
            $propertyUnit = null;
            $propertyUnitId = 0;
            if ($request->request->has('propertyId')) {
                $propertyId = $request->request->get('propertyId');
                $property = $em->getRepository('tdCMBundle:Property')->find($propertyId);
            } elseif ($request->request->has('propertyUnitId')) {
                $propertyUnitId = $request->request->get('propertyUnitId');
                $propertyUnit = $em->getRepository('tdCMBundle:PropertyUnit')->find($propertyUnitId);
            } else {
                new Response('Parametri neispravni!');
            }
        } else {
            new Response('Parametri neispravni!');
        }

        if (!is_null($property)) {
            // Create new SetupProperty
            if (!is_null($propertyUnit)) {
                // Create new SetupPropertyUnit
                $property = $propertyUnit->getProperty();
            } else {
//                return new Response('Ne postoji objekt ni jedinica');
            }
        } else {
            $propertyUnitId = 0;
        }


        $agent = $em->getRepository('tdCMBundle:Client')->findOneByIdMojaTvrtka($property->getIdAgent());

        $webshops = $em->getRepository('tdCMBundle:Webshop')->getWebshopsForSelectElement();

        // dohvati ili kreiraj portalKorisnik
//        $portalKorisnik = $this->get('td_cm.obrada.portalkorisnik_obrada')->createNewPortalKorisnik($portal, $property->getIdAgent());

        // dohvati ili dodaj spu u CM
//        $spu = $this->get('td_cm.obrada.setup_obrada')->addPropertyToChannelManager($property, $portalKorisnik);

//        return $this->render('@tdCM/SetUp/Snippets/spBookingCom.html.twig', array(
        return $this->render('@tdCM/PortalKorisnik/Snippets/createPortalKorisnik.html.twig', array(
            'property' => $property,
            'webshops' => $webshops,
            'portal' => $portal,
            'agent' => $agent,
            'propertyUnitId' => $propertyUnitId,
        ));
    }
}
