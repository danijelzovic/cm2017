<?php
/**
 * Created by PhpStorm.
 * User: danijel
 * Date: 12.10.2016.
 * Time: 21:36
 */

namespace td\CMBundle\Obrada;


use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NoResultException;
use Symfony\Component\HttpFoundation\JsonResponse;
use td\CMBundle\Entity\Property;
use td\CMBundle\Entity\PropertyUnit;
use td\CMBundle\Entity\Webshop;
use td\CMBundle\Entity\WebshopProperty;
use td\CMBundle\Entity\WebshopPropertyUnit;

class WebshopObrada
{

    protected $em;
    protected $setupObrada;

    /**
     * WebshopObrada constructor.
     * @param EntityManager $em
     * @param SetupObrada $setupObrada
     */
    public function __construct(EntityManager $em, SetupObrada $setupObrada)
    {
        $this->em = $em;
        $this->setupObrada = $setupObrada;
    }

    /**
     *  Create or Update new WebshopProperty and WebshopPropertyUnits
     *
     * @param Property $property
     * @param Webshop $webshop
     * @param $idCompany
     * @return WebshopProperty
     */
    public function createOrUpdateWebshopPropertyUnits(Property $property, Webshop $webshop, $idCompany)
    {
        //Check if webshopProperty exsist
        try {
            $webshopProperty = $this->em->getRepository('tdCMBundle:WebshopProperty')->findOneBy(array(
                'property' => $property,
                'webshop' => $webshop,
            ));
        } catch (NoResultException $e) {
            $webshopProperty = $this->createWebshopProperty($property, $webshop, $idCompany);
        }

        /* @var PropertyUnit $propertyUnit */
        foreach ($property->getPropertyUnits() as $propertyUnit) {
            if (0 == $propertyUnit->getDeleted()) { //Create WebshopProperty only for not deleted propertyUnit
                $idHead = $this->setupObrada->getIdHeadBookerTech($propertyUnit, $idCompany);
                if (-1 == $idHead) {
                    $idHead = 0;
                }
                $webshopPropertyUnit = null;
                //Check if webshopPropertyUnit exsist
                try {
                    $webshopPropertyUnit = $this->em->getRepository('tdCMBundle:WebshopPropertyUnit')->findOneBy(array(
                        'propertyUnit' => $propertyUnit,
                        'webshop' => $webshop,
                    ));
                } catch (NoResultException $e) {
                    $webshopPropertyUnit = $this->createWebshopPropertyUnit($webshop, $propertyUnit, $idCompany);
                }
                $webshopPropertyUnit->setCjenikProdajni($idHead); // Update idHead
                $this->em->persist($webshopPropertyUnit);
            }
        }
        $this->em->flush();

        return $webshopProperty;
    }

    /**
     * @param Property $property
     * @param Webshop $webshop
     * @param $idCompany
     * @return WebshopProperty
     */
    public function createWebshopProperty(Property $property, Webshop $webshop, $idCompany)
    {
        $webshopProperty = new WebshopProperty();
        $webshopProperty->setProperty($property);
        $webshopProperty->setWebshop($webshop);
        $webshopProperty->setApproved(true);
        $webshopProperty->setApprovedDate(new DateTime());
        $webshopProperty->setDisabled(false);
        $webshopProperty->setApprovedBy($idCompany);
        $this->em->persist($webshopProperty);
        $this->em->flush();
        return $webshopProperty;
    }

    /**
     * @param Property $property
     * @param Webshop $webshop
     * @param $idCompany
     * @param $propertyUnit
     * @return WebshopPropertyUnit
     */
    public function createWebshopPropertyUnit(Webshop $webshop, PropertyUnit $propertyUnit, $idCompany)
    {
        $webshopPropertyUnit = new WebshopPropertyUnit();
        $webshopPropertyUnit->setWebshop($webshop);
        $webshopPropertyUnit->setProperty($propertyUnit->getProperty());
        $webshopPropertyUnit->setPropertyUnit($propertyUnit);
        $webshopPropertyUnit->setApprovedBy($idCompany);
        $this->em->persist($webshopPropertyUnit);
        $this->em->flush();

        //Create WebshopPropert if this is first webshopPropertyUnit
        $countWebshopPropertyUnits = (int)$this->em->getRepository('tdCMBundle:WebshopPropertyUnit')->countWebshopPropertyUnits(
            $webshop,
            $propertyUnit->getProperty()
        );

        if(1 == $countWebshopPropertyUnits){
            $this->createWebshopProperty($propertyUnit->getProperty(), $webshop, $idCompany);
        }

        return $webshopPropertyUnit;
    }

    /**
     * Check if it's last webshopPropertyUnit and delete webshopProperty if it is.
     *
     * @param WebshopPropertyUnit $webshopPropertyUnit
     */
    public function deleteWebshopPropertyUnit(WebshopPropertyUnit $webshopPropertyUnit)
    {
        // Check if this is the last webshopPropertyUnit.
        $countWebshopPropertyUnits = (int)$this->em->getRepository('tdCMBundle:WebshopPropertyUnit')->countWebshopPropertyUnits(
            $webshopPropertyUnit->getWebshop(),
            $webshopPropertyUnit->getProperty()
        );

        // If it's last then delete corresponding WebshopPorperty
        if(1 == $countWebshopPropertyUnits){
            //Delete WebshopProperty
            $this->em->getRepository('tdCMBundle:WebshopProperty')->createQueryBuilder('wp')
                ->delete()
                ->where('wp.webshop = :webshop')
                ->andWhere('wp.property = :property')
                ->setParameter('webshop', $webshopPropertyUnit->getWebshop())
                ->setParameter('property', $webshopPropertyUnit->getProperty())
                ->getQuery()
                ->execute();
        }
        $this->em->remove($webshopPropertyUnit);

        return;
    }

}