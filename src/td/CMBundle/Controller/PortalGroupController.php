<?php

namespace td\CMBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use td\CMBundle\Entity\PortalGroup;
use td\CMBundle\Form\PortalGroupType;

/**
 * PortalGroup controller.
 *
 */
class PortalGroupController extends Controller
{

    /**
     * Lists all PortalGroup entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('tdCMBundle:PortalGroup')->findAll();

        return $this->render('tdCMBundle:PortalGroup:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new PortalGroup entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new PortalGroup();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('portalgroup_show', array('id' => $entity->getId())));
        }

        return $this->render('tdCMBundle:PortalGroup:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a PortalGroup entity.
     *
     * @param PortalGroup $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(PortalGroup $entity)
    {
        $form = $this->createForm(new PortalGroupType(), $entity, array(
            'action' => $this->generateUrl('portalgroup_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Izradi',
            'attr' => array(
                'class' => 'btn-sm btn-success',
            )));

        return $form;
    }

    /**
     * Displays a form to create a new PortalGroup entity.
     *
     */
    public function newAction()
    {
        $entity = new PortalGroup();
        $form   = $this->createCreateForm($entity);

        return $this->render('tdCMBundle:PortalGroup:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a PortalGroup entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('tdCMBundle:PortalGroup')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PortalGroup entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('tdCMBundle:PortalGroup:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing PortalGroup entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('tdCMBundle:PortalGroup')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PortalGroup entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('tdCMBundle:PortalGroup:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a PortalGroup entity.
    *
    * @param PortalGroup $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(PortalGroup $entity)
    {
        $form = $this->createForm(new PortalGroupType(), $entity, array(
            'action' => $this->generateUrl('portalgroup_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Ažuriraj',
            'attr' => array(
                'class' => 'btn-primary btn-sm',
            )
        ));

        return $form;
    }
    /**
     * Edits an existing PortalGroup entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('tdCMBundle:PortalGroup')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PortalGroup entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('portalgroup_edit', array('id' => $id)));
        }

        return $this->render('tdCMBundle:PortalGroup:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a PortalGroup entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('tdCMBundle:PortalGroup')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find PortalGroup entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('portalgroup'));
    }

    /**
     * Creates a form to delete a PortalGroup entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('portalgroup_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                'label' => 'Obriši',
                'attr' => array(
                    'class' => 'btn-danger btn-sm',
                )
            ))
            ->getForm()
        ;
    }
}
