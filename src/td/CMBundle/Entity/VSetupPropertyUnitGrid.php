<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VSetupPropertyUnitGrid
 *
 * @ORM\Table(name="v_cm_setup_property_unit_grid")
 * @ORM\Entity
 */
class VSetupPropertyUnitGrid
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="idSetupProperty", type="integer")
     */
    private $idSetupProperty;


    /**
     * @ORM\ManyToOne(targetEntity="SetupProperty")
     * @ORM\JoinColumn(name="idSetupProperty", referencedColumnName="id")
     **/
    private $setupProperty;

    /**
     * @var integer
     *
     * @ORM\Column(name="idPortalKorisnik", type="integer")
     */
    private $idPortalKorisnik;

    /**
     * @var string
     *
     * @ORM\Column(name="idPropertyUnitFront", type="string", length=255)
     */
    private $idPropertyUnitFront;

    /**
     * @var string
     *
     * @ORM\Column(name="idPropertyUnitNaPortalu", type="string", length=255)
     */
    private $idPropertyUnitNaPortalu;

    /**
     * @var string
     *
     * @ORM\Column(name="nazivUnita", type="string", length=255)
     */
    private $nazivUnita;

    /**
     * @var boolean
     *
     * @ORM\Column(name="aktivan", type="boolean")
     */
    private $aktivan;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datumKreiranja", type="datetime")
     */
    private $datumKreiranja;


    /**
     * @var integer
     *
     * @ORM\Column(name="brojNeuspjelihTransfera", type="integer")
     */
    private $brojNeuspjelihTransfera;

    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idSetupProperty
     *
     * @param integer $idSetupProperty
     *
     * @return VSetupPropertyUnitGrid
     */
    public function setIdSetupProperty($idSetupProperty)
    {
        $this->idSetupProperty = $idSetupProperty;

        return $this;
    }

    /**
     * Get idSetupProperty
     *
     * @return integer
     */
    public function getIdSetupProperty()
    {
        return $this->idSetupProperty;
    }

    /**
     * Set idPortalKorisnik
     *
     * @param integer $idPortalKorisnik
     *
     * @return VSetupPropertyUnitGrid
     */
    public function setIdPortalKorisnik($idPortalKorisnik)
    {
        $this->idPortalKorisnik = $idPortalKorisnik;

        return $this;
    }

    /**
     * Get idPortalKorisnik
     *
     * @return integer
     */
    public function getIdPortalKorisnik()
    {
        return $this->idPortalKorisnik;
    }

    /**
     * Set idPropertyUnitFront
     *
     * @param string $idPropertyUnitFront
     *
     * @return VSetupPropertyUnitGrid
     */
    public function setIdPropertyUnitFront($idPropertyUnitFront)
    {
        $this->idPropertyUnitFront = $idPropertyUnitFront;

        return $this;
    }

    /**
     * Get idPropertyUnitFront
     *
     * @return string
     */
    public function getIdPropertyUnitFront()
    {
        return $this->idPropertyUnitFront;
    }

    /**
     * Set idPropertyUnitNaPortalu
     *
     * @param string $idPropertyUnitNaPortalu
     *
     * @return VSetupPropertyUnitGrid
     */
    public function setIdPropertyUnitNaPortalu($idPropertyUnitNaPortalu)
    {
        $this->idPropertyUnitNaPortalu = $idPropertyUnitNaPortalu;

        return $this;
    }

    /**
     * Get idPropertyUnitNaPortalu
     *
     * @return string
     */
    public function getIdPropertyUnitNaPortalu()
    {
        return $this->idPropertyUnitNaPortalu;
    }

    /**
     * Set nazivUnita
     *
     * @param string $nazivUnita
     *
     * @return VSetupPropertyUnitGrid
     */
    public function setNazivUnita($nazivUnita)
    {
        $this->nazivUnita = $nazivUnita;

        return $this;
    }

    /**
     * Get nazivUnita
     *
     * @return string
     */
    public function getNazivUnita()
    {
        return $this->nazivUnita;
    }

    /**
     * Set aktivan
     *
     * @param boolean $aktivan
     *
     * @return VSetupPropertyUnitGrid
     */
    public function setAktivan($aktivan)
    {
        $this->aktivan = $aktivan;

        return $this;
    }

    /**
     * Get aktivan
     *
     * @return boolean
     */
    public function getAktivan()
    {
        return $this->aktivan;
    }

    /**
     * Set datumKreiranja
     *
     * @param \DateTime $datumKreiranja
     *
     * @return VSetupPropertyUnitGrid
     */
    public function setDatumKreiranja($datumKreiranja)
    {
        $this->datumKreiranja = $datumKreiranja;

        return $this;
    }

    /**
     * Get datumKreiranja
     *
     * @return \DateTime
     */
    public function getDatumKreiranja()
    {
        return $this->datumKreiranja;
    }

    /**
     * Set brojNeuspjelihTransfera
     *
     * @param integer $brojNeuspjelihTransfera
     *
     * @return VSetupPropertyUnitGrid
     */
    public function setBrojNeuspjelihTransfera($brojNeuspjelihTransfera)
    {
        $this->brojNeuspjelihTransfera = $brojNeuspjelihTransfera;

        return $this;
    }

    /**
     * Get brojNeuspjelihTransfera
     *
     * @return integer
     */
    public function getBrojNeuspjelihTransfera()
    {
        return $this->brojNeuspjelihTransfera;
    }

    /**
     * Set setupProperty
     *
     * @param \td\CMBundle\Entity\SetupProperty $setupProperty
     *
     * @return VSetupPropertyUnitGrid
     */
    public function setSetupProperty(\td\CMBundle\Entity\SetupProperty $setupProperty = null)
    {
        $this->setupProperty = $setupProperty;

        return $this;
    }

    /**
     * Get setupProperty
     *
     * @return \td\CMBundle\Entity\SetupProperty
     */
    public function getSetupProperty()
    {
        return $this->setupProperty;
    }
}
