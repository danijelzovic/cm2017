<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Webshop
 *
 * @ORM\Table(name="webshop", indexes={@ORM\Index(name="id_company", columns={"id_company"}), @ORM\Index(name="type", columns={"type"}), @ORM\Index(name="deleted", columns={"deleted"})})
 * @ORM\Entity(repositoryClass="td\CMBundle\Entity\Repository\WebshopRepository")
 */
class Webshop
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_company", type="integer", nullable=false)
     */
    private $idCompanyKorisnik = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="type", type="boolean", nullable=false)
     */
    private $type = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="accountuse", type="string", length=3, nullable=false)
     */
    private $accountuse = '';

    /**
     * @var string
     *
     * @ORM\Column(name="template_own", type="string", length=32, nullable=false)
     */
    private $templateOwn = '';

    /**
     * @var string
     *
     * @ORM\Column(name="template", type="string", length=32, nullable=false)
     */
    private $template = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="ftemplate", type="smallint", nullable=false)
     */
    private $ftemplate;

    /**
     * @var string
     *
     * @ORM\Column(name="wordpress", type="string", length=16, nullable=false)
     */
    private $wordpress = '';

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=64, nullable=false)
     */
    private $url = '';

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title = '';

    /**
     * @var string
     *
     * @ORM\Column(name="title_en", type="string", length=255, nullable=false)
     */
    private $titleEn = '';

    /**
     * @var string
     *
     * @ORM\Column(name="title_de", type="string", length=255, nullable=false)
     */
    private $titleDe = '';

    /**
     * @var string
     *
     * @ORM\Column(name="title_it", type="string", length=255, nullable=false)
     */
    private $titleIt = '';

    /**
     * @var string
     *
     * @ORM\Column(name="mailfrom_name", type="string", length=255, nullable=false)
     */
    private $mailfromName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="owner", type="string", length=255, nullable=false)
     */
    private $owner = '';

    /**
     * @var string
     *
     * @ORM\Column(name="web", type="string", length=255, nullable=false)
     */
    private $web = '';

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="receive_inquiry", type="boolean", nullable=false)
     */
    private $receiveInquiry = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="email_res", type="string", length=255, nullable=false)
     */
    private $emailRes = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="receive_reservation", type="boolean", nullable=false)
     */
    private $receiveReservation = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="sms_inq", type="string", length=255, nullable=false)
     */
    private $smsInq = '';

    /**
     * @var string
     *
     * @ORM\Column(name="sms_res", type="string", length=255, nullable=false)
     */
    private $smsRes = '';

    /**
     * @var string
     *
     * @ORM\Column(name="email_sender", type="string", length=255, nullable=false)
     */
    private $emailSender = '';

    /**
     * @var string
     *
     * @ORM\Column(name="name_sender", type="string", length=255, nullable=false)
     */
    private $nameSender = '';

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=255, nullable=false)
     */
    private $tel = '';

    /**
     * @var string
     *
     * @ORM\Column(name="mob", type="string", length=255, nullable=false)
     */
    private $mob = '';

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text", nullable=false)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="skype", type="string", length=255, nullable=false)
     */
    private $skype;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=false)
     */
    private $city = '';

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=255, nullable=false)
     */
    private $region = '';

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=255, nullable=false)
     */
    private $fax = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_culture", type="boolean", nullable=false)
     */
    private $showCulture = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_nature", type="boolean", nullable=false)
     */
    private $showNature = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_gastro", type="boolean", nullable=false)
     */
    private $showGastro = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_sport", type="boolean", nullable=false)
     */
    private $showSport = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_shop", type="boolean", nullable=false)
     */
    private $showShop = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_entertain", type="boolean", nullable=false)
     */
    private $showEntertain = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_events", type="boolean", nullable=false)
     */
    private $showEvents = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_where", type="boolean", nullable=false)
     */
    private $showWhere = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_gallery", type="boolean", nullable=false)
     */
    private $showGallery = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="ponuda_vrijedi_dana", type="smallint", nullable=false)
     */
    private $ponudaVrijediDana = '2';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ponuda_istice_sat", type="time", nullable=false)
     */
    private $ponudaIsticeSat = '15:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="meni_countries", type="string", length=255, nullable=false)
     */
    private $meniCountries = 'HR';

    /**
     * @var string
     *
     * @ORM\Column(name="meni_default_country", type="string", length=2, nullable=false)
     */
    private $meniDefaultCountry = 'HR';

    /**
     * @var string
     *
     * @ORM\Column(name="dobavna_provizija", type="decimal", precision=6, scale=2, nullable=false)
     */
    private $dobavnaProvizija;

    /**
     * @var string
     *
     * @ORM\Column(name="b2b_to_b2c", type="decimal", precision=6, scale=2, nullable=false)
     */
    private $b2bToB2c;

    /**
     * @var boolean
     *
     * @ORM\Column(name="b2c_zaokruzivanje", type="boolean", nullable=false)
     */
    private $b2cZaokruzivanje;

    /**
     * @var boolean
     *
     * @ORM\Column(name="zajednicki", type="boolean", nullable=false)
     */
    private $zajednicki;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false)
     */
    private $deleted = '0';

	 /**
     * @ORM\OneToMany(targetEntity="PortalKorisnik", mappedBy="webshop")
     */
	private $portaliKorisnici;
	
	/**
     * @ORM\OneToMany(targetEntity="WebshopPartner", mappedBy="webshop")
     */
	private $webshopPartneri;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->portaliKorisnici = new \Doctrine\Common\Collections\ArrayCollection();
        $this->webshopPartneri = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCompanyKorisnik
     *
     * @param integer $idCompanyKorisnik
     *
     * @return Webshop
     */
    public function setIdCompanyKorisnik($idCompanyKorisnik)
    {
        $this->idCompanyKorisnik = $idCompanyKorisnik;

        return $this;
    }

    /**
     * Get idCompanyKorisnik
     *
     * @return integer
     */
    public function getIdCompanyKorisnik()
    {
        return $this->idCompanyKorisnik;
    }

    /**
     * Set type
     *
     * @param boolean $type
     *
     * @return Webshop
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return boolean
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set accountuse
     *
     * @param string $accountuse
     *
     * @return Webshop
     */
    public function setAccountuse($accountuse)
    {
        $this->accountuse = $accountuse;

        return $this;
    }

    /**
     * Get accountuse
     *
     * @return string
     */
    public function getAccountuse()
    {
        return $this->accountuse;
    }

    /**
     * Set templateOwn
     *
     * @param string $templateOwn
     *
     * @return Webshop
     */
    public function setTemplateOwn($templateOwn)
    {
        $this->templateOwn = $templateOwn;

        return $this;
    }

    /**
     * Get templateOwn
     *
     * @return string
     */
    public function getTemplateOwn()
    {
        return $this->templateOwn;
    }

    /**
     * Set template
     *
     * @param string $template
     *
     * @return Webshop
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template
     *
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set ftemplate
     *
     * @param integer $ftemplate
     *
     * @return Webshop
     */
    public function setFtemplate($ftemplate)
    {
        $this->ftemplate = $ftemplate;

        return $this;
    }

    /**
     * Get ftemplate
     *
     * @return integer
     */
    public function getFtemplate()
    {
        return $this->ftemplate;
    }

    /**
     * Set wordpress
     *
     * @param string $wordpress
     *
     * @return Webshop
     */
    public function setWordpress($wordpress)
    {
        $this->wordpress = $wordpress;

        return $this;
    }

    /**
     * Get wordpress
     *
     * @return string
     */
    public function getWordpress()
    {
        return $this->wordpress;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Webshop
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Webshop
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set titleEn
     *
     * @param string $titleEn
     *
     * @return Webshop
     */
    public function setTitleEn($titleEn)
    {
        $this->titleEn = $titleEn;

        return $this;
    }

    /**
     * Get titleEn
     *
     * @return string
     */
    public function getTitleEn()
    {
        return $this->titleEn;
    }

    /**
     * Set titleDe
     *
     * @param string $titleDe
     *
     * @return Webshop
     */
    public function setTitleDe($titleDe)
    {
        $this->titleDe = $titleDe;

        return $this;
    }

    /**
     * Get titleDe
     *
     * @return string
     */
    public function getTitleDe()
    {
        return $this->titleDe;
    }

    /**
     * Set titleIt
     *
     * @param string $titleIt
     *
     * @return Webshop
     */
    public function setTitleIt($titleIt)
    {
        $this->titleIt = $titleIt;

        return $this;
    }

    /**
     * Get titleIt
     *
     * @return string
     */
    public function getTitleIt()
    {
        return $this->titleIt;
    }

    /**
     * Set mailfromName
     *
     * @param string $mailfromName
     *
     * @return Webshop
     */
    public function setMailfromName($mailfromName)
    {
        $this->mailfromName = $mailfromName;

        return $this;
    }

    /**
     * Get mailfromName
     *
     * @return string
     */
    public function getMailfromName()
    {
        return $this->mailfromName;
    }

    /**
     * Set owner
     *
     * @param string $owner
     *
     * @return Webshop
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return string
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set web
     *
     * @param string $web
     *
     * @return Webshop
     */
    public function setWeb($web)
    {
        $this->web = $web;

        return $this;
    }

    /**
     * Get web
     *
     * @return string
     */
    public function getWeb()
    {
        return $this->web;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Webshop
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set receiveInquiry
     *
     * @param boolean $receiveInquiry
     *
     * @return Webshop
     */
    public function setReceiveInquiry($receiveInquiry)
    {
        $this->receiveInquiry = $receiveInquiry;

        return $this;
    }

    /**
     * Get receiveInquiry
     *
     * @return boolean
     */
    public function getReceiveInquiry()
    {
        return $this->receiveInquiry;
    }

    /**
     * Set emailRes
     *
     * @param string $emailRes
     *
     * @return Webshop
     */
    public function setEmailRes($emailRes)
    {
        $this->emailRes = $emailRes;

        return $this;
    }

    /**
     * Get emailRes
     *
     * @return string
     */
    public function getEmailRes()
    {
        return $this->emailRes;
    }

    /**
     * Set receiveReservation
     *
     * @param boolean $receiveReservation
     *
     * @return Webshop
     */
    public function setReceiveReservation($receiveReservation)
    {
        $this->receiveReservation = $receiveReservation;

        return $this;
    }

    /**
     * Get receiveReservation
     *
     * @return boolean
     */
    public function getReceiveReservation()
    {
        return $this->receiveReservation;
    }

    /**
     * Set smsInq
     *
     * @param string $smsInq
     *
     * @return Webshop
     */
    public function setSmsInq($smsInq)
    {
        $this->smsInq = $smsInq;

        return $this;
    }

    /**
     * Get smsInq
     *
     * @return string
     */
    public function getSmsInq()
    {
        return $this->smsInq;
    }

    /**
     * Set smsRes
     *
     * @param string $smsRes
     *
     * @return Webshop
     */
    public function setSmsRes($smsRes)
    {
        $this->smsRes = $smsRes;

        return $this;
    }

    /**
     * Get smsRes
     *
     * @return string
     */
    public function getSmsRes()
    {
        return $this->smsRes;
    }

    /**
     * Set emailSender
     *
     * @param string $emailSender
     *
     * @return Webshop
     */
    public function setEmailSender($emailSender)
    {
        $this->emailSender = $emailSender;

        return $this;
    }

    /**
     * Get emailSender
     *
     * @return string
     */
    public function getEmailSender()
    {
        return $this->emailSender;
    }

    /**
     * Set nameSender
     *
     * @param string $nameSender
     *
     * @return Webshop
     */
    public function setNameSender($nameSender)
    {
        $this->nameSender = $nameSender;

        return $this;
    }

    /**
     * Get nameSender
     *
     * @return string
     */
    public function getNameSender()
    {
        return $this->nameSender;
    }

    /**
     * Set tel
     *
     * @param string $tel
     *
     * @return Webshop
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set mob
     *
     * @param string $mob
     *
     * @return Webshop
     */
    public function setMob($mob)
    {
        $this->mob = $mob;

        return $this;
    }

    /**
     * Get mob
     *
     * @return string
     */
    public function getMob()
    {
        return $this->mob;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Webshop
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set skype
     *
     * @param string $skype
     *
     * @return Webshop
     */
    public function setSkype($skype)
    {
        $this->skype = $skype;

        return $this;
    }

    /**
     * Get skype
     *
     * @return string
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Webshop
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set region
     *
     * @param string $region
     *
     * @return Webshop
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set fax
     *
     * @param string $fax
     *
     * @return Webshop
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set showCulture
     *
     * @param boolean $showCulture
     *
     * @return Webshop
     */
    public function setShowCulture($showCulture)
    {
        $this->showCulture = $showCulture;

        return $this;
    }

    /**
     * Get showCulture
     *
     * @return boolean
     */
    public function getShowCulture()
    {
        return $this->showCulture;
    }

    /**
     * Set showNature
     *
     * @param boolean $showNature
     *
     * @return Webshop
     */
    public function setShowNature($showNature)
    {
        $this->showNature = $showNature;

        return $this;
    }

    /**
     * Get showNature
     *
     * @return boolean
     */
    public function getShowNature()
    {
        return $this->showNature;
    }

    /**
     * Set showGastro
     *
     * @param boolean $showGastro
     *
     * @return Webshop
     */
    public function setShowGastro($showGastro)
    {
        $this->showGastro = $showGastro;

        return $this;
    }

    /**
     * Get showGastro
     *
     * @return boolean
     */
    public function getShowGastro()
    {
        return $this->showGastro;
    }

    /**
     * Set showSport
     *
     * @param boolean $showSport
     *
     * @return Webshop
     */
    public function setShowSport($showSport)
    {
        $this->showSport = $showSport;

        return $this;
    }

    /**
     * Get showSport
     *
     * @return boolean
     */
    public function getShowSport()
    {
        return $this->showSport;
    }

    /**
     * Set showShop
     *
     * @param boolean $showShop
     *
     * @return Webshop
     */
    public function setShowShop($showShop)
    {
        $this->showShop = $showShop;

        return $this;
    }

    /**
     * Get showShop
     *
     * @return boolean
     */
    public function getShowShop()
    {
        return $this->showShop;
    }

    /**
     * Set showEntertain
     *
     * @param boolean $showEntertain
     *
     * @return Webshop
     */
    public function setShowEntertain($showEntertain)
    {
        $this->showEntertain = $showEntertain;

        return $this;
    }

    /**
     * Get showEntertain
     *
     * @return boolean
     */
    public function getShowEntertain()
    {
        return $this->showEntertain;
    }

    /**
     * Set showEvents
     *
     * @param boolean $showEvents
     *
     * @return Webshop
     */
    public function setShowEvents($showEvents)
    {
        $this->showEvents = $showEvents;

        return $this;
    }

    /**
     * Get showEvents
     *
     * @return boolean
     */
    public function getShowEvents()
    {
        return $this->showEvents;
    }

    /**
     * Set showWhere
     *
     * @param boolean $showWhere
     *
     * @return Webshop
     */
    public function setShowWhere($showWhere)
    {
        $this->showWhere = $showWhere;

        return $this;
    }

    /**
     * Get showWhere
     *
     * @return boolean
     */
    public function getShowWhere()
    {
        return $this->showWhere;
    }

    /**
     * Set showGallery
     *
     * @param boolean $showGallery
     *
     * @return Webshop
     */
    public function setShowGallery($showGallery)
    {
        $this->showGallery = $showGallery;

        return $this;
    }

    /**
     * Get showGallery
     *
     * @return boolean
     */
    public function getShowGallery()
    {
        return $this->showGallery;
    }

    /**
     * Set ponudaVrijediDana
     *
     * @param integer $ponudaVrijediDana
     *
     * @return Webshop
     */
    public function setPonudaVrijediDana($ponudaVrijediDana)
    {
        $this->ponudaVrijediDana = $ponudaVrijediDana;

        return $this;
    }

    /**
     * Get ponudaVrijediDana
     *
     * @return integer
     */
    public function getPonudaVrijediDana()
    {
        return $this->ponudaVrijediDana;
    }

    /**
     * Set ponudaIsticeSat
     *
     * @param \DateTime $ponudaIsticeSat
     *
     * @return Webshop
     */
    public function setPonudaIsticeSat($ponudaIsticeSat)
    {
        $this->ponudaIsticeSat = $ponudaIsticeSat;

        return $this;
    }

    /**
     * Get ponudaIsticeSat
     *
     * @return \DateTime
     */
    public function getPonudaIsticeSat()
    {
        return $this->ponudaIsticeSat;
    }

    /**
     * Set meniCountries
     *
     * @param string $meniCountries
     *
     * @return Webshop
     */
    public function setMeniCountries($meniCountries)
    {
        $this->meniCountries = $meniCountries;

        return $this;
    }

    /**
     * Get meniCountries
     *
     * @return string
     */
    public function getMeniCountries()
    {
        return $this->meniCountries;
    }

    /**
     * Set meniDefaultCountry
     *
     * @param string $meniDefaultCountry
     *
     * @return Webshop
     */
    public function setMeniDefaultCountry($meniDefaultCountry)
    {
        $this->meniDefaultCountry = $meniDefaultCountry;

        return $this;
    }

    /**
     * Get meniDefaultCountry
     *
     * @return string
     */
    public function getMeniDefaultCountry()
    {
        return $this->meniDefaultCountry;
    }

    /**
     * Set dobavnaProvizija
     *
     * @param string $dobavnaProvizija
     *
     * @return Webshop
     */
    public function setDobavnaProvizija($dobavnaProvizija)
    {
        $this->dobavnaProvizija = $dobavnaProvizija;

        return $this;
    }

    /**
     * Get dobavnaProvizija
     *
     * @return string
     */
    public function getDobavnaProvizija()
    {
        return $this->dobavnaProvizija;
    }

    /**
     * Set b2bToB2c
     *
     * @param string $b2bToB2c
     *
     * @return Webshop
     */
    public function setB2bToB2c($b2bToB2c)
    {
        $this->b2bToB2c = $b2bToB2c;

        return $this;
    }

    /**
     * Get b2bToB2c
     *
     * @return string
     */
    public function getB2bToB2c()
    {
        return $this->b2bToB2c;
    }

    /**
     * Set b2cZaokruzivanje
     *
     * @param boolean $b2cZaokruzivanje
     *
     * @return Webshop
     */
    public function setB2cZaokruzivanje($b2cZaokruzivanje)
    {
        $this->b2cZaokruzivanje = $b2cZaokruzivanje;

        return $this;
    }

    /**
     * Get b2cZaokruzivanje
     *
     * @return boolean
     */
    public function getB2cZaokruzivanje()
    {
        return $this->b2cZaokruzivanje;
    }

    /**
     * Set zajednicki
     *
     * @param boolean $zajednicki
     *
     * @return Webshop
     */
    public function setZajednicki($zajednicki)
    {
        $this->zajednicki = $zajednicki;

        return $this;
    }

    /**
     * Get zajednicki
     *
     * @return boolean
     */
    public function getZajednicki()
    {
        return $this->zajednicki;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Webshop
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Add portaliKorisnici
     *
     * @param \td\CMBundle\Entity\PortalKorisnik $portaliKorisnici
     *
     * @return Webshop
     */
    public function addPortaliKorisnici(\td\CMBundle\Entity\PortalKorisnik $portaliKorisnici)
    {
        $this->portaliKorisnici[] = $portaliKorisnici;

        return $this;
    }

    /**
     * Remove portaliKorisnici
     *
     * @param \td\CMBundle\Entity\PortalKorisnik $portaliKorisnici
     */
    public function removePortaliKorisnici(\td\CMBundle\Entity\PortalKorisnik $portaliKorisnici)
    {
        $this->portaliKorisnici->removeElement($portaliKorisnici);
    }

    /**
     * Get portaliKorisnici
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPortaliKorisnici()
    {
        return $this->portaliKorisnici;
    }

    /**
     * Add webshopPartneri
     *
     * @param \td\CMBundle\Entity\WebshopPartner $webshopPartneri
     *
     * @return Webshop
     */
    public function addWebshopPartneri(\td\CMBundle\Entity\WebshopPartner $webshopPartneri)
    {
        $this->webshopPartneri[] = $webshopPartneri;

        return $this;
    }

    /**
     * Remove webshopPartneri
     *
     * @param \td\CMBundle\Entity\WebshopPartner $webshopPartneri
     */
    public function removeWebshopPartneri(\td\CMBundle\Entity\WebshopPartner $webshopPartneri)
    {
        $this->webshopPartneri->removeElement($webshopPartneri);
    }

    /**
     * Get webshopPartneri
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWebshopPartneri()
    {
        return $this->webshopPartneri;
    }
}
