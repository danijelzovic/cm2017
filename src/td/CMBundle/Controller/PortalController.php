<?php

namespace td\CMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Response;
use td\CMBundle\Entity\Portal;
use Symfony\Component\HttpFoundation\Request;
use td\CMBundle\Form\PortalType;

class PortalController extends Controller
{
	/**
	 * @param $name
	 * @return Response
     */
	public function indexAction($name)
    {
		
        return $this->render('tdCMBundle:Default:index.html.twig', array('name' => $name));
    }

	/**
	 * Get list of all portals
	 *
	 * @param Request $request
	 * @return Response
	 * @throws \Doctrine\ORM\ORMException
     */
	public function index2Action(Request $request)
	{
		$group = $request->query->get('group');
		$em = $this->getDoctrine()->getManager();
		$viewType = $request->query->get('view_type');
		if(is_null($group)){
			$portals = $em->getRepository('tdCMBundle:Portal')->findAll();
		}else{
			$em2 = $this->get('doctrine.orm.entity_manager');
			$portals = $em->getRepository('tdCMBundle:Portal')->findByPortalGroup($em2->getReference('tdCMBundle:PortalGroup',$group));
		}
		$groups = $em->getRepository('tdCMBundle:PortalGroup')->findAll();

		return $this->render('@tdCM/Portal/index2.html.twig', array(
			'entities' => $portals,
			'groups' => $groups,
			'groupSelected' => $group,
			'viewType' => $viewType,
			));
	}

	/**
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
	public function kreirajAction(Request $request)
	{
		
		// create a task and give it some dummy data for this example
		$portal = new Portal();
		$portal->setNazivPortala('Upiši naziv portala');
		
		/*  STARO
		$form = $this->createFormBuilder($task)
			->add('task', 'text')
			->add('dueDate', 'date', array('widget' => 'choice', 'format' => 'dd.MM.yyyy'))
			->add('izvrsitelj', 'entity', array('class' => 'AcmeTaskBundle:Izvrsitelj', 'property' => 'naziv'))
			->add('save', 'submit', array('label' => 'Create Task'))
			->add('saveAndAdd', 'submit', array('label' => 'Save and Add'))
			->getForm();
		*/
		
		$form = $this->createForm(new PortalType(), $portal);
		
		$form->handleRequest($request);
		
		if($form->isValid()) {
			//perform some action, such as saving the task to the database
		
			$em = $this->getDoctrine()->getManager();
			$em->persist($portal);
			$em->flush();	
	
			$nextAction = $form->get('saveAndAdd')->isClicked()
				? 'td_cm_portal_kreiraj'
				: 'td_cm_portal_pregled';
				
			return $this->redirect($this->generateUrl($nextAction));
		}
		
		return $this->render('tdCMBundle:Portal:index.html.twig', array('form' => $form->createView()));
		
        //return $this->render('AcmeTaskBundle:Default:index.html.twig', array('name' => $name));
	}

	/**
	 * @param Portal $portal
	 * @return Response
     */
	public function showAction(Portal $portal)
	{
		$em = $this->getDoctrine()->getManager();
		$groups = $em->getRepository('tdCMBundle:PortalGroup')->findAllArray();
		return $this->render('@tdCM/Portal/show.html.twig',array(
			'portal' => $portal,
			'groups' => $groups,
			'portalPolje' => get_object_vars($portal),
		));
	}

	/**
	 * @param Portal $portal
	 * @return Response
     */
	public function show2Action(Portal $portal)
	{
		$form = $this->createDeleteForm($portal->getId());
		return $this->render('@tdCM/Portal/show2.html.twig',array(
			'entity'=>$portal,
			'delete_form' => $form->createView(),
		));
	}

	/**
	 * @return Response
     */
	public function createAction()
	{
		$em = $this->getDoctrine()->getManager();
		$groups = $em->getRepository('tdCMBundle:PortalGroup')->findAllArray();
		return $this->render('@tdCM/Portal/create.html.twig', array(
			'groups' => json_encode($groups),
		));
	}

	/**
	 * @return Response
     */
	public function newAction()
	{
		$entity = new Portal();
		$form = $this->createCreateForm($entity);

		return $this->render('@tdCM/Portal/new.html.twig', array(
			'entity' => $entity,
			'form' => $form->createView(),
		));
	}

	/**
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
	public function create2Action(Request $request)
	{
		$entity = new Portal();
		$form = $this->createCreateForm($entity);
		$form->handleRequest($request);

		if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$entity->upload();
			$em->persist($entity);
			$em->flush();

			return $this->redirect($this->generateUrl('portal_show2', array('id' => $entity->getId())));
		}

		return $this->render('tdCMBundle:Portal:new.html.twig', array(
			'entity' => $entity,
			'form'   => $form->createView(),
		));
	}

	/**
	 * @param $id
	 * @return Response
     */
	public function editAction($id)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('tdCMBundle:Portal')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Portal nije moguće pronaći.');
		}

		$editForm = $this->createEditForm($entity);
		$deleteForm = $this->createDeleteForm($id);

		return $this->render('tdCMBundle:Portal:edit.html.twig', array(
			'entity'      => $entity,
			'edit_form'   => $editForm->createView(),
			'delete_form' => $deleteForm->createView(),
		));
	}

	/**
	 * @param Request $request
	 * @param $id
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
	public function updateAction(Request $request, $id)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('tdCMBundle:Portal')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Portal nije moguće pronaći.');
		}

		$deleteForm = $this->createDeleteForm($id);
		$editForm = $this->createEditForm($entity);
		$editForm->handleRequest($request);

		if ($editForm->isValid()) {
			$entity->upload();
			$em->flush();

			return $this->redirect($this->generateUrl('portal_edit', array('id' => $id)));
		}

		return $this->render('tdCMBundle:Portal:edit.html.twig', array(
			'entity'      => $entity,
			'edit_form'   => $editForm->createView(),
			'delete_form' => $deleteForm->createView(),
		));
	}

	/**
	 * @param Request $request
	 * @param $id
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
	public function deleteAction(Request $request, $id)
	{
		$form = $this->createDeleteForm($id);
		$form->handleRequest($request);

		if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('tdCMBundle:Portal')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Portal nije moguće pronaći.');
			}

			$em->remove($entity);
			$em->flush();
		}

		return $this->redirect($this->generateUrl('portal'));
	}

	/**
	 * Get all portals for select
	 *
	 * @return Response
     */
	public function allPortalsSelectAction()
	{
		//Sigurnost
		$portals = $this->getDoctrine()->getManager()->getRepository('tdCMBundle:Portal')->findAll();
		$selectOptions = '<select><option value="">Svi</option>';
		foreach ($portals as $portal) {
			$selectOptions .= '<option value="' . $portal->getNaziv() . '">' . $portal->getNaziv() . '</option>';
		}
		$selectOptions .= '</select>';
		return Response::create($selectOptions);

	}

	/**
	 * @param Portal $entity
	 * @return \Symfony\Component\Form\Form
     */
	private function createCreateForm(Portal $entity)
	{
		$form = $this->createForm(new PortalType(), $entity, array(
			'action' => $this->generateUrl('portal_create2'),
			'method' => 'POST',
		));

		$form->add('submit', 'submit', array(
			'label' => 'Izradi',
			'attr' => array(
				'class' => 'btn-sm btn-success',
			)));

		return $form;
	}

	/**
	 * @param Portal $entity
	 * @return \Symfony\Component\Form\Form
     */
	private function createEditForm(Portal $entity)
	{
		$form = $this->createForm(new PortalType(), $entity, array(
			'action' => $this->generateUrl('portal_update', array('id' => $entity->getId())),
			'method' => 'PUT',
		));

		$form->add('submit', 'submit', array(
			'label' => 'Ažuriraj',
			'attr' => array(
				'class' => 'btn-primary btn-sm',
			)
		));

		return $form;
	}

	/**
	 * @param $id
	 * @return \Symfony\Component\Form\Form
     */
	private function createDeleteForm($id)
	{
		return $this->createFormBuilder()
			->setAction($this->generateUrl('portal_delete', array('id' => $id)))
			->setMethod('DELETE')
			->add('submit', 'submit', array(
				'label' => 'Obriši',
				'attr' => array(
					'class' => 'btn-danger btn-sm',
				)
			))
			->getForm();
	}
}
