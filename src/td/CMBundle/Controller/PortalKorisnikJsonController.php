<?php

namespace td\CMBundle\Controller;

//use Composer\Config\JsonConfigSource;
use DateTime;
use Doctrine\ORM\Query;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use JMS;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use td\CMBundle\Entity\Client;
use td\CMBundle\Entity\Portal;
use td\CMBundle\Entity\PortalKorisnik;
use td\CMBundle\Entity\UnitRate;
use td\CMBundle\Entity\SetupProperty;
use td\CMBundle\Entity\SetupPropertyUnit;
use td\CMBundle\Entity\Property;
use td\CMBundle\Entity\PropertyUnit;
use td\CMBundle\Entity\Webshop;
use td\CMBundle\Entity\WebshopProperty;
use td\CMBundle\Entity\WebshopPropertyUnit;
//use td\CMBundle\Form\Type\PortalKorisnikType;

class PortalKorisnikJsonController extends Controller
{

    private $idCompanyKorisnik = 1;
    private $menu;
    private $lanTranslation = 'hr';

    //Upravljanje sesijom - za sada se koristit cookies
    private function sesija()
    {
        $array = explode(":", $_COOKIE['tdapplogin']);
        if (crypt($array[0], 'he6765zfh') == $array[1]) {
            $this->idCompanyKorisnik = $array[4];
            $companyStatus = $array[6];
            $em = $this->getDoctrine()->getManager();
            $connection = $em->getConnection();
            $statement = $connection->prepare("SELECT menu_path FROM company_status WHERE id= :id");
            $statement->bindValue('id', $companyStatus);
            $statement->execute();
            $results = $statement->fetchAll();
            $row = $results[0];
            $this->menu = $row['menu_path'];
        } else {
            header("location: https://agencije.atrade.hr");
            exit;
        }
    }

    /**
     * DEPRECATED (BACKEND) - because list portals is not listed by connectivity anymore
     *
     * @return JsonResponse
     */
    public function portalsUsersConnectedJsonAction()
    {
        $portalsUsersConnected = $this->getDoctrine()->getManager()->getRepository('tdCMBundle:PortalKorisnik')->findByConnectedGrouped();
//        $serializer = JMS\Serializer\SerializerBuilder::create()->build();
//        $moguceStavkes = $serializer->serialize($portalsUsersConnected, 'json');
//        dump($portalsUsersConnected);
        return new JsonResponse($portalsUsersConnected);
    }

    /**
     * DEPRECATED (BACKEND) - because list portals is not listed by connectivity anymore
     *
     * @return JsonResponse
     */
    public function portalsUsersNotConnectedJsonAction()
    {
        $portalsUsersNotConnected = $this->getDoctrine()->getManager()->getRepository('tdCMBundle:PortalKorisnik')->findByNotConnectedGrouped();
        return new JsonResponse($portalsUsersNotConnected);
    }

    /**
     * Get portalKorisnik items for given portal
     *
     * @param Portal $portal
     * @return JsonResponse
     */
    public function portalKorisniciByPortalAction(Portal $portal)
    {
        //Sigurnost
        $portalKorisnici = $this->getDoctrine()->getManager()
            ->getRepository('tdCMBundle:PortalKorisnik')
            ->findByPortalCustom($portal);

        return new JsonResponse($portalKorisnici);
    }

    /**
     * @return JsonResponse
     */
    public function portalKorisniciAllAction()
    {
        $portalKorisnici = $this->getDoctrine()->getManager()
            ->getRepository('tdCMBundle:PortalKorisnik')
            ->findAllCustom();

        return new JsonResponse($portalKorisnici);
    }

    /**
     * ??
     *
     * @return JsonResponse
     */
    public function povezaniPortaliAction()
    {
        $this->provjeriPortalKorisniks();
        $em = $this->getDoctrine()->getManager();

        $povezaniPortali = $em->getRepository('tdCMBundle:Portal')->findPovezaniByIdCompanyKorisnik($this->idCompanyKorisnik);

        if (!$povezaniPortali) {
            throw $this->createNotFoundException(
                'Nema zapisa u tablici portalkorisnik'
            );
        }

        $response = new JsonResponse($povezaniPortali);
        //prikaz podataka u tablici
        return $response;
    }

    /**
     * ??
     *
     * @return JsonResponse
     */
    public function nepovezaniPortaliAction()
    {
        $this->provjeriPortalKorisniks();
        $em = $this->getDoctrine()->getManager();
        $nepovezaniPortali = $em->getRepository('tdCMBundle:Portal')->findNepovezaniByIdCompanyKorisnik($this->idCompanyKorisnik);
        return new JsonResponse($nepovezaniPortali);
    }

    /**
     * DEPRECATED
     *
     * @return JsonResponse
     */
    public function sviPortaliAction()
    {
        $this->provjeriPortalKorisniks();
        $em = $this->getDoctrine()->getManager();
        $sviPortali = $em->getRepository('tdCMBundle:Portal')->findSviByIdCompanyKorisnik($this->idCompanyKorisnik);
        return new JsonResponse($sviPortali);
    }

    /**
     * Bookertech - dohvat svih portala za danog/prijavljenog korisnika (company)
     *
     * @return JsonResponse
     */
    public function sviPortaliBTJsonAction()
    {
        $loginData = $this->get('td_cm.obrada.cookie_obrada');
        if ($loginData->obradaCookie()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $this->provjeriPortalKorisniks();
                $portals = $em->getRepository('tdCMBundle:Portal')->findSviByIdCompanyKorisnik(
                    $loginData->getIdCompanyKorisnik(),
                    $loginData->getLanTranslate());
            } catch (Exception $e) {
                $portals = array('Error!');
            }
        } else {
            $portals = array('Error!');
        }
        return new JsonResponse($portals);
    }

    /**
     * Change concetivity status of the portal
     *
     * @param Portal $portal
     * @return JsonResponse
     */
    public function changePortalStatusJsonAction(Portal $portal)
    {
        $loginData = $this->get('td_cm.obrada.cookie_obrada');
        $message = array(
            'status' => 'Error!',
            'message' => 'Input data is not valid or you have no rights!',
        );

        if ($loginData->obradaCookie()) {
            $request = $this->get('request');
            $dataJson = $request->getContent();
            $data = json_decode($dataJson, true);

            if (true == $data['povezan']) {
                $status = 1;
            } else {
                $status = 0;
            }
            $em = $this->getDoctrine()->getManager();
            $portalKorisnik = $em->getRepository('tdCMBundle:PortalKorisnik')->findOneBy(array(
                'portal' => $portal,
                'idCompanyKorisnik' => $loginData->getIdCompanyKorisnik(),
            ));
            if ($portalKorisnik) {
                $portalKorisnik->setPovezan($status);
                $em->persist($portalKorisnik);
                $em->flush();
//            $message = array(  DEBUG
//                'status' => 'ok',
//                'dataIn' => $status,
//                'dataInRaw' => $request->getContent(),
//            );
                $message = array(
                    'status' => 'ok',
                    'message' => 'Aktivacija portala ' . $portal->getNaziv() . ' je uspješna',
                );
            } else {
                $message['message'] = 'Došlo je do greške u sustavu. Greška će biti uklonjena u najkraćem mogućem roku.';
                //Akcija za slanje maila
            }

        }
        return new JsonResponse($message);
    }

    /**
     * Get Portal properties for formatting on front
     *
     * @param Portal $portal
     * @return JsonResponse
     */
    public function getPortalAction(Portal $portal)
    {
        $loginData = $this->get('td_cm.obrada.cookie_obrada');
        $message = array(
            'status' => 'Error!',
            'message' => 'Input data is not valid or you have no rights!',
        );

        if ($loginData->obradaCookie()) {
            $portalKorisnik = $this->getDoctrine()->getManager()->getRepository('tdCMBundle:PortalKorisnik')->findOneBy(array(
                'portal' => $portal,
                'idCompanyKorisnik' => $loginData->getIdCompanyKorisnik(),
            ));

            $message = array(
                'id' => $portal->getId(),
                'naziv' => $portal->getNaziv(),
                'link' => $portal->getLink(),
                'viewTypeConnect' => $portal->getViewTypeConnect(),
                'viewTypeSetup' => $portal->getViewTypeSetup(),
                'connectProperty' => $portal->getConnectProperty(),
                'connectPropertyUnit' => $portal->getConnectPropertyUnit(),
                'manageActivityProperty' => $portal->getManageActivityProperty(),
                'manageActivityPropertyUnit' => $portal->getManageActivityPropertyUnit(),
                'portalConnected' => $portalKorisnik->getPovezan(),
                'mustHaveWebshop' => $portal->getMustHaveWebshop(),
                'linkPropertyUnitOnPortal' => $portal->getLinkPropertyUnitOnPortal(),
                'settingsTemplate' => $portal->getSettingsTemplate(),
            );
        }
        return new JsonResponse($message);
    }

    /**
     * Handle request for portal activation becauese that portal must have a webshop
     *
     * @return JsonResponse
     * @internal param Portal $portal
     */
    public function activationRequestAction()
    {
        $loginData = $this->get('td_cm.obrada.cookie_obrada');
        $data = json_decode($this->get('request')->getContent(), true);

        $message = array(
            'status' => 'Error!',
            'message' => 'Input data is not valid or you have no rights!',
        );


        if ($loginData->obradaCookie()) {
            $em = $this->getDoctrine()->getManager();
            $portal = $em->getRepository('tdCMBundle:Portal')->find($data['id']);
            $portalKorisnik = $em->getRepository('tdCMBundle:PortalKorisnik')->findOneBy(array(
                'portal' => $portal,
                'idCompanyKorisnik' => $loginData->getIdCompanyKorisnik(),
            ));

            if ($portalKorisnik) {
                //Set pending status
                $portalKorisnik->setPending(true);
                $em->persist($portalKorisnik);
                $em->flush();
                //Send an email to admin
                $sendTo = $this->container->getParameter('mailer_send_to');
                $sendFrom = $this->container->getParameter('mailer_send_from');
                $mailMessage = \Swift_Message::newInstance()
                    ->setSubject('Zahjtev za aktivacijom portala ' . $portal->getNaziv() . ' za korisnika ' . $portalKorisnik->getIdCompanyKorisnik())
                    ->setFrom($sendFrom)
                    ->setTo($sendTo)
                    ->setBody('Poslan je zahjtev za aktivacijom portala ' . $portal->getNaziv() . ' za korisnika ' . $portalKorisnik->getIdCompanyKorisnik(), 'text/plain');
                $this->get('mailer')->send($mailMessage);
                $message = array(
                    'status' => 'ok',
                    'message' => 'Zahtjev je uspješno poslan',
                );
            }
        }

        return new JsonResponse($message);
    }

    private function provjeriPortalKorisniks()
    {
        $em = $this->getDoctrine()->getManager();
        $neprijavljeniPortali = $em->getRepository('tdCMBundle:Portal')->findNeprijavljeniPortali($this->get('td_cm.obrada.cookie_obrada')->getIdCompanyKorisnik());
        foreach ($neprijavljeniPortali as $np) {
            $this->get('td_cm.obrada.portalKorisnk_obrada')->createNewPortalKorisnik($np, $this->get('td_cm.obrada.cookie_obrada')->getIdCompanyKorisnik());
        }
    }

    /**
     * Save Webshop in PortalKorisnik
     *  NOT and create WebshopProperties and WebshopPropertyUnits if not exists
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function saveWebshopChangeAction(Request $request)
    {
        $loginData = $this->get('td_cm.obrada.cookie_obrada');
        $message = array(
            'status' => 'error',
            'message' => 'Greška',
        );

        if ($loginData->obradaCookie()) { // Check if user has rights
            $data = json_decode($request->getContent(), true);
            if (is_array($data)) { // Check if sent data is array
                if (array_key_exists('id', $data)) { // Check if key ID webshop exists in parameters
                    $em = $this->getDoctrine()->getManager();
                    $portalKorisnik = $em->getRepository('tdCMBundle:PortalKorisnik')->find($data['id']);
                    $webshop = $em->getRepository('tdCMBundle:Webshop')->find($data['idWebshopRates']);

                    $portalKorisnik->setWebshop($webshop);
                    $portalKorisnik->setPending(false);
                    $portalKorisnik->setPovezan(true);
                    $portalKorisnik->setCjenikKupovni($data['cjenikKupovni']);
                    $em->flush();

                    $message = array(
                        'status' => 'ok',
                        'message' => 'Webshop <b>[' . $webshop->getId() . ']' . $webshop->getTitle() .
                            '</b> je dodijeljen na portalKorisnik <b>' . $portalKorisnik->getId() . '</b>'
                    );


                } else { //Message if ID Webshop missing in parameters
                    $message['message'] = 'Neispravni paramteri.';
                }
            } else { //Message if can't get array from sent parameters
                $message['message'] = 'Poslani pdoaci nisu točni. (is_array($data)) ' . $request->getContent();
            }
        } else { //Mesagge if there is no rights
            $message['message'] = 'Nemate ovlaštenja za rad!';
        }

        return new JsonResponse($message);
    }

    /**
     * Change status Povezan
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function changeStatusPovezanAction(Request $request)
    {
        $loginData = $this->get('td_cm.obrada.cookie_obrada');
        $message = array(
            'status' => 'error',
            'message' => 'Greška',
        );

        if ($loginData->obradaCookie()) { // Check if user has rights
            $data = json_decode($request->getContent(), true);
            if (is_array($data)) { // Check if sent data is array
                if (array_key_exists('id', $data) && array_key_exists('povezan', $data)) { // Check if key ID webshop exists in parameters
                    $em = $this->getDoctrine()->getManager();
                    $portalKorisnik = $em->getRepository('tdCMBundle:PortalKorisnik')->find($data['id']);
                    $portalKorisnik->setPovezan($data['povezan']);
                    $portalKorisnik->setPending(false);
//
                    $message = array(
                        'status' => 'ok',
                        'message' => 'Status promijenjen'
                    );
                    $em->flush();

                } else { //Message if ID Webshop missing in parameters
                    $message['message'] = 'Neispravni paramteri.';
                }
            } else { //Message if can't get array from sent parameters
                $message['message'] = 'Poslani pdoaci nisu točni. (is_array($data)) ' . $request->getContent();
            }
        } else { //Mesagge if there is no rights
            $message['message'] = 'Nemate ovlaštenja za rad!';
        }

        return new JsonResponse($message);
    }

    /**
     * Search if company exists
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function searchCompanyAction(Request $request)
    {
        $loginData = $this->get('td_cm.obrada.cookie_obrada');
        $message = array(
            'status' => 'error',
            'message' => 'Greška',
        );

        if ($loginData->obradaCookie()) { // Check if user has rights
            $data = json_decode($request->getContent(), true);
            if (is_array($data)) { // Check if sent data is array
                if (array_key_exists('id', $data) && array_key_exists('portalId', $data)) { // Check if key ID Company exists in parameters
                    $em = $this->getDoctrine()->getManager();
                    if ($company = $em->getRepository('tdCMBundle:Client')->findOneByIdMojaTvrtka($data['id'])) {
                        if (is_null($portalKorisnk = $em->getRepository('tdCMBundle:PortalKorisnik')->findOneBy(array(
                            'portal' => $em->getReference('tdCMBundle:Portal', $data['portalId']),
                            'idCompanyKorisnik' => $data['id']
                        )))) {
                            $message = array(
                                'status' => 'ok',
                                'message' => 'Pronađen je comapny <br> <b>[' . $company->getIdMojaTvrtka() . '] ' . $company->getNaziv() . '</b>',
                                'data' => array(
                                    'id' => $company->getIdMojaTvrtka(),
                                    'naziv' => $company->getNaziv(),
                                )
                            );
                        } else {
                            $message['message'] = 'Portal je već prijavljen za ovog korisnka!';
                        }
                    } else {
                        $message['message'] = 'Company ne postoji!';
                    }
                } else { //Message if ID Webshop missing in parameters
                    $message['message'] = 'Neispravni paramteri.';
                }
            } else { //Message if can't get array from sent parameters
                $message['message'] = 'Poslani pdoaci nisu točni. (is_array($data)) ' . $request->getContent();
            }
        } else { //Mesagge if there is no rights
            $message['message'] = 'Nemate ovlaštenja za rad!';
        }

        return new JsonResponse($message);
    }

    /**
     * Add comapny to portal and create new portalKorisnik item
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function addCompanyAction(Portal $portal, Request $request)
    {
        $loginData = $this->get('td_cm.obrada.cookie_obrada');
        $message = array(
            'status' => 'error',
            'message' => 'Greška',
        );

//        $portal = new Portal();
//        $request = $this->get('request');
        if ($loginData->obradaCookie()) { // Check if user has rights
            $data = json_decode($request->getContent(), true);
            if (is_array($data)) { // Check if sent data is array
                if (array_key_exists('id', $data)) { // Check if key ID Company exists in parameters
                    $em = $this->getDoctrine()->getManager();
                    /* @var $company Client */
                    if ($company = $em->getRepository('tdCMBundle:Client')->findOneBy(array(
                        'idMojaTvrtka' => $data['id']
                    ))
                    ) {
//                        return new JsonResponse(array(
//                            'id' => $data['id'],
//                            'company' => $company->getIdMojaTvrtka(),
//                            'portal' => $portal->getNaziv(),
//                        ));
                        $portalKorisnik = $this->get('td_cm.obrada.portalkorisnik_obrada')->createNewPortalKorisnik($portal, $company->getIdMojaTvrtka());
                        $portalKorisnik->setPending(true);
                        $em->persist($portalKorisnik);
                        $em->flush();
                        $message = array(
                            'status' => 'ok',
                            'message' => 'Prijavljen je portal <b>' . $portal->getNaziv() . '</b> za korisnika <b>[' . $company->getIdMojaTvrtka() . '] ' .
                                $company->getNaziv() . '</b>. <br>Šifra veze: <b>' . $portalKorisnik->getId() . '</b>',
                            'data' => array(
                                'id' => $portalKorisnik->getId(),
                                'idCompanyKorisnik' => $portalKorisnik->getIdCompanyKorisnik(),
                                'nazivCompanyKorisnik' => $company->getNaziv(),
                                'idWebshopRates' => $portalKorisnik->getIdWebshopRates(),
                                'pending' => $portalKorisnik->getPending(),
                                'povezan' => $portalKorisnik->getPovezan(),
                            )
                        );
                    } else {
                        $message['message'] = 'Company ne postoji!';
                    }
                } else { //Message if ID Webshop missing in parameters
                    $message['message'] = 'Neispravni paramteri.';
                }
            } else { //Message if can't get array from sent parameters
                $message['message'] = 'Poslani pdoaci nisu točni. (is_array($data)) ' . $request->getContent();
            }
        } else { //Mesagge if there is no rights
            $message['message'] = 'Nemate ovlaštenja za rad!';
        }

        return new JsonResponse($message);
    }

    /**
     *  Create portalKorisnik and create setupProperty and/or setupPropertyUnit
     *    If parameter propertyId is given then create portalKorisnik and setupProperty
     *    If parameter propertyUnitId is given then create portalKorisnik (if not exist),
     *    setupProperty (if not exist) and setupPropertyUnit
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function createPortalKorisnikJsonAction(Request $request)
    {
        $loginData = $this->get('td_cm.obrada.cookie_obrada');
        $message = array(
            'status' => 'error',
            'message' => 'Greška',
        );

//        $portal = new Portal();
//        $request = $this->get('request');
        if ($loginData->obradaCookie()) { // Check if user has rights
            $data = json_decode($request->getContent(), true);
            if (is_array($data)) { // Check if sent data is array
                if (array_key_exists('webshopId', $data) and
                    (array_key_exists('portalId', $data) or array_key_exists('portalUnitId', $data)) and
                    array_key_exists('propertyId', $data)
                ) { // Check if key parameters exists
                    $em = $this->getDoctrine()->getManager();

                    // Get Objects
                    $property = null;
                    $propertyUnit = null;
                    if (array_key_exists('portalId', $data)) {
                        $property = $em->getRepository('tdCMBundle:Property')->find($data['propertyId']);
                    } else {
                        $propertyUnit = $em->getRepository('tdCMBundle:PropertyUnit')->find($data['propertyUnitId']);
                        $property = $propertyUnit->getProperty();
                    }
                    $portal = $em->getRepository('tdCMBundle:Portal')->find($data['portalId']);
                    $webshop = $em->getRepository('tdCMBundle:Webshop')->find($data['webshopId']);

                    /* @var $company Client */
                    if ($company = $em->getRepository('tdCMBundle:Client')->findOneByIdMojaTvrtka($property->getIdAgent())) {
                        // Create portalKorisnik if not exist and set active
                        $portalKorisnik = $this->get('td_cm.obrada.portalkorisnik_obrada')->createNewPortalKorisnik($portal, $property->getIdAgent(), $webshop);
                        $portalKorisnik->setPovezan(true);
                        $em->persist($portalKorisnik);
                        $em->flush();


                        // Create setupProperty if not exist
                        $setupProperty = $this->get('td_cm.obrada.setup_obrada')->createSetupProperty($property, $portalKorisnik);

                        if (!is_null($propertyUnit)) { // Create setupPropertyUnit if not exist
                            $setupPropertyUnit = $this->get('td_cm.obrada.setup_obrada')->createSetupPropertyUnit($propertyUnit, $portalKorisnik);
                            $setupPropertyUnitId = $setupPropertyUnit->getId();
                        }else{
                            $setupPropertyUnitId = 0;
                        }

                        $message = array(
                            'status' => 'ok',
                            'message' => 'Prijavljen je portal <b>' . $portal->getNaziv() . '</b> za korisnika <b>[' . $company->getIdMojaTvrtka() . '] ' .
                                $company->getNaziv() . '</b>. <br>Šifra veze: <b>' . $portalKorisnik->getId() . '</b>',
                            'data' => array(
                                'id' => $portalKorisnik->getId(),
                                'idCompanyKorisnik' => $portalKorisnik->getIdCompanyKorisnik(),
                                'nazivCompanyKorisnik' => $company->getNaziv(),
                                'idWebshopRates' => $portalKorisnik->getIdWebshopRates(),
                                'setupPropertyId' => $setupProperty->getId(),
                                'setupPropertyUnitId' => $setupPropertyUnitId,
                            )
                        );
                    } else {
                        $message['message'] = 'Company ne postoji!';
                    }
                } else { //Message if ID Webshop missing in parameters
                    $message['message'] = 'Neispravni paramteri.';
                }
            } else { //Message if can't get array from sent parameters
                $message['message'] = 'Poslani pdoaci nisu točni. (is_array($data)) ' . $request->getContent();
            }
        } else { //Mesagge if there is no rights
            $message['message'] = 'Nemate ovlaštenja za rad!';
        }

        return new JsonResponse($message);
    }

    public function verifyPortalKorisnikJsonAction(Request $request)
    {
        $loginData = $this->get('td_cm.obrada.cookie_obrada');
        $message = array(
            'status' => 'error',
            'message' => 'Greška',
        );

//        $portal = new Portal();
//        $request = $this->get('request');
        if ($loginData->obradaCookie()) { // Check if user has rights
            $data = json_decode($request->getContent(), true);
            if (is_array($data)) { // Check if sent data is array
                if (
                    array_key_exists('portalId', $data) and
                    (array_key_exists('propertyId', $data) or array_key_exists('propertyUnitId', $data))
                ) { // Check if key parameters exists
                    $em = $this->getDoctrine()->getManager();

                    // Get Objects
                    if (array_key_exists('propertyId', $data)) {
                        $property = $em->getRepository('tdCMBundle:Property')->find($data['propertyId']);
                    } else {
                        $propertyUnit = $em->getRepository('tdCMBundle:PropertyUnit')->find($data['propertyUnitId']);
                        $property = $propertyUnit->getProperty();
                    }
                    $portal = $em->getRepository('tdCMBundle:Portal')->find($data['portalId']);

                    $portalKorisnik = $em->getRepository('tdCMBundle:PortalKorisnik')->findOneBy(array(
                        'idCompanyKorisnik' => $property->getIdAgent(),
                        'portal' => $portal
                    ));

                    if (is_null($portalKorisnik)) {
                        $message = array(
                            'status' => 'ok',
                            'message' => 'Potrebno je izraditi novu vezu portalKorisnik.',
                            'data' => array(
                                'portalKorisnikExist' => false,
                            )
                        );
                    } else {
                        $message = array(
                            'status' => 'ok',
                            'message' => 'Portalkorisnik postoji.',
                            'data' => array(
                                'portalKorisnikExist' => true,
                            )
                        );
                    }


                } else { //Message if ID Webshop missing in parameters
                    $message['message'] = 'Neispravni paramteri.';
                }
            } else { //Message if can't get array from sent parameters
                $message['message'] = 'Poslani pdoaci nisu točni. (is_array($data)) ' . $request->getContent();
            }
        } else { //Mesagge if there is no rights
            $message['message'] = 'Nemate ovlaštenja za rad!';
        }

        return new JsonResponse($message);
    }

    /**
     * Create WebshopProeprties and WebshopPropertyUnits if not exist by given Webshop and user in PortalKorisnik
     *
     * @param Webshop $webshop
     * @param PortalKorisnik $portalKorisnik
     * @return array
     */
    private function createWebshopPropertiesUnits(Webshop $webshop, PortalKorisnik $portalKorisnik)
    {
        //Odrediti Property i Property Unite

        // Initialization
        $loginData = $this->get('td_cm.obrada.cookie_obrada');
        $message = array(
            'status' => 'error',
            'message' => 'Greška kod kreiranja WebshopProperty i WebshopPropertyUnit',
        );
        $em = $this->getDoctrine()->getManager();
        $setupObrada = $this->get('td_cm.obrada.setup_obrada');

        // Get properties which not exists in WebshopProperty
        $properties = $em->getRepository('tdCMBundle:Property')->createQueryBuilder('p')
            ->where('p.deleted = :deleted')
            ->andWhere('p.idCompany = :idCompanyKorisnik OR p.idAgent = :idCompanyKorisnik')
            ->andWhere('p.id NOT IN (SELECT wp.idProperty FROM tdCMBundle:WebshopProperty wp WHERE wp.disabled = false AND wp.webshop = :webshop)')
            ->setParameter('deleted', 0)
            ->setParameter('idCompanyKorisnik', $portalKorisnik->getIdCompanyKorisnik())
            ->setParameter('webshop', $webshop)
            ->setMaxResults(1000)
            ->getQuery()
            ->getResult();

        $message['message'] = '';

        /* @var Property $property */
        foreach ($properties as $property) {
            $webshopProperty = new WebshopProperty();
            $webshopProperty->setProperty($property);
            $webshopProperty->setWebshop($webshop);
            $webshopProperty->setApproved(true);
            $webshopProperty->setApprovedDate(new DateTime());
            $webshopProperty->setDisabled(false);
            $webshopProperty->setApprovedBy($loginData->getIdCurrentUserClient());
            $em->persist($webshopProperty);

            /* @var PropertyUnit $propertyUnit */
            foreach ($property->getPropertyUnits() as $propertyUnit) {
                if (0 == $propertyUnit->getDeleted()) { //Create WebshopProperty only for not deleted propertyUnit
                    $idHead = $setupObrada->getIdHeadBookerTech($propertyUnit, $portalKorisnik->getIdCompanyKorisnik());
                    if (-1 == $idHead) {
                        $idHead = 0;
                        $message['message'] = 'Za propertyUnit ' . $propertyUnit->getPropertyUnitFront() . ' nije bilo moguće odrediti cjenik.';
                    }
                    $webshopPropertyUnit = new WebshopPropertyUnit();
                    $webshopPropertyUnit->setWebshop($webshop);
                    $webshopPropertyUnit->setProperty($property);
                    $webshopPropertyUnit->setPropertyUnit($propertyUnit);
                    $webshopPropertyUnit->setCjenikProdajni($idHead); // Ovo dovršiti
                    $webshopPropertyUnit->setApprovedBy($loginData->getIdCurrentUserClient());
                    $em->persist($webshopPropertyUnit);
                }
            }
            $em->flush();

        }
        $message['status'] = 'ok';
        $message['message'] = $message['message'] . 'Promjena je uspješno izvršena za ID portal korisnik ' . $portalKorisnik->getId();
        return $message;
    }
}
