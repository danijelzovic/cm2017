<?php

namespace td\CMBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use td\CMBundle\Entity\SetupPropertyUnitLog;

/**
 * SetupPropertyUnitLog controller.
 *
 */
class SetupPropertyUnitLogController extends Controller
{

    /**
     * Lists all SetupPropertyUnitLog entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('tdCMBundle:SetupPropertyUnitLog')->findAll();

        return $this->render('tdCMBundle:SetupPropertyUnitLog:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a SetupPropertyUnitLog entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('tdCMBundle:SetupPropertyUnitLog')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SetupPropertyUnitLog entity.');
        }

        return $this->render('tdCMBundle:SetupPropertyUnitLog:show.html.twig', array(
            'entity'      => $entity,
        ));
    }
}
