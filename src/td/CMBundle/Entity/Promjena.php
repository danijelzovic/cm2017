<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Exclude;

/**
 * Promjena
 *
 * @ORM\Table(name="cm_promjena")
 * @ORM\Entity(repositoryClass="td\CMBundle\Entity\PromjenaRepository")
 */
class Promjena
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="grupa_promjene", type="string", length=255)
     */
    private $grupaPromjene;

    /**
     * @var string
     *
     * @ORM\Column(name="tip_promjene", type="string", length=255)
     */
    private $tipPromjene;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_company", type="integer")
     */
    private $idCompany;

    /**
     * @var string
     *
     * @ORM\Column(name="unit_code", type="string", length=255)
     */
    private $unitCode;

    /**
     * @ORM\ManyToOne(targetEntity="PropertyUnit")
     * @ORM\JoinColumn(name="id_unit", referencedColumnName="id")
     * @Exclude
     **/
    private $propertyUnit;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_from", type="date")
     */
    private $dateFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_to", type="date")
     */
    private $dateTo;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_reservation", type="integer")
     */
    private $idReservation;

    /**
     * @ORM\ManyToOne(targetEntity="Webshop")
     * @ORM\JoinColumn(name="id_webshop", referencedColumnName="id", onDelete="CASCADE")
     * @Exclude
     */
    private $webshop;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_pricelist", type="integer")
     */
    private $idPricelist;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datumvrijeme", type="datetime")
     */
    private $datumvrijeme;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_user_client", type="integer")
     */
    private $idUserClient;

    /**
     * @ORM\ManyToOne(targetEntity="Client")
     * @ORM\JoinColumn(name="id_user_client", referencedColumnName="id_client")
     * @Exclude
     **/
    private $userClient;

    /**
     * @var string
     *
     * @ORM\Column(name="mjesto_nastanka", type="string", length=255)
     */
    private $mjestoNastanka;

    /**
     * @ORM\OneToMany(targetEntity="Transfer", mappedBy="promjena")
     */
    private $transferi;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->transferi = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set grupaPromjene
     *
     * @param string $grupaPromjene
     *
     * @return Promjena
     */
    public function setGrupaPromjene($grupaPromjene)
    {
        $this->grupaPromjene = $grupaPromjene;

        return $this;
    }

    /**
     * Get grupaPromjene
     *
     * @return string
     */
    public function getGrupaPromjene()
    {
        return $this->grupaPromjene;
    }

    /**
     * Set tipPromjene
     *
     * @param string $tipPromjene
     *
     * @return Promjena
     */
    public function setTipPromjene($tipPromjene)
    {
        $this->tipPromjene = $tipPromjene;

        return $this;
    }

    /**
     * Get tipPromjene
     *
     * @return string
     */
    public function getTipPromjene()
    {
        return $this->tipPromjene;
    }

    /**
     * Set idCompany
     *
     * @param integer $idCompany
     *
     * @return Promjena
     */
    public function setIdCompany($idCompany)
    {
        $this->idCompany = $idCompany;

        return $this;
    }

    /**
     * Get idCompany
     *
     * @return integer
     */
    public function getIdCompany()
    {
        return $this->idCompany;
    }

    /**
     * Set unitCode
     *
     * @param string $unitCode
     *
     * @return Promjena
     */
    public function setUnitCode($unitCode)
    {
        $this->unitCode = $unitCode;

        return $this;
    }

    /**
     * Get unitCode
     *
     * @return string
     */
    public function getUnitCode()
    {
        return $this->unitCode;
    }

    /**
     * Set dateFrom
     *
     * @param \DateTime $dateFrom
     *
     * @return Promjena
     */
    public function setDateFrom($dateFrom)
    {
        $this->dateFrom = $dateFrom;

        return $this;
    }

    /**
     * Get dateFrom
     *
     * @return \DateTime
     */
    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    /**
     * Set dateTo
     *
     * @param \DateTime $dateTo
     *
     * @return Promjena
     */
    public function setDateTo($dateTo)
    {
        $this->dateTo = $dateTo;

        return $this;
    }

    /**
     * Get dateTo
     *
     * @return \DateTime
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * Set idReservation
     *
     * @param integer $idReservation
     *
     * @return Promjena
     */
    public function setIdReservation($idReservation)
    {
        $this->idReservation = $idReservation;

        return $this;
    }

    /**
     * Get idReservation
     *
     * @return integer
     */
    public function getIdReservation()
    {
        return $this->idReservation;
    }

    /**
     * Set idPricelist
     *
     * @param integer $idPricelist
     *
     * @return Promjena
     */
    public function setIdPricelist($idPricelist)
    {
        $this->idPricelist = $idPricelist;

        return $this;
    }

    /**
     * Get idPricelist
     *
     * @return integer
     */
    public function getIdPricelist()
    {
        return $this->idPricelist;
    }

    /**
     * Set datumvrijeme
     *
     * @param \DateTime $datumvrijeme
     *
     * @return Promjena
     */
    public function setDatumvrijeme($datumvrijeme)
    {
        $this->datumvrijeme = $datumvrijeme;

        return $this;
    }

    /**
     * Get datumvrijeme
     *
     * @return \DateTime
     */
    public function getDatumvrijeme()
    {
        return $this->datumvrijeme;
    }

    /**
     * Set idUserClient
     *
     * @param integer $idUserClient
     *
     * @return Promjena
     */
    public function setIdUserClient($idUserClient)
    {
        $this->idUserClient = $idUserClient;

        return $this;
    }

    /**
     * Get idUserClient
     *
     * @return integer
     */
    public function getIdUserClient()
    {
        return $this->idUserClient;
    }

    /**
     * Set mjestoNastanka
     *
     * @param string $mjestoNastanka
     *
     * @return Promjena
     */
    public function setMjestoNastanka($mjestoNastanka)
    {
        $this->mjestoNastanka = $mjestoNastanka;

        return $this;
    }

    /**
     * Get mjestoNastanka
     *
     * @return string
     */
    public function getMjestoNastanka()
    {
        return $this->mjestoNastanka;
    }

    /**
     * Set propertyUnit
     *
     * @param \td\CMBundle\Entity\PropertyUnit $propertyUnit
     *
     * @return Promjena
     */
    public function setPropertyUnit(\td\CMBundle\Entity\PropertyUnit $propertyUnit = null)
    {
        $this->propertyUnit = $propertyUnit;

        return $this;
    }

    /**
     * Get propertyUnit
     *
     * @return \td\CMBundle\Entity\PropertyUnit
     */
    public function getPropertyUnit()
    {
        return $this->propertyUnit;
    }

    /**
     * Set webshop
     *
     * @param \td\CMBundle\Entity\Webshop $webshop
     *
     * @return Promjena
     */
    public function setWebshop(\td\CMBundle\Entity\Webshop $webshop = null)
    {
        $this->webshop = $webshop;

        return $this;
    }

    /**
     * Get webshop
     *
     * @return \td\CMBundle\Entity\Webshop
     */
    public function getWebshop()
    {
        return $this->webshop;
    }

    /**
     * Set userClient
     *
     * @param \td\CMBundle\Entity\Client $userClient
     *
     * @return Promjena
     */
    public function setUserClient(\td\CMBundle\Entity\Client $userClient = null)
    {
        $this->userClient = $userClient;

        return $this;
    }

    /**
     * Get userClient
     *
     * @return \td\CMBundle\Entity\Client
     */
    public function getUserClient()
    {
        return $this->userClient;
    }

    /**
     * Add transferi
     *
     * @param \td\CMBundle\Entity\Transfer $transferi
     *
     * @return Promjena
     */
    public function addTransferi(\td\CMBundle\Entity\Transfer $transferi)
    {
        $this->transferi[] = $transferi;

        return $this;
    }

    /**
     * Remove transferi
     *
     * @param \td\CMBundle\Entity\Transfer $transferi
     */
    public function removeTransferi(\td\CMBundle\Entity\Transfer $transferi)
    {
        $this->transferi->removeElement($transferi);
    }

    /**
     * Get transferi
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransferi()
    {
        return $this->transferi;
    }
}
