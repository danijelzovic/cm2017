<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WebshopProperty
 *
 * @ORM\Table(name="webshop_property", uniqueConstraints={@ORM\UniqueConstraint(name="id_shop", columns={"id_shop", "id_property"})}, indexes={@ORM\Index(name="online", columns={"online"}), @ORM\Index(name="approved", columns={"approved"}), @ORM\Index(name="disabled", columns={"disabled"}), @ORM\Index(name="id_property", columns={"id_property"}), @ORM\Index(name="inquiry", columns={"inquiry"}), @ORM\Index(name="rank_country", columns={"rank_country"})})
 * @ORM\Entity
 */
class WebshopProperty
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_shop", type="integer", nullable=false)
     */
    private $idShop = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webshop")
     * @ORM\JoinColumn(name="id_shop", referencedColumnName="id", onDelete="CASCADE")
     */
    private $webshop;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id_property", type="integer", nullable=false)
     */
    private $idProperty = null;

    /**
     * @ORM\ManyToOne(targetEntity="Property")
     * @ORM\JoinColumn(name="id_property", referencedColumnName="id")
     **/
    private $property;
    
    /**
     * @var string
     *
     * @ORM\Column(name="comm_title_hr", type="string", length=64, nullable=false)
     */
    private $commTitleHr = '';

    /**
     * @var string
     *
     * @ORM\Column(name="comm_title_en", type="string", length=64, nullable=false)
     */
    private $commTitleEn = '';

    /**
     * @var string
     *
     * @ORM\Column(name="comm_title_de", type="string", length=64, nullable=false)
     */
    private $commTitleDe = '';

    /**
     * @var string
     *
     * @ORM\Column(name="comm_title_it", type="string", length=64, nullable=false)
     */
    private $commTitleIt = '';

    /**
     * @var string
     *
     * @ORM\Column(name="comm_title_si", type="string", length=64, nullable=false)
     */
    private $commTitleSi = '';

    /**
     * @var string
     *
     * @ORM\Column(name="comm_title_ru", type="string", length=64, nullable=false)
     */
    private $commTitleRu = '';

    /**
     * @var string
     *
     * @ORM\Column(name="email_upiti", type="string", length=255, nullable=false)
     */
    private $emailUpiti = '';

    /**
     * @var string
     *
     * @ORM\Column(name="email_rezervacije", type="string", length=255, nullable=false)
     */
    private $emailRezervacije = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="online", type="boolean", nullable=false)
     */
    private $online = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="inquiry", type="boolean", nullable=false)
     */
    private $inquiry = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="card", type="boolean", nullable=false)
     */
    private $card = false;

    /**
     * @var string
     *
     * @ORM\Column(name="sms1_res", type="string", length=32, nullable=false)
     */
    private $sms1Res = '';

    /**
     * @var string
     *
     * @ORM\Column(name="sms2_res", type="string", length=32, nullable=false)
     */
    private $sms2Res = '';

    /**
     * @var string
     *
     * @ORM\Column(name="sms1_inq", type="string", length=32, nullable=false)
     */
    private $sms1Inq = '';

    /**
     * @var string
     *
     * @ORM\Column(name="sms2_inq", type="string", length=32, nullable=false)
     */
    private $sms2Inq = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="disabled", type="boolean", nullable=false)
     */
    private $disabled = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="approved", type="boolean", nullable=true)
     */
    private $approved;

    /**
     * @var integer
     *
     * @ORM\Column(name="approved_by", type="integer", nullable=false)
     */
    private $approvedBy = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="approved_date", type="datetime", nullable=false)
     */
    private $approvedDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="rank_country", type="smallint", nullable=false)
     */
    private $rankCountry = 999;

    /**
     * @var integer
     *
     * @ORM\Column(name="rank_region", type="smallint", nullable=false)
     */
    private $rankRegion = 999;

    /**
     * @var integer
     *
     * @ORM\Column(name="rank_place", type="smallint", nullable=false)
     */
    private $rankPlace = 999;

    /**
     * @var integer
     *
     * @ORM\Column(name="contact_id", type="integer", nullable=false)
     */
    private $contactId = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_hr", type="text", nullable=false)
     */
    private $contactHr = '';

    /**
     * @var string
     *
     * @ORM\Column(name="contact_en", type="text", nullable=false)
     */
    private $contactEn = '';

    /**
     * @var string
     *
     * @ORM\Column(name="contact_de", type="text", nullable=false)
     */
    private $contactDe = '';

    /**
     * @var string
     *
     * @ORM\Column(name="contact_it", type="text", nullable=false)
     */
    private $contactIt = '';

    /**
     * @var string
     *
     * @ORM\Column(name="contact_si", type="text", nullable=false)
     */
    private $contactSi = '';

    /**
     * @var string
     *
     * @ORM\Column(name="contact_ru", type="text", nullable=false)
     */
    private $contactRu = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="pp_webshop", type="integer", nullable=false)
     */
    private $ppWebshop = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="pp_provision", type="decimal", precision=4, scale=2, nullable=false)
     */
    private $ppProvision = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="pp_fix_amount", type="decimal", precision=6, scale=2, nullable=false)
     */
    private $ppFixAmount = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="b2b_to_b2c", type="decimal", precision=6, scale=2, nullable=false)
     */
    private $b2bToB2c = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="b2c_zaokruzivanje", type="boolean", nullable=false)
     */
    private $b2cZaokruzivanje = false;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_company_partner", type="integer", nullable=false)
     */
    private $idCompanyPartner = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_webshop_partner", type="integer", nullable=false)
     */
    private $idWebshopPartner = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="brisi", type="boolean", nullable=false)
     */
    private $brisi = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="atraveo", type="boolean", nullable=false)
     */
    private $atraveo = false;



    /**
     * Set idShop
     *
     * @param integer $idShop
     * @return WebshopProperty
     */
    public function setIdShop($idShop)
    {
        $this->idShop = $idShop;

        return $this;
    }

    /**
     * Get idShop
     *
     * @return integer 
     */
    public function getIdShop()
    {
        return $this->idShop;
    }

    /**
     * Set idProperty
     *
     * @param integer $idProperty
     * @return WebshopProperty
     */
    public function setIdProperty($idProperty)
    {
        $this->idProperty = $idProperty;

        return $this;
    }

    /**
     * Get idProperty
     *
     * @return integer 
     */
    public function getIdProperty()
    {
        return $this->idProperty;
    }

    /**
     * Set commTitleHr
     *
     * @param string $commTitleHr
     * @return WebshopProperty
     */
    public function setCommTitleHr($commTitleHr)
    {
        $this->commTitleHr = $commTitleHr;

        return $this;
    }

    /**
     * Get commTitleHr
     *
     * @return string 
     */
    public function getCommTitleHr()
    {
        return $this->commTitleHr;
    }

    /**
     * Set commTitleEn
     *
     * @param string $commTitleEn
     * @return WebshopProperty
     */
    public function setCommTitleEn($commTitleEn)
    {
        $this->commTitleEn = $commTitleEn;

        return $this;
    }

    /**
     * Get commTitleEn
     *
     * @return string 
     */
    public function getCommTitleEn()
    {
        return $this->commTitleEn;
    }

    /**
     * Set commTitleDe
     *
     * @param string $commTitleDe
     * @return WebshopProperty
     */
    public function setCommTitleDe($commTitleDe)
    {
        $this->commTitleDe = $commTitleDe;

        return $this;
    }

    /**
     * Get commTitleDe
     *
     * @return string 
     */
    public function getCommTitleDe()
    {
        return $this->commTitleDe;
    }

    /**
     * Set commTitleIt
     *
     * @param string $commTitleIt
     * @return WebshopProperty
     */
    public function setCommTitleIt($commTitleIt)
    {
        $this->commTitleIt = $commTitleIt;

        return $this;
    }

    /**
     * Get commTitleIt
     *
     * @return string 
     */
    public function getCommTitleIt()
    {
        return $this->commTitleIt;
    }

    /**
     * Set commTitleSi
     *
     * @param string $commTitleSi
     * @return WebshopProperty
     */
    public function setCommTitleSi($commTitleSi)
    {
        $this->commTitleSi = $commTitleSi;

        return $this;
    }

    /**
     * Get commTitleSi
     *
     * @return string 
     */
    public function getCommTitleSi()
    {
        return $this->commTitleSi;
    }

    /**
     * Set commTitleRu
     *
     * @param string $commTitleRu
     * @return WebshopProperty
     */
    public function setCommTitleRu($commTitleRu)
    {
        $this->commTitleRu = $commTitleRu;

        return $this;
    }

    /**
     * Get commTitleRu
     *
     * @return string 
     */
    public function getCommTitleRu()
    {
        return $this->commTitleRu;
    }

    /**
     * Set emailUpiti
     *
     * @param string $emailUpiti
     * @return WebshopProperty
     */
    public function setEmailUpiti($emailUpiti)
    {
        $this->emailUpiti = $emailUpiti;

        return $this;
    }

    /**
     * Get emailUpiti
     *
     * @return string 
     */
    public function getEmailUpiti()
    {
        return $this->emailUpiti;
    }

    /**
     * Set emailRezervacije
     *
     * @param string $emailRezervacije
     * @return WebshopProperty
     */
    public function setEmailRezervacije($emailRezervacije)
    {
        $this->emailRezervacije = $emailRezervacije;

        return $this;
    }

    /**
     * Get emailRezervacije
     *
     * @return string 
     */
    public function getEmailRezervacije()
    {
        return $this->emailRezervacije;
    }

    /**
     * Set online
     *
     * @param boolean $online
     * @return WebshopProperty
     */
    public function setOnline($online)
    {
        $this->online = $online;

        return $this;
    }

    /**
     * Get online
     *
     * @return boolean 
     */
    public function getOnline()
    {
        return $this->online;
    }

    /**
     * Set inquiry
     *
     * @param boolean $inquiry
     * @return WebshopProperty
     */
    public function setInquiry($inquiry)
    {
        $this->inquiry = $inquiry;

        return $this;
    }

    /**
     * Get inquiry
     *
     * @return boolean 
     */
    public function getInquiry()
    {
        return $this->inquiry;
    }

    /**
     * Set card
     *
     * @param boolean $card
     * @return WebshopProperty
     */
    public function setCard($card)
    {
        $this->card = $card;

        return $this;
    }

    /**
     * Get card
     *
     * @return boolean 
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
     * Set sms1Res
     *
     * @param string $sms1Res
     * @return WebshopProperty
     */
    public function setSms1Res($sms1Res)
    {
        $this->sms1Res = $sms1Res;

        return $this;
    }

    /**
     * Get sms1Res
     *
     * @return string 
     */
    public function getSms1Res()
    {
        return $this->sms1Res;
    }

    /**
     * Set sms2Res
     *
     * @param string $sms2Res
     * @return WebshopProperty
     */
    public function setSms2Res($sms2Res)
    {
        $this->sms2Res = $sms2Res;

        return $this;
    }

    /**
     * Get sms2Res
     *
     * @return string 
     */
    public function getSms2Res()
    {
        return $this->sms2Res;
    }

    /**
     * Set sms1Inq
     *
     * @param string $sms1Inq
     * @return WebshopProperty
     */
    public function setSms1Inq($sms1Inq)
    {
        $this->sms1Inq = $sms1Inq;

        return $this;
    }

    /**
     * Get sms1Inq
     *
     * @return string 
     */
    public function getSms1Inq()
    {
        return $this->sms1Inq;
    }

    /**
     * Set sms2Inq
     *
     * @param string $sms2Inq
     * @return WebshopProperty
     */
    public function setSms2Inq($sms2Inq)
    {
        $this->sms2Inq = $sms2Inq;

        return $this;
    }

    /**
     * Get sms2Inq
     *
     * @return string 
     */
    public function getSms2Inq()
    {
        return $this->sms2Inq;
    }

    /**
     * Set disabled
     *
     * @param boolean $disabled
     * @return WebshopProperty
     */
    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;

        return $this;
    }

    /**
     * Get disabled
     *
     * @return boolean 
     */
    public function getDisabled()
    {
        return $this->disabled;
    }

    /**
     * Set approved
     *
     * @param boolean $approved
     * @return WebshopProperty
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;

        return $this;
    }

    /**
     * Get approved
     *
     * @return boolean 
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * Set approvedBy
     *
     * @param integer $approvedBy
     * @return WebshopProperty
     */
    public function setApprovedBy($approvedBy)
    {
        $this->approvedBy = $approvedBy;

        return $this;
    }

    /**
     * Get approvedBy
     *
     * @return integer 
     */
    public function getApprovedBy()
    {
        return $this->approvedBy;
    }

    /**
     * Set approvedDate
     *
     * @param \DateTime $approvedDate
     * @return WebshopProperty
     */
    public function setApprovedDate($approvedDate)
    {
        $this->approvedDate = $approvedDate;

        return $this;
    }

    /**
     * Get approvedDate
     *
     * @return \DateTime 
     */
    public function getApprovedDate()
    {
        return $this->approvedDate;
    }

    /**
     * Set rankCountry
     *
     * @param integer $rankCountry
     * @return WebshopProperty
     */
    public function setRankCountry($rankCountry)
    {
        $this->rankCountry = $rankCountry;

        return $this;
    }

    /**
     * Get rankCountry
     *
     * @return integer 
     */
    public function getRankCountry()
    {
        return $this->rankCountry;
    }

    /**
     * Set rankRegion
     *
     * @param integer $rankRegion
     * @return WebshopProperty
     */
    public function setRankRegion($rankRegion)
    {
        $this->rankRegion = $rankRegion;

        return $this;
    }

    /**
     * Get rankRegion
     *
     * @return integer 
     */
    public function getRankRegion()
    {
        return $this->rankRegion;
    }

    /**
     * Set rankPlace
     *
     * @param integer $rankPlace
     * @return WebshopProperty
     */
    public function setRankPlace($rankPlace)
    {
        $this->rankPlace = $rankPlace;

        return $this;
    }

    /**
     * Get rankPlace
     *
     * @return integer 
     */
    public function getRankPlace()
    {
        return $this->rankPlace;
    }

    /**
     * Set contactId
     *
     * @param integer $contactId
     * @return WebshopProperty
     */
    public function setContactId($contactId)
    {
        $this->contactId = $contactId;

        return $this;
    }

    /**
     * Get contactId
     *
     * @return integer 
     */
    public function getContactId()
    {
        return $this->contactId;
    }

    /**
     * Set contactHr
     *
     * @param string $contactHr
     * @return WebshopProperty
     */
    public function setContactHr($contactHr)
    {
        $this->contactHr = $contactHr;

        return $this;
    }

    /**
     * Get contactHr
     *
     * @return string 
     */
    public function getContactHr()
    {
        return $this->contactHr;
    }

    /**
     * Set contactEn
     *
     * @param string $contactEn
     * @return WebshopProperty
     */
    public function setContactEn($contactEn)
    {
        $this->contactEn = $contactEn;

        return $this;
    }

    /**
     * Get contactEn
     *
     * @return string 
     */
    public function getContactEn()
    {
        return $this->contactEn;
    }

    /**
     * Set contactDe
     *
     * @param string $contactDe
     * @return WebshopProperty
     */
    public function setContactDe($contactDe)
    {
        $this->contactDe = $contactDe;

        return $this;
    }

    /**
     * Get contactDe
     *
     * @return string 
     */
    public function getContactDe()
    {
        return $this->contactDe;
    }

    /**
     * Set contactIt
     *
     * @param string $contactIt
     * @return WebshopProperty
     */
    public function setContactIt($contactIt)
    {
        $this->contactIt = $contactIt;

        return $this;
    }

    /**
     * Get contactIt
     *
     * @return string 
     */
    public function getContactIt()
    {
        return $this->contactIt;
    }

    /**
     * Set contactSi
     *
     * @param string $contactSi
     * @return WebshopProperty
     */
    public function setContactSi($contactSi)
    {
        $this->contactSi = $contactSi;

        return $this;
    }

    /**
     * Get contactSi
     *
     * @return string 
     */
    public function getContactSi()
    {
        return $this->contactSi;
    }

    /**
     * Set contactRu
     *
     * @param string $contactRu
     * @return WebshopProperty
     */
    public function setContactRu($contactRu)
    {
        $this->contactRu = $contactRu;

        return $this;
    }

    /**
     * Get contactRu
     *
     * @return string 
     */
    public function getContactRu()
    {
        return $this->contactRu;
    }

    /**
     * Set ppWebshop
     *
     * @param integer $ppWebshop
     * @return WebshopProperty
     */
    public function setPpWebshop($ppWebshop)
    {
        $this->ppWebshop = $ppWebshop;

        return $this;
    }

    /**
     * Get ppWebshop
     *
     * @return integer 
     */
    public function getPpWebshop()
    {
        return $this->ppWebshop;
    }

    /**
     * Set ppProvision
     *
     * @param string $ppProvision
     * @return WebshopProperty
     */
    public function setPpProvision($ppProvision)
    {
        $this->ppProvision = $ppProvision;

        return $this;
    }

    /**
     * Get ppProvision
     *
     * @return string 
     */
    public function getPpProvision()
    {
        return $this->ppProvision;
    }

    /**
     * Set ppFixAmount
     *
     * @param string $ppFixAmount
     * @return WebshopProperty
     */
    public function setPpFixAmount($ppFixAmount)
    {
        $this->ppFixAmount = $ppFixAmount;

        return $this;
    }

    /**
     * Get ppFixAmount
     *
     * @return string 
     */
    public function getPpFixAmount()
    {
        return $this->ppFixAmount;
    }

    /**
     * Set b2bToB2c
     *
     * @param string $b2bToB2c
     * @return WebshopProperty
     */
    public function setB2bToB2c($b2bToB2c)
    {
        $this->b2bToB2c = $b2bToB2c;

        return $this;
    }

    /**
     * Get b2bToB2c
     *
     * @return string 
     */
    public function getB2bToB2c()
    {
        return $this->b2bToB2c;
    }

    /**
     * Set b2cZaokruzivanje
     *
     * @param boolean $b2cZaokruzivanje
     * @return WebshopProperty
     */
    public function setB2cZaokruzivanje($b2cZaokruzivanje)
    {
        $this->b2cZaokruzivanje = $b2cZaokruzivanje;

        return $this;
    }

    /**
     * Get b2cZaokruzivanje
     *
     * @return boolean 
     */
    public function getB2cZaokruzivanje()
    {
        return $this->b2cZaokruzivanje;
    }

    /**
     * Set idCompanyPartner
     *
     * @param integer $idCompanyPartner
     * @return WebshopProperty
     */
    public function setIdCompanyPartner($idCompanyPartner)
    {
        $this->idCompanyPartner = $idCompanyPartner;

        return $this;
    }

    /**
     * Get idCompanyPartner
     *
     * @return integer 
     */
    public function getIdCompanyPartner()
    {
        return $this->idCompanyPartner;
    }

    /**
     * Set idWebshopPartner
     *
     * @param integer $idWebshopPartner
     * @return WebshopProperty
     */
    public function setIdWebshopPartner($idWebshopPartner)
    {
        $this->idWebshopPartner = $idWebshopPartner;

        return $this;
    }

    /**
     * Get idWebshopPartner
     *
     * @return integer 
     */
    public function getIdWebshopPartner()
    {
        return $this->idWebshopPartner;
    }

    /**
     * Set brisi
     *
     * @param boolean $brisi
     * @return WebshopProperty
     */
    public function setBrisi($brisi)
    {
        $this->brisi = $brisi;

        return $this;
    }

    /**
     * Get brisi
     *
     * @return boolean 
     */
    public function getBrisi()
    {
        return $this->brisi;
    }

    /**
     * Set atraveo
     *
     * @param boolean $atraveo
     * @return WebshopProperty
     */
    public function setAtraveo($atraveo)
    {
        $this->atraveo = $atraveo;

        return $this;
    }

    /**
     * Get atraveo
     *
     * @return boolean 
     */
    public function getAtraveo()
    {
        return $this->atraveo;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set webshop
     *
     * @param \td\CMBundle\Entity\Webshop $webshop
     * @return WebshopProperty
     */
    public function setWebshop(\td\CMBundle\Entity\Webshop $webshop = null)
    {
        $this->webshop = $webshop;

        return $this;
    }

    /**
     * Get webshop
     *
     * @return \td\CMBundle\Entity\Webshop 
     */
    public function getWebshop()
    {
        return $this->webshop;
    }

    /**
     * Set property
     *
     * @param \td\CMBundle\Entity\Property $property
     * @return WebshopProperty
     */
    public function setProperty(\td\CMBundle\Entity\Property $property = null)
    {
        $this->property = $property;

        return $this;
    }

    /**
     * Get property
     *
     * @return \td\CMBundle\Entity\Property 
     */
    public function getProperty()
    {
        return $this->property;
    }
}
