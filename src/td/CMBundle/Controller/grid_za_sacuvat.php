<?
//Za thrace Bundle
	public function gridAction()
	{
	/*
		// $repository = $this->getDoctrine()->getManager()->getRepository('tdCMBundle:SetUp');
		
		
		// $qb2 = $repository->createQueryBuilder('s');
		// $qb2->select('s.id, s.idWebshop, s.idUnit, s.aktivan, s')
            // // ->leftJoin('s.orders', 'w')
            // ->groupBy('s.id')
        // ;
		// // $qb2->setParameters(array(1 => $portalKorisnik, 2 => 0, 3 => true));
		// $query = $qb2->getQuery();
		// $userManagementDataGrid = $query->getResult();
		$source = new Entity('tdCMBundle:SetUp');
		$grid = $this->get('grid');
		// $source->manipulateRow($callback);
		$grid->setSource($source);
		$grid->getColumn('idUnitNaPortalu')->setFilterType('input2');
		$grid->setLimits(50);
		$grid->isReadyForRedirect();
		
		$userManagementDataGrid = $this->container->get('thrace_data_grid.provider')->get('user_management');

        return $this->render('tdCMBundle:SetUp:grid3.html.twig',array(
            'grid' => $grid,
        ));
		*/
		
		/** @var \Thrace\DataGridBundle\DataGrid\DataGridInterface */
        $userManagementDataGrid = $this->container->get('thrace_data_grid.provider')->get('user_management');

        return $this->render('tdCMBundle:SetUp:grid.html.twig',array(
            'userManagementDataGrid' => $userManagementDataGrid,
        ));
	}
	
	//APY test
	public function myGridAction()
	{
		// Creates simple grid based on your entity (ORM)
        $source = new Entity('tdCMBundle:SetUp');
		$grid = $this->get('grid');
		$grid->setSource($source);
		
		$grid->setLimits(array(10, 20, 50));
		// $grid->setLimits(50);
		
		// Set the identifier of the grid
		// Add a column
		// Show/Hide columns
		// Set default filters
		// Set the default order
		// Set the default page
		$grid->setDefaultPage(1);
		// Set max results
		// Set prefix titles
		// Add mass actions
		// Add row actions
		// Manipulate the query builder
		// Manipulate rows data
		// Manipulate columns
		$MyColumn = new BlankColumn(array('id' => 'My Column', 'title'=>'My Column', 'size' => '54'));
        $grid->addColumn($MyColumn, 3);
		// Manipulate column render cell
		// Set items per page selector
		// Set the data for Entity and Document sources
		// Exports
		
		$grid->isReadyForRedirect();
		return $this->render('tdCMBundle:SetUp:grid3.html.twig', array('grid' => $grid));
	}
	
	//Datatheke jqGrid TEST ------- TEST -------- TEST
	public function dtGridAction(Request $request)
	{
		 return $this->render('tdCMBundle:SetUp:grid4.html.twig');
	}
	
	
	//Ovo je test - TEST
	public function jqGridAction(Request $request)
    {
		 $datagrid = $this->get('datatheke.datagrid')->createHttpDataGrid('tdCMBundle:User', array(), 'jqgrid');

        return $datagrid->handleRequest($request);
    }
	
	//Ovo je test - TEST
	public function persistRowAction(Request $request)
	{
		if(isset($request))
			{
				$user = $this->getDoctrine()->getManager()->getRepository('tdCMBundle:User')->find($request->request->get("id"));
				$user->setFirstName($request->request->get("firstName"));
				$user->setLastName($request->request->get("lastName"));
				// $user->setLastName($request->request->get("enabled"));
				// $user->setEnabled($request->request->get("enabled"));
				if ($request->request->get("enabled")=="Yes")
				{
					$user->setEnabled(true);
				}
				else
				{
					$user->setEnabled(false);
				}

				$em = $this->getDoctrine()->getEntityManager();
				$em->persist($user);
				$em->flush();
			}
		
		 $datagrid = $this->get('datatheke.datagrid')->createHttpDataGrid('tdCMBundle:User', array(), 'jqgrid');

        return $datagrid->handleRequest($request);
	}
	
	//Pregled setupa grid
	public function setupGridAction()
	{
		//Treba dohvatit sve setupove za portalKorisnik za odabrani portal i za odabranega korisnika.
		//Najdi portal
		//Dohvat portala
		$idPortal = 3;
		$idCompanyKorisnik = 1;
		$em = $this->getDoctrine()->getManager();
		$portal = $em->getRepository('tdCMBundle:Portal')->find($idPortal);
		
		//Pronađi portal korisnik za idCompanyKorisnik i idPortal
		$repository = $this->getDoctrine()->getManager()->getRepository('tdCMBundle:PortalKorisnik');
	
		//Dohvat veze portalKorisnik
		$portalKorisnik = $repository->findOneBy(
			array('portal' => $portal, 'idCompanyKorisnik' => $idCompanyKorisnik)
		);
		
		$this->setupGridPodaci($portalKorisnik);
		
		return $this->render('tdCMBundle:SetUp:setupPortalGrid3.html.twig');
	}
		
	public function setupDataJsonAction(Request $request)
    {
		/*
		//Dohvat setupa
		// $pager = $this->get('datatheke.pager')->createPager('tdCMBundle:SetupGrid', array());

		
		// // Retrieve QueryBuilder to join on the Adress Entity
		
		
		// // $qb->addSelect('w')->from('tdCMBundle:Webshop','w')->where('e.idWebshop = w.id');
		// // $qb->select('s')->from('tdCMBundle:SetUp','s')
				// // ->where('s.id > 262');
		 // // // Add field 'city' to the pager
		// $pager->getAdapter()->addField(new Field('propertyUnit.property.name', Field::TYPE_STRING, 'p.property.name'), 'propertyName');
		// $pager->getAdapter()->addField(new Field('webshop.title', Field::TYPE_STRING, 'w.title'), 'webshopTitle');
		
		// //postavaljanje filtera
		// // $pager->getAdapter()->setFilter(new Filter(array $fields = array(), array $values = array(), array $operators = array(), array $logical = array()));
		// $fields = array();
		// $values = array();
		// $operators = array();
		// $logical = array();
		
		
		
		// $fields[] = 'portalKorisnik';
		// $operators[] = '=';
		// $values[] = $portalKorisnik->getId();
		
		// $fields[] = 'idUnitNaPortalu';
		// $operators[] = 'X';
		// // $values[] = 'X';
		
		// $fields[] = 'idUnit';
		// $operators[] = '!=';
		// $values[] = '0';
		
		// $pager->getAdapter()->setFilter(new Filter($fields,  $values,  $operators,  $logical));
		// $pager->getAdapter()->count();
		

		// // $datagrid = $this->get('datatheke.datagrid')->createHttpDataGrid($pager, 'jqgrid');
		
		// $datagrid = $this->get('datatheke.datagrid')->createHttpDataGrid($pager, array(), 'jqgrid');
		*/
		
		//S obzirom da koristin loadonce mora san stavit da mi inicijalno pošalje sve redove
		$request->query->set('rows', '1000');
		//Staro	- DATATHEKE
		$datagrid = $this->get('datatheke.datagrid')->createHttpDataGrid('tdCMBundle:SetupGrid', array(), 'jqgrid');

        return $datagrid->handleRequest($request);
    }
	
	public function setupPersistRowAction(Request $request)
	{
		if(isset($request))
		{
			if($request->request->get('idUnitNaPortalu') != '')
			{
				
				//ne moren ga iskat po idUnit jer u setupu more bit više istih unita, ali po različitim portalima. Moran dodat nova polja u SetupGrid
				$setup = $this->getDoctrine()->getManager()->getRepository('tdCMBundle:SetUp')->find($request->request->get('idSetup'));
				$setup->setIdUnitNaPortalu($request->request->get('idUnitNaPortalu'));
				if ($request->request->get("aktivan")=="Yes")
				{
					$setup->setAktivan(true);
				}
				else
				{
					$setup->setAktivan(false);
				}
				
				$em = $this->getDoctrine()->getEntityManager();
				$em->persist($setup);
				$em->flush();
			}
		}
		
		 // $datagrid = $this->get('datatheke.datagrid')->createHttpDataGrid('tdCMBundle:SetUp', array(), 'jqgrid');
		 $datagrid = $this->get('datatheke.datagrid')->createHttpDataGrid('tdCMBundle:SetupGrid', array(), 'jqgrid');

        return $datagrid->handleRequest($request);
	}
	
	
	//Pomoćne funkcije
	//Funkcija za pripremu pomoćne tablice
	public function setupGridPodaci($portalKorisnik)
	{
	
		$em = $this->getDoctrine()->getManager();
		
		//Brisanje pomoćne tablice
		$sql = "truncate table cm_setup_grid";
		$stmt = $em->getConnection()->prepare($sql);
		$stmt->execute();
		
		//Novo punjenje pomoćne tablice
		$sql = "insert into cm_setup_grid 
			(idPortal, idSetup, idUnit, mjesto, nazivObjekta, nazivUnita, skrbnikId, skrbnikNaziv, vlasnikId, vlasnikNaziv, aktivan) 
			select 
			pk.id_portal, s.id, s.id_unit, pr.city, pr.name, p.name2, cs.id_client, cs.naziv, ow.id_client, ow.naziv, s.aktivan
			from cm_set_up as s 
			left join property_unit as p on s.id_unit = p.id 
			left join cm_portal_korisnik as pk on s.id_portal_korisnik = pk.id 
			left join property as pr on p.id_property = pr.id
			left join client as cs on pr.id_agent_c = cs.id_client
			left join client as ow	on pr.id_owner = ow.id_client
			where
			s.id_portal_korisnik = :idPortalKorisnik and
			s.id_unit is not null and
			s.id_unit_na_portalu is null";
		$params = array('idPortalKorisnik'=>$portalKorisnik->getId());

		$stmt = $em->getConnection()->prepare($sql);
		$stmt->execute($params);
		
		/*
		//punjenje tablice SetupGrid
		// //Tab2 - WS CM NOT Portal
		// $repository = $this->getDoctrine()->getManager()->getRepository('tdCMBundle:SetUp');
		// $qb2 = $repository->createQueryBuilder('s');
		// $qb2->where(
					// $qb2->expr()->andX(
						// $qb2->expr()->eq('s.portalKorisnik', '?1'),
						// $qb2->expr()->neq('s.idWebshop', '?2'), // WS != 0
						// $qb2->expr()->neq('s.idUnit', '?2'), // ID Unit != 0
						// // $qb2->expr()->eq('s.aktivan', '?3'), // Aktivan DA
						// $qb2->expr()->orX(
							// $qb2->expr()->isNull('s.idUnitNaPortalu'), // Unit na portalu NIJE definiran
							// $qb2->expr()->eq('s.idUnitNaPortalu', '?4')
						// )
					// )
				// );
		// $qb2->setParameters(array(1 => $portalKorisnik, 2 => 0, 4 => ''));
		// $query = $qb2->getQuery();
		// $setUpi = $query->getResult();
		
		// //podaci koji su mi bitni
		// foreach($setUpi as $setup)
		// {
			// //Dohvat svih podataka
			// //kreiranje novog objekta setupGrid
			// $setupGrid = new SetupGrid();
			// //ažuriranje setupGrid
			// $setupGrid->setIdSetup($setup->getId());
			// $setupGrid->setIdPortal($portalKorisnik->getPortal()->getId());
			// $setupGrid->setIdUnit($setup->getIdUnit());
			// $setupGrid->setNazivObjekta($setup->getPropertyUnit()->getProperty()->getName());
			
			// if($setup->getPropertyUnit()->getName() != null)
			// {
				// $setupGrid->setNazivUnita($setup->getPropertyUnit()->getName2());
			// }
			// else
			// {
				// $setupGrid->setNazivUnita($setup->getPropertyUnit()->getName());
			// }
			
			// $setupGrid->setMjesto($setup->getPropertyUnit()->getProperty()->getCity());
			
			// if($setup->getPropertyUnit()->getProperty()->getIdAgentC() != 0)
			// {
				// $setupGrid->setSkrbnikId($setup->getPropertyUnit()->getProperty()->getAgentC()->getIdClient());
				// $setupGrid->setSkrbnikNaziv($setup->getPropertyUnit()->getProperty()->getAgentC()->getNaziv());
			// }
			// else
			// {
				// $setupGrid->setSkrbnikId($setup->getPropertyUnit()->getProperty()->getOwner()->getIdClient());
			// }
			
			// if($setup->getPropertyUnit()->getProperty()->getIdAgentC() != 0)
			// {
				// $setupGrid->setSkrbnikId($setup->getPropertyUnit()->getProperty()->getAgentC()->getIdClient());
				// $setupGrid->setSkrbnikNaziv($setup->getPropertyUnit()->getProperty()->getAgentC()->getNaziv());
			// }
			// else
			// {
				// $setupGrid->setSkrbnikId($setup->getPropertyUnit()->getProperty()->getAgentC()->getIdClient());
			// }
						
			// if($setup->getPropertyUnit()->getProperty()->getIdOwner() != 0)
			// {
				// $setupGrid->setVlasnikId($setup->getPropertyUnit()->getProperty()->getOwner()->getIdClient());
				// $setupGrid->setVlasnikNaziv($setup->getPropertyUnit()->getProperty()->getOwner()->getNaziv());
			// }
			// else
			// {
				// $setupGrid->setVlasnikId($setup->getPropertyUnit()->getProperty()->getOwner()->getIdClient());
			// }
			
			// //spremanje u bazu setupGrid
			// $em->persist($setupGrid);
			// $em->flush();
		// }
		
		//ID	 IDUnit	IDUnitNaPortalu 	Naziv objekta	Naziv unita	Mjesto	Skrbnik	Vlasnik	
		*/
	}
	?>