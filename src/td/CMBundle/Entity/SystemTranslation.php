<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SystemTranslation
 *
 * @ORM\Table(name="system_translation")
 * @ORM\Entity(repositoryClass="td\CMBundle\Repository\SystemTranslationRepository")
 */
class SystemTranslation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_company", type="integer", unique=true)
     */
    private $idCompany;

    /**
     * @var int
     *
     * @ORM\Column(name="id_was", type="integer")
     */
    private $idWas;

    /**
     * @var int
     *
     * @ORM\Column(name="id_webshop", type="integer")
     */
    private $idWebshop;

    /**
     * @var string
     *
     * @ORM\Column(name="id_table", type="string", length=32)
     */
    private $idTable;

    /**
     * @var string
     *
     * @ORM\Column(name="id_field", type="string", length=32)
     */
    private $idField;

    /**
     * @var int
     *
     * @ORM\Column(name="id_object", type="integer")
     */
    private $idObject;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=2)
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(name="naslov", type="string", length=255)
     */
    private $naslov;

    /**
     * @var string
     *
     * @ORM\Column(name="translation", type="string", length=1024)
     */
    private $translation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCompany
     *
     * @param integer $idCompany
     *
     * @return SystemTranslation
     */
    public function setIdCompany($idCompany)
    {
        $this->idCompany = $idCompany;

        return $this;
    }

    /**
     * Get idCompany
     *
     * @return int
     */
    public function getIdCompany()
    {
        return $this->idCompany;
    }

    /**
     * Set idWas
     *
     * @param integer $idWas
     *
     * @return SystemTranslation
     */
    public function setIdWas($idWas)
    {
        $this->idWas = $idWas;

        return $this;
    }

    /**
     * Get idWas
     *
     * @return int
     */
    public function getIdWas()
    {
        return $this->idWas;
    }

    /**
     * Set idWebshop
     *
     * @param integer $idWebshop
     *
     * @return SystemTranslation
     */
    public function setIdWebshop($idWebshop)
    {
        $this->idWebshop = $idWebshop;

        return $this;
    }

    /**
     * Get idWebshop
     *
     * @return int
     */
    public function getIdWebshop()
    {
        return $this->idWebshop;
    }

    /**
     * Set idTable
     *
     * @param string $idTable
     *
     * @return SystemTranslation
     */
    public function setIdTable($idTable)
    {
        $this->idTable = $idTable;

        return $this;
    }

    /**
     * Get idTable
     *
     * @return string
     */
    public function getIdTable()
    {
        return $this->idTable;
    }

    /**
     * Set idField
     *
     * @param string $idField
     *
     * @return SystemTranslation
     */
    public function setIdField($idField)
    {
        $this->idField = $idField;

        return $this;
    }

    /**
     * Get idField
     *
     * @return string
     */
    public function getIdField()
    {
        return $this->idField;
    }

    /**
     * Set idObject
     *
     * @param integer $idObject
     *
     * @return SystemTranslation
     */
    public function setIdObject($idObject)
    {
        $this->idObject = $idObject;

        return $this;
    }

    /**
     * Get idObject
     *
     * @return int
     */
    public function getIdObject()
    {
        return $this->idObject;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return SystemTranslation
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set naslov
     *
     * @param string $naslov
     *
     * @return SystemTranslation
     */
    public function setNaslov($naslov)
    {
        $this->naslov = $naslov;

        return $this;
    }

    /**
     * Get naslov
     *
     * @return string
     */
    public function getNaslov()
    {
        return $this->naslov;
    }

    /**
     * Set translation
     *
     * @param string $translation
     *
     * @return SystemTranslation
     */
    public function setTranslation($translation)
    {
        $this->translation = $translation;

        return $this;
    }

    /**
     * Get translation
     *
     * @return string
     */
    public function getTranslation()
    {
        return $this->translation;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return SystemTranslation
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return SystemTranslation
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
