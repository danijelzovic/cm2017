<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UnitRateStavke
 *
 * @ORM\Table(name="cm_unit_rate")
 * @ORM\Entity(repositoryClass="td\CMBundle\Entity\Repository\UnitRateRepository")
 */
class UnitRate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="idRateBookingCom", type="integer")
     */
    private $idRateBookingCom;

    /**
     * @var integer
     *
     * @ORM\Column(name="idRate", type="integer")
     */
    private $idRate;

    /**
     * @ORM\ManyToOne(targetEntity="Rate", inversedBy="unitRates")
     * @ORM\JoinColumn(name="idRate", referencedColumnName="id", onDelete="RESTRICT")
     */
    private $rate;

    /**
     * @ORM\ManyToOne(targetEntity="SetupPropertyUnit", inversedBy="unitRates")
     * @ORM\JoinColumn(name="idSetupPropertyUnit", referencedColumnName="id", onDelete="CASCADE")
     */
    private $setupPropertyUnit;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_cjenik", type="integer")
     */
    private $idCjenik = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="samoCjenik", type="boolean")
     */
    private $samoCjenik;

    /**
     * @var string
     *
     * @ORM\Column(name="id_povecanje", type="decimal", nullable=true)
     */
    private $povecanje;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_zaokruzivanje", type="smallint", nullable=true)
     */
    private $zaokruzivanje;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="vrijedi_od", type="date")
     */
    private $vrijediOd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="vrijedi_do", type="date")
     */
    private $vrijediDo;


    /**
     * @var integer
     *
     * @ORM\Column(name="idSet", type="smallint", nullable=true)
     */
    private $idSet;

    /**
     * @ORM\ManyToOne(targetEntity="ItempricelistSets")
     * @ORM\JoinColumn(name="idSet", referencedColumnName="id", onDelete="CASCADE")
     */
    private $itempricelistSet;

    /**
     * @var boolean
     *
     * @ORM\Column(name="postojeciSet", type="boolean")
     */
    private $postojeciSet;

    /**
     * @var boolean
     *
     * @ORM\Column(name="aktivan", type="boolean")
     */
    private $aktivan;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idRate
     *
     * @param integer $idRate
     *
     * @return UnitRate
     */
    public function setIdRate($idRate)
    {
        $this->idRate = $idRate;

        return $this;
    }

    /**
     * Get idRate
     *
     * @return integer
     */
    public function getIdRate()
    {
        return $this->idRate;
    }

    /**
     * Set idCjenik
     *
     * @param integer $idCjenik
     *
     * @return UnitRate
     */
    public function setIdCjenik($idCjenik)
    {
        $this->idCjenik = $idCjenik;

        return $this;
    }

    /**
     * Get idCjenik
     *
     * @return integer
     */
    public function getIdCjenik()
    {
        return $this->idCjenik;
    }

    /**
     * Set samoCjenik
     *
     * @param boolean $samoCjenik
     *
     * @return UnitRate
     */
    public function setSamoCjenik($samoCjenik)
    {
        $this->samoCjenik = $samoCjenik;

        return $this;
    }

    /**
     * Get samoCjenik
     *
     * @return boolean
     */
    public function getSamoCjenik()
    {
        return $this->samoCjenik;
    }

    /**
     * Set povecanje
     *
     * @param string $povecanje
     *
     * @return UnitRate
     */
    public function setPovecanje($povecanje)
    {
        $this->povecanje = $povecanje;

        return $this;
    }

    /**
     * Get povecanje
     *
     * @return string
     */
    public function getPovecanje()
    {
        return $this->povecanje;
    }

    /**
     * Set zaokruzivanje
     *
     * @param integer $zaokruzivanje
     *
     * @return UnitRate
     */
    public function setZaokruzivanje($zaokruzivanje)
    {
        $this->zaokruzivanje = $zaokruzivanje;

        return $this;
    }

    /**
     * Get zaokruzivanje
     *
     * @return integer
     */
    public function getZaokruzivanje()
    {
        return $this->zaokruzivanje;
    }

    /**
     * Set vrijediOd
     *
     * @param \DateTime $vrijediOd
     *
     * @return UnitRate
     */
    public function setVrijediOd($vrijediOd)
    {
        $this->vrijediOd = $vrijediOd;

        return $this;
    }

    /**
     * Get vrijediOd
     *
     * @return \DateTime
     */
    public function getVrijediOd()
    {
        return $this->vrijediOd;
    }

    /**
     * Set vrijediDo
     *
     * @param \DateTime $vrijediDo
     *
     * @return UnitRate
     */
    public function setVrijediDo($vrijediDo)
    {
        $this->vrijediDo = $vrijediDo;

        return $this;
    }

    /**
     * Get vrijediDo
     *
     * @return \DateTime
     */
    public function getVrijediDo()
    {
        return $this->vrijediDo;
    }

    /**
     * Set idSet
     *
     * @param integer $idSet
     *
     * @return UnitRate
     */
    public function setIdSet($idSet)
    {
        $this->idSet = $idSet;

        return $this;
    }

    /**
     * Get idSet
     *
     * @return integer
     */
    public function getIdSet()
    {
        return $this->idSet;
    }

    /**
     * Set postojeciSet
     *
     * @param boolean $postojeciSet
     *
     * @return UnitRate
     */
    public function setPostojeciSet($postojeciSet)
    {
        $this->postojeciSet = $postojeciSet;

        return $this;
    }

    /**
     * Get postojeciSet
     *
     * @return boolean
     */
    public function getPostojeciSet()
    {
        return $this->postojeciSet;
    }

    /**
     * Set aktivan
     *
     * @param boolean $aktivan
     *
     * @return UnitRate
     */
    public function setAktivan($aktivan)
    {
        $this->aktivan = $aktivan;

        return $this;
    }

    /**
     * Get aktivan
     *
     * @return boolean
     */
    public function getAktivan()
    {
        return $this->aktivan;
    }

    /**
     * Set rate
     *
     * @param \td\CMBundle\Entity\Rate $rate
     *
     * @return UnitRate
     */
    public function setRate(\td\CMBundle\Entity\Rate $rate = null)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return \td\CMBundle\Entity\Rate
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Set setupPropertyUnit
     *
     * @param \td\CMBundle\Entity\SetupPropertyUnit $setupPropertyUnit
     *
     * @return UnitRate
     */
    public function setSetupPropertyUnit(\td\CMBundle\Entity\SetupPropertyUnit $setupPropertyUnit = null)
    {
        $this->setupPropertyUnit = $setupPropertyUnit;

        return $this;
    }

    /**
     * Get setupPropertyUnit
     *
     * @return \td\CMBundle\Entity\SetupPropertyUnit
     */
    public function getSetupPropertyUnit()
    {
        return $this->setupPropertyUnit;
    }

    /**
     * Set itempricelistSet
     *
     * @param \td\CMBundle\Entity\ItempricelistSets $itempricelistSet
     *
     * @return UnitRate
     */
    public function setItempricelistSet(\td\CMBundle\Entity\ItempricelistSets $itempricelistSet = null)
    {
        $this->itempricelistSet = $itempricelistSet;

        return $this;
    }

    /**
     * Get itempricelistSet
     *
     * @return \td\CMBundle\Entity\ItempricelistSets
     */
    public function getItempricelistSet()
    {
        return $this->itempricelistSet;
    }

    /**
     * Set idRateBookingCom
     *
     * @param integer $idRateBookingCom
     *
     * @return UnitRate
     */
    public function setIdRateBookingCom($idRateBookingCom)
    {
        $this->idRateBookingCom = $idRateBookingCom;

        return $this;
    }

    /**
     * Get idRateBookingCom
     *
     * @return integer
     */
    public function getIdRateBookingCom()
    {
        return $this->idRateBookingCom;
    }
}
