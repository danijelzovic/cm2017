<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Exclude;

/**
 * Transfer
 *
 * @ORM\Table(name="cm_transfer")
 * @ORM\Entity(repositoryClass="td\CMBundle\Entity\TransferRepository")
 * @ExclusionPolicy("none")
 */
class Transfer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_CM_promjena", type="integer")
     */
    private $idCMPromjena;

    /**
     * @ORM\ManyToOne(targetEntity="Promjena", inversedBy="transferi")
     * @ORM\JoinColumn(name="ID_CM_promjena", referencedColumnName="id")
     *
     */
    private $promjena;

    /**
     * @ORM\ManyToOne(targetEntity="SetupPropertyUnit", inversedBy="transferi")
     * @ORM\JoinColumn(name="idSetupPropertyUnit", referencedColumnName="id")
     */
    private $setupPropertyUnit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="OK", type="boolean")
     */
    private $ok = false;

    /**
     * @ORM\OneToMany(targetEntity="TransferLog", mappedBy="transfer")
     * @Exclude
     */
    private $transferLogovi;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->transferLogovi = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCMPromjena
     *
     * @param integer $idCMPromjena
     *
     * @return Transfer
     */
    public function setIdCMPromjena($idCMPromjena)
    {
        $this->idCMPromjena = $idCMPromjena;

        return $this;
    }

    /**
     * Get idCMPromjena
     *
     * @return integer
     */
    public function getIdCMPromjena()
    {
        return $this->idCMPromjena;
    }

    /**
     * Set ok
     *
     * @param boolean $ok
     *
     * @return Transfer
     */
    public function setOk($ok)
    {
        $this->ok = $ok;

        return $this;
    }

    /**
     * Get ok
     *
     * @return boolean
     */
    public function getOk()
    {
        return $this->ok;
    }

    /**
     * Set promjena
     *
     * @param \td\CMBundle\Entity\Promjena $promjena
     *
     * @return Transfer
     */
    public function setPromjena(\td\CMBundle\Entity\Promjena $promjena = null)
    {
        $this->promjena = $promjena;

        return $this;
    }

    /**
     * Get promjena
     *
     * @return \td\CMBundle\Entity\Promjena
     */
    public function getPromjena()
    {
        return $this->promjena;
    }

    /**
     * Set setupPropertyUnit
     *
     * @param \td\CMBundle\Entity\SetupPropertyUnit $setupPropertyUnit
     *
     * @return Transfer
     */
    public function setSetupPropertyUnit(\td\CMBundle\Entity\SetupPropertyUnit $setupPropertyUnit = null)
    {
        $this->setupPropertyUnit = $setupPropertyUnit;

        return $this;
    }

    /**
     * Get setupPropertyUnit
     *
     * @return \td\CMBundle\Entity\SetupPropertyUnit
     */
    public function getSetupPropertyUnit()
    {
        return $this->setupPropertyUnit;
    }

    /**
     * Add transferLogovi
     *
     * @param \td\CMBundle\Entity\TransferLog $transferLogovi
     *
     * @return Transfer
     */
    public function addTransferLogovi(\td\CMBundle\Entity\TransferLog $transferLogovi)
    {
        $this->transferLogovi[] = $transferLogovi;

        return $this;
    }

    /**
     * Remove transferLogovi
     *
     * @param \td\CMBundle\Entity\TransferLog $transferLogovi
     */
    public function removeTransferLogovi(\td\CMBundle\Entity\TransferLog $transferLogovi)
    {
        $this->transferLogovi->removeElement($transferLogovi);
    }

    /**
     * Get transferLogovi
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransferLogovi()
    {
        return $this->transferLogovi;
    }
}
