<?php
/**
 * Created by PhpStorm.
 * User: danijel
 * Date: 4.5.2016.
 * Time: 20:19
 */

namespace td\CMBundle\Obrada;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\NoResultException;
use td\CMBundle\Entity\PortalKorisnik;
use Doctrine\ORM\EntityManager;
use td\CMBundle\Entity\Portal;

class RateObrada
{
    protected $em;
    protected $cookieObrada;

    /**
     * @param EntityManager $em
     * @param cookieObrada $cookieObrada
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Create new portalKorisnik based on PORTAL and idCompany
     *
     * @param Portal $portal
     * @param $idCompanyKorisnik
     * @return PortalKorisnik
     */
//    public function createNewPortalKorisnik(Portal $portal, $idCompanyKorisnik)
//    {
//        $portalKorisnik = $this->em->getRepository('tdCMBundle:PortalKorisnik')->findOneBy(array(
//            'portal' => $portal,
//            'idCompanyKorisnik' => $idCompanyKorisnik,
//        ));
//        if (is_null($portalKorisnik)) {
//            $portalKorisnik = new PortalKorisnik();
//            $portalKorisnik->setFotoBrojPokusaja(self::BROJ_POKUSAJA);
//            $portalKorisnik->setCjenikBrojPokusaja(self::BROJ_POKUSAJA);
//            $portalKorisnik->setIdCompanyKorisnik($idCompanyKorisnik);
//            $portalKorisnik->setOpisBrojPokusaja(self::BROJ_POKUSAJA);
//            $portalKorisnik->setPodaciBrojPokusaja(self::BROJ_POKUSAJA);
//            $portalKorisnik->setRaspolozivostBrojPokusaja(self::BROJ_POKUSAJA);
//            $portalKorisnik->setPortal($portal);
//            $portalKorisnik->setPovezan(self::POVEZAN);
//            $this->em->persist($portalKorisnik);
//            $this->em->flush();
//        }
//
//        return $portalKorisnik;
//    }

}