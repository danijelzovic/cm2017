<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SetupBiljeska
 *
 * @ORM\Table(name="cm_setup_biljeska")
 * @ORM\Entity
 */
class SetupBiljeska
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="idSetup", type="integer")
     */
    private $idSetup;

    /**
     * @var integer
     *
     * @ORM\Column(name="idBiljeska", type="integer")
     */
    private $idBiljeska;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idSetup
     *
     * @param integer $idSetup
     * @return SetupBiljeska
     */
    public function setIdSetup($idSetup)
    {
        $this->idSetup = $idSetup;

        return $this;
    }

    /**
     * Get idSetup
     *
     * @return integer 
     */
    public function getIdSetup()
    {
        return $this->idSetup;
    }

    /**
     * Set idBiljeska
     *
     * @param integer $idBiljeska
     * @return SetupBiljeska
     */
    public function setIdBiljeska($idBiljeska)
    {
        $this->idBiljeska = $idBiljeska;

        return $this;
    }

    /**
     * Get idBiljeska
     *
     * @return integer 
     */
    public function getIdBiljeska()
    {
        return $this->idBiljeska;
    }
}
