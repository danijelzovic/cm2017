<?php
/**
 * Created by PhpStorm.
 * User: danijel
 * Date: 15.10.2016.
 * Time: 12:32
 */

namespace td\CMBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;
use td\CMBundle\Entity\Property;
use td\CMBundle\Entity\Webshop;

class WebshopPropertyUnitRepository extends EntityRepository
{
    public function countWebshopPropertyUnits(Webshop $webshop, Property $property)
    {
        return $this->createQueryBuilder('wpu')
            ->select('COUNT(wpu.id)')
            ->where('wpu.webshop = :webshop')
            ->andWhere('wpu.property = :property')
            ->setParameter('webshop', $webshop)
            ->setParameter('property', $property)
            ->getQuery()
            ->getSingleScalarResult();
    }

}