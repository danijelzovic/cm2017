<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WebshopUnit
 *
 * @ORM\Table(name="cm_webshop_units")
 * @ORM\Entity
 */
class WebshopUnit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_webshop_unit", type="integer")
     */
    private $idWebshopUnit;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idWebshopUnit
     *
     * @param integer $idWebshopUnit
     * @return WebshopUnit
     */
    public function setIdWebshopUnit($idWebshopUnit)
    {
        $this->idWebshopUnit = $idWebshopUnit;

        return $this;
    }

    /**
     * Get idWebshopUnit
     *
     * @return integer 
     */
    public function getIdWebshopUnit()
    {
        return $this->idWebshopUnit;
    }
}
