<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VSetupPropertyAll
 *
 * @ORM\Table(name="v_cm_setup_property_all_grid")
 * @ORM\Entity(repositoryClass="td\CMBundle\Repository\VSetupPropertyAllRepository")
 */
class VSetupPropertyAll
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="idPortalKorisnik", type="integer", nullable=true)
     */
    private $idPortalKorisnik;

    /**
     * @var string
     *
     * @ORM\Column(name="idCompanyKorisnik", type="string", length=255)
     */
    private $idCompanyKorisnik;

    /**
     * @var string
     *
     * @ORM\Column(name="nazivCompanyKorisnik", type="string", length=255)
     */
    private $nazivCompanyKorisnik;

    /**
     * @var int
     *
     * @ORM\Column(name="idWebshop", type="integer")
     */
    private $idWebshop;

    /**
     * @var string
     *
     * @ORM\Column(name="nameWebshop", type="string", length=255)
     */
    private $nameWebshop;

    /**
     * @var int
     *
     * @ORM\Column(name="idPortal", type="integer")
     */
    private $idPortal;

    /**
     * @var string
     *
     * @ORM\Column(name="nazivPortal", type="string", length=255)
     */
    private $nazivPortal;

    /**
     * @var string
     *
     * @ORM\Column(name="idPropertyFront", type="string", length=255)
     */
    private $idPropertyFront;

    /**
     * @var string
     *
     * @ORM\Column(name="idPropertyNaPortalu", type="string", length=255, nullable=true)
     */
    private $idPropertyNaPortalu;

    /**
     * @var string
     *
     * @ORM\Column(name="nazivProperty", type="string", length=255, nullable=true)
     */
    private $nazivProperty;

    /**
     * @var string
     *
     * @ORM\Column(name="mjesto", type="string", length=255, nullable=true)
     */
    private $mjesto;

    /**
     * @var string
     *
     * @ORM\Column(name="skrbnik", type="string", length=255, nullable=true)
     */
    private $skrbnik;

    /**
     * @var string
     *
     * @ORM\Column(name="vlasnik", type="string", length=255, nullable=true)
     */
    private $vlasnik;

    /**
     * @var bool
     *
     * @ORM\Column(name="aktivan", type="boolean")
     */
    private $aktivan;

    /**
     * @var bool
     *
     * @ORM\Column(name="povezan", type="boolean")
     */
    private $povezan;

    /**
     * @var int
     *
     * @ORM\Column(name="brojUnita", type="integer")
     */
    private $brojUnita;

    /**
     * @var int
     *
     * @ORM\Column(name="brojNeprijavljenih", type="integer")
     */
    private $brojNeprijavljenih;

    /**
     * @var int
     *
     * @ORM\Column(name="brojAktivnih", type="integer")
     */
    private $brojAktivnih;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datumKreiranja", type="datetimetz")
     */
    private $datumKreiranja;

    /**
     * @var int
     *
     * @ORM\Column(name="brojNeuspjelihTransfera", type="integer")
     */
    private $brojNeuspjelihTransfera;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPortalKorisnik
     *
     * @param integer $idPortalKorisnik
     *
     * @return VSetupPropertyAll
     */
    public function setIdPortalKorisnik($idPortalKorisnik)
    {
        $this->idPortalKorisnik = $idPortalKorisnik;

        return $this;
    }

    /**
     * Get idPortalKorisnik
     *
     * @return int
     */
    public function getIdPortalKorisnik()
    {
        return $this->idPortalKorisnik;
    }

    /**
     * Set idCompanyKorisnik
     *
     * @param string $idCompanyKorisnik
     *
     * @return VSetupPropertyAll
     */
    public function setIdCompanyKorisnik($idCompanyKorisnik)
    {
        $this->idCompanyKorisnik = $idCompanyKorisnik;

        return $this;
    }

    /**
     * Get idCompanyKorisnik
     *
     * @return string
     */
    public function getIdCompanyKorisnik()
    {
        return $this->idCompanyKorisnik;
    }

    /**
     * Set nazivCompanyKorisnik
     *
     * @param string $nazivCompanyKorisnik
     *
     * @return VSetupPropertyAll
     */
    public function setNazivCompanyKorisnik($nazivCompanyKorisnik)
    {
        $this->nazivCompanyKorisnik = $nazivCompanyKorisnik;

        return $this;
    }

    /**
     * Get nazivCompanyKorisnik
     *
     * @return string
     */
    public function getNazivCompanyKorisnik()
    {
        return $this->nazivCompanyKorisnik;
    }

    /**
     * Set idWebshop
     *
     * @param integer $idWebshop
     *
     * @return VSetupPropertyAll
     */
    public function setIdWebshop($idWebshop)
    {
        $this->idWebshop = $idWebshop;

        return $this;
    }

    /**
     * Get idWebshop
     *
     * @return int
     */
    public function getIdWebshop()
    {
        return $this->idWebshop;
    }

    /**
     * Set nameWebshop
     *
     * @param string $nameWebshop
     *
     * @return VSetupPropertyAll
     */
    public function setNameWebshop($nameWebshop)
    {
        $this->nameWebshop = $nameWebshop;

        return $this;
    }

    /**
     * Get nameWebshop
     *
     * @return string
     */
    public function getNameWebshop()
    {
        return $this->nameWebshop;
    }

    /**
     * Set idPortal
     *
     * @param integer $idPortal
     *
     * @return VSetupPropertyAll
     */
    public function setIdPortal($idPortal)
    {
        $this->idPortal = $idPortal;

        return $this;
    }

    /**
     * Get idPortal
     *
     * @return int
     */
    public function getIdPortal()
    {
        return $this->idPortal;
    }

    /**
     * Set nazivPortal
     *
     * @param string $nazivPortal
     *
     * @return VSetupPropertyAll
     */
    public function setNazivPortal($nazivPortal)
    {
        $this->nazivPortal = $nazivPortal;

        return $this;
    }

    /**
     * Get nazivPortal
     *
     * @return string
     */
    public function getNazivPortal()
    {
        return $this->nazivPortal;
    }

    /**
     * Set idPropertyFront
     *
     * @param string $idPropertyFront
     *
     * @return VSetupPropertyAll
     */
    public function setIdPropertyFront($idPropertyFront)
    {
        $this->idPropertyFront = $idPropertyFront;

        return $this;
    }

    /**
     * Get idPropertyFront
     *
     * @return string
     */
    public function getIdPropertyFront()
    {
        return $this->idPropertyFront;
    }

    /**
     * Set idPropertyNaPortalu
     *
     * @param string $idPropertyNaPortalu
     *
     * @return VSetupPropertyAll
     */
    public function setIdPropertyNaPortalu($idPropertyNaPortalu)
    {
        $this->idPropertyNaPortalu = $idPropertyNaPortalu;

        return $this;
    }

    /**
     * Get idPropertyNaPortalu
     *
     * @return string
     */
    public function getIdPropertyNaPortalu()
    {
        return $this->idPropertyNaPortalu;
    }

    /**
     * Set nazivProperty
     *
     * @param string $nazivProperty
     *
     * @return VSetupPropertyAll
     */
    public function setNazivProperty($nazivProperty)
    {
        $this->nazivProperty = $nazivProperty;

        return $this;
    }

    /**
     * Get nazivProperty
     *
     * @return string
     */
    public function getNazivProperty()
    {
        return $this->nazivProperty;
    }

    /**
     * Set mjesto
     *
     * @param string $mjesto
     *
     * @return VSetupPropertyAll
     */
    public function setMjesto($mjesto)
    {
        $this->mjesto = $mjesto;

        return $this;
    }

    /**
     * Get mjesto
     *
     * @return string
     */
    public function getMjesto()
    {
        return $this->mjesto;
    }

    /**
     * Set skrbnik
     *
     * @param string $skrbnik
     *
     * @return VSetupPropertyAll
     */
    public function setSkrbnik($skrbnik)
    {
        $this->skrbnik = $skrbnik;

        return $this;
    }

    /**
     * Get skrbnik
     *
     * @return string
     */
    public function getSkrbnik()
    {
        return $this->skrbnik;
    }

    /**
     * Set vlasnik
     *
     * @param string $vlasnik
     *
     * @return VSetupPropertyAll
     */
    public function setVlasnik($vlasnik)
    {
        $this->vlasnik = $vlasnik;

        return $this;
    }

    /**
     * Get vlasnik
     *
     * @return string
     */
    public function getVlasnik()
    {
        return $this->vlasnik;
    }

    /**
     * Set aktivan
     *
     * @param boolean $aktivan
     *
     * @return VSetupPropertyAll
     */
    public function setAktivan($aktivan)
    {
        $this->aktivan = $aktivan;

        return $this;
    }

    /**
     * Get aktivan
     *
     * @return bool
     */
    public function getAktivan()
    {
        return $this->aktivan;
    }

    /**
     * Set povezan
     *
     * @param boolean $povezan
     *
     * @return VSetupPropertyAll
     */
    public function setPovezan($povezan)
    {
        $this->povezan = $povezan;

        return $this;
    }

    /**
     * Get povezan
     *
     * @return bool
     */
    public function getPovezan()
    {
        return $this->povezan;
    }

    /**
     * Set brojUnita
     *
     * @param integer $brojUnita
     *
     * @return VSetupPropertyAll
     */
    public function setBrojUnita($brojUnita)
    {
        $this->brojUnita = $brojUnita;

        return $this;
    }

    /**
     * Get brojUnita
     *
     * @return int
     */
    public function getBrojUnita()
    {
        return $this->brojUnita;
    }

    /**
     * Set brojNeprijavljenih
     *
     * @param integer $brojNeprijavljenih
     *
     * @return VSetupPropertyAll
     */
    public function setBrojNeprijavljenih($brojNeprijavljenih)
    {
        $this->brojNeprijavljenih = $brojNeprijavljenih;

        return $this;
    }

    /**
     * Get brojNeprijavljenih
     *
     * @return int
     */
    public function getBrojNeprijavljenih()
    {
        return $this->brojNeprijavljenih;
    }

    /**
     * Set brojAktivnih
     *
     * @param integer $brojAktivnih
     *
     * @return VSetupPropertyAll
     */
    public function setBrojAktivnih($brojAktivnih)
    {
        $this->brojAktivnih = $brojAktivnih;

        return $this;
    }

    /**
     * Get brojAktivnih
     *
     * @return int
     */
    public function getBrojAktivnih()
    {
        return $this->brojAktivnih;
    }

    /**
     * Set datumKreiranja
     *
     * @param \DateTime $datumKreiranja
     *
     * @return VSetupPropertyAll
     */
    public function setDatumKreiranja($datumKreiranja)
    {
        $this->datumKreiranja = $datumKreiranja;

        return $this;
    }

    /**
     * Get datumKreiranja
     *
     * @return \DateTime
     */
    public function getDatumKreiranja()
    {
        return $this->datumKreiranja;
    }

    /**
     * Set brojNeuspjelihTransfera
     *
     * @param integer $brojNeuspjelihTransfera
     *
     * @return VSetupPropertyAll
     */
    public function setBrojNeuspjelihTransfera($brojNeuspjelihTransfera)
    {
        $this->brojNeuspjelihTransfera = $brojNeuspjelihTransfera;

        return $this;
    }

    /**
     * Get brojNeuspjelihTransfera
     *
     * @return int
     */
    public function getBrojNeuspjelihTransfera()
    {
        return $this->brojNeuspjelihTransfera;
    }
}
