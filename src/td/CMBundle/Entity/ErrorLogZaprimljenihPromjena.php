<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ErrorLogZaprimljenihPromjena
 *
 * @ORM\Table(name="cm_error_log_zaprimljenih_promjena")
 * @ORM\Entity
 */
class ErrorLogZaprimljenihPromjena
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="idPromjena", type="integer")
     */
    private $idPromjena;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPromjena
     *
     * @param integer $idPromjena
     * @return ErrorLogZaprimljenihPromjena
     */
    public function setIdPromjena($idPromjena)
    {
        $this->idPromjena = $idPromjena;

        return $this;
    }

    /**
     * Get idPromjena
     *
     * @return integer 
     */
    public function getIdPromjena()
    {
        return $this->idPromjena;
    }
}
