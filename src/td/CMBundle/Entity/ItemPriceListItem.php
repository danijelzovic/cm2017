<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItemPriceListItem
 *
 * @ORM\Table(name="itempricelist_item")
 * @ORM\Entity
 */
class ItemPriceListItem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=1, options={"default":"A"})
     */
    private $status = "A";

    /**
     * @var integer
     *
     * @ORM\Column(name="id_company", type="integer", options={"unsigned":true, "default":0})
     */
    private $idCompany = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_was", type="integer")
     */
    private $idWas;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_webshop", type="integer")
     */
    private $idWebshop;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_pricelist", type="integer")
     */
    private $idPricelist;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_group", type="integer")
     */
    private $idGroup;

    /**
     * @var boolean
     *
     * @ORM\Column(name="sistemski", type="boolean")
     */
    private $sistemski;

    /**
     * @var boolean
     *
     * @ORM\Column(name="stavka_cijeli_sustav", type="boolean")
     */
    private $stavkaCijeliSustav;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_back", type="boolean", options={"default":0})
     */
    private $useBack = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_front", type="boolean", options={"default":0})
     */
    private $useFront = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordering", type="smallint", options={"unsigned":true, "default":0})
     */
    private $ordering = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="system", type="string", length=64)
     */
    private $system;

    /**
     * @var boolean
     *
     * @ORM\Column(name="proizvod", type="boolean", options={"default":0})
     */
    private $proizvod = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="oznaka", type="string", length=32)
     */
    private $oznaka;

    /**
     * @var string
     *
     * @ORM\Column(name="valuta", type="string", length=3, options={"default":"EUR"})
     */
    private $valuta = "EUR";

    /**
     * @var string
     *
     * @ORM\Column(name="vezni_dokument", type="string", length=255)
     */
    private $vezniDokument;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_entered_by", type="integer", options={"unsigned":true, "default":0})
     */
    private $idEnteredBy = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_entered", type="datetime", options={"default":"0000-00-00 00:00:00"})
     */
    private $timeEntered;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_changed_by", type="integer", options={"unsigned":true, "default":0})
     */
    private $idChangedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_change", type="datetime", options={"default":"0000-00-00 00:00:00"})
     */
    private $timeChange;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=32)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="type_calculation", type="string", length=32)
     */
    private $typeCalculation;

    /**
     * @var string
     *
     * @ORM\Column(name="tip_dodatka", type="string", length=1, options={"default":"I"})
     */
    private $tipDodatka = "I";

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_quantity", type="boolean", options={"default":0})
     */
    private $useQuantity = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="max_quantity", type="smallint", options={"default":0})
     */
    private $maxQuantity = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_code", type="boolean", options={"unsigned":true, "default":0})
     */
    private $useCode = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255)
     */
    private $code;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_age", type="boolean", options={"unsigned":true, "default":0})
     */
    private $useAge = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="age_from", type="smallint", options={"unsigned":true, "default":0})
     */
    private $ageFrom = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="age_till", type="smallint", options={"unsigned":true})
     */
    private $ageTill;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_period_booking", type="boolean", options={"unsigned":true, "default":0})
     */
    private $usePeriodBooking = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="period_booking_from", type="date")
     */
    private $periodBookingFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="period_booking_till", type="date")
     */
    private $periodBookingTill;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_arrivals_before", type="boolean", options={"default":0})
     */
    private $useArrivalsBefore = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="arrivals_before", type="date")
     */
    private $arrivalsBefore;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_departures_after", type="boolean", options={"default":0})
     */
    private $useDeparturesAfter = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="departures_after", type="date")
     */
    private $departuresAfter;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_stay_min", type="boolean", options={"unsigned":true, "default":0})
     */
    private $useStayMin = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="stay_min", type="smallint", options={"unsigned":true})
     */
    private $stayMin;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_stay_max", type="boolean", options={"unsigned":true, "default":0})
     */
    private $useStayMax = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="stay_max", type="smallint", options={"unsigned":true})
     */
    private $stayMax;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_num_guest", type="boolean")
     */
    private $useNumGest;

    /**
     * @var integer
     *
     * @ORM\Column(name="num_guest_from", type="smallint")
     */
    private $numGuestFrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="num_guest_till", type="smallint")
     */
    private $numGuestTill;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_num_adults", type="boolean")
     */
    private $useNumAdults;

    /**
     * @var integer
     *
     * @ORM\Column(name="num_adults_from", type="smallint")
     */
    private $numAdultsFrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="num_adults_till", type="smallint")
     */
    private $numAdultsTill;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_num_children", type="boolean")
     */
    private $useNumChildren;

    /**
     * @var integer
     *
     * @ORM\Column(name="children_age", type="smallint")
     */
    private $childrenAge;

    /**
     * @var integer
     *
     * @ORM\Column(name="num_children_from", type="smallint")
     */
    private $numChildrenFrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="num_children_till", type="smallint")
     */
    private $numChildrenTill;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_bed_type", type="boolean")
     */
    private $useBedType;

    /**
     * @var string
     *
     * @ORM\Column(name="bed_type", type="string", length=16)
     */
    private $bedType;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_bed_from", type="boolean")
     */
    private $useBedFrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="num_bed_from", type="smallint")
     */
    private $numBedFrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="num_bed_to", type="smallint")
     */
    private $numBedTo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_arrival_day", type="boolean", options={"unsigned":true, "default":0})
     */
    private $useArrivalDay = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="arrival_day", type="string", length=255)
     */
    private $arrivalDay;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_release", type="boolean")
     */
    private $useRelease;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_service", type="boolean", options={"unsigned":true, "default":0})
     */
    private $useService = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="service", type="string", length=255)
     */
    private $service;

    /**
     * @var string
     *
     * @ORM\Column(name="basic_service", type="string", length=255)
     */
    private $basicService;

    /**
     * @var boolean
     *
     * @ORM\Column(name="no_b2b_koef", type="boolean", options={"default":0})
     */
    private $noB2bKoef = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="info_stavka", type="boolean")
     */
    private $infoStavka;

    /**
     * @var string
     *
     * @ORM\Column(name="tip_placanja", type="string", length=1)
     */
    private $tipPlacanja;

    /**
     * @var integer
     *
     * @ORM\Column(name="change_stanje", type="integer")
     */
    private $changeStanje;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ne_fakturirati", type="boolean", options={"default":0})
     */
    private $neFakturirati = 0;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return ItemPriceListItem
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set idCompany
     *
     * @param integer $idCompany
     * @return ItemPriceListItem
     */
    public function setIdCompany($idCompany)
    {
        $this->idCompany = $idCompany;

        return $this;
    }

    /**
     * Get idCompany
     *
     * @return integer 
     */
    public function getIdCompany()
    {
        return $this->idCompany;
    }

    /**
     * Set idWas
     *
     * @param integer $idWas
     * @return ItemPriceListItem
     */
    public function setIdWas($idWas)
    {
        $this->idWas = $idWas;

        return $this;
    }

    /**
     * Get idWas
     *
     * @return integer 
     */
    public function getIdWas()
    {
        return $this->idWas;
    }

    /**
     * Set idWebshop
     *
     * @param integer $idWebshop
     * @return ItemPriceListItem
     */
    public function setIdWebshop($idWebshop)
    {
        $this->idWebshop = $idWebshop;

        return $this;
    }

    /**
     * Get idWebshop
     *
     * @return integer 
     */
    public function getIdWebshop()
    {
        return $this->idWebshop;
    }

    /**
     * Set idPricelist
     *
     * @param integer $idPricelist
     * @return ItemPriceListItem
     */
    public function setIdPricelist($idPricelist)
    {
        $this->idPricelist = $idPricelist;

        return $this;
    }

    /**
     * Get idPricelist
     *
     * @return integer 
     */
    public function getIdPricelist()
    {
        return $this->idPricelist;
    }

    /**
     * Set idGroup
     *
     * @param integer $idGroup
     * @return ItemPriceListItem
     */
    public function setIdGroup($idGroup)
    {
        $this->idGroup = $idGroup;

        return $this;
    }

    /**
     * Get idGroup
     *
     * @return integer 
     */
    public function getIdGroup()
    {
        return $this->idGroup;
    }

    /**
     * Set sistemski
     *
     * @param boolean $sistemski
     * @return ItemPriceListItem
     */
    public function setSistemski($sistemski)
    {
        $this->sistemski = $sistemski;

        return $this;
    }

    /**
     * Get sistemski
     *
     * @return boolean 
     */
    public function getSistemski()
    {
        return $this->sistemski;
    }

    /**
     * Set stavkaCijeliSustav
     *
     * @param boolean $stavkaCijeliSustav
     * @return ItemPriceListItem
     */
    public function setStavkaCijeliSustav($stavkaCijeliSustav)
    {
        $this->stavkaCijeliSustav = $stavkaCijeliSustav;

        return $this;
    }

    /**
     * Get stavkaCijeliSustav
     *
     * @return boolean 
     */
    public function getStavkaCijeliSustav()
    {
        return $this->stavkaCijeliSustav;
    }

    /**
     * Set useBack
     *
     * @param boolean $useBack
     * @return ItemPriceListItem
     */
    public function setUseBack($useBack)
    {
        $this->useBack = $useBack;

        return $this;
    }

    /**
     * Get useBack
     *
     * @return boolean 
     */
    public function getUseBack()
    {
        return $this->useBack;
    }

    /**
     * Set useFront
     *
     * @param boolean $useFront
     * @return ItemPriceListItem
     */
    public function setUseFront($useFront)
    {
        $this->useFront = $useFront;

        return $this;
    }

    /**
     * Get useFront
     *
     * @return boolean 
     */
    public function getUseFront()
    {
        return $this->useFront;
    }

    /**
     * Set ordering
     *
     * @param integer $ordering
     * @return ItemPriceListItem
     */
    public function setOrdering($ordering)
    {
        $this->ordering = $ordering;

        return $this;
    }

    /**
     * Get ordering
     *
     * @return integer 
     */
    public function getOrdering()
    {
        return $this->ordering;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ItemPriceListItem
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set system
     *
     * @param string $system
     * @return ItemPriceListItem
     */
    public function setSystem($system)
    {
        $this->system = $system;

        return $this;
    }

    /**
     * Get system
     *
     * @return string 
     */
    public function getSystem()
    {
        return $this->system;
    }

    /**
     * Set proizvod
     *
     * @param boolean $proizvod
     * @return ItemPriceListItem
     */
    public function setProizvod($proizvod)
    {
        $this->proizvod = $proizvod;

        return $this;
    }

    /**
     * Get proizvod
     *
     * @return boolean 
     */
    public function getProizvod()
    {
        return $this->proizvod;
    }

    /**
     * Set oznaka
     *
     * @param string $oznaka
     * @return ItemPriceListItem
     */
    public function setOznaka($oznaka)
    {
        $this->oznaka = $oznaka;

        return $this;
    }

    /**
     * Get oznaka
     *
     * @return string 
     */
    public function getOznaka()
    {
        return $this->oznaka;
    }

    /**
     * Set valuta
     *
     * @param string $valuta
     * @return ItemPriceListItem
     */
    public function setValuta($valuta)
    {
        $this->valuta = $valuta;

        return $this;
    }

    /**
     * Get valuta
     *
     * @return string 
     */
    public function getValuta()
    {
        return $this->valuta;
    }

    /**
     * Set vezniDokument
     *
     * @param string $vezniDokument
     * @return ItemPriceListItem
     */
    public function setVezniDokument($vezniDokument)
    {
        $this->vezniDokument = $vezniDokument;

        return $this;
    }

    /**
     * Get vezniDokument
     *
     * @return string 
     */
    public function getVezniDokument()
    {
        return $this->vezniDokument;
    }

    /**
     * Set idEnteredBy
     *
     * @param integer $idEnteredBy
     * @return ItemPriceListItem
     */
    public function setIdEnteredBy($idEnteredBy)
    {
        $this->idEnteredBy = $idEnteredBy;

        return $this;
    }

    /**
     * Get idEnteredBy
     *
     * @return integer 
     */
    public function getIdEnteredBy()
    {
        return $this->idEnteredBy;
    }

    /**
     * Set timeEntered
     *
     * @param \DateTime $timeEntered
     * @return ItemPriceListItem
     */
    public function setTimeEntered($timeEntered)
    {
        $this->timeEntered = $timeEntered;

        return $this;
    }

    /**
     * Get timeEntered
     *
     * @return \DateTime 
     */
    public function getTimeEntered()
    {
        return $this->timeEntered;
    }

    /**
     * Set idChangedBy
     *
     * @param integer $idChangedBy
     * @return ItemPriceListItem
     */
    public function setIdChangedBy($idChangedBy)
    {
        $this->idChangedBy = $idChangedBy;

        return $this;
    }

    /**
     * Get idChangedBy
     *
     * @return integer 
     */
    public function getIdChangedBy()
    {
        return $this->idChangedBy;
    }

    /**
     * Set timeChange
     *
     * @param \DateTime $timeChange
     * @return ItemPriceListItem
     */
    public function setTimeChange($timeChange)
    {
        $this->timeChange = $timeChange;

        return $this;
    }

    /**
     * Get timeChange
     *
     * @return \DateTime 
     */
    public function getTimeChange()
    {
        return $this->timeChange;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return ItemPriceListItem
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set typeCalculation
     *
     * @param string $typeCalculation
     * @return ItemPriceListItem
     */
    public function setTypeCalculation($typeCalculation)
    {
        $this->typeCalculation = $typeCalculation;

        return $this;
    }

    /**
     * Get typeCalculation
     *
     * @return string 
     */
    public function getTypeCalculation()
    {
        return $this->typeCalculation;
    }

    /**
     * Set tipDodatka
     *
     * @param string $tipDodatka
     * @return ItemPriceListItem
     */
    public function setTipDodatka($tipDodatka)
    {
        $this->tipDodatka = $tipDodatka;

        return $this;
    }

    /**
     * Get tipDodatka
     *
     * @return string 
     */
    public function getTipDodatka()
    {
        return $this->tipDodatka;
    }

    /**
     * Set useQuantity
     *
     * @param boolean $useQuantity
     * @return ItemPriceListItem
     */
    public function setUseQuantity($useQuantity)
    {
        $this->useQuantity = $useQuantity;

        return $this;
    }

    /**
     * Get useQuantity
     *
     * @return boolean 
     */
    public function getUseQuantity()
    {
        return $this->useQuantity;
    }

    /**
     * Set maxQuantity
     *
     * @param integer $maxQuantity
     * @return ItemPriceListItem
     */
    public function setMaxQuantity($maxQuantity)
    {
        $this->maxQuantity = $maxQuantity;

        return $this;
    }

    /**
     * Get maxQuantity
     *
     * @return integer 
     */
    public function getMaxQuantity()
    {
        return $this->maxQuantity;
    }

    /**
     * Set useCode
     *
     * @param boolean $useCode
     * @return ItemPriceListItem
     */
    public function setUseCode($useCode)
    {
        $this->useCode = $useCode;

        return $this;
    }

    /**
     * Get useCode
     *
     * @return boolean 
     */
    public function getUseCode()
    {
        return $this->useCode;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return ItemPriceListItem
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set useAge
     *
     * @param boolean $useAge
     * @return ItemPriceListItem
     */
    public function setUseAge($useAge)
    {
        $this->useAge = $useAge;

        return $this;
    }

    /**
     * Get useAge
     *
     * @return boolean 
     */
    public function getUseAge()
    {
        return $this->useAge;
    }

    /**
     * Set ageFrom
     *
     * @param integer $ageFrom
     * @return ItemPriceListItem
     */
    public function setAgeFrom($ageFrom)
    {
        $this->ageFrom = $ageFrom;

        return $this;
    }

    /**
     * Get ageFrom
     *
     * @return integer 
     */
    public function getAgeFrom()
    {
        return $this->ageFrom;
    }

    /**
     * Set ageTill
     *
     * @param integer $ageTill
     * @return ItemPriceListItem
     */
    public function setAgeTill($ageTill)
    {
        $this->ageTill = $ageTill;

        return $this;
    }

    /**
     * Get ageTill
     *
     * @return integer 
     */
    public function getAgeTill()
    {
        return $this->ageTill;
    }

    /**
     * Set usePeriodBooking
     *
     * @param boolean $usePeriodBooking
     * @return ItemPriceListItem
     */
    public function setUsePeriodBooking($usePeriodBooking)
    {
        $this->usePeriodBooking = $usePeriodBooking;

        return $this;
    }

    /**
     * Get usePeriodBooking
     *
     * @return boolean 
     */
    public function getUsePeriodBooking()
    {
        return $this->usePeriodBooking;
    }

    /**
     * Set periodBookingFrom
     *
     * @param \DateTime $periodBookingFrom
     * @return ItemPriceListItem
     */
    public function setPeriodBookingFrom($periodBookingFrom)
    {
        $this->periodBookingFrom = $periodBookingFrom;

        return $this;
    }

    /**
     * Get periodBookingFrom
     *
     * @return \DateTime 
     */
    public function getPeriodBookingFrom()
    {
        return $this->periodBookingFrom;
    }

    /**
     * Set periodBookingTill
     *
     * @param \DateTime $periodBookingTill
     * @return ItemPriceListItem
     */
    public function setPeriodBookingTill($periodBookingTill)
    {
        $this->periodBookingTill = $periodBookingTill;

        return $this;
    }

    /**
     * Get periodBookingTill
     *
     * @return \DateTime 
     */
    public function getPeriodBookingTill()
    {
        return $this->periodBookingTill;
    }

    /**
     * Set useArrivalsBefore
     *
     * @param boolean $useArrivalsBefore
     * @return ItemPriceListItem
     */
    public function setUseArrivalsBefore($useArrivalsBefore)
    {
        $this->useArrivalsBefore = $useArrivalsBefore;

        return $this;
    }

    /**
     * Get useArrivalsBefore
     *
     * @return boolean 
     */
    public function getUseArrivalsBefore()
    {
        return $this->useArrivalsBefore;
    }

    /**
     * Set arrivalsBefore
     *
     * @param \DateTime $arrivalsBefore
     * @return ItemPriceListItem
     */
    public function setArrivalsBefore($arrivalsBefore)
    {
        $this->arrivalsBefore = $arrivalsBefore;

        return $this;
    }

    /**
     * Get arrivalsBefore
     *
     * @return \DateTime 
     */
    public function getArrivalsBefore()
    {
        return $this->arrivalsBefore;
    }

    /**
     * Set useDeparturesAfter
     *
     * @param boolean $useDeparturesAfter
     * @return ItemPriceListItem
     */
    public function setUseDeparturesAfter($useDeparturesAfter)
    {
        $this->useDeparturesAfter = $useDeparturesAfter;

        return $this;
    }

    /**
     * Get useDeparturesAfter
     *
     * @return boolean 
     */
    public function getUseDeparturesAfter()
    {
        return $this->useDeparturesAfter;
    }

    /**
     * Set departuresAfter
     *
     * @param \DateTime $departuresAfter
     * @return ItemPriceListItem
     */
    public function setDeparturesAfter($departuresAfter)
    {
        $this->departuresAfter = $departuresAfter;

        return $this;
    }

    /**
     * Get departuresAfter
     *
     * @return \DateTime 
     */
    public function getDeparturesAfter()
    {
        return $this->departuresAfter;
    }

    /**
     * Set useStayMin
     *
     * @param boolean $useStayMin
     * @return ItemPriceListItem
     */
    public function setUseStayMin($useStayMin)
    {
        $this->useStayMin = $useStayMin;

        return $this;
    }

    /**
     * Get useStayMin
     *
     * @return boolean 
     */
    public function getUseStayMin()
    {
        return $this->useStayMin;
    }

    /**
     * Set stayMin
     *
     * @param integer $stayMin
     * @return ItemPriceListItem
     */
    public function setStayMin($stayMin)
    {
        $this->stayMin = $stayMin;

        return $this;
    }

    /**
     * Get stayMin
     *
     * @return integer 
     */
    public function getStayMin()
    {
        return $this->stayMin;
    }

    /**
     * Set useStayMax
     *
     * @param boolean $useStayMax
     * @return ItemPriceListItem
     */
    public function setUseStayMax($useStayMax)
    {
        $this->useStayMax = $useStayMax;

        return $this;
    }

    /**
     * Get useStayMax
     *
     * @return boolean 
     */
    public function getUseStayMax()
    {
        return $this->useStayMax;
    }

    /**
     * Set stayMax
     *
     * @param integer $stayMax
     * @return ItemPriceListItem
     */
    public function setStayMax($stayMax)
    {
        $this->stayMax = $stayMax;

        return $this;
    }

    /**
     * Get stayMax
     *
     * @return integer 
     */
    public function getStayMax()
    {
        return $this->stayMax;
    }

    /**
     * Set useNumGest
     *
     * @param boolean $useNumGest
     * @return ItemPriceListItem
     */
    public function setUseNumGest($useNumGest)
    {
        $this->useNumGest = $useNumGest;

        return $this;
    }

    /**
     * Get useNumGest
     *
     * @return boolean 
     */
    public function getUseNumGest()
    {
        return $this->useNumGest;
    }

    /**
     * Set numGuestFrom
     *
     * @param integer $numGuestFrom
     * @return ItemPriceListItem
     */
    public function setNumGuestFrom($numGuestFrom)
    {
        $this->numGuestFrom = $numGuestFrom;

        return $this;
    }

    /**
     * Get numGuestFrom
     *
     * @return integer 
     */
    public function getNumGuestFrom()
    {
        return $this->numGuestFrom;
    }

    /**
     * Set numGuestTill
     *
     * @param integer $numGuestTill
     * @return ItemPriceListItem
     */
    public function setNumGuestTill($numGuestTill)
    {
        $this->numGuestTill = $numGuestTill;

        return $this;
    }

    /**
     * Get numGuestTill
     *
     * @return integer 
     */
    public function getNumGuestTill()
    {
        return $this->numGuestTill;
    }

    /**
     * Set useNumAdults
     *
     * @param boolean $useNumAdults
     * @return ItemPriceListItem
     */
    public function setUseNumAdults($useNumAdults)
    {
        $this->useNumAdults = $useNumAdults;

        return $this;
    }

    /**
     * Get useNumAdults
     *
     * @return boolean 
     */
    public function getUseNumAdults()
    {
        return $this->useNumAdults;
    }

    /**
     * Set numAdultsFrom
     *
     * @param integer $numAdultsFrom
     * @return ItemPriceListItem
     */
    public function setNumAdultsFrom($numAdultsFrom)
    {
        $this->numAdultsFrom = $numAdultsFrom;

        return $this;
    }

    /**
     * Get numAdultsFrom
     *
     * @return integer 
     */
    public function getNumAdultsFrom()
    {
        return $this->numAdultsFrom;
    }

    /**
     * Set numAdultsTill
     *
     * @param integer $numAdultsTill
     * @return ItemPriceListItem
     */
    public function setNumAdultsTill($numAdultsTill)
    {
        $this->numAdultsTill = $numAdultsTill;

        return $this;
    }

    /**
     * Get numAdultsTill
     *
     * @return integer 
     */
    public function getNumAdultsTill()
    {
        return $this->numAdultsTill;
    }

    /**
     * Set useNumChildren
     *
     * @param boolean $useNumChildren
     * @return ItemPriceListItem
     */
    public function setUseNumChildren($useNumChildren)
    {
        $this->useNumChildren = $useNumChildren;

        return $this;
    }

    /**
     * Get useNumChildren
     *
     * @return boolean 
     */
    public function getUseNumChildren()
    {
        return $this->useNumChildren;
    }

    /**
     * Set childrenAge
     *
     * @param integer $childrenAge
     * @return ItemPriceListItem
     */
    public function setChildrenAge($childrenAge)
    {
        $this->childrenAge = $childrenAge;

        return $this;
    }

    /**
     * Get childrenAge
     *
     * @return integer 
     */
    public function getChildrenAge()
    {
        return $this->childrenAge;
    }

    /**
     * Set numChildrenFrom
     *
     * @param integer $numChildrenFrom
     * @return ItemPriceListItem
     */
    public function setNumChildrenFrom($numChildrenFrom)
    {
        $this->numChildrenFrom = $numChildrenFrom;

        return $this;
    }

    /**
     * Get numChildrenFrom
     *
     * @return integer 
     */
    public function getNumChildrenFrom()
    {
        return $this->numChildrenFrom;
    }

    /**
     * Set numChildrenTill
     *
     * @param integer $numChildrenTill
     * @return ItemPriceListItem
     */
    public function setNumChildrenTill($numChildrenTill)
    {
        $this->numChildrenTill = $numChildrenTill;

        return $this;
    }

    /**
     * Get numChildrenTill
     *
     * @return integer 
     */
    public function getNumChildrenTill()
    {
        return $this->numChildrenTill;
    }

    /**
     * Set useBedType
     *
     * @param boolean $useBedType
     * @return ItemPriceListItem
     */
    public function setUseBedType($useBedType)
    {
        $this->useBedType = $useBedType;

        return $this;
    }

    /**
     * Get useBedType
     *
     * @return boolean 
     */
    public function getUseBedType()
    {
        return $this->useBedType;
    }

    /**
     * Set bedType
     *
     * @param string $bedType
     * @return ItemPriceListItem
     */
    public function setBedType($bedType)
    {
        $this->bedType = $bedType;

        return $this;
    }

    /**
     * Get bedType
     *
     * @return string 
     */
    public function getBedType()
    {
        return $this->bedType;
    }

    /**
     * Set useBedFrom
     *
     * @param boolean $useBedFrom
     * @return ItemPriceListItem
     */
    public function setUseBedFrom($useBedFrom)
    {
        $this->useBedFrom = $useBedFrom;

        return $this;
    }

    /**
     * Get useBedFrom
     *
     * @return boolean 
     */
    public function getUseBedFrom()
    {
        return $this->useBedFrom;
    }

    /**
     * Set numBedFrom
     *
     * @param integer $numBedFrom
     * @return ItemPriceListItem
     */
    public function setNumBedFrom($numBedFrom)
    {
        $this->numBedFrom = $numBedFrom;

        return $this;
    }

    /**
     * Get numBedFrom
     *
     * @return integer 
     */
    public function getNumBedFrom()
    {
        return $this->numBedFrom;
    }

    /**
     * Set numBedTo
     *
     * @param integer $numBedTo
     * @return ItemPriceListItem
     */
    public function setNumBedTo($numBedTo)
    {
        $this->numBedTo = $numBedTo;

        return $this;
    }

    /**
     * Get numBedTo
     *
     * @return integer 
     */
    public function getNumBedTo()
    {
        return $this->numBedTo;
    }

    /**
     * Set useArrivalDay
     *
     * @param boolean $useArrivalDay
     * @return ItemPriceListItem
     */
    public function setUseArrivalDay($useArrivalDay)
    {
        $this->useArrivalDay = $useArrivalDay;

        return $this;
    }

    /**
     * Get useArrivalDay
     *
     * @return boolean 
     */
    public function getUseArrivalDay()
    {
        return $this->useArrivalDay;
    }

    /**
     * Set arrivalDay
     *
     * @param string $arrivalDay
     * @return ItemPriceListItem
     */
    public function setArrivalDay($arrivalDay)
    {
        $this->arrivalDay = $arrivalDay;

        return $this;
    }

    /**
     * Get arrivalDay
     *
     * @return string 
     */
    public function getArrivalDay()
    {
        return $this->arrivalDay;
    }

    /**
     * Set useRelease
     *
     * @param boolean $useRelease
     * @return ItemPriceListItem
     */
    public function setUseRelease($useRelease)
    {
        $this->useRelease = $useRelease;

        return $this;
    }

    /**
     * Get useRelease
     *
     * @return boolean 
     */
    public function getUseRelease()
    {
        return $this->useRelease;
    }

    /**
     * Set useService
     *
     * @param boolean $useService
     * @return ItemPriceListItem
     */
    public function setUseService($useService)
    {
        $this->useService = $useService;

        return $this;
    }

    /**
     * Get useService
     *
     * @return boolean 
     */
    public function getUseService()
    {
        return $this->useService;
    }

    /**
     * Set service
     *
     * @param string $service
     * @return ItemPriceListItem
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return string 
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set basicService
     *
     * @param string $basicService
     * @return ItemPriceListItem
     */
    public function setBasicService($basicService)
    {
        $this->basicService = $basicService;

        return $this;
    }

    /**
     * Get basicService
     *
     * @return string 
     */
    public function getBasicService()
    {
        return $this->basicService;
    }

    /**
     * Set noB2bKoef
     *
     * @param boolean $noB2bKoef
     * @return ItemPriceListItem
     */
    public function setNoB2bKoef($noB2bKoef)
    {
        $this->noB2bKoef = $noB2bKoef;

        return $this;
    }

    /**
     * Get noB2bKoef
     *
     * @return boolean 
     */
    public function getNoB2bKoef()
    {
        return $this->noB2bKoef;
    }

    /**
     * Set infoStavka
     *
     * @param boolean $infoStavka
     * @return ItemPriceListItem
     */
    public function setInfoStavka($infoStavka)
    {
        $this->infoStavka = $infoStavka;

        return $this;
    }

    /**
     * Get infoStavka
     *
     * @return boolean 
     */
    public function getInfoStavka()
    {
        return $this->infoStavka;
    }

    /**
     * Set tipPlacanja
     *
     * @param string $tipPlacanja
     * @return ItemPriceListItem
     */
    public function setTipPlacanja($tipPlacanja)
    {
        $this->tipPlacanja = $tipPlacanja;

        return $this;
    }

    /**
     * Get tipPlacanja
     *
     * @return string 
     */
    public function getTipPlacanja()
    {
        return $this->tipPlacanja;
    }

    /**
     * Set changeStanje
     *
     * @param integer $changeStanje
     * @return ItemPriceListItem
     */
    public function setChangeStanje($changeStanje)
    {
        $this->changeStanje = $changeStanje;

        return $this;
    }

    /**
     * Get changeStanje
     *
     * @return integer 
     */
    public function getChangeStanje()
    {
        return $this->changeStanje;
    }

    /**
     * Set neFakturirati
     *
     * @param boolean $neFakturirati
     * @return ItemPriceListItem
     */
    public function setNeFakturirati($neFakturirati)
    {
        $this->neFakturirati = $neFakturirati;

        return $this;
    }

    /**
     * Get neFakturirati
     *
     * @return boolean 
     */
    public function getNeFakturirati()
    {
        return $this->neFakturirati;
    }
}
