<?php

namespace td\CMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PortalCodeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('id', 'text', array('attr'=>array('disabled'=>'disabled')))
//            ->add('code', 'text', array('attr'=>array('disabled'=>'disabled')))
//            ->add('code', 'text')

            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
                $code = $event->getData();
                $form = $event->getForm();

                if (!$code || $code->getCode() === null) {
                    $form->add('id', 'text');
                    $form->add('description', 'textarea', array('label' => 'Opis'));
                } else {
                    $form->add('id', 'text', array('attr' => array('readonly' => 'readonly')));
                    $form->add('description', 'textarea', array('label' => 'Opis'));
                }
            });

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'td\CMBundle\Entity\PortalCode'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'td_cmbundle_portalcode';
    }
}
