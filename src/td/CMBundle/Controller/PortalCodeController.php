<?php

namespace td\CMBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Response;
use td\CMBundle\Entity\PortalCode;
use td\CMBundle\Form\PortalCodeType;

/**
 * PortalCode controller.
 *
 */
class PortalCodeController extends Controller
{

    /**
     * Lists all PortalCode entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('tdCMBundle:PortalCode')->findAll();

        return $this->render('tdCMBundle:PortalCode:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new PortalCode entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new PortalCode();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('portalcode_show', array('id' => $entity->getId())));
        }

        return $this->render('tdCMBundle:PortalCode:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a PortalCode entity.
     *
     * @param PortalCode $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(PortalCode $entity)
    {
        $form = $this->createForm(new PortalCodeType(), $entity, array(
            'action' => $this->generateUrl('portalcode_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Izradi',
            'attr' => array(
                'class' => 'btn-sm btn-success',
            )));

        return $form;
    }

    /**
     * Displays a form to create a new PortalCode entity.
     *
     */
    public function newAction()
    {
        $entity = new PortalCode();
        $form   = $this->createCreateForm($entity);

        return $this->render('tdCMBundle:PortalCode:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a PortalCode entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('tdCMBundle:PortalCode')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PortalCode entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('tdCMBundle:PortalCode:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing PortalCode entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('tdCMBundle:PortalCode')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PortalCode entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('tdCMBundle:PortalCode:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a PortalCode entity.
    *
    * @param PortalCode $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(PortalCode $entity)
    {
        $form = $this->createForm(new PortalCodeType(), $entity, array(
            'action' => $this->generateUrl('portalcode_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Ažuriraj',
            'attr' => array(
                'class' => 'btn-primary btn-sm',
            )
        ));

        return $form;
    }
    /**
     * Edits an existing PortalCode entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('tdCMBundle:PortalCode')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PortalCode entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('portalcode_edit', array('id' => $id)));
        }

        return $this->render('tdCMBundle:PortalCode:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a PortalCode entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('tdCMBundle:PortalCode')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find PortalCode entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('portalcode'));
    }

    /**
     * Creates a form to delete a PortalCode entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('portalcode_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                'label' => 'Obriši',
                'attr' => array(
                    'class' => 'btn-danger btn-sm',
                )
            ))
            ->getForm()
            ;
    }
}
