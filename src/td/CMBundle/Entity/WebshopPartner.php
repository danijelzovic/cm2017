<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WebshopPartner
 *
 * @ORM\Table(name="webshop_partner")
 * @ORM\Entity
 */
class WebshopPartner
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

	/**
     * @ORM\ManyToOne(targetEntity="Webshop", inversedBy="webshopPartneri")
     * @ORM\JoinColumn(name="id_webshop", referencedColumnName="id", onDelete="CASCADE")
	 */
	private $webshop;
	
    /**
     * @var integer
     *
     * @ORM\Column(name="id_company_partner", type="integer")
     */
    private $idCompanyPartner;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_webshop_partner", type="integer")
     */
    private $idWebshopPartner;

	/**
     * @ORM\ManyToOne(targetEntity="Webshop")
     * @ORM\JoinColumn(name="id_webshop_partner", referencedColumnName="id", onDelete="CASCADE")
	 */
	private $webshopPartner;
	
    /**
     * @var integer
     *
     * @ORM\Column(name="id_webshop_partnerov", type="integer")
     */
    private $idWebshopPartnerov;

    /**
     * @var boolean
     *
     * @ORM\Column(name="korisnik", type="boolean")
     */
    private $korisnik;

    /**
     * @var string
     *
     * @ORM\Column(name="partner_je", type="string", length=1)
     */
    private $partnerJe;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="provizija", type="decimal")
     */
    private $provizija;

    /**
     * @var boolean
     *
     * @ORM\Column(name="proslijedi", type="boolean")
     */
    private $proslijedi;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pricelist_out", type="boolean")
     */
    private $pricelistOut;

    /**
     * @var string
     *
     * @ORM\Column(name="referentni_cjenik", type="string", length=3)
     */
    private $referentniCjenik;

    /**
     * @var string
     *
     * @ORM\Column(name="b2b_to_b2c", type="decimal")
     */
    private $b2bToB2c;

    /**
     * @var boolean
     *
     * @ORM\Column(name="b2c_zaokruzivanje", type="boolean")
     */
    private $b2cZaokruzivanje;
  

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCompanyPartner
     *
     * @param integer $idCompanyPartner
     *
     * @return WebshopPartner
     */
    public function setIdCompanyPartner($idCompanyPartner)
    {
        $this->idCompanyPartner = $idCompanyPartner;

        return $this;
    }

    /**
     * Get idCompanyPartner
     *
     * @return integer
     */
    public function getIdCompanyPartner()
    {
        return $this->idCompanyPartner;
    }

    /**
     * Set idWebshopPartner
     *
     * @param integer $idWebshopPartner
     *
     * @return WebshopPartner
     */
    public function setIdWebshopPartner($idWebshopPartner)
    {
        $this->idWebshopPartner = $idWebshopPartner;

        return $this;
    }

    /**
     * Get idWebshopPartner
     *
     * @return integer
     */
    public function getIdWebshopPartner()
    {
        return $this->idWebshopPartner;
    }

    /**
     * Set idWebshopPartnerov
     *
     * @param integer $idWebshopPartnerov
     *
     * @return WebshopPartner
     */
    public function setIdWebshopPartnerov($idWebshopPartnerov)
    {
        $this->idWebshopPartnerov = $idWebshopPartnerov;

        return $this;
    }

    /**
     * Get idWebshopPartnerov
     *
     * @return integer
     */
    public function getIdWebshopPartnerov()
    {
        return $this->idWebshopPartnerov;
    }

    /**
     * Set korisnik
     *
     * @param boolean $korisnik
     *
     * @return WebshopPartner
     */
    public function setKorisnik($korisnik)
    {
        $this->korisnik = $korisnik;

        return $this;
    }

    /**
     * Get korisnik
     *
     * @return boolean
     */
    public function getKorisnik()
    {
        return $this->korisnik;
    }

    /**
     * Set partnerJe
     *
     * @param string $partnerJe
     *
     * @return WebshopPartner
     */
    public function setPartnerJe($partnerJe)
    {
        $this->partnerJe = $partnerJe;

        return $this;
    }

    /**
     * Get partnerJe
     *
     * @return string
     */
    public function getPartnerJe()
    {
        return $this->partnerJe;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return WebshopPartner
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set provizija
     *
     * @param string $provizija
     *
     * @return WebshopPartner
     */
    public function setProvizija($provizija)
    {
        $this->provizija = $provizija;

        return $this;
    }

    /**
     * Get provizija
     *
     * @return string
     */
    public function getProvizija()
    {
        return $this->provizija;
    }

    /**
     * Set proslijedi
     *
     * @param boolean $proslijedi
     *
     * @return WebshopPartner
     */
    public function setProslijedi($proslijedi)
    {
        $this->proslijedi = $proslijedi;

        return $this;
    }

    /**
     * Get proslijedi
     *
     * @return boolean
     */
    public function getProslijedi()
    {
        return $this->proslijedi;
    }

    /**
     * Set pricelistOut
     *
     * @param boolean $pricelistOut
     *
     * @return WebshopPartner
     */
    public function setPricelistOut($pricelistOut)
    {
        $this->pricelistOut = $pricelistOut;

        return $this;
    }

    /**
     * Get pricelistOut
     *
     * @return boolean
     */
    public function getPricelistOut()
    {
        return $this->pricelistOut;
    }

    /**
     * Set referentniCjenik
     *
     * @param string $referentniCjenik
     *
     * @return WebshopPartner
     */
    public function setReferentniCjenik($referentniCjenik)
    {
        $this->referentniCjenik = $referentniCjenik;

        return $this;
    }

    /**
     * Get referentniCjenik
     *
     * @return string
     */
    public function getReferentniCjenik()
    {
        return $this->referentniCjenik;
    }

    /**
     * Set b2bToB2c
     *
     * @param string $b2bToB2c
     *
     * @return WebshopPartner
     */
    public function setB2bToB2c($b2bToB2c)
    {
        $this->b2bToB2c = $b2bToB2c;

        return $this;
    }

    /**
     * Get b2bToB2c
     *
     * @return string
     */
    public function getB2bToB2c()
    {
        return $this->b2bToB2c;
    }

    /**
     * Set b2cZaokruzivanje
     *
     * @param boolean $b2cZaokruzivanje
     *
     * @return WebshopPartner
     */
    public function setB2cZaokruzivanje($b2cZaokruzivanje)
    {
        $this->b2cZaokruzivanje = $b2cZaokruzivanje;

        return $this;
    }

    /**
     * Get b2cZaokruzivanje
     *
     * @return boolean
     */
    public function getB2cZaokruzivanje()
    {
        return $this->b2cZaokruzivanje;
    }

    /**
     * Set webshop
     *
     * @param \td\CMBundle\Entity\Webshop $webshop
     *
     * @return WebshopPartner
     */
    public function setWebshop(\td\CMBundle\Entity\Webshop $webshop = null)
    {
        $this->webshop = $webshop;

        return $this;
    }

    /**
     * Get webshop
     *
     * @return \td\CMBundle\Entity\Webshop
     */
    public function getWebshop()
    {
        return $this->webshop;
    }

    /**
     * Set webshopPartner
     *
     * @param \td\CMBundle\Entity\Webshop $webshopPartner
     *
     * @return WebshopPartner
     */
    public function setWebshopPartner(\td\CMBundle\Entity\Webshop $webshopPartner = null)
    {
        $this->webshopPartner = $webshopPartner;

        return $this;
    }

    /**
     * Get webshopPartner
     *
     * @return \td\CMBundle\Entity\Webshop
     */
    public function getWebshopPartner()
    {
        return $this->webshopPartner;
    }
}
