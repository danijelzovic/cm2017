<?php

namespace td\CMBundle\Controller;


use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Exception;
use td\CMBundle\Entity\SetupProperty;
use td\CMBundle\Entity\SetupPropertyLog;
use td\CMBundle\Entity\SetupPropertyUnit;
use td\CMBundle\Entity\SetupPropertyUnitLog;
use td\CMBundle\Entity\UnitRate;
use Sensio\Bundle\GeneratorBundle\Tests\Generator\DoctrineEntityGeneratorTest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use td\CMBundle\Entity\PortalKorisnik;
use td\CMBundle\Entity\Promjena;
use td\CMBundle\Entity\Transfer;
use td\CMBundle\Entity\TransferLog;
use JMS;
use Datatheke\Bundle\PagerBundle\Pager\Field;
use td\CMBundle\Obrada\TransferObrada;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\PhpBridgeSessionStorage;


/**
 * Promjena controller.
 *
 */
class PromjenaController extends Controller
{

    /**
     * List all changes in classical form
     *
     */
    public function indexAction()
    {
//        $em = $this->getDoctrine()->getManager();
//
//        $entities = $em->getRepository('tdCMBundle:Promjena')->findBy(array(),null,50,1);

//        return $this->render('tdCMBundle:Promjena:index.html.twig', array(
//            'entities' => $entities
//        ));

        return $this->redirectToRoute('promjena_page');
    }

    /**
     * List all changes in classical form with pagionation parameteres
     *
     * Lists all Promjena entities straničeno.
     * @param $limit
     * @param $offset
     * @param $sort
     * @param $smjer
     * @return Response
     */
    public function indexStrAction($limit, $offset, $sort, $smjer)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('tdCMBundle:Promjena');
        $qb = $repo->createQueryBuilder('a');
        $qb->select('COUNT(a)');
        $count = $qb->getQuery()->getSingleScalarResult();
//        $count = 500;

        $brStr = $count / $limit - 1;

        if ($limit == 0) {
//            $entities = $em->getRepository('tdCMBundle:Promjena')->findAll();
            $entities = $em->getRepository('tdCMBundle:Promjena')->findBy(array(), $sort, 100, 0);
            $limit = 100;
            $offset = 0;


        } else {
            if ($offset < 0) $offset = 0;
            $entities = $em->getRepository('tdCMBundle:Promjena')->findBy(array(), array($sort => $smjer), $limit, $offset);
        }

        return $this->render('tdCMBundle:Promjena:index.html.twig', array(
            'entities' => $entities,
            'limit' => $limit,
            'offset' => $offset,
            'brStr' => $brStr,
            'sort' => $sort,
            'smjer' => $smjer
        ));
    }

    /**
     * Prikaz svih promjena preko tablice JqGrid
     *
     * @return Response
     */
    function indexJqGridAction()
    {
        $em = $this->getDoctrine()->getManager();

        //Grupe promjena - za select seearch
        $grupePromjena = $em->getRepository('tdCMBundle:PromjenaGrupa')->findAll();
        $grupePromjenaStr = ':[Sve]';
        foreach ($grupePromjena as $gp) {
            $grupePromjenaStr .= ';' . $gp->getNaziv() . ':' . $gp->getNaziv();
        }

        //Tipovi promjena - za sleect search
        $tipoviPromjena = $em->getRepository('tdCMBundle:PromjenaTip')->findAll();
        $tipoviPromjenaStr = ':[Sve]';
        foreach ($tipoviPromjena as $tp) {
            $tipoviPromjenaStr .= ';' . $tp->getNaziv() . ':' . $tp->getNaziv();
        }


        return $this->render('tdCMBundle:Promjena:indexJqGrid.html.twig', array(
            'grupePromjenaStr' => $grupePromjenaStr,
            'tipoviPromjenaStr' => $tipoviPromjenaStr,
            'test' => '',
        ));
    }

    public function testAction(Request $request)
    {
        $datagrid = $this->get('datatheke.datagrid')->createHttpDataGrid('tdCMBundle:Promjena');
        $view = $datagrid->handleRequest($this->getRequest());

        return $this->render('@tdCM/Promjena/test.html.twig', array('datagrid' => $view));
//        return array('datagrid' => $view);
    }

    public function test2Action(Request $request)
    {
        return $this->render('tdCMBundle:Promjena:test2.html.twig');
    }

    /**
     * Vraćanje podataka Promjene za JqGrid
     *
     * @param Request $request
     * @return mixed
     */
    function promjenaGridJsonAction(Request $request)
    {
//        $idSetupProperty = $request->query->get('idSetupProperty');

        //S obzirom da koristin loadonce mora san stavit da mi inicijalno pošalje sve redove
        $request->query->set('rows', '10000');
        //Staro	- DATATHEKE

        $pager = $this->get('datatheke.pager')->createPager('tdCMBundle:Promjena');

        /**
         * Customize the QueryBuilder
         */

        // Retrieve QueryBuilder to join on the Adress Entity
        $qb = $pager->getAdapter()->getQueryBuilder();
        $qb->addSelect('uc')
            ->addSelect('pu')
            ->leftJoin('e.userClient', 'uc')
            ->leftjoin('e.propertyUnit', 'pu')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->gte('e.dateTo', '?1'),
                    $qb->expr()->exists('SELECT spu FROM tdCMBundle:SetupPropertyUnit spu WHERE spu.propertyUnit = e.propertyUnit AND spu.aktivan = true')
                )
            );
        $qb->setParameters(array(1 => new DateTime()));

        // Add field 'city' to the pager
        $pager->getAdapter()->addField(new Field('client.naziv', Field::TYPE_STRING, 'uc.naziv'), 'userClientName');
        $pager->getAdapter()->addField(new Field('propertyUnit.name', Field::TYPE_STRING, 'pu.name'), 'propertyUnitName');
//        $pager->getAdapter()->addField(new Field('propertyUnit.name', Field::TYPE_STRING, 'pu.name'), 'name');

        /**
         * Create the DataGrid
         */

        $datagrid = $this->get('datatheke.datagrid')->createHttpDataGrid($pager, array(), 'jqgrid');
//            $datagrid = $this->get('datatheke.datagrid')->createHttpDataGrid('tdCMBundle:SetupGrid', array(), 'jqgrid');

        return $datagrid->handleRequest($request);
    }

    /**
     * Finds and displays a Promjena entity.
     *
     * @param $id
     * @return Response
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('tdCMBundle:Promjena')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Promjena entity.');
        }

        return $this->render('tdCMBundle:Promjena:show.html.twig', array(
            'entity' => $entity,
        ));
    }

    /**
     * Obradi jednu promjenu
     *
     * @param Promjena $promjena
     * @return Response
     */
    public function obradiPromjenuAction(Promjena $promjena)
    {
        //Sigurnost
//        $loginData = $this->get('td_cm.obrada.cookie_obrada');
        if ($promjena->getDatumvrijeme() < new DateTime('yesterday')) {
            return array(
                'status' => 'Error!',
                'message' => 'Promjena nije valjana!',
            );
        }
//        if ($loginData->obradaCookie()) {
        if (true) { //Ne pregledavam sigurnost za sada
            if ($promjena->getDateTo() > new DateTime()) { //Create transfer only if Promjena is valid
                $countTransfers = $this->kreirajTransfereZaPromjenu($promjena);
            }
            return $this->forward('tdCMBundle:Transfer:obradiNoveTransfere');
        } else {
            return JsonResponse::create(array(
                'status' => 'Error!',
                'message' => 'You are not authorized!'
            ));
        }
    }

    /**
     *Traži nove promjene i obrađuje skupno - KADA SE NADOGRADI TABLICA PROMJENA UGRADITI I UVJET ZASTAVICE OBRAĐENO
     *
     */
    public function obradiSvePromjeneAction()
    {
        $em = $this->getDoctrine()->getManager();
        $novePromjene = $em->getRepository('tdCMBundle:Promjena')->findAllNovePromjene();
        foreach ($novePromjene as $promjena) {
            $this->kreirajTransfereZaPromjenu($promjena);
        }
        return $this->forward('tdCMBundle:Transfer:obradiNoveTransfere');
    }

    /**
     * Create Transfer for given Change
     *
     * @param Promjena $promjena
     * @return int Count of Transfers
     */
    private function kreirajTransfereZaPromjenu(Promjena $promjena)
    {
        // Initialization
        $em = $this->getDoctrine()->getManager();
        $countTransfers = 0;

        // Get all active and setted spus by given change
        $setupPropertyUnits = $em->getRepository('tdCMBundle:SetupPropertyUnit')->findByPromjenaAktivno($promjena);


        //Za svaki setupPropertyUnit kreiraj Transfer
        /* @var SetupPropertyUnit $setupPropertyUnit */
        foreach ($setupPropertyUnits as $setupPropertyUnit) {
            $transfer = $em->getRepository('tdCMBundle:Transfer')->findOneBy(
                array(
                    'promjena' => $promjena,
                    'setupPropertyUnit' => $setupPropertyUnit,
                )
            );

            if (is_null($transfer) && $setupPropertyUnit->getPortalKorisnik()->getPortal()->getProcessChange()) {
                $transfer = new Transfer();
                $transfer->setPromjena($promjena);
                $transfer->setSetupPropertyUnit($setupPropertyUnit);
                $em->persist($transfer);
                $em->flush();
                $setupPropertyUnit->addTransferi($transfer);
                $countTransfers++;
            } elseif (!is_null($transfer)) {
                $countTransfers++;
            }
        }

        return $countTransfers;
    }

    /**
     * @param SetupPropertyUnit $setupPropertyUnit - PREBACITI U REPOSITORY
     * @param Promjena $promjena
     * @return bool
     */
    private function postojeNedovrsenePromjene(SetupPropertyUnit $setupPropertyUnit, Promjena $promjena)
    {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->getRepository('tdCMBundle:Transfer')->createQueryBuilder('t');
        $qb->join('t.promjena', 'p')->where(
            $qb->expr()->andX(
                $qb->expr()->eq('t.ok', '?1'),
                $qb->expr()->eq('t.setupPropertyUnit', '?2'),
                $qb->expr()->gt('p.dateTo', '?3'),
                $qb->expr()->neq('t.promjena', '?4'),
                $qb->expr()->lt('p.datumvrijeme', '?5')
            )
        )->orderBy('p.dateTo', 'ASC');
        $qb->setParameters(array(
            1 => false,
            2 => $setupPropertyUnit,
            3 => new DateTime(),
            4 => $promjena,
            5 => $promjena->getDatumvrijeme()
        ));
        $query = $qb->getQuery();
        $prijasnjiTransferi = $query->getResult();

        if (count($prijasnjiTransferi) > 0) {
            return true;
        } else {
            return false;
        }
    }

}
