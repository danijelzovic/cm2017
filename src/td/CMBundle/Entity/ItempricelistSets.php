<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItempricelistSets
 *
 * @ORM\Table(name="itempricelist_sets")
 * @ORM\Entity
 */
class ItempricelistSets
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_company", type="integer")
     */
    private $idCompany;

    /**
     * @var string
     *
     * @ORM\Column(name="tip_dodatka", type="string", length=1)
     */
    private $tipDodatka;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="service", type="string", length=2)
     */
    private $service;

    /**
     * @var string
     *
     * @ORM\Column(name="tags", type="string", length=255)
     */
    private $tags;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_code", type="boolean")
     */
    private $useCode;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255)
     */
    private $code;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_period_booking", type="boolean")
     */
    private $usePeriodBooking;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="period_booking_from", type="date")
     */
    private $periodBookingFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="period_booking_till", type="date")
     */
    private $periodBookingTill;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_stay_min", type="boolean")
     */
    private $useStayMin;

    /**
     * @var integer
     *
     * @ORM\Column(name="stay_min", type="integer")
     */
    private $stayMin;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_stay_max", type="boolean")
     */
    private $useStayMax;

    /**
     * @var integer
     *
     * @ORM\Column(name="stay_max", type="integer")
     */
    private $stayMax;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_num_guest", type="boolean")
     */
    private $useNumGuest;

    /**
     * @var integer
     *
     * @ORM\Column(name="num_guest_from", type="integer")
     */
    private $numGuestFrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="num_guest_till", type="integer")
     */
    private $numGuestTill;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_arrival_day", type="boolean")
     */
    private $useArrivalDay;

    /**
     * @var string
     *
     * @ORM\Column(name="arrival_day", type="string", length=255)
     */
    private $arrivalDay;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordering", type="integer")
     */
    private $ordering;

    /**
     * @ORM\OneToMany(targetEntity="PropertyUnitItempricelistSetoviItem", mappedBy="itempricelistSet")
     */
    private $propertyUnitItempricelistSetoviItems;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->propertyUnitItempricelistSetoviItems = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCompany
     *
     * @param integer $idCompany
     *
     * @return ItempricelistSets
     */
    public function setIdCompany($idCompany)
    {
        $this->idCompany = $idCompany;

        return $this;
    }

    /**
     * Get idCompany
     *
     * @return integer
     */
    public function getIdCompany()
    {
        return $this->idCompany;
    }

    /**
     * Set tipDodatka
     *
     * @param string $tipDodatka
     *
     * @return ItempricelistSets
     */
    public function setTipDodatka($tipDodatka)
    {
        $this->tipDodatka = $tipDodatka;

        return $this;
    }

    /**
     * Get tipDodatka
     *
     * @return string
     */
    public function getTipDodatka()
    {
        return $this->tipDodatka;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ItempricelistSets
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set service
     *
     * @param string $service
     *
     * @return ItempricelistSets
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return string
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set tags
     *
     * @param string $tags
     *
     * @return ItempricelistSets
     */
    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * Get tags
     *
     * @return string
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set useCode
     *
     * @param boolean $useCode
     *
     * @return ItempricelistSets
     */
    public function setUseCode($useCode)
    {
        $this->useCode = $useCode;

        return $this;
    }

    /**
     * Get useCode
     *
     * @return boolean
     */
    public function getUseCode()
    {
        return $this->useCode;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return ItempricelistSets
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set usePeriodBooking
     *
     * @param boolean $usePeriodBooking
     *
     * @return ItempricelistSets
     */
    public function setUsePeriodBooking($usePeriodBooking)
    {
        $this->usePeriodBooking = $usePeriodBooking;

        return $this;
    }

    /**
     * Get usePeriodBooking
     *
     * @return boolean
     */
    public function getUsePeriodBooking()
    {
        return $this->usePeriodBooking;
    }

    /**
     * Set periodBookingFrom
     *
     * @param \DateTime $periodBookingFrom
     *
     * @return ItempricelistSets
     */
    public function setPeriodBookingFrom($periodBookingFrom)
    {
        $this->periodBookingFrom = $periodBookingFrom;

        return $this;
    }

    /**
     * Get periodBookingFrom
     *
     * @return \DateTime
     */
    public function getPeriodBookingFrom()
    {
        return $this->periodBookingFrom;
    }

    /**
     * Set periodBookingTill
     *
     * @param \DateTime $periodBookingTill
     *
     * @return ItempricelistSets
     */
    public function setPeriodBookingTill($periodBookingTill)
    {
        $this->periodBookingTill = $periodBookingTill;

        return $this;
    }

    /**
     * Get periodBookingTill
     *
     * @return \DateTime
     */
    public function getPeriodBookingTill()
    {
        return $this->periodBookingTill;
    }

    /**
     * Set useStayMin
     *
     * @param boolean $useStayMin
     *
     * @return ItempricelistSets
     */
    public function setUseStayMin($useStayMin)
    {
        $this->useStayMin = $useStayMin;

        return $this;
    }

    /**
     * Get useStayMin
     *
     * @return boolean
     */
    public function getUseStayMin()
    {
        return $this->useStayMin;
    }

    /**
     * Set stayMin
     *
     * @param integer $stayMin
     *
     * @return ItempricelistSets
     */
    public function setStayMin($stayMin)
    {
        $this->stayMin = $stayMin;

        return $this;
    }

    /**
     * Get stayMin
     *
     * @return integer
     */
    public function getStayMin()
    {
        return $this->stayMin;
    }

    /**
     * Set useStayMax
     *
     * @param boolean $useStayMax
     *
     * @return ItempricelistSets
     */
    public function setUseStayMax($useStayMax)
    {
        $this->useStayMax = $useStayMax;

        return $this;
    }

    /**
     * Get useStayMax
     *
     * @return boolean
     */
    public function getUseStayMax()
    {
        return $this->useStayMax;
    }

    /**
     * Set stayMax
     *
     * @param integer $stayMax
     *
     * @return ItempricelistSets
     */
    public function setStayMax($stayMax)
    {
        $this->stayMax = $stayMax;

        return $this;
    }

    /**
     * Get stayMax
     *
     * @return integer
     */
    public function getStayMax()
    {
        return $this->stayMax;
    }

    /**
     * Set useNumGuest
     *
     * @param boolean $useNumGuest
     *
     * @return ItempricelistSets
     */
    public function setUseNumGuest($useNumGuest)
    {
        $this->useNumGuest = $useNumGuest;

        return $this;
    }

    /**
     * Get useNumGuest
     *
     * @return boolean
     */
    public function getUseNumGuest()
    {
        return $this->useNumGuest;
    }

    /**
     * Set numGuestFrom
     *
     * @param integer $numGuestFrom
     *
     * @return ItempricelistSets
     */
    public function setNumGuestFrom($numGuestFrom)
    {
        $this->numGuestFrom = $numGuestFrom;

        return $this;
    }

    /**
     * Get numGuestFrom
     *
     * @return integer
     */
    public function getNumGuestFrom()
    {
        return $this->numGuestFrom;
    }

    /**
     * Set numGuestTill
     *
     * @param integer $numGuestTill
     *
     * @return ItempricelistSets
     */
    public function setNumGuestTill($numGuestTill)
    {
        $this->numGuestTill = $numGuestTill;

        return $this;
    }

    /**
     * Get numGuestTill
     *
     * @return integer
     */
    public function getNumGuestTill()
    {
        return $this->numGuestTill;
    }

    /**
     * Set useArrivalDay
     *
     * @param boolean $useArrivalDay
     *
     * @return ItempricelistSets
     */
    public function setUseArrivalDay($useArrivalDay)
    {
        $this->useArrivalDay = $useArrivalDay;

        return $this;
    }

    /**
     * Get useArrivalDay
     *
     * @return boolean
     */
    public function getUseArrivalDay()
    {
        return $this->useArrivalDay;
    }

    /**
     * Set arrivalDay
     *
     * @param string $arrivalDay
     *
     * @return ItempricelistSets
     */
    public function setArrivalDay($arrivalDay)
    {
        $this->arrivalDay = $arrivalDay;

        return $this;
    }

    /**
     * Get arrivalDay
     *
     * @return string
     */
    public function getArrivalDay()
    {
        return $this->arrivalDay;
    }

    /**
     * Set ordering
     *
     * @param integer $ordering
     *
     * @return ItempricelistSets
     */
    public function setOrdering($ordering)
    {
        $this->ordering = $ordering;

        return $this;
    }

    /**
     * Get ordering
     *
     * @return integer
     */
    public function getOrdering()
    {
        return $this->ordering;
    }

    /**
     * Add propertyUnitItempricelistSetoviItem
     *
     * @param \td\CMBundle\Entity\PropertyUnitItempricelistSetoviItem $propertyUnitItempricelistSetoviItem
     *
     * @return ItempricelistSets
     */
    public function addPropertyUnitItempricelistSetoviItem(\td\CMBundle\Entity\PropertyUnitItempricelistSetoviItem $propertyUnitItempricelistSetoviItem)
    {
        $this->propertyUnitItempricelistSetoviItems[] = $propertyUnitItempricelistSetoviItem;

        return $this;
    }

    /**
     * Remove propertyUnitItempricelistSetoviItem
     *
     * @param \td\CMBundle\Entity\PropertyUnitItempricelistSetoviItem $propertyUnitItempricelistSetoviItem
     */
    public function removePropertyUnitItempricelistSetoviItem(\td\CMBundle\Entity\PropertyUnitItempricelistSetoviItem $propertyUnitItempricelistSetoviItem)
    {
        $this->propertyUnitItempricelistSetoviItems->removeElement($propertyUnitItempricelistSetoviItem);
    }

    /**
     * Get propertyUnitItempricelistSetoviItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPropertyUnitItempricelistSetoviItems()
    {
        return $this->propertyUnitItempricelistSetoviItems;
    }
}
