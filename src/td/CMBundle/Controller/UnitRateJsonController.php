<?php

namespace td\CMBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JMS;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use td\CMBundle\Entity\Rate;
use td\CMBundle\Entity\SetupPropertyUnit;
use td\CMBundle\Entity\UnitRate;

/**
 * Class UnitRateJsonController
 * @package td\CMBundle\Controller
 */
class UnitRateJsonController extends Controller
{
    /**
     * Dohvat rateova i ažuriranje baze s dohvaćenim podacima iz Booking.com-a
     *
     * @param SetupPropertyUnit $setupPropertyUnit
     * @return JsonResponse
     */
    public function getUnitRatesAction(SetupPropertyUnit $setupPropertyUnit)
    {
        // Ovdje dohvćam podatke s Booking.com-a, zatim uspoređujem s zapisima koje imam u bazi i vraćam na ekran odgovarajuće podatke
        //Koraci:
        // 1. Dohvat booking.com podataka
        // 2. usporedba da li postoje svi rateovi u bazi (po potrebi ažuriram naziv)
        // 3. slažem povratnu informaciju u obliku polja koje se formira na temelju podatka u bazi
        // 4. Stavljanje u JSON i slanje na front.

        //Provjera korisnika

        //Inicijalizacija
        $em = $this->getDoctrine()->getManager();

        // 1. Dohvat booking.com podataka
        $podaciBookingCom = $this->get('td_cm.obrada.setup_obrada')->getBookingComRoomRates($setupPropertyUnit->getSetupProperty()->getIdPropertyNaPortalu());
        $rates = $this->get('td_cm.obrada.setup_obrada')->updateLocalDataBookingCom($setupPropertyUnit, $podaciBookingCom);

        // 2. usporedba da li postoje svi rateovi u bazi (po potrebi ažuriram naziv)
//        $unitRates = $em->getRepository('tdCMBundle:UnitRate')->findBy(array(
//            'setupPropertyUnit' => $setupPropertyUnit,
//        ));
//
//        //Ovdje pripremam id-ove koje imam u bazi
//        $ratesIds = array();
//
//        /* @var UnitRate $unitRate */
//        foreach ($unitRates as $unitRate) {
//            $ratesIds[] = $unitRate->getIdRateBookingCom();
//        }
//
//        //Prolaz po svim rateovima
//        foreach ($podaciBookingCom as $polje) {
//            if ($polje['id'] == $setupPropertyUnit->getIdPropertyUnitNaPortalu()) {
//                //NASTAVITI
//                // pogledati unitRatesAction iz RateControllera
//                $rates = $polje['rates'];
//                foreach ($rates as $key => $rate) {
//                    //Inicijalizacija dodatnih polja
//                    $rates[$key]['idHead'] = 0;
//                    $rates[$key]['idUnitRate'] = 0;
//                    if ('1' != $rate['readonly'] && '1' != $rate['is_child_rate']) {    //Gledaju se samo rateovi koji se mogu postaviti.
//                        if (!in_array((int)$rate['id'], $ratesIds)) {
//                            //Dohvati ili kreiraj novi zapis Rate
//                            if (null == $rateDB = $em->getRepository('tdCMBundle:Rate')->findOneByRateIdPortal($rate['id'])) {
//                                $rateDB = new Rate();
//                                $rateDB->setDatumKreiranja(new \DateTime());
//                                $rateDB->setMaxPersons($rate['max_persons']);
//                                $rateDB->setPolicy($rate['policy']);
//                                $rateDB->setPolicyId($rate['policy_id']);
//                                $rateDB->setRateIdPortal($rate['id']);
//                                $rateDB->setRateName($rate['rate_name']);
//                                $rateDB->setReadonly(false);
//                                $rateDB->setIsChildRate(false);
//                                $rateDB->setFixedOccupancy(0);
//                                $em->persist($rateDB);
//                                $em->flush();
//                            }
//
//                            //Kreiraj UnitRateStavke
//                            $unitRate = new UnitRate();
//                            $unitRate->setIdRateBookingCom($rate['id']);
//                            $unitRate->setSetupPropertyUnit($setupPropertyUnit);
//                            $unitRate->setRate($rateDB);
//                            $unitRate->setSamoCjenik(true);
//                            $unitRate->setVrijediOd(new \DateTime("2015-01-01"));
//                            $unitRate->setVrijediDo(new \DateTime("2015-12-31"));
//                            $unitRate->setPostojeciSet(false);
//                            $unitRate->setAktivan(false);
//                            $em->persist($unitRate);
//                            $em->flush();
//                            $rates[$key]['idUnitRate'] = $unitRate->getId();
//                        } else {
//                            /* @var UnitRate $unitRateHead */
//                            foreach ($unitRates as $unitRateHead) {
//                                if ($unitRateHead->getIdRateBookingCom() == (int)$rate['id']) {
//                                    $rates[$key]['idHead'] = $unitRateHead->getIdCjenik();
//                                    $rates[$key]['idUnitRate'] = $unitRateHead->getId();
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }

        return new JsonResponse($rates);
    }

    /**
     * Dohvat i vraćanje cjenika za zadani SetupPropertyUnit JSON - OVO JE SAMO ZA TEST
     *
     * @param SetupPropertyUnit $setupPropertyUnit
     * @return JsonResponse
     */
    public function idHeadForSetupPropertyUnitAction(SetupPropertyUnit $setupPropertyUnit)
    {
        //Provjera autorizacije
        $loginData = $this->get('td_cm.obrada.cookie_obrada');

        //Provjeri webshop
        if ($webshop = $setupPropertyUnit->getPortalKorisnik()->getWebshop()) {

            //Dohvaćanje cjenika i vraćanje
            $idHead = $this->get('td_cm.obrada.setup_obrada')->odrediCjenik($setupPropertyUnit);
            $status = 'ok';
            $message = "Cjenik je uspješno određen";
        } else {
            $idHead = null;
            $status = 'Error!';
            $message = "Nije postavljen Webshop!";
        }

        $data = array();
        $data[] = array(
            'idHead' => $idHead,
            'status' => $status,
            'message' => $message
        );

        return new JsonResponse($data);
    }

    /**
     *  Save idHead for each rate of setupPropertyUnit
     *
     * @return JsonResponse
     */
    public function saveIdHeadAction()
    {
        if (!is_null($data = $this->firstProcessOfRequestData())) {//Ovdje je obrađena i sigurnost

            // Initialization
            $em = $this->getDoctrine()->getManager();
            $rates = $data['rates'];

            $messages = array();
            $firstIdUnitRate = 0;
            foreach ($rates as $rate) {
                if ((int)$rate['readonly'] !== 1 and (int)$rate['is_child_rate'] !== 1) {
                    $idHead = (int)$rate['idcjenikcustom'] !== 0 ? (int)$rate['idcjenikcustom'] : (int)$rate['idHead'];
                    $messages[] = $this->updateIdHeadUnitRate((int)$rate['idUnitRate'], $idHead);
                    if ($firstIdUnitRate == 0) {
                        $firstIdUnitRate = (int)$rate['idUnitRate'];
                    }
                }
            }

            // Calculate stataus Povezan on SetupPropertyUnit
            $this->recalculateStatusPovezanOnSetupPropertyUnitByIdUnitRate($firstIdUnitRate);

            if (array_key_exists('selectedSetupPropertyUnit', $data)) {
                $setupPropertyUnit = $em->getRepository('tdCMBundle:SetupPropertyUnit')->find($data['selectedSetupPropertyUnit']['idSpu']);
                $countSettedRates = (int)$em->getRepository('tdCMBundle:UnitRate')->countSettedUnitRates($setupPropertyUnit);
                $message['countSettedRates'] = $countSettedRates;
            }


            //Send email if everything is set correct if not already sent.
//        $portalKorisnik = $setupPropertyUnit->getPortalKorisnik();
//        if( $setupPropertyUnit->getAktivan()    &&
//            !$setupPropertyUnit->getPovezan()   &&
//            0 < $countSettedRates               &&
//            !$setupPropertyUnit->getPending()
//        ){
//            $sendFrom = $this->container->getParameter('mailer_send_from');
//            $sendTo = $this->container->getParameter('mailer_send_to');
//            $mailMessage = Swift_Message::newInstance()
//                ->setSubject('Smještajna jedinica <b>' . $setupPropertyUnit->getPropertyUnit()->getPropertyUnitFront() . '</b> je uspješno postavljena. ' .
//                    'Potrebno je odobriti povezivanje i postaviti status na povezano.')
//                ->setFrom($sendFrom)
//                ->setTo($sendTo)
//                ->setBody('Smještajna jedinica <b>' . $setupPropertyUnit->getPropertyUnit()->getPropertyUnitFront() . '</b> je uspješno postavljena. ' .
//                    'Potrebno je odobriti povezivanje i postaviti status na povezano. Korisnik je ' .
//                    $portalKorisnik->getIdCompanyKorisnik() , 'text/plain');
//            $this->get('mailer')->send($mailMessage);
//            $setupPropertyUnit->setPending(true);
//            $em->flush();
//            $message['message'] .= '<br>Poslan je zahtjev za odobrenjem veze na smještajnoj jedinici <br><b>' . $setupPropertyUnit->getPropertyUnit()->getPropertyUnitFront() . '</b>';
//        }
        } else {
            $messages[] = array(
                'status' => 'Error!',
                'message' => 'Input data is not valid!',
            );
        }

        return new JsonResponse($messages);
    }

    /**
     *  Save idHead for given UnitRateStavke. Only for backend
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response|static
     */
    public function saveIdHeadInUnitRateAction(Request $request)
    {
        $data = $this->firstProcessOfRequestData(); //Ovdje je obrađena i sigurnost
        $message['status'] = 'Error!';
        $loginData = $this->get('td_cm.obrada.cookie_obrada');
        if ($loginData->obradaCookie()) {
            if (!is_null($request->request->get('idUnitRate')) and !is_null($request->request->get('idHead'))) {
                $message = $this->updateIdHeadUnitRate($request->request->get('idUnitRate'), $request->request->get('idHead'));
                // Calculate status Povezan on SPU
                $this->recalculateStatusPovezanOnSetupPropertyUnitByIdUnitRate($request->request->get('idUnitRate'));
            } else {
                $message['message'] = 'Neispravni parametri!';
            }
        } else {
            $message = array(
                'message' => 'Nemate ovlaštenja za rad!',
            );
        }
        return JsonResponse::create($message);
    }

    /**
     *  Check if user is Authenticated and JSON decode
     *
     * @return array|mixed
     */
    private function firstProcessOfRequestData()
    {
        $loginData = $this->get('td_cm.obrada.cookie_obrada');
        if ($loginData->obradaCookie()) {
            $request = $this->get('request');
            $dataJson = $request->getContent();
            $data = json_decode($dataJson, true);
        } else {
            $data = null;
        }
        return $data;
    }

    /**
     * Persist idHead to database and calculate povezan on unit
     *
     * @param $idUnitRate
     * @param $idHead
     * @return mixed
     */
    private function updateIdHeadUnitRate($idUnitRate, $idHead)
    {
        $em = $this->getDoctrine()->getManager();
        $puipli = $em->getRepository('tdCMBundle:PropertyUnitItemPriceListItems')->findOneByIdHead($idHead);

        try {
            $unitRate = $em->getRepository('tdCMBundle:UnitRate')->find($idUnitRate);
            if (!$unitRate->getRate()->getIsChildRate() && !$unitRate->getRate()->getReadonly()) {
                if (!is_null($puipli) or $idHead == 0) {
                    $unitRate->setIdCjenik($idHead);
                    $em->persist($unitRate);
                    $em->flush();
                    $message['status'] = 'ok';
                    $message['message'] = 'Postavke za rate ' . $unitRate->getRate()->getRateName() . ' su uspješno pohranjene.';
                } else {
                    $message['status'] = 'Error!';
                    $message['message'] = 'Zadani cjenik ne postoji za rate ' . $unitRate->getRate()->getRateName() . '!';
                }
            } else {
                $message['status'] = 'Error!';
                $message['message'] = 'Nisu dozvoljene promjene za rate' . $unitRate->getRate()->getRateName() . '!';
            }
        } catch (EntityNotFoundException $ex) {
            $message['status'] = 'Error!';
            $message['message'] = 'Nisu dozvoljene promjene za zadani rate!';
        }
        return $message;
    }

    /**
     * @param $idUnitRate
     */
    private function recalculateStatusPovezanOnSetupPropertyUnitByIdUnitRate($idUnitRate)
    {
        $em = $this->getDoctrine()->getManager();
        if (!is_null($unitRate = $em->getRepository('tdCMBundle:UnitRate')->find($idUnitRate))) {
            $setupPropertyUnit = $this->get('td_cm.obrada.setup_obrada')->calculatePovezanSetupPropertyUnit($unitRate->getSetupPropertyUnit());
            $em->persist($setupPropertyUnit);
            $em->flush();
        }
    }

}
