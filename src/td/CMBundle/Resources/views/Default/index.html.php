<!-- src/td/CMBundle/Resources/views/Default/index.html.php -->

<?php $view->extend('::base.html.php') ?>

<?php $view['slots']->set('title', 'CM') ?>



<?php $view['slots']->start('body') ?>
<div class="container-fluid">

    <div class="row">
        <div class="col-md-9">
            <?php echo $view->escape($debug) ?>
            <br/>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div role="tabpanel">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#povezani" aria-controls="povezani" role="tab"
                                                              data-toggle="tab">Povezani</a></li>
                    <li role="presentation"><a href="#nepovezani" aria-controls="nepovezani" role="tab"
                                               data-toggle="tab">Nepovezani</a></li>
                    <li role="presentation"><a href="#svi" aria-controls="svi" role="tab" data-toggle="tab">Svi</a></li>
                </ul>

                <!-- Tab panes -->

                <div class="tab-content">

                    <!-- Povezani -->
                    <div role="tabpanel" class="tab-pane active" id="povezani">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th class="text-center vert-align" rowspan="2">ID</th>
                                    <th class="text-center vert-align" rowspan="2">Logo</th>
                                    <th class="text-center vert-align" rowspan="2">Naziv</th>
                                    <th class="text-center" colspan="4">Smještajne jedinice</th>
                                    <th class="text-center vert-align" rowspan="2">Postavke veze</th>
                                    <th class="text-center vert-align" rowspan="2">Kontakt podaci</th>
                                </tr>
                                <tr>
                                    <th class="text-center">Uređivanje</th>
                                    <th class="text-center">Ukupno</th>
                                    <th class="text-center">Povezano</th>
                                    <th class="text-center">Nepovezano</th>
                                    <!-- <th>Br.Upita</th>
                                    <th>Br.Rezer. osoba</th>
                                    <th>Br.Odb.Rezer.</th> -->
                                </tr>
                                </thead>
                                <tbody>

                                <?php foreach ($povezani as $item) { ?>
                                <tr>

                                    <td class="text-center vert-align">
                                        <div class="text-center"><?php echo $item['id'] ?></div>
                                    </td>

                                    <td class="text-center"><img
                                            src="<?php echo $view['assets']->getUrl('bundles/tdcm/images/') . $item['thumbnail'] ?>"
                                            alt="..."
                                            class="img-thumbnail"></td>
                                    <td class="text-center vert-align">
                                        <ul class="list-unstyled">
                                            <li><?php echo $item['naziv'] ?></li>
                                            <li><a href="<?php echo $item['link'] ?>"></a></li>
                                        </ul>
                                    </td>
                                    <td class="text-center vert-align">
                                        <div class="vcenter">
                                            <a class="btn btn-default" data-toggle="tooltip" data-placement="top"
                                               title="Popis unita za portal"
                                               href="<?php echo $view['router']->generate('td_cm_setUp_indexbt', array('portal' => $item['id'])) ?>"><span
                                                    class="glyphicon glyphicon-pencil"></span></a>
                                        </div>
                                    </td>
                                    <td class="text-center vert-align"><?php echo($item['prijavljeni'] + $item['neprijavljeni'] ) ?></td>
                                    <td class="text-center vert-align"><?php echo $item['prijavljeni'] ?></td>
                                    <td class="text-center vert-align"><?php echo $item['neprijavljeni'] ?></td>
                                    <!-- <td>#upita</td>
                                    <td>#rezer</td>
                                    <td>#odbrezer</td> -->
                                    <td class="text-center vert-align">
                                        <a class="btn btn-default" data-toggle="tooltip"
                                           data-placement="top"
                                           title="Promijeni postavke portala"
                                           href="<?php echo $view['router']->generate('td_cm_portalKorisnik_azuriraj', array('idPortal' => $item['id'])) ?>"><span
                                                class="glyphicon glyphicon-pencil"></span>
                                        </a>
                                    </td>
                                    <td class="text-center vert-align">
                                        <ul class="list-unstyled">
                                            <li>Kontakt osoba:<?php echo $item['contactPerson'] ?></li>
                                            <li>email:<?php echo $item['contactEmail'] ?></li>
                                            <li>telefon:<?php echo $item['contactTel'] ?></li>
                                        </ul>
                                    </td>
                                    <!--<td><button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Obriši setup"><span class="glyphicon glyphicon-remove"></span></button></td>-->
                                </tr>
                                <?php }?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- Nepovezani -->
                    <div role="tabpanel" class="tab-pane" id="nepovezani">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Logo</th>
                                    <th>Naziv</th>
                                    <th>URL</th>
                                    <th>Opis</th>
                                    <th>Kontakt osoba</th>
                                    <th>E-mail</th>
                                    <th>Telefon</th>
                                    <th>Opcije</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php foreach ($nepovezani as $item) { ?>
                                <tr>

                                    <td class="center vert-align">
                                        <div class="center-block"><?php echo $item->getId() ?></div>
                                    </td>

                                    <td class="center vert-align"><img
                                            src="<?php echo $view['assets']->getUrl('bundles/tdcm/images/') . $item->getThumbnail() ?>" alt="..."
                                            class="img-thumbnail"></td>
                                    <td class="center vert-align"><?php echo $item->getNaziv() ?></td>
                                    <td class="center vert-align"><a href="<?php echo $item->getLink() ?>"><?php echo $item->getLink() ?></a></td>
                                    <td class="center vert-align"><?php echo $item->getDescription() ?></td>
                                    <td class="center vert-align"><?php echo $item->getContactPerson() ?></td>
                                    <td class="center vert-align"><?php echo $item->getContactEmail() ?></td>
                                    <td class="center vert-align"><?php echo $item->getContactTel() ?></td>
                                    <!--<td><button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Obriši setup"><span class="glyphicon glyphicon-remove"></span></button></td>-->

                                    <td class="center vert-align"><a class="btn btn-default" data-toggle="tooltip"
                                                                     data-placement="top" title="Poveži"
                                                                     href="<?php $view['router']->generate('td_cm_portalKorisnik_povezi', array('idPortal' => $item->getId())) ?>"><span
                                                class="glyphicon glyphicon-link"></span></a></td>
                                </tr>
                                <?php }  ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- Svi -->
                    <div role="tabpanel" class="tab-pane" id="svi">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Logo</th>
                                    <th>Naziv</th>
                                    <th>URL</th>
                                    <th>Opis</th>
                                    <th>Kontakt osoba</th>
                                    <th>E-mail</th>
                                    <th>Telefon</th>
                                    <!-- <th>Opcije</th> -->
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($portali as $item) { ?>
                                <tr>

                                    <td class="center vert-align">
                                        <div class="center-block"><?php echo $item->getId() ?></div>
                                    </td>

                                    <td class="center vert-align"><img
                                            src="<?php echo $view['assets']->getUrl('bundles/tdcm/images/') . $item->getThumbnail() ?>" alt="..."
                                            class="img-thumbnail"></td>
                                    <td class="center vert-align"><?php echo $item->getNaziv() ?></td>
                                    <td class="center vert-align"><a href="<?php echo $item->getLink() ?>"><?php echo $item->getLink() ?></a></td>
                                    <td class="center vert-align"><?php echo $item->getDescription() ?></td>
                                    <td class="center vert-align"><?php echo $item->getContactPerson() ?></td>
                                    <td class="center vert-align"><?php echo $item->getContactEmail() ?></td>
                                    <td class="center vert-align"><?php echo $item->getContactTel() ?></td>
                                    <!--<td><button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Obriši setup"><span class="glyphicon glyphicon-remove"></span></button></td>-->

                                </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

<?php $view['slots']->stop() ?>