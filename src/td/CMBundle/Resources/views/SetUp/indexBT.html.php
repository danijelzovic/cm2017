<!-- src/td/CMBundle/Resources/views/Default/index.html.php -->

<?php $view->extend('::base.html.php') ?>

<?php $view['slots']->set('title', 'CM') ?>



<?php $view['slots']->start('body') ?>
<div class="container-fluid" ng-app="cmApp">

    <div class="row">
        <div class="col-md-9">
            <?php echo $view->escape($debug) ?>
            <br/>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php echo $portalKorisnik->getPortal()->getNaziv() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">

        </div>

    </div>

    <div ng-controller="ObjektiCtrl">
        <div class="row">
            <div class="col-md-3 pull-right">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></span>
                    <input type="text" class="form-control" placeholder="Traži..." aria-describedby="basic-addon1" ng-model="query">
                </div>

               <br>
            </div>
        </div>
        <div class="row">

            <ul class="list-unstyled">
                <li ng-repeat="cmProperty in cmProperties | filter:query">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading"><strong>{{ cmProperty.propertyNaziv }}</strong></div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>[{{ cmProperty.idFront }}] Tip objekta: {{ cmProperty.propertyType }}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <button class="btn btn-default">Smještajne jedinice</button>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </li>
            </ul>

        </div>
    </div>

</div>


<div class="modal fade bs-example-modal-lg" id="setupPropertyUnitModal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="transferModalLabel">Poruka</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Zatvori</button>
            </div>
        </div>
    </div>
</div>

<?php $view['slots']->stop() ?>

<?php $view['slots']->start('dodatniJs') ?>

<script>


    $('#setupPropertyUnitModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var spuTransferiUrl = button.data('transfer'); // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
//        modal.find('.modal-body').load(spuTransferiUrl, function () {
//            if (naslov)
//                $('#transferModalLabel').text(naslov);
//            else $('#transferModalLabel').text('Popis transfera');
//        });
    });

    $('#setupPropertyUnitModal').on('hidden.bs.modal', function (e) {
        // do something...

        if (1 == refresh) {
            alert(refresh);
            promjenaNepovezano = 1;
            ponovoPostaviT1();
        }
    });

    var cmApp = angular.module('cmApp', ['ngTable', 'ngTableResizableColumns']);

    cmApp.controller('ObjektiCtrl', ['$scope', '$filter', 'NgTableParams', function ($scope, $filter, NgTableParams) {
        $scope.cmProperties = <?php echo $setupPropertiesJson; ?>;
    }]);

</script>

<?php $view['slots']->stop() ?>
