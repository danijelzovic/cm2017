<?php
namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="product_order")
 * @ORM\Entity
 */
class Order
{
    /**
     * @var integer 
     *
     * @ORM\Id @ORM\Column(name="id", type="integer")
     * 
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string 
     *
     * @ORM\Column(type="string", name="order_identifier", length=255, nullable=true, unique=true)
     */
    protected $identifier;

    /**
     * @var decimal
     *
     * @ORM\Column(type="decimal", scale=2)
     */
    protected $total;

    /**
     * @ORM\ManyToOne(targetEntity="User",  inversedBy="orders")
     */   
    protected $user;

    public function getId()
    {
        return $this->id;
    }

    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }

    public function getIdentifier()
    {
        return $this->identifier;
    }

    public function setTotal($total)
    {
        $this->total = $total;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function setUser(User $user = null)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }
}
