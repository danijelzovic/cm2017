<?php

namespace td\CMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\MaxDepth;

/**
 * Client
 *
 * @ORM\Table(name="client", uniqueConstraints={@ORM\UniqueConstraint(name="id_crm_klijent", columns={"id_crm_klijent"})}, indexes={@ORM\Index(name="id_company", columns={"id_company"}), @ORM\Index(name="first_name", columns={"first_name"}), @ORM\Index(name="last_name", columns={"last_name"}), @ORM\Index(name="language", columns={"language"}), @ORM\Index(name="deleted", columns={"deleted"}), @ORM\Index(name="id_laserline", columns={"id_laserline"}), @ORM\Index(name="id_posrednik", columns={"id_posrednik"}), @ORM\Index(name="id_was", columns={"id_was"}), @ORM\Index(name="active", columns={"active"}), @ORM\Index(name="username", columns={"username"}), @ORM\Index(name="id_moja_tvrtka", columns={"id_moja_tvrtka"}), @ORM\Index(name="id_belong_company", columns={"id_belong_company"}), @ORM\Index(name="id_moja_tvrtka_2", columns={"id_moja_tvrtka", "active"}), @ORM\Index(name="naziv", columns={"naziv"}), @ORM\Index(name="people_id_transfer", columns={"people_id_transfer"})})
 * @ORM\Entity
 * @ExclusionPolicy("all")
 */
class Client
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_client", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="naziv", type="string", length=255, nullable=false)
     * @Expose
     */
    private $naziv;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_crm_klijent", type="bigint", nullable=true)
     */
    private $idCrmKlijent;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_client_stari", type="integer", nullable=false)
     * @Expose
     */
    private $idClientStari;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_company", type="integer", nullable=false)
     * @Expose
     */
    private $idCompany = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="id_bureau", type="integer", nullable=false)
     */
    private $idBureau;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_kampanja", type="integer", nullable=false)
     */
    private $idKampanja;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_moja_tvrtka", type="integer", nullable=false)
     */
    private $idMojaTvrtka;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_was", type="integer", nullable=false)
     */
    private $idWas;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_first_company", type="integer", nullable=false)
     */
    private $idFirstCompany = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="id_posrednik", type="integer", nullable=false)
     */
    private $idPosrednik;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_belong_company", type="integer", nullable=false)
     */
    private $idBelongCompany;

    /**
     * @var boolean
     *
     * @ORM\Column(name="p_iznajmljivac", type="boolean", nullable=false)
     */
    private $pIznajmljivac;

    /**
     * @var boolean
     *
     * @ORM\Column(name="osoba", type="boolean", nullable=false)
     */
    private $osoba;

    /**
     * @var boolean
     *
     * @ORM\Column(name="gost", type="boolean", nullable=false)
     */
    private $gost;

    /**
     * @var boolean
     *
     * @ORM\Column(name="orgjedinica", type="boolean", nullable=false)
     */
    private $orgjedinica;

    /**
     * @var boolean
     *
     * @ORM\Column(name="klijent", type="boolean", nullable=false)
     */
    private $klijent = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     * @Expose
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(name="menu_status", type="string", length=64, nullable=false)
     */
    private $menuStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="id_language_tdapp", type="string", length=2, nullable=false)
     */
    private $idLanguageTdapp = 'hr';

    /**
     * @var string
     *
     * @ORM\Column(name="is_agency", type="string", length=2, nullable=false)
     */
    private $isAgency = 'NE';

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=254, nullable=false)
     */
    private $username = '';

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="adminpassword", type="string", length=255, nullable=false)
     */
    private $adminpassword;

    /**
     * @var string
     *
     * @ORM\Column(name="pass", type="string", length=254, nullable=false)
     */
    private $pass = '';

    /**
     * @var string
     *
     * @ORM\Column(name="mdpass", type="string", length=64, nullable=false)
     */
    private $mdpass = '';

    /**
     * @var string
     *
     * @ORM\Column(name="r333", type="string", length=10, nullable=false)
     */
    private $r333;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=15, nullable=false)
     */
    private $title = '';

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=254, nullable=false)
     * @Expose
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=254, nullable=false)
     * @Expose
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="nickname", type="string", length=64, nullable=false)
     */
    private $nickname;

    /**
     * @var string
     *
     * @ORM\Column(name="oib", type="string", length=32, nullable=true)
     */
    private $oib;

    /**
     * @var string
     *
     * @ORM\Column(name="pdvid", type="string", length=128, nullable=false)
     */
    private $pdvid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pdv", type="boolean", nullable=false)
     */
    private $pdv;

    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=254, nullable=false)
     */
    private $company;

    /**
     * @var string
     *
     * @ORM\Column(name="company_short", type="string", length=32, nullable=false)
     */
    private $companyShort;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="entered", type="datetime", nullable=false)
     */
    private $entered = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastvisit", type="datetime", nullable=false)
     */
    private $lastvisit = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_login", type="datetime", nullable=false)
     */
    private $lastLogin;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=254, nullable=false)
     */
    private $address = '';

    /**
     * @var string
     *
     * @ORM\Column(name="zip", type="string", length=7, nullable=false)
     */
    private $zip = '';

    /**
     * @var string
     *
     * @ORM\Column(name="place", type="string", length=40, nullable=false)
     */
    private $place = '';

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=40, nullable=false)
     */
    private $country = '';

    /**
     * @var string
     *
     * @ORM\Column(name="id_nationality", type="string", length=3, nullable=false)
     */
    private $idNationality;

    /**
     * @var string
     *
     * @ORM\Column(name="guest_type", type="string", length=1, nullable=false)
     */
    private $guestType = 'I';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birth_date", type="date", nullable=true)
     */
    private $birthDate;

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=30, nullable=false)
     */
    private $tel = '';

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=30, nullable=false)
     */
    private $fax = '';

    /**
     * @var string
     *
     * @ORM\Column(name="mob", type="string", length=30, nullable=false)
     */
    private $mob = '';

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=254, nullable=false)
     */
    private $email = '';

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=2, nullable=false)
     */
    private $language = '';

    /**
     * @var string
     *
     * @ORM\Column(name="o_sif_jezika", type="string", length=32, nullable=false)
     */
    private $oSifJezika;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="text", nullable=false)
     */
    private $note;

    /**
     * @var string
     *
     * @ORM\Column(name="agency_code", type="string", length=64, nullable=false)
     */
    private $agencyCode;

    /**
     * @var string
     *
     * @ORM\Column(name="email_sender_tz", type="string", length=64, nullable=false)
     */
    private $emailSenderTz;

    /**
     * @var string
     *
     * @ORM\Column(name="web", type="string", length=255, nullable=false)
     */
    private $web;

    /**
     * @var boolean
     *
     * @ORM\Column(name="newsletter", type="boolean", nullable=false)
     */
    private $newsletter = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false)
     */
    private $deleted = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastchange", type="datetime", nullable=false)
     */
    private $lastchange = 'CURRENT_TIMESTAMP';

    /**
     * @var integer
     *
     * @ORM\Column(name="lasertransfer", type="integer", nullable=false)
     */
    private $lasertransfer;

    /**
     * @var string
     *
     * @ORM\Column(name="lasertransferstatus", type="string", length=2, nullable=false)
     */
    private $lasertransferstatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lasertransfertime", type="datetime", nullable=false)
     */
    private $lasertransfertime;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_laserline", type="bigint", nullable=false)
     */
    private $idLaserline;

    /**
     * @var string
     *
     * @ORM\Column(name="privat_kucni_broj", type="string", length=32, nullable=false)
     */
    private $privatKucniBroj;

    /**
     * @var string
     *
     * @ORM\Column(name="o_privat_sif_drzave", type="string", length=32, nullable=false)
     */
    private $oPrivatSifDrzave;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_crm_centrala", type="integer", nullable=false)
     */
    private $idCrmCentrala;

    /**
     * @var boolean
     *
     * @ORM\Column(name="prava_administriranje", type="boolean", nullable=false)
     */
    private $pravaAdministriranje;

    /**
     * @var boolean
     *
     * @ORM\Column(name="prava_vidisveprojekte", type="boolean", nullable=false)
     */
    private $pravaVidisveprojekte;

    /**
     * @var boolean
     *
     * @ORM\Column(name="prava_privatni", type="boolean", nullable=false)
     */
    private $pravaPrivatni;

    /**
     * @var boolean
     *
     * @ORM\Column(name="prava_brisanjeurzapisnika", type="boolean", nullable=false)
     */
    private $pravaBrisanjeurzapisnika;

    /**
     * @var boolean
     *
     * @ORM\Column(name="prava_samouprojektima", type="boolean", nullable=false)
     */
    private $pravaSamouprojektima;

    /**
     * @var integer
     *
     * @ORM\Column(name="delete_import_code", type="integer", nullable=false)
     */
    private $deleteImportCode;

    /**
     * @var string
     *
     * @ORM\Column(name="CallerIDNum", type="string", length=64, nullable=false)
     */
    private $calleridnum;

    /**
     * @var string
     *
     * @ORM\Column(name="sip", type="string", length=64, nullable=false)
     */
    private $sip;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="VRIJEME", type="datetime", nullable=false)
     */
    private $vrijeme;

    /**
     * @var integer
     *
     * @ORM\Column(name="people_id_transfer", type="integer", nullable=false)
     */
    private $peopleIdTransfer;

    /**
     * @var integer
     *
     * @ORM\Column(name="company_id_transfer", type="integer", nullable=false)
     */
    private $companyIdTransfer;

    /**
     * @var boolean
     *
     * @ORM\Column(name="import", type="boolean", nullable=false)
     */
    private $import;


    /**
     * Set naziv
     *
     * @param string $naziv
     * @return Client
     */
    public function setNaziv($naziv)
    {
        $this->naziv = $naziv;

        return $this;
    }

    /**
     * Get naziv
     *
     * @return string 
     */
    public function getNaziv()
    {
        return $this->naziv;
    }

    /**
     * Set idCrmKlijent
     *
     * @param integer $idCrmKlijent
     * @return Client
     */
    public function setIdCrmKlijent($idCrmKlijent)
    {
        $this->idCrmKlijent = $idCrmKlijent;

        return $this;
    }

    /**
     * Get idCrmKlijent
     *
     * @return integer 
     */
    public function getIdCrmKlijent()
    {
        return $this->idCrmKlijent;
    }

    /**
     * Set idClientStari
     *
     * @param integer $idClientStari
     * @return Client
     */
    public function setIdClientStari($idClientStari)
    {
        $this->idClientStari = $idClientStari;

        return $this;
    }

    /**
     * Get idClientStari
     *
     * @return integer 
     */
    public function getIdClientStari()
    {
        return $this->idClientStari;
    }

    /**
     * Set idCompany
     *
     * @param integer $idCompany
     * @return Client
     */
    public function setIdCompany($idCompany)
    {
        $this->idCompany = $idCompany;

        return $this;
    }

    /**
     * Get idCompany
     *
     * @return integer 
     */
    public function getIdCompany()
    {
        return $this->idCompany;
    }

    /**
     * Set idBureau
     *
     * @param integer $idBureau
     * @return Client
     */
    public function setIdBureau($idBureau)
    {
        $this->idBureau = $idBureau;

        return $this;
    }

    /**
     * Get idBureau
     *
     * @return integer 
     */
    public function getIdBureau()
    {
        return $this->idBureau;
    }

    /**
     * Set idKampanja
     *
     * @param integer $idKampanja
     * @return Client
     */
    public function setIdKampanja($idKampanja)
    {
        $this->idKampanja = $idKampanja;

        return $this;
    }

    /**
     * Get idKampanja
     *
     * @return integer 
     */
    public function getIdKampanja()
    {
        return $this->idKampanja;
    }

    /**
     * Set idMojaTvrtka
     *
     * @param integer $idMojaTvrtka
     * @return Client
     */
    public function setIdMojaTvrtka($idMojaTvrtka)
    {
        $this->idMojaTvrtka = $idMojaTvrtka;

        return $this;
    }

    /**
     * Get idMojaTvrtka
     *
     * @return integer 
     */
    public function getIdMojaTvrtka()
    {
        return $this->idMojaTvrtka;
    }

    /**
     * Set idWas
     *
     * @param integer $idWas
     * @return Client
     */
    public function setIdWas($idWas)
    {
        $this->idWas = $idWas;

        return $this;
    }

    /**
     * Get idWas
     *
     * @return integer 
     */
    public function getIdWas()
    {
        return $this->idWas;
    }

    /**
     * Set idFirstCompany
     *
     * @param integer $idFirstCompany
     * @return Client
     */
    public function setIdFirstCompany($idFirstCompany)
    {
        $this->idFirstCompany = $idFirstCompany;

        return $this;
    }

    /**
     * Get idFirstCompany
     *
     * @return integer 
     */
    public function getIdFirstCompany()
    {
        return $this->idFirstCompany;
    }

    /**
     * Set idPosrednik
     *
     * @param integer $idPosrednik
     * @return Client
     */
    public function setIdPosrednik($idPosrednik)
    {
        $this->idPosrednik = $idPosrednik;

        return $this;
    }

    /**
     * Get idPosrednik
     *
     * @return integer 
     */
    public function getIdPosrednik()
    {
        return $this->idPosrednik;
    }

    /**
     * Set idBelongCompany
     *
     * @param integer $idBelongCompany
     * @return Client
     */
    public function setIdBelongCompany($idBelongCompany)
    {
        $this->idBelongCompany = $idBelongCompany;

        return $this;
    }

    /**
     * Get idBelongCompany
     *
     * @return integer 
     */
    public function getIdBelongCompany()
    {
        return $this->idBelongCompany;
    }

    /**
     * Set pIznajmljivac
     *
     * @param boolean $pIznajmljivac
     * @return Client
     */
    public function setPIznajmljivac($pIznajmljivac)
    {
        $this->pIznajmljivac = $pIznajmljivac;

        return $this;
    }

    /**
     * Get pIznajmljivac
     *
     * @return boolean 
     */
    public function getPIznajmljivac()
    {
        return $this->pIznajmljivac;
    }

    /**
     * Set osoba
     *
     * @param boolean $osoba
     * @return Client
     */
    public function setOsoba($osoba)
    {
        $this->osoba = $osoba;

        return $this;
    }

    /**
     * Get osoba
     *
     * @return boolean 
     */
    public function getOsoba()
    {
        return $this->osoba;
    }

    /**
     * Set gost
     *
     * @param boolean $gost
     * @return Client
     */
    public function setGost($gost)
    {
        $this->gost = $gost;

        return $this;
    }

    /**
     * Get gost
     *
     * @return boolean 
     */
    public function getGost()
    {
        return $this->gost;
    }

    /**
     * Set orgjedinica
     *
     * @param boolean $orgjedinica
     * @return Client
     */
    public function setOrgjedinica($orgjedinica)
    {
        $this->orgjedinica = $orgjedinica;

        return $this;
    }

    /**
     * Get orgjedinica
     *
     * @return boolean 
     */
    public function getOrgjedinica()
    {
        return $this->orgjedinica;
    }

    /**
     * Set klijent
     *
     * @param boolean $klijent
     * @return Client
     */
    public function setKlijent($klijent)
    {
        $this->klijent = $klijent;

        return $this;
    }

    /**
     * Get klijent
     *
     * @return boolean 
     */
    public function getKlijent()
    {
        return $this->klijent;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Client
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set menuStatus
     *
     * @param string $menuStatus
     * @return Client
     */
    public function setMenuStatus($menuStatus)
    {
        $this->menuStatus = $menuStatus;

        return $this;
    }

    /**
     * Get menuStatus
     *
     * @return string 
     */
    public function getMenuStatus()
    {
        return $this->menuStatus;
    }

    /**
     * Set idLanguageTdapp
     *
     * @param string $idLanguageTdapp
     * @return Client
     */
    public function setIdLanguageTdapp($idLanguageTdapp)
    {
        $this->idLanguageTdapp = $idLanguageTdapp;

        return $this;
    }

    /**
     * Get idLanguageTdapp
     *
     * @return string 
     */
    public function getIdLanguageTdapp()
    {
        return $this->idLanguageTdapp;
    }

    /**
     * Set isAgency
     *
     * @param string $isAgency
     * @return Client
     */
    public function setIsAgency($isAgency)
    {
        $this->isAgency = $isAgency;

        return $this;
    }

    /**
     * Get isAgency
     *
     * @return string 
     */
    public function getIsAgency()
    {
        return $this->isAgency;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return Client
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Client
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set adminpassword
     *
     * @param string $adminpassword
     * @return Client
     */
    public function setAdminpassword($adminpassword)
    {
        $this->adminpassword = $adminpassword;

        return $this;
    }

    /**
     * Get adminpassword
     *
     * @return string 
     */
    public function getAdminpassword()
    {
        return $this->adminpassword;
    }

    /**
     * Set pass
     *
     * @param string $pass
     * @return Client
     */
    public function setPass($pass)
    {
        $this->pass = $pass;

        return $this;
    }

    /**
     * Get pass
     *
     * @return string 
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * Set mdpass
     *
     * @param string $mdpass
     * @return Client
     */
    public function setMdpass($mdpass)
    {
        $this->mdpass = $mdpass;

        return $this;
    }

    /**
     * Get mdpass
     *
     * @return string 
     */
    public function getMdpass()
    {
        return $this->mdpass;
    }

    /**
     * Set r333
     *
     * @param string $r333
     * @return Client
     */
    public function setR333($r333)
    {
        $this->r333 = $r333;

        return $this;
    }

    /**
     * Get r333
     *
     * @return string 
     */
    public function getR333()
    {
        return $this->r333;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Client
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Client
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Client
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set nickname
     *
     * @param string $nickname
     * @return Client
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;

        return $this;
    }

    /**
     * Get nickname
     *
     * @return string 
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * Set oib
     *
     * @param string $oib
     * @return Client
     */
    public function setOib($oib)
    {
        $this->oib = $oib;

        return $this;
    }

    /**
     * Get oib
     *
     * @return string 
     */
    public function getOib()
    {
        return $this->oib;
    }

    /**
     * Set pdvid
     *
     * @param string $pdvid
     * @return Client
     */
    public function setPdvid($pdvid)
    {
        $this->pdvid = $pdvid;

        return $this;
    }

    /**
     * Get pdvid
     *
     * @return string 
     */
    public function getPdvid()
    {
        return $this->pdvid;
    }

    /**
     * Set pdv
     *
     * @param boolean $pdv
     * @return Client
     */
    public function setPdv($pdv)
    {
        $this->pdv = $pdv;

        return $this;
    }

    /**
     * Get pdv
     *
     * @return boolean 
     */
    public function getPdv()
    {
        return $this->pdv;
    }

    /**
     * Set company
     *
     * @param string $company
     * @return Client
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return string 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set companyShort
     *
     * @param string $companyShort
     * @return Client
     */
    public function setCompanyShort($companyShort)
    {
        $this->companyShort = $companyShort;

        return $this;
    }

    /**
     * Get companyShort
     *
     * @return string 
     */
    public function getCompanyShort()
    {
        return $this->companyShort;
    }

    /**
     * Set entered
     *
     * @param \DateTime $entered
     * @return Client
     */
    public function setEntered($entered)
    {
        $this->entered = $entered;

        return $this;
    }

    /**
     * Get entered
     *
     * @return \DateTime 
     */
    public function getEntered()
    {
        return $this->entered;
    }

    /**
     * Set lastvisit
     *
     * @param \DateTime $lastvisit
     * @return Client
     */
    public function setLastvisit($lastvisit)
    {
        $this->lastvisit = $lastvisit;

        return $this;
    }

    /**
     * Get lastvisit
     *
     * @return \DateTime 
     */
    public function getLastvisit()
    {
        return $this->lastvisit;
    }

    /**
     * Set lastLogin
     *
     * @param \DateTime $lastLogin
     * @return Client
     */
    public function setLastLogin($lastLogin)
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    /**
     * Get lastLogin
     *
     * @return \DateTime 
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Client
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set zip
     *
     * @param string $zip
     * @return Client
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string 
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set place
     *
     * @param string $place
     * @return Client
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return string 
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Client
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set idNationality
     *
     * @param string $idNationality
     * @return Client
     */
    public function setIdNationality($idNationality)
    {
        $this->idNationality = $idNationality;

        return $this;
    }

    /**
     * Get idNationality
     *
     * @return string 
     */
    public function getIdNationality()
    {
        return $this->idNationality;
    }

    /**
     * Set guestType
     *
     * @param string $guestType
     * @return Client
     */
    public function setGuestType($guestType)
    {
        $this->guestType = $guestType;

        return $this;
    }

    /**
     * Get guestType
     *
     * @return string 
     */
    public function getGuestType()
    {
        return $this->guestType;
    }

    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     * @return Client
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime 
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set tel
     *
     * @param string $tel
     * @return Client
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return string 
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * @return Client
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set mob
     *
     * @param string $mob
     * @return Client
     */
    public function setMob($mob)
    {
        $this->mob = $mob;

        return $this;
    }

    /**
     * Get mob
     *
     * @return string 
     */
    public function getMob()
    {
        return $this->mob;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Client
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set language
     *
     * @param string $language
     * @return Client
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string 
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set oSifJezika
     *
     * @param string $oSifJezika
     * @return Client
     */
    public function setOSifJezika($oSifJezika)
    {
        $this->oSifJezika = $oSifJezika;

        return $this;
    }

    /**
     * Get oSifJezika
     *
     * @return string 
     */
    public function getOSifJezika()
    {
        return $this->oSifJezika;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return Client
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set agencyCode
     *
     * @param string $agencyCode
     * @return Client
     */
    public function setAgencyCode($agencyCode)
    {
        $this->agencyCode = $agencyCode;

        return $this;
    }

    /**
     * Get agencyCode
     *
     * @return string 
     */
    public function getAgencyCode()
    {
        return $this->agencyCode;
    }

    /**
     * Set emailSenderTz
     *
     * @param string $emailSenderTz
     * @return Client
     */
    public function setEmailSenderTz($emailSenderTz)
    {
        $this->emailSenderTz = $emailSenderTz;

        return $this;
    }

    /**
     * Get emailSenderTz
     *
     * @return string 
     */
    public function getEmailSenderTz()
    {
        return $this->emailSenderTz;
    }

    /**
     * Set web
     *
     * @param string $web
     * @return Client
     */
    public function setWeb($web)
    {
        $this->web = $web;

        return $this;
    }

    /**
     * Get web
     *
     * @return string 
     */
    public function getWeb()
    {
        return $this->web;
    }

    /**
     * Set newsletter
     *
     * @param boolean $newsletter
     * @return Client
     */
    public function setNewsletter($newsletter)
    {
        $this->newsletter = $newsletter;

        return $this;
    }

    /**
     * Get newsletter
     *
     * @return boolean 
     */
    public function getNewsletter()
    {
        return $this->newsletter;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return Client
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set lastchange
     *
     * @param \DateTime $lastchange
     * @return Client
     */
    public function setLastchange($lastchange)
    {
        $this->lastchange = $lastchange;

        return $this;
    }

    /**
     * Get lastchange
     *
     * @return \DateTime 
     */
    public function getLastchange()
    {
        return $this->lastchange;
    }

    /**
     * Set lasertransfer
     *
     * @param integer $lasertransfer
     * @return Client
     */
    public function setLasertransfer($lasertransfer)
    {
        $this->lasertransfer = $lasertransfer;

        return $this;
    }

    /**
     * Get lasertransfer
     *
     * @return integer 
     */
    public function getLasertransfer()
    {
        return $this->lasertransfer;
    }

    /**
     * Set lasertransferstatus
     *
     * @param string $lasertransferstatus
     * @return Client
     */
    public function setLasertransferstatus($lasertransferstatus)
    {
        $this->lasertransferstatus = $lasertransferstatus;

        return $this;
    }

    /**
     * Get lasertransferstatus
     *
     * @return string 
     */
    public function getLasertransferstatus()
    {
        return $this->lasertransferstatus;
    }

    /**
     * Set lasertransfertime
     *
     * @param \DateTime $lasertransfertime
     * @return Client
     */
    public function setLasertransfertime($lasertransfertime)
    {
        $this->lasertransfertime = $lasertransfertime;

        return $this;
    }

    /**
     * Get lasertransfertime
     *
     * @return \DateTime 
     */
    public function getLasertransfertime()
    {
        return $this->lasertransfertime;
    }

    /**
     * Set idLaserline
     *
     * @param integer $idLaserline
     * @return Client
     */
    public function setIdLaserline($idLaserline)
    {
        $this->idLaserline = $idLaserline;

        return $this;
    }

    /**
     * Get idLaserline
     *
     * @return integer 
     */
    public function getIdLaserline()
    {
        return $this->idLaserline;
    }

    /**
     * Set privatKucniBroj
     *
     * @param string $privatKucniBroj
     * @return Client
     */
    public function setPrivatKucniBroj($privatKucniBroj)
    {
        $this->privatKucniBroj = $privatKucniBroj;

        return $this;
    }

    /**
     * Get privatKucniBroj
     *
     * @return string 
     */
    public function getPrivatKucniBroj()
    {
        return $this->privatKucniBroj;
    }

    /**
     * Set oPrivatSifDrzave
     *
     * @param string $oPrivatSifDrzave
     * @return Client
     */
    public function setOPrivatSifDrzave($oPrivatSifDrzave)
    {
        $this->oPrivatSifDrzave = $oPrivatSifDrzave;

        return $this;
    }

    /**
     * Get oPrivatSifDrzave
     *
     * @return string 
     */
    public function getOPrivatSifDrzave()
    {
        return $this->oPrivatSifDrzave;
    }

    /**
     * Set idCrmCentrala
     *
     * @param integer $idCrmCentrala
     * @return Client
     */
    public function setIdCrmCentrala($idCrmCentrala)
    {
        $this->idCrmCentrala = $idCrmCentrala;

        return $this;
    }

    /**
     * Get idCrmCentrala
     *
     * @return integer 
     */
    public function getIdCrmCentrala()
    {
        return $this->idCrmCentrala;
    }

    /**
     * Set pravaAdministriranje
     *
     * @param boolean $pravaAdministriranje
     * @return Client
     */
    public function setPravaAdministriranje($pravaAdministriranje)
    {
        $this->pravaAdministriranje = $pravaAdministriranje;

        return $this;
    }

    /**
     * Get pravaAdministriranje
     *
     * @return boolean 
     */
    public function getPravaAdministriranje()
    {
        return $this->pravaAdministriranje;
    }

    /**
     * Set pravaVidisveprojekte
     *
     * @param boolean $pravaVidisveprojekte
     * @return Client
     */
    public function setPravaVidisveprojekte($pravaVidisveprojekte)
    {
        $this->pravaVidisveprojekte = $pravaVidisveprojekte;

        return $this;
    }

    /**
     * Get pravaVidisveprojekte
     *
     * @return boolean 
     */
    public function getPravaVidisveprojekte()
    {
        return $this->pravaVidisveprojekte;
    }

    /**
     * Set pravaPrivatni
     *
     * @param boolean $pravaPrivatni
     * @return Client
     */
    public function setPravaPrivatni($pravaPrivatni)
    {
        $this->pravaPrivatni = $pravaPrivatni;

        return $this;
    }

    /**
     * Get pravaPrivatni
     *
     * @return boolean 
     */
    public function getPravaPrivatni()
    {
        return $this->pravaPrivatni;
    }

    /**
     * Set pravaBrisanjeurzapisnika
     *
     * @param boolean $pravaBrisanjeurzapisnika
     * @return Client
     */
    public function setPravaBrisanjeurzapisnika($pravaBrisanjeurzapisnika)
    {
        $this->pravaBrisanjeurzapisnika = $pravaBrisanjeurzapisnika;

        return $this;
    }

    /**
     * Get pravaBrisanjeurzapisnika
     *
     * @return boolean 
     */
    public function getPravaBrisanjeurzapisnika()
    {
        return $this->pravaBrisanjeurzapisnika;
    }

    /**
     * Set pravaSamouprojektima
     *
     * @param boolean $pravaSamouprojektima
     * @return Client
     */
    public function setPravaSamouprojektima($pravaSamouprojektima)
    {
        $this->pravaSamouprojektima = $pravaSamouprojektima;

        return $this;
    }

    /**
     * Get pravaSamouprojektima
     *
     * @return boolean 
     */
    public function getPravaSamouprojektima()
    {
        return $this->pravaSamouprojektima;
    }

    /**
     * Set deleteImportCode
     *
     * @param integer $deleteImportCode
     * @return Client
     */
    public function setDeleteImportCode($deleteImportCode)
    {
        $this->deleteImportCode = $deleteImportCode;

        return $this;
    }

    /**
     * Get deleteImportCode
     *
     * @return integer 
     */
    public function getDeleteImportCode()
    {
        return $this->deleteImportCode;
    }

    /**
     * Set calleridnum
     *
     * @param string $calleridnum
     * @return Client
     */
    public function setCalleridnum($calleridnum)
    {
        $this->calleridnum = $calleridnum;

        return $this;
    }

    /**
     * Get calleridnum
     *
     * @return string 
     */
    public function getCalleridnum()
    {
        return $this->calleridnum;
    }

    /**
     * Set sip
     *
     * @param string $sip
     * @return Client
     */
    public function setSip($sip)
    {
        $this->sip = $sip;

        return $this;
    }

    /**
     * Get sip
     *
     * @return string 
     */
    public function getSip()
    {
        return $this->sip;
    }

    /**
     * Set vrijeme
     *
     * @param \DateTime $vrijeme
     * @return Client
     */
    public function setVrijeme($vrijeme)
    {
        $this->vrijeme = $vrijeme;

        return $this;
    }

    /**
     * Get vrijeme
     *
     * @return \DateTime 
     */
    public function getVrijeme()
    {
        return $this->vrijeme;
    }

    /**
     * Set peopleIdTransfer
     *
     * @param integer $peopleIdTransfer
     * @return Client
     */
    public function setPeopleIdTransfer($peopleIdTransfer)
    {
        $this->peopleIdTransfer = $peopleIdTransfer;

        return $this;
    }

    /**
     * Get peopleIdTransfer
     *
     * @return integer 
     */
    public function getPeopleIdTransfer()
    {
        return $this->peopleIdTransfer;
    }

    /**
     * Set companyIdTransfer
     *
     * @param integer $companyIdTransfer
     * @return Client
     */
    public function setCompanyIdTransfer($companyIdTransfer)
    {
        $this->companyIdTransfer = $companyIdTransfer;

        return $this;
    }

    /**
     * Get companyIdTransfer
     *
     * @return integer 
     */
    public function getCompanyIdTransfer()
    {
        return $this->companyIdTransfer;
    }

    /**
     * Set import
     *
     * @param boolean $import
     * @return Client
     */
    public function setImport($import)
    {
        $this->import = $import;

        return $this;
    }

    /**
     * Get import
     *
     * @return boolean 
     */
    public function getImport()
    {
        return $this->import;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
